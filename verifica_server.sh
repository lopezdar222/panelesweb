#!/bin/bash

# Realiza la solicitud POST
response=$(curl --max-time 5 -s -o /dev/null -w "%{http_code}" -X POST -d "username=dario&password=hola123" localhost:3000/api/login_test)

# Obtiene la marca de tiempo
timestamp=$(date +"%Y-%m-%d %H:%M:%S")

# Verifica si la respuesta es un código de error (por ejemplo, 4xx o 5xx)
if [ $response -ne 201 ]; then
  # Ejecuta el comando pm2 restart server
  pm2 stop server
  pm2 stop server_socket_p80
  pm2 stop server_socket_p81
  pm2 stop server_socket_p82
  pm2 stop server_socket_p83
  pm2 stop server_socket_p84
  pm2 stop server_socket_p85
  pm2 stop server_socket_p86
  pm2 stop server_socket_p87
  pm2 stop server_socket_p88
  pm2 stop server_socket_p89
  pm2 stop server_socket_p90
  pm2 stop server_batch
  
  pm2 delete server
  pm2 delete server_socket_p80
  pm2 delete server_socket_p81
  pm2 delete server_socket_p82
  pm2 delete server_socket_p83
  pm2 delete server_socket_p84
  pm2 delete server_socket_p85
  pm2 delete server_socket_p86
  pm2 delete server_socket_p87
  pm2 delete server_socket_p88
  pm2 delete server_socket_p89
  pm2 delete server_socket_p90
  pm2 delete server_batch
  
  cd /home/dario/panelesweb
  
  pm2 start ./src/server.js -i 8 --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p80.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p81.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p82.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p83.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p84.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p85.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p86.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p87.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p88.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p89.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_socket_p90.js --max-memory-restart 300M --node-args="--expose-gc"
  pm2 start ./src/server_batch.js --max-memory-restart 100M
  
  pm2 save --force
  
  pm2 startup

  error_message="La solicitud POST devolvió un código de error $response"
  
  # Imprime el mensaje con la marca de tiempo en la consola
  # echo "$timestamp, $error_message"
  
  # Guarda el mensaje con la marca de tiempo en el archivo de registro
  echo "$timestamp, $error_message" >> verifica_server_nodejs_log.txt

fi

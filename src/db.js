const { Pool } = require('pg');
const { createClient } = require('@redis/client');
//const ip = require('ip');
//url Redis Local
const urlRedisClient = 'redis://200.58.106.199:6379';
//url Redis Servidor
//const urlRedisClient = 'redis://192.168.200.76:6379';

const poolWrite_AWS = new Pool({
    //AWS Proxy:
    ///host: 'proxy-1.proxy-cjgme8oigakn.us-east-1.rds.amazonaws.com',
    //port: 5432,
    //AWS:
    //host: 'panelesweb-cluster.cluster-cjgme8oigakn.us-east-1.rds.amazonaws.com',
    //port: 5432,
    //DonWeb:
    //host: '192.168.200.182',
    //publica:
    host: '149.50.139.207',
    port: 6432,
    user: 'postgres',
    password: 'paneles0110',
    database: 'panelesweb',
    idleTimeoutMillis: 10000,  // Tiempo de inactividad en milisegundos (30 segundos)
});

const poolRead_AWS = new Pool({
    //AWS Proxy:
    //host: 'proxy-1-read-only.endpoint.proxy-cjgme8oigakn.us-east-1.rds.amazonaws.com',
    //port: 5432,
    //AWS:
    port: 5432,
    host: 'panelesweb-cluster.cluster-ro-cjgme8oigakn.us-east-1.rds.amazonaws.com',
    //DonWeb:
    //host: '192.168.200.76',
    //publica:
    //host: '66.97.47.80',
    //port: 6432,
    user: 'postgres',
    password: 'paneles0110',
    database: 'panelesweb',
    idleTimeoutMillis: 10000,  // Tiempo de inactividad en milisegundos (30 segundos)
});

const pool = new Pool({
    //publica:
    host: '149.50.139.207',
    //local:
    //host: 'localhost',
    //port: 5432,
    //HAProxy:
    port: 6432,
    user: 'postgres',
    password: 'paneles0110',
    database: 'panelesweb',
});

const poolRead = new Pool({
    //publica:
    host: '149.50.139.207',
    //local:
    //host: 'localhost',
    //port: 5432,
    //HAProxy:
    port: 5433,
    user: 'postgres',
    password: 'paneles0110',
    database: 'panelesweb',
});

const poolDirect = new Pool({
    //publica:
    host: '149.50.139.207',
    //local:
    //host: 'localhost',
    port: 5432,
    user: 'postgres',
    password: 'paneles0110',
    database: 'panelesweb',
});

// Realizar una consulta de prueba
async function pruebaConexion() {
    try {
        const client = await poolRead_AWS.connect();

        const sql = `SELECT NOW() as now;`;

        client.query(sql, (error, result) => {
            if (error) {
                console.error('Error al conectar con PostgreSQL:', error);
            } else {
                console.log('Conexión exitosa con PostgreSQL. Hora actual:', result.rows[0].now);
            }
        });

        client.release();
    } catch (error) {
        console.error('Error al conectarse a la base de datos:', error);
    }
}

async function procesoEscrituraIngreso() {
    try {
        const redisClient = createClient({
            url: urlRedisClient,
            password: 'paneles0110'
        });
        const cod_elemento = `proc_002`;
        let reply = '';
        let estado = '';
        await redisClient.connect();

        reply = await redisClient.get(cod_elemento);
        estado = JSON.parse(reply);
        estado.activos++;
        await redisClient.set(cod_elemento, JSON.stringify(estado));
        await redisClient.disconnect();

    } catch (error) {
        console.error('Error al conectarse a la base de datos:', error);
    }
}

async function procesoEscrituraEgreso() {
    try {
        const redisClient = createClient({
            url: urlRedisClient,
            password: 'paneles0110'
        });
        const cod_elemento = `proc_002`;
        let reply = '';
        let estado = '';
        await redisClient.connect();

        reply = await redisClient.get(cod_elemento);
        estado = JSON.parse(reply);
        estado.activos--;
        await redisClient.set(cod_elemento, JSON.stringify(estado));
        await redisClient.disconnect();

    } catch (error) {
        console.error('Error al conectarse a la base de datos:', error);
    }
}

async function insertLogMessage(message) {
    try {
        await procesoEscrituraIngreso();
        const client = await poolWrite_AWS.connect();

        const sql = `INSERT INTO console_logs (mensaje_log, fecha_hora) VALUES ($1, NOW());`;
        const values = [message];

        await client.query(sql, values);

        client.release();
        await procesoEscrituraEgreso();
    } 
    catch (error) {
        console.error('Error al insertar el mensaje en la base de datos:', error);
    }
}

async function handlerSQL(sql) {
try {
    await procesoEscrituraIngreso();
    const client = await poolWrite_AWS.connect();

    const result = await client.query(sql);

    client.release();
    await procesoEscrituraEgreso();

    return result;
    } catch (error) {
        console.error('Error al interactuar con base de datos:', error);
        return null;
    }
}

async function handlerSQLRead(sql) {
try {
    const client = await poolRead_AWS.connect();

    const result = await client.query(sql);

    client.release();
    
    return result;

    } catch (error) {
        console.error('Error al interactuar con base de datos:', error);
        return null;
    }
}

async function handlerSQLDirect(sql) {
try {
    await procesoEscrituraIngreso();
    const client = await poolWrite_AWS.connect();

    const result = await client.query(sql);

    client.release();
    await procesoEscrituraEgreso();
    
    return result;

    } catch (error) {
        console.error('Error al interactuar con base de datos:', error);
        return null;
    }
}

module.exports = {
    pruebaConexion,
    insertLogMessage,
    handlerSQL,
    handlerSQLRead,
    handlerSQLDirect
};
const { createClient } = require('@redis/client');


//url Redis Local
const urlRedisClient = 'redis://200.58.106.199:6379';
//url Redis Servidor
//const urlRedisClient = 'redis://192.168.200.76:6379';

const redisClient = createClient({
    url: urlRedisClient,
    password: 'paneles0110'
});


async function conectarRedis() {
    try {
        await redisClient.connect();
        console.log('Conectado a Redis');

        //const succeeded = await redisClient.flushDb();
        //console.log('Todos los elementos han sido borrados:', succeeded);

        const reply = await redisClient.ping();
        console.log('Respuesta PING:', reply); // Debería ser "PONG"

        const cod_elemento = `proc_002`;
        const nuevo = {
            id_proceso: 2,
            activos: 0,
            en_espera: 0,
            tope: 30
        };
        console.log(JSON.stringify(nuevo));

        await redisClient.set(cod_elemento, JSON.stringify(nuevo));
        console.log(`Session ${cod_elemento} initialized`);

    } catch (err) {
        console.error('Error en la conexión a Redis:', err);
    } finally {
        await redisClient.disconnect();
    }
}

async function leerRedis() {
    try {
        await redisClient.connect();
        console.log('Conectado a Redis para Lectura:');

        const cod_elemento = `proc_002`;
        const reply = await redisClient.get(cod_elemento);
        const data = JSON.parse(reply);

        console.log(data);        
    } catch (err) {
        console.error('Error en la lectura a Redis:', err);
    } finally {
        await redisClient.disconnect();
    }
}

async function blanquearCuentasClientes() {
    try {
        const patron = "proc_003_*"; // Patrón para buscar claves
        await redisClient.connect();
        const keys = await redisClient.keys(patron); // Buscar claves que coincidan
    
        console.log(keys);
        if (keys.length > 0) {
            //await redisClient.del(...keys); // Eliminar todas las claves encontradas
            console.log(`✅ Se eliminarán ${keys.length} claves que comenzaban con ${patron}`);
            for (const key of keys) {
                await redisClient.del(key); // Eliminar clave individualmente
                console.log(`✅ Clave eliminada: ${key}`);
            }
        } else {
            console.log("❌ No se encontraron claves con el prefijo 'proc_003_'");
        }
        await redisClient.disconnect();
    } catch (error) {
        console.error("Error al eliminar las claves en Redis:", error);
    }
}

async function ejecutarFunciones() {
    console.log('/****************Paso1:');
    await conectarRedis();
    console.log('/****************Paso2:');
    await leerRedis();
}

async function ejecutarFuncionesClientes() {
    await blanquearCuentasClientes();
}

//ejecutarFunciones();
ejecutarFuncionesClientes();
const path = require('path');
const db = require(__dirname + '/db');
const axios = require('axios');
const { format, subDays } = require('date-fns');

function getYesterdayDate() {
    const yesterday = subDays(new Date(), 1);
    return format(yesterday, 'yyyy-MM-dd');
}

async function rpt_Plataforma_2() {
    try {
        const fecha_proceso = getYesterdayDate();
        let query = '';
        query = `Delete from rpt_plataforma_club where id_plataforma = 2 and fecha = '${fecha_proceso}'`;
        await db.handlerSQL(query);

        const url = `https://wallet-uat.emaraplay.com/bot/report/txs`;
        // Datos que deseas enviar en la petición POST
        //robyloco	50229488
        //roby1	50228286
        //robyvarios	50228290
        //venezuelaplataa   50407999
        const masters = [50192357, 50228286, 50229488, 50228290, 50407999];
        for (let i = 0; i < masters.length; i++) {
            let postData = {
                agent:{
                    username:"admin",
                    password:"Juanambar18."
                },
                report:{
                    master : masters[i],
                    date : String(fecha_proceso)
                }
            };
            //console.log(`Procesando maestro ${masters[i]}`);
            let response = await axios.post(url, postData);
            let datos = response.data;

            // Procesar e insertar los datos en PostgreSQL
            for (const record of datos) {
                query = `SELECT Insertar_Rpt_Plataforma_Club(2,` +
                                                            `'${fecha_proceso}', ` +
                                                            `'${record.alias}', ` +
                                                            `${record.summary.total.operaciones}, ` +
                                                            `${record.summary.total.cantidad}, ` +
                                                            `${record.summary.cargas.operaciones}, ` +
                                                            `${record.summary.cargas.cantidad}, ` +
                                                            `${record.summary.descargas.operaciones}, ` +
                                                            `${record.summary.descargas.cantidad}); `;
                //console.log(query);
                await db.handlerSQL(query);
            }
        }
        query = `SELECT Insertar_Rpt_Plataforma_Club_Totales(2, '${fecha_proceso}'); `;
        //console.log(query);
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error');
    }
};

rpt_Plataforma_2();
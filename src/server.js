const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const bcrypt = require('bcrypt');
const db = require(__dirname + '/db');
const ejs = require('ejs');
const axios = require('axios');
const { Console } = require('console');
const multer  = require('multer');
const { createClient } = require('@redis/client');
const { Parser } = require('json2csv');
///////////////////////////////////////////////////////////////////////////////
//const qrcode = require('qrcode');
//const { Client, LocalAuth } = require('whatsapp-web.js');
///////////////////////////////////////////////////////////////////////////////
const app = express();
const PORT = process.env.PORT || 3000;
///////////////////////////////////////////////////////////////////////////////
db.pruebaConexion();
//dbMongo.pruebaConexion();
app.listen(PORT, () => {
    db.insertLogMessage(`Servidor en funcionamiento en el puerto ${PORT}`);
});
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());
app.use(express.json()); // Para analizar JSON
app.use(express.urlencoded({ extended: true })); // Para analizar datos de formularios URL-encoded
const ejecucion_servidor = true;
let pahtChromium = '';
if (ejecucion_servidor) {
    pahtChromium = '/usr/bin/chromium-browser';
} else {
    pahtChromium = 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe';
}
const ejecucion_servidor_numero = 1;
// Configurar EJS como motor de vistas/////////////////////////////////////////
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../public/views'));
const dias_historial = 4;
const dias_historial_operaciones = 1;
const horas_historial_operaciones = 1;
const horas_historial_notificaciones = 12;
const horas_historial_clientes = 24;
/*API MP sin funcionamiento:*/
// Configuración de sesiones
app.use(session({
    secret: 'tu-secreto-seguro',
    resave: false,
    saveUninitialized: true
}));

// Rutas
app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

app.get('/monitoreo_cuentas_cobro', async (req, res) => {
    res.render('monitoreo_cuentas_cobro', { title: 'Monitoreo Cuentas de Cobro' });
});

app.get('/agentes_ayuda', async (req, res) => {
    res.render('agentes_ayuda', { title: 'Tutorial de Agentes' });
});

app.get('/tokens_ayuda', async (req, res) => {
    res.render('tokens_ayuda', { title: 'Tutorial de Tokens de Registro' });
});

app.get('/usuarios_ayuda', async (req, res) => {
    res.render('usuarios_ayuda', { title: 'Tutorial de Usuarios' });
});

app.get('/usuarios_clientes_ayuda', async (req, res) => {
    res.render('usuarios_clientes_ayuda', { title: 'Tutorial de Clientes' });
});

app.get('/monitoreo_cuentas_cobro_ayuda', async (req, res) => {
    res.render('monitoreo_cuentas_cobro_ayuda', { title: 'Tutorial de Monitoreo Cuentas Cobro' });
});

app.get('/cuentas_cobro_ayuda', async (req, res) => {
    res.render('cuentas_cobro_ayuda', { title: 'Tutorial de Cuentas para Transferencias' });
});

app.get('/cuentas_cobro_descargas_ayuda', async (req, res) => {
    res.render('cuentas_cobro_descargas_ayuda', { title: 'Tutorial de Descargas de Cuentas' });
});

app.get('/configuracion_oficinias_ayuda', async (req, res) => {
    res.render('configuracion_oficinias_ayuda', { title: 'Tutorial de Configuracion de Oficinas' });
});

app.get('/monitoreo_landingweb_ayuda', async (req, res) => {
    res.render('monitoreo_landingweb_ayuda', { title: 'Monitoreo de Solicitudes' });
});

app.get('/monitoreo_notificaciones_ayuda', async (req, res) => {
    res.render('monitoreo_notificaciones_ayuda', { title: 'Monitoreo de Transferencias' });
});

app.get('/reportes', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('reportes', { message: 'error de sesión', title: 'Reportes'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;

        res.render('reportes', { message: 'ok', title: 'Reportes', id_rol: id_rol, id_oficina : id_oficina});
    }
    catch (error) {
        res.render('reportes', { message: 'error', title: 'Reportes'});
    }
});

app.get('/reportes_detalle', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('reportes_detalle', { message: 'error de sesión', title: 'Detalle de Cargas por Agente y Plataforma'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query1 = `select fecha,` +
                            `id_oficina,` +
                            `oficina,` +
                            `agente_usuario,` +
                            `plataforma,` +
                            `TO_CHAR(cargas, '999,999,999,999') as cargas,` +
                            `TO_CHAR(bonos, '999,999,999,999') as bonos,` +
                            `porcentaje_automaticas,` +
                            `cant_automaticas,` +
                            `TO_CHAR(cant_cargas, '999,999,999,999') as cant_cargas,` +
                            `TO_CHAR(retiros, '999,999,999,999') as retiros,` +
                            `TO_CHAR(cant_retiros, '999,999,999,999') as cant_retiros,` +
                            `TO_CHAR(pl_cargas, '999,999,999,999') as pl_cargas,` +
                            `TO_CHAR(pl_cant_cargas, '999,999,999,999') as pl_cant_cargas,` +
                            `TO_CHAR(pl_retiros, '999,999,999,999') as pl_retiros,` +
                            `TO_CHAR(pl_cant_retiros, '999,999,999,999') as pl_cant_retiros ` +
                        `from rpt_Operaciones_Plataforma `;
        //query1 = query1 + `where id_oficina = 1 `;
        if (id_rol > 1) {
            query1 = query1 + `where id_oficina = ${id_oficina} `;
        }
        query1 = query1 + `order by fecha desc, plataforma, oficina, agente_usuario;`;
        //console.log(query1);
        const result1 = await db.handlerSQLRead(query1);
        if (result1.rows.length == 0) {
            res.render('reportes_detalle', { message: 'No hay Información', title: 'Detalle de Cargas por Agente y Plataforma'});
            return;
        }
        const datos = result1.rows;

        res.render('reportes_detalle', { message: 'ok', 
                                        title: 'Detalle de Cargas por Agente y Plataforma',
                                        id_usuario : id_usuario,
                                        id_token : id_token,
                                        datos: datos, 
                                        id_rol: id_rol, 
                                        id_oficina : id_oficina});
    }
    catch (error) {
        res.render('reportes_detalle', { message: 'error', title: 'Detalle de Cargas por Agente y Plataforma'});
    }
});

app.get('/reportes_operadores', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('reportes_operadores', { message: 'error de sesión', title: 'Detalle por Operador'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let id_oficina_reporte = result.rows[0].id_oficina;
        if (id_rol == 1) {
            id_oficina_reporte = 0;
        }   
        let query1 = `select fecha,` +
                            `oficina,` +
                            `usuario,` +
                            `accion,` +
                            `estado,` +
                            `TO_CHAR(SUM(carga_importe), '999,999,999,999') as cargas,` +
                            `TO_CHAR(SUM(carga_bono), '999,999,999,999') as bonos,` +
                            `CASE WHEN SUM(es_carga) > 0 THEN ` +
                            `ROUND(SUM(carga_automatica)::numeric / SUM(es_carga)::numeric * 100, 2)  ` +
                            `ELSE 0 END AS porcentaje_automaticas,` +
                            `TO_CHAR(SUM(es_carga) - SUM(carga_automatica), '999,999,999,999') as cant_manuales,` +
                            `TO_CHAR(SUM(carga_automatica), '999,999,999,999') as cant_automaticas,` +
                            `TO_CHAR(SUM(es_carga), '999,999,999,999') as cant_cargas,` +
                            `TO_CHAR(SUM(retiro_importe), '999,999,999,999') as retiros,` +
                            `TO_CHAR(SUM(es_retiro), '999,999,999,999') as cant_retiros ` +
                        `from rpt_Operaciones_Operadores('Todas','Todos','Todas',${id_oficina_reporte}) ` +
                        `GROUP BY fecha, oficina, usuario, accion, estado ` +
                        `ORDER BY fecha DESC, oficina, usuario, accion, estado`;
        //console.log(query1);
        const result1 = await db.handlerSQLRead(query1);
        if (result1.rows.length == 0) {
            res.render('reportes_operadores', { message: 'No hay Información', title: 'Detalle por Operador'});
            return;
        }
        const datos = result1.rows;

        res.render('reportes_operadores', { message: 'ok', 
                                        title: 'Detalle por Operador',
                                        id_usuario : id_usuario,
                                        id_token : id_token,
                                        datos: datos, 
                                        id_rol: id_rol, 
                                        id_oficina : id_oficina});
    }
    catch (error) {
        res.render('reportes_operadores', { message: 'error', title: 'Detalle por Operador'});
    }
});

app.get('/reportes_recupero', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    console.log(1);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('reportes_recupero', { message: 'error de sesión', title: 'Detalle Recuperos por Agente y Plataforma'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;

        let query2 = `SELECT cr.id_estado,` +
                            `ag.id_agente,` +
                            `ag.agente_usuario,` +
                            `o.id_oficina,` +
                            `o.oficina,` +
                            `TO_CHAR(COUNT(cr.id_contactos_recupero), '999,999,999,999') as contactos,` +
                            `TO_CHAR(SUM(rt.ingresos), '999,999,999,999') as ingresos,` +
                            `TO_CHAR(SUM(rt.registros), '999,999,999,999') as registros,` +
                            `TO_CHAR(COUNT(DISTINCT op.id_operacion) + COUNT(DISTINCT oph.id_operacion), '999,999,999,999') AS cargas ` +
                        `FROM	contactos_recupero cr JOIN registro_token rt ` + 
                                `ON (rt.id_registro_token = cr.id_registro_token) `;
                                    //`AND cr.id_contactos_recupero > 5 AND cr.id_contactos_recupero < 5006) `;
        if (id_rol > 1) {
            query2 = query2 + `JOIN agente ag ON (rt.id_usuario = ag.id_agente and ag.id_oficina = ${id_oficina}) `;
        } else {
            query2 = query2 + `JOIN agente ag ON (rt.id_usuario = ag.id_agente) `;
        }
        query2 = query2 + `JOIN oficina o ON (o.id_oficina = ag.id_oficina) ` + 
                        `LEFT JOIN cliente cl ON (cl.id_registro_token = rt.id_registro_token) ` +
                        `LEFT JOIN operacion op ON (cl.id_cliente = op.id_cliente AND op.id_accion IN (1) AND op.id_estado = 2) ` +
                        `LEFT JOIN operacion_hist oph ON (cl.id_cliente = oph.id_cliente AND oph.id_accion IN (1) AND oph.id_estado = 2) ` +
                        `GROUP BY cr.id_estado, ag.id_agente, ag.agente_usuario, o.id_oficina, o.oficina ` +
                        `ORDER BY o.oficina, ag.agente_usuario, cr.id_estado`;
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        const datos_recupero = result2.rows;

        res.render('reportes_recupero', { message: 'ok', 
                                        title: 'Detalle Recuperos por Agente y Plataforma',
                                        id_usuario : id_usuario,
                                        id_token : id_token,
                                        id_rol: id_rol, 
                                        id_oficina : id_oficina, 
                                        datos_recupero : datos_recupero});
    }
    catch (error) {
        res.render('reportes_recupero', { message: 'error', title: 'Detalle Recuperos por Agente y Plataforma'});
    }
});

app.get('/reportes_cuentas', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('reportes_cuentas', { message: 'error de sesión', title: 'Detalle de Saldos de Cuentas Bancarias'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;

        let query2 = `select id_oficina,` +
                    `oficina,` +
                    `id_cuenta_bancaria,` +
                    `nombre,` +
                    `alias,` +
                    `cbu,` +
                    `billetera,` +
                    `TO_CHAR(monto_descarga, '999,999,999,999') as monto_descarga,` +
                    `TO_CHAR(monto_saldo, '999,999,999,999') as monto_saldo,` +
                    `TO_CHAR(fecha_hora_saldo, 'DD/MM/YYYY HH24:MI') as fecha_hora_saldo ` +
                `from rpt_Cuentas_Bancarias_Completo ` +
                `where fecha_hora_saldo > NOW() - INTERVAL '3 DAYS' `;
        //query1 = query1 + `where id_oficina = 1 `;
        if (id_rol > 1) {
            query2 = query2 + `and id_oficina = ${id_oficina} `;
        }
        query2 = query2 + `order by id_oficina, id_cuenta_bancaria, fecha_hora_saldo DESC;`;
        //console.log(query1);
        const result2 = await db.handlerSQLRead(query2);
        const datos_cuentas = result2.rows;

        res.render('reportes_cuentas', { message: 'ok', title: 'Detalle de Saldos de Cuentas Bancarias', id_rol: id_rol, id_oficina : id_oficina, datos_cuentas : datos_cuentas});
    }
    catch (error) {
        res.render('reportes_cuentas', { message: 'error', title: 'Detalle de Saldos de Cuentas Bancarias'});
    }
});

app.get('/recupero_whatsapp', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    // const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('recupero_whatsapp', { message: 'error de sesión', title: 'Recupero Whatsapp'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        const id_rol = result.rows[0].id_rol;
        let query2 = `select id_sesion_recupero_whatsapp,` +
                                `id_usuario,` +
                                `numero_telefono,` +
                                `id_estado,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI') as fecha_hora_creacion,` +
                                `TO_CHAR(fecha_hora_cierre, 'DD/MM/YYYY HH24:MI') as fecha_hora_cierre,` +
                                `id_oficina, ` +
                                `oficina,` +
                                `usuario ` +
                        `from Obtener_Sesion_Recupero_Whatsapp(${id_rol}, ${id_oficina});`;
        const result2 = await db.handlerSQLRead(query2);
        //console.log(query2);
        if (result2.rows.length == 0) {
            //console.log('sin datos');
            res.render('recupero_whatsapp', { message: 'vacio', title: 'Recupero Whatsapp (No hay Sesiones)', id_oficina : id_oficina, id_usuario : id_usuario});
            return;
        }
        //console.log('con datos');
        const datos = result2.rows;
        res.render('recupero_whatsapp', { message: 'ok', title: 'Recupero Whatsapp', datos: datos, id_rol: id_rol, id_oficina : id_oficina, id_usuario : id_usuario});
    }
    catch (error) {
        res.render('recupero_whatsapp', { message: 'error', title: 'Recupero Whatsapp'});
    }
});

app.get('/recupero_whatsapp_abrir', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_oficina = parseInt(req.query.id_oficina, 10);
        const id_usuario = parseInt(req.query.id_usuario, 10);
        res.render('recupero_whatsapp_abrir', { message: 'ok', title: 'Abrir Sesion Recupero', id_oficina : id_oficina, id_usuario : id_usuario });
    }
    catch (error) {
        res.render('recupero_whatsapp_abrir', { message: 'error', title: 'Abrir Sesion Recupero'});
    }
});

app.get('/recupero_whatsapp_cerrar', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_oficina = parseInt(req.query.id_oficina, 10);
        res.render('recupero_whatsapp_cerrar', { message: 'ok', title: 'Cerrar Sesion Recupero', id_oficina : id_oficina });
    }
    catch (error) {
        res.render('recupero_whatsapp_cerrar', { message: 'error', title: 'Cerrar Sesion Recupero'});
    }
});

//Rutas Whatsapp
app.post('/add-session/:id_oficina/:id_usuario', async (req, res) => {
    const { id_oficina, id_usuario } = req.params;
    try {
        let paht3001 = '';
        if (ejecucion_servidor) {
            paht3001 = `http://192.168.200.165:3001/add-session/${id_oficina}/${id_usuario}`;
        } else {
            paht3001 = `http://localhost:3001/add-session/${id_oficina}/${id_usuario}`;
        }        
        // Hacemos la solicitud al servicio en el puerto 3001
        const response = await axios.post(paht3001, req.body);

        // Devolvemos la respuesta del servicio 3001 al cliente
        res.json(response.data);
    } catch (error) {
        console.error('Error al llamar al servicio en 3001:', error);
        res.status(500).json({ error: 'Error al comunicarse con el servicio en 3001' });
    }
});

app.post('/end-session/:id_oficina', async (req, res) => {
    const { id_oficina } = req.params;
    try {
        let paht3001 = '';
        if (ejecucion_servidor) {
            paht3001 = `http://192.168.200.165:3001/end-session/${id_oficina}`;
        } else {
            paht3001 = `http://localhost:3001/end-session/${id_oficina}`;
        }        
        // Hacemos la solicitud al servicio en el puerto 3001
        const response = await axios.post(paht3001, req.body);

        // Devolvemos la respuesta del servicio 3001 al cliente
        res.json(response.data);    
    } catch (error) {
        db.insertLogMessage(`Error ending session: ${error}`);
        res.status(500).send(`Error ending session: ${error}`);
    }
});
//Rutas Whatsapp *****************************************************

app.get('/usuarios_clientes_completo_filtro', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios_clientes_completo_filtro', { message: 'error de sesión', title: 'Busqueda Clientes'});
            return;
        }
        res.render('usuarios_clientes_completo_filtro', { message: 'ok', title: 'Busqueda Clientes', id_usuario: id_usuario, id_token: id_token});
    }
    catch (error) {
        res.render('usuarios_clientes_completo_filtro', { message: 'error', title: 'Busqueda Clientes'});
    }
});

app.get('/usuarios_clientes_completo_busqueda', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const cliente_busca = req.query.cliente_busca;
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios_clientes_completo_busqueda', { message: 'error de sesión', title: 'Clientes'});
            return;
        }

        const id_oficina = result.rows[0].id_oficina;
        let id_rol = result.rows[0].id_rol;

        let query2 = `select id_cliente,` +
                                `cliente_usuario,` +
                                `bloqueado,` +
                                `agente_usuario, ` +
                                `plataforma, ` +
                                `oficina ` +
                        `from Clientes_Usuarios_Operador_Busqueda(${id_rol}, ${id_oficina}, ${id_usuario}, '${cliente_busca}');`;
        const result2 = await db.handlerSQLRead(query2);

        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_completo_busqueda', { message: 'No hay Resultados', title: 'Clientes'});
            return;
        }

        const datos = result2.rows;
        res.render('usuarios_clientes_completo_busqueda', { message: 'ok', title: 'Clientes', datos: datos, id_rol: id_rol, id_usuario: id_usuario, id_token: id_token});
    }
    catch (error) {
        res.render('usuarios_clientes_completo_busqueda', { message: 'error', title: 'Clientes'});
    }
});

app.get('/usuarios_clientes_completo', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    // const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios_clientes_completo', { message: 'error de sesión', title: 'Clientes'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let id_rol = result.rows[0].id_rol;
        if (id_rol == 3) {
            id_rol = 2;
        }
        let query2 = `select id_cliente,` +
                                `cliente_usuario,` +
                                // `cliente_password,` +
                                `bloqueado,` +
                                //`id_agente,` +
                                `agente_usuario, ` +
                                //`id_plataforma,` +
                                `plataforma, ` +
                                //`id_oficina,` +
                                `oficina,` +
                                //`visto_cliente,` +
                                `visto_operador ` + 
                        `from Clientes_Usuarios_Operador_Operacion(${id_rol}, ${id_oficina}, ${id_usuario});`;
                        /*`from v_Clientes `;
        if (result.rows[0].id_rol > 1) {
            query2 = query2 + `where id_oficina = ${id_oficina} `;
        }
        query2 = query2 + `order by visto_operador, ult_operacion desc`;*/
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_completo', { message: 'No Hay Clientes Con Actividad Reciente', title: 'Clientes', id_rol: id_rol, id_usuario: id_usuario, id_token: id_token});
            return;
        }
        const datos = result2.rows;
        res.render('usuarios_clientes_completo', { message: 'ok', title: 'Clientes', datos: datos, id_rol: id_rol, id_usuario: id_usuario, id_token: id_token});
    }
    catch (error) {
        res.render('usuarios_clientes_completo', { message: 'error', title: 'Clientes'});
    }
});

app.get('/usuarios_clientes', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    // const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios_clientes', { message: 'error de sesión', title: 'Clientes'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        const id_rol = result.rows[0].id_rol;
        const usuario_ope = result.rows[0].usuario;
        let query2 = `select    id_cliente,` +
                                `cliente_usuario,` +
                                //`cliente_password,` +
                                //`bloqueado,` +
                                //`id_agente,` +
                                `agente_usuario, ` +
                                //`id_plataforma,` +
                                `plataforma, ` +
                                //`id_oficina,` +
                                `oficina,` +
                                //`visto_cliente,` +
                                `TO_CHAR(ult_operacion, 'DD/MM/YYYY HH24:MI:SS') as ult_operacion,` +
                                `visto_operador, ` +
                                `no_leidos, ` +
                                `id_usuario, ` +
                                `usuario, ` +
                                `visita ` +
                        `from Clientes_Usuarios_Operador_Chat(${id_rol}, ${id_oficina}, ${id_usuario}) `;
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes', { message: 'No hay Clientes', title: 'Clientes'});
            return;
        }
        const datos = result2.rows;
        res.render('usuarios_clientes', { message: 'ok', title: 'Clientes', datos: datos, id_rol: id_rol, id_oficina : id_oficina, id_usuario : id_usuario, usuario_ope : usuario_ope});
    }
    catch (error) {
        res.render('usuarios_clientes', { message: 'error', title: 'Clientes'});
    }
});

app.get('/usuarios_clientes_actualiza', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    // const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from usuario where id_usuario = ${id_usuario}`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios_clientes_actualiza', { message: 'error de sesión', title: 'Clientes'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        const id_rol = result.rows[0].id_rol;
        const usuario_ope = result.rows[0].usuario;
        let query2 = `select    id_cliente,` +
                                `cliente_usuario,` +
                                `agente_usuario, ` +
                                `plataforma, ` +
                                `oficina,` +
                                `TO_CHAR(ult_operacion, 'DD/MM/YYYY HH24:MI:SS') as ult_operacion,` +
                                `visto_operador, ` +
                                `no_leidos, ` +
                                `id_usuario, ` +
                                `usuario, ` +
                                `visita ` +
                        `from Clientes_Usuarios_Operador_Chat(${id_rol}, ${id_oficina}, ${id_usuario}) `;
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_actualiza', { message: 'No hay Clientes', title: 'Clientes'});
            return;
        }
        const datos = result2.rows;
        res.render('usuarios_clientes_actualiza', { message: 'ok', title: 'Clientes', datos: datos, id_rol: id_rol, id_oficina : id_oficina, id_usuario : id_usuario, usuario_ope : usuario_ope});
    }
    catch (error) {
        res.render('usuarios_clientes_actualiza', { message: 'error', title: 'Clientes'});
    }
});

app.get('/usuarios_clientes_mensaje_masivo', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_oficina = parseInt(req.query.id_oficina, 10);
        res.render('usuarios_clientes_mensaje_masivo', { message: 'ok', title: 'Mensaje Masivo a Clientes', id_oficina : id_oficina });
    }
    catch (error) {
        res.render('usuarios_clientes_mensaje_masivo', { message: 'error', title: 'Mensaje Masivo a Clientes'});
    }
});

app.get('/usuarios_clientes_password', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const query = `select cliente_usuario ` +
                        `from cliente where id_cliente = ${id_cliente}`;        
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios_clientes_password', { message: 'Cliente No encontrado', title: 'Cambio de Contraseña'});
            return;
        }
        const cliente_usuario = result.rows[0].cliente_usuario;
        //console.log(datos);
        res.render('usuarios_clientes_password', { message: 'ok', title: 'Cambio de Contraseña', id_cliente : id_cliente, cliente_usuario : cliente_usuario });
    }
    catch (error) {
        res.render('usuarios_clientes_password', { message: 'error', title: 'Cambio de Contraseña'});
    }
});

app.get('/usuarios_clientes_info', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const query2 = `select id_cliente,` +
                                `cliente_usuario,` +
                                `cliente_password,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI') as fecha_hora_creacion,` +
                                `correo_electronico, ` +
                                `telefono, ` +
                                `id_token, ` +
                                `ingresos, ` +
                                `registros, ` +
                                `cargaron, ` +
                                `total_cargas, ` +
                                `total_importe, ` +
                                `total_bono, ` +
                                `id_registro_token, ` +
                                `de_agente, ` +
                                `cliente_referente ` +
                        `from Obtener_Cliente_Registro(${id_cliente})`;
                        //`from v_Cliente_Registro where id_cliente = ${id_cliente}`;
        
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_info', { message: 'Cliente No encontrado', title: 'Información de Registro'});
            return;
        }
        const datos = result2.rows[0];

        const query3 = `select ip,` +
                            `moneda,` +
                            `monto,` +
                            `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                            `fecha_hora_cierre ` +
                        `from Clientes_Sesiones(${id_cliente});`;

        const result3 = await db.handlerSQLRead(query3);
        const datos_sesiones = result3.rows;

        //console.log(datos);
        res.render('usuarios_clientes_info', { message: 'ok', title: 'Información de Registro', datos : datos, datos_sesiones : datos_sesiones });
    }
    catch (error) {
        res.render('usuarios_clientes_info', { message: 'error', title: 'Información de Registro'});
    }
});

app.get('/usuarios_clientes_ip', async (req, res) => {
    try 
    {
        const query = `select ip,` +
                            `id_cliente_bloqueante, ` +
                            `cliente_usuario_bloqueante, ` +
                            `agente_usuario_bloqueante, ` +
                            `oficina_bloqueante, ` +
                            `id_cliente_afectado, ` +
                            `cliente_usuario_afectado, ` +
                            `agente_usuario_afectado, ` +
                            `oficina_afectada ` +
                        `from Clientes_Bloqueantes_IP();`;
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        const datos = result.rows;
        //console.log(datos);
        res.render('usuarios_clientes_ip', { message: 'ok', title: 'IPs de Clientes Bloqueados', datos : datos });
    }
    catch (error) {
        res.render('usuarios_clientes_ip', { message: 'error', title: 'IPs de Clientes Bloqueados'});
    }
});

app.get('/usuarios_clientes_chat_largo', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const visita = parseInt(req.query.visita, 10);
        const id_rol = parseInt(req.query.id_rol, 10);
        const id_cliente = parseInt(req.query.id_cliente, 10);
        let query0 = '';
        let result0 = '';
        let chat_title = '';
        let en_sesion = '';
        let query = '';
        if (visita == 0) {
            query0 = `select cliente_usuario, en_sesion from cliente where id_cliente = ${id_cliente};`;
            //console.log(query);
            result0 = await db.handlerSQLRead(query0);
            en_sesion = result0.rows[0].en_sesion;
            chat_title = 'Chat con Cliente: ' + result0.rows[0].cliente_usuario;

            query = `select id_cliente_chat,` +
                        `id_cliente, ` +
                        `mensaje, ` +
                        `nombre_original, ` +
                        `nombre_guardado, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,` +
                        `TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, ` +
                        `enviado_cliente, ` +
                        `visto_cliente, ` +
                        `visto_operador, ` +
                        `id_usuario, ` +
                        `usuario, ` +
                        `marca_baja, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) ` +
                        `OVER (ORDER BY fecha_hora_creacion) AS misma_fecha ` +
                        `from Obtener_Cliente_Chat_Operador(${id_cliente}, ${id_rol});`;
        } else {
            query0 = `select alias_sesion from visita_sesion where id_visita_sesion = ${id_cliente};`;
            //console.log(query);
            result0 = await db.handlerSQLRead(query0);
            chat_title = 'Chat con Cliente Vsitante (Alias): ' + result0.rows[0].alias_sesion;

            query = `select id_cliente_chat,` +
                        `id_cliente, ` +
                        `mensaje, ` +
                        `nombre_original, ` +
                        `nombre_guardado, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,` +
                        `TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, ` +
                        `enviado_cliente, ` +
                        `visto_cliente, ` +
                        `visto_operador, ` +
                        `id_usuario, ` +
                        `usuario, ` +
                        `marca_baja, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) ` +
                        `OVER (ORDER BY fecha_hora_creacion) AS misma_fecha ` +
                        `from Obtener_Cliente_Visita_Chat_Operador(${id_cliente}, ${id_rol});`;
        }
        
        //console.log(query);
        const result = await db.handlerSQL(query);
        const datos = result.rows;
        //console.log(datos);
        res.render('usuarios_clientes_chat_largo', { message: 'ok', title: chat_title, id_cliente : id_cliente, datos : datos, en_sesion : en_sesion, visita : visita });
    }
    catch (error) {
        res.render('usuarios_clientes_chat_largo', { message: 'error', title: 'Chat'});
    }
});

app.get('/usuarios_clientes_chat', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const id_rol = parseInt(req.query.id_rol, 10);
        const query0 = `select cliente_usuario, en_sesion from cliente where id_cliente = ${id_cliente};`;
        //console.log(query);
        const result0 = await db.handlerSQLRead(query0);
        const en_sesion = result0.rows[0].en_sesion;
        const chat_title = 'Chat con Cliente ' + result0.rows[0].cliente_usuario;

        const query = `select id_cliente_chat,` +
                                `id_cliente, ` +
                                `mensaje, ` +
                                `nombre_original, ` +
                                `nombre_guardado, ` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,` +
                                `TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, ` +
                                `enviado_cliente, ` +
                                `visto_cliente, ` +
                                `visto_operador, ` +
                                `id_usuario, ` +
                                `usuario, ` +
                                `marca_baja, ` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) ` +
                                `OVER (ORDER BY fecha_hora_creacion) AS misma_fecha ` +
                        `from Obtener_Cliente_Chat_Operador(${id_cliente}, ${id_rol});`;
        //console.log(query);
        const result = await db.handlerSQL(query);
        const datos = result.rows;
        //console.log(datos);
        res.render('usuarios_clientes_chat', { message: 'ok', title: chat_title, id_cliente : id_cliente, datos : datos, en_sesion : en_sesion });
    }
    catch (error) {
        res.render('usuarios_clientes_chat', { message: 'error', title: 'Chat'});
    }
});

app.get('/usuarios_clientes_chat_detalle', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente_chat = parseInt(req.query.id_cliente_chat, 10);
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const id_usuario = parseInt(req.query.id_usuario, 10);
        const id_rol = parseInt(req.query.id_rol, 10);
        const mensaje_operador = req.query.mensaje.replace('<<','/');
        const visita = parseInt(req.query.visita, 10);
        let query = '';

        if (visita == 0) {
            query = `select id_cliente_chat,` +
                        `id_cliente, ` +
                        `mensaje, ` +
                        `nombre_original, ` +
                        `nombre_guardado, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,` +
                        `TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, ` +
                        `enviado_cliente, ` +
                        `visto_cliente, ` +
                        `visto_operador, ` +
                        `id_usuario, ` +
                        `usuario, ` +
                        `marca_baja, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) ` +
                        `OVER (ORDER BY fecha_hora_creacion) AS misma_fecha `;
            if (id_cliente_chat == 0) {
                if (mensaje_operador == '') {
                    query = query + `from Obtener_Cliente_Chat_Operador(${id_cliente}, ${id_rol});`; 
                } else {
                    query = query + `from Insertar_Cliente_Chat(${id_cliente}, false, '${mensaje_operador}', ${id_usuario});`;
                }
            } else {
                query = query + `from Obtener_Cliente_Chat_Eliminar(${id_cliente_chat}, ${id_cliente}, ${id_usuario});`;
            }
        } else {
            query = `select id_cliente_chat,` +
                        `id_cliente, ` +
                        `mensaje, ` +
                        `nombre_original, ` +
                        `nombre_guardado, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,` +
                        `TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, ` +
                        `enviado_cliente, ` +
                        `visto_cliente, ` +
                        `visto_operador, ` +
                        `id_usuario, ` +
                        `usuario, ` +
                        `marca_baja, ` +
                        `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) ` +
                        `OVER (ORDER BY fecha_hora_creacion) AS misma_fecha `;
            if (id_cliente_chat == 0) {
                if (mensaje_operador == '') {
                    query = query + `from Obtener_Cliente_Visita_Chat_Operador(${id_cliente}, ${id_rol});`; 
                } else {
                    query = query + `from Insertar_Cliente_Visita_Chat(${id_cliente}, false, '${mensaje_operador}', ${id_usuario});`;
                }
            } else {
                query = query + `from Obtener_Cliente_Visita_Chat_Eliminar(${id_cliente_chat}, ${id_cliente}, ${id_usuario});`;
            }
        }
        //console.log(query);
        const result = await db.handlerSQL(query);
        const datos = result.rows;
        //console.log(datos);
        res.render('usuarios_clientes_chat_detalle', { message: 'ok', datos : datos, visita : visita });
    }
    catch (error) {
        res.render('usuarios_clientes_chat_detalle', { message: 'sin mensajes' });
    }
});

app.post('/usuarios_clientes_chat_finalizar/:id_cliente/:id_usuario/:id_rol/:visita', async (req, res) => {
    try 
    {
        const { id_cliente, id_usuario, id_rol, visita } = req.params;
        let query = '';
        if (visita == 0) {
            query = `select id_cliente from Finalizar_Cliente_Chat(${id_cliente}, ${id_usuario});`;
        } else {
            query = `select id_cliente from Finalizar_Cliente_Visita_Chat(${id_cliente}, ${id_usuario});`;
        }
        //console.log(query);
        await db.handlerSQL(query);
        res.status(201).json({ message: 'Chat Finalizado' });
    }
    catch (error) {
        //console.log('baja cliente nook');
        res.status(500).json({ message: 'Error al Finalizar Chat' });
    }
});

app.get('/usuarios_clientes_bloqueo', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const query2 = `select id_cliente,` +
                                `bloqueado ` +
                        `from cliente where id_cliente = ${id_cliente}`;
        
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_bloqueo', { message: 'error', title : 'Cliente No encontrado', title: 'Bloqueo de Clientes'});
            return;
        }
        const bloqueado = result2.rows[0].bloqueado;
        //console.log(datos);
        res.render('usuarios_clientes_bloqueo', { message: 'ok', title: 'Bloqueo de Clientes', id_cliente : id_cliente, bloqueado : bloqueado });
    }
    catch (error) {
        res.render('usuarios_clientes_bloqueo', { message: 'error', title: 'Bloqueo de Clientes'});
    }
});

app.get('/usuarios_clientes_carga', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const query2 = `select id_cliente,` +
                                `cliente_usuario,` +
                                `agente_usuario, ` +
                                `plataforma, ` +
                                `id_oficina, ` +
                                `oficina ` +
                        //`from v_Clientes where id_cliente = ${id_cliente}`;
                        `from Obtener_Datos_Cliente2(${id_cliente})`;
        
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_carga', { message: 'Cliente No encontrado', title: 'Carga de Fichas'});
            return;
        }
        const datos = result2.rows[0];
        
        const query3 = `select id_cuenta_bancaria, ` +
                                `nombre || ' - ' || alias || ' - ' || cbu as cuenta_bancaria ` +
                        `from cuenta_bancaria where id_oficina = ${datos.id_oficina} and marca_eliminada = false order by nombre`;
        const result3 = await db.handlerSQLRead(query3);

        if (result3.rows.length == 0) {
            res.render('usuarios_clientes_carga', { message: 'error', title: 'No Hay Cuentas Bancarias Disponibles!'});
            return;
        }
        const datos_cuentas = result3.rows;

        //console.log(datos);
        res.render('usuarios_clientes_carga', { message: 'ok', title: 'Carga de Fichas', datos : datos, datos_cuentas : datos_cuentas });
    }
    catch (error) {
        res.render('usuarios_clientes_carga', { message: 'error', title: 'Carga de Fichas'});
    }
});

app.get('/usuarios_clientes_retiro', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cliente = parseInt(req.query.id_cliente, 10);
        const query2 = `select id_cliente,` +
                                `cliente_usuario,` +
                                `agente_usuario, ` +
                                `plataforma, ` +
                                `id_oficina, ` +
                                `oficina ` +
                        //`from v_Clientes where id_cliente = ${id_cliente}`;
                        `from Obtener_Datos_Cliente2(${id_cliente})`;
        
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios_clientes_retiro', { message: 'Cliente No encontrado', title: 'Retiro de Fichas'});
            return;
        }
        const datos = result2.rows[0];

        //console.log(datos);
        res.render('usuarios_clientes_retiro', { message: 'ok', title: 'Retiro de Fichas', datos : datos });
    }
    catch (error) {
        res.render('usuarios_clientes_retiro', { message: 'error', title: 'Retiro de Fichas'});
    }
});

app.get('/tokens', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('agentes', { message: 'error de sesión', title: 'Tokens de Registro'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query2 = `select    id_registro_token,` +
                                `id_token,` +
                                `activo,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `url_juegos,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `de_agente,` +
                                `bono_creacion,` +
                                `bono_carga_1,` +
                                `bono_carga_1_porcentaje,` +
                                `ingresos,` +
                                `registros,` +
                                `cargaron,` +
                                `total_cargas,` +
                                `total_importe,` +
                                `total_bono ` +
                        `from Tokens_Completo_Operador(${id_rol}, ${id_oficina});`;
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('tokens', { message: 'No hay Tokens de Registro', title: 'Tokens de Registro', id_rol : id_rol, id_oficina : id_oficina});
            return;
        }
        const datos = result2.rows;
        res.render('tokens', { message: 'ok', title: 'Tokens de Registro', datos: datos, id_rol : id_rol, id_oficina : id_oficina});
    }
    catch (error) {
        res.render('tokens', { message: 'error', title: 'Tokens de Registro'});
    }
});

app.get('/tokens_ver', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_agente = parseInt(req.query.id_agente, 10);
    const id_rol = parseInt(req.query.id_rol, 10);
    const id_registro_token = parseInt(req.query.id_registro_token, 10);
    //console.log(id_registro_token);
    try {
        let query2 = `select  id_registro_token,` +
                                `id_token,` +
                                `agente_usuario,` +
                                `cliente_usuario,` +
                                `activo,` +
                                `bono_carga_1,` +
                                `ingresos,` +
                                `registros,` +
                                `cargaron,` +
                                `total_cargas,` +
                                `total_importe,` +
                                `total_bono ` +
                        `from Obtener_Token_Completo_Clientes(${id_agente}, ${id_registro_token});`;
                        /*`from v_Tokens_Completo_Clientes where id_agente = ${id_agente} ` +
                        `and id_registro_token_cliente = ${id_registro_token} ` +
                        `order by cargaron desc, registros desc, ingresos desc`;*/
        /*if (id_registro_token > 0) {
            query2 = query2 + `and id_registro_token_cliente = ${id_registro_token} `;
        }
        query2 = query2 + `order by cargaron desc, registros desc, ingresos desc`;*/
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('tokens_ver', { message: 'No hay Tokens de Registro', title: 'Tokens de Registro de Clientes', id_rol : id_rol, id_oficina : id_oficina});
            return;
        }
        const cantidad = result2.rows.length;
        const datos = result2.rows;
        res.render('tokens_ver', { message: 'ok', title: 'Tokens de Registro de Clientes', datos: datos, id_rol : id_rol, id_registro_token : id_registro_token, cantidad : cantidad});
    }
    catch (error) {
        res.render('tokens_ver', { message: 'error', title: 'Tokens de Registro de Clientes'});
    }
});

app.get('/tokens_nuevo', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_oficina = req.query.id_oficina;
    const id_rol = req.query.id_rol;
    try {
        let query2 = `select    id_agente,` +
                                `Concat(oficina, ' - ', plataforma, ' - ', agente_usuario) as agente ` +
                        `from v_Agentes where marca_baja = false`;
        if (id_rol > 1) {
            query2 = query2 + ` and id_oficina = ${id_oficina}`;
        }
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('tokens_nuevo', { message: 'Token Inexistente', title: 'Creación de Token'});
            return;
        }
        const datos = result2.rows;
        res.render('tokens_nuevo', { message: 'ok', title: 'Creación de Token', datos: datos});
    }
    catch (error) {
        res.render('tokens_nuevo', { message: 'error', title: 'Creación de Token'});
    }
});


app.get('/tokens_editar', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_token = req.query.id_token;
    try {
        const query2 = `select    id_registro_token,` +
                                `id_token,` +
                                `activo,` +
                                `bono_creacion,` +
                                `bono_carga_1,` +
                                `bono_carga_1_porcentaje,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `url_juegos,` +
                                `observaciones ` +
                        //`from v_Tokens where id_registro_token = ${id_token}`;
                        `from Obtener_Token(${id_token})`;
        
        const result2 = await db.handlerSQLRead(query2);
        //console.log(query2);
        if (result2.rows.length == 0) {
            res.render('tokens_editar', { message: 'Token Inexistente', title: 'Edición de Token'});
            return;
        }
        const datos = result2.rows[0];
        res.render('tokens_editar', { message: 'ok', title: 'Edición de Token', datos: datos});
    }
    catch (error) {
        res.render('tokens_editar', { message: 'error', title: 'Edición de Token'});
    }
});

app.get('/agentes', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('agentes', { message: 'error de sesión', title: 'Agentes'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query2 = `select    id_agente,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja,` +
                                `id_oficina,` +
                                `oficina ` +
                        `from v_Agentes`;
        if (result.rows[0].id_rol > 1) {
            query2 = query2 + ` where id_oficina = ${id_oficina}`;
            if (result.rows[0].id_rol == 3) {
                query2 = query2 + ` and id_usuario = ${id_usuario}`;
            }
        }
        query2 = query2 + ` order by plataforma, oficina, marca_baja, agente_usuario`;
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('agentes', { message: 'No hay Agentes', title: 'Agentes', id_rol : id_rol, id_oficina : id_oficina});
            return;
        }
        const datos = result2.rows;
        res.render('agentes', { message: 'ok', title: 'Agentes', datos: datos, id_rol : id_rol, id_oficina : id_oficina});
    }
    catch (error) {
        res.render('agentes', { message: 'error', title: 'Agentes', id_rol : id_rol});
    }
});

app.get('/agentes_editar', async (req, res) => {
    try
    {
        // Obtener los parámetros de la URL
        const id_agente = parseInt(req.query.id_agente, 10);
        const id_rol = parseInt(req.query.id_rol, 10);
        let query2 = `select    id_agente,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja,` +
                                `tokens_bono_carga_1,` +
                                `tokens_bono_carga_1_porcentaje,` +
                                `tokens_bono_creacion,` +
                                `id_oficina,` +
                                `oficina ` +
                        `from v_Agentes where id_agente = ${id_agente}`;
        
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('agentes_editar', { message: 'Agente No encontrado', title: 'Edición de Agente', id_rol : id_rol});
            return;
        }

        //const datos = result2.rows;
        const datos = [
            {   id_agente: result2.rows[0].id_agente,
                agente_usuario: result2.rows[0].agente_usuario, 
                agente_password: result2.rows[0].agente_password,
                fecha_hora_creacion : result2.rows[0].fecha_hora_creacion,
                marca_baja : result2.rows[0].marca_baja, 
                id_plataforma : result2.rows[0].id_plataforma,
                plataforma : result2.rows[0].plataforma,
                id_oficina : result2.rows[0].id_oficina,
                tokens_bono_carga_1 : result2.rows[0].tokens_bono_carga_1,
                tokens_bono_carga_1_porcentaje : result2.rows[0].tokens_bono_carga_1_porcentaje,
                tokens_bono_creacion : result2.rows[0].tokens_bono_creacion,
                oficina : result2.rows[0].oficina}
        ];
        
        let query3 = '';
        let result3 = '';
        let datos_oficina = '';

        if (id_rol == 1)
        {
            query3 = `select id_oficina, oficina from v_Oficinas where marca_baja = false`;
            result3 = await db.handlerSQLRead(query3);
            datos_oficina = result3.rows;
        } else {
            datos_oficina = [
                {   id_oficina : result2.rows[0].id_oficina,
                    oficina : result2.rows[0].oficina}
            ];
        }

        const query4 = `select id_plataforma, plataforma from v_Plataformas where marca_baja = false`;
        const result4 = await db.handlerSQLRead(query4);
        const datos_plataforma = result4.rows;

        //console.log(datos);
        res.render('agentes_editar', { message: 'ok', title: 'Edición de Agente', datos: datos, datos_oficina : datos_oficina, id_rol : id_rol, datos_plataforma : datos_plataforma });
    }
    catch (error) {
        res.render('agentes_editar', { message: 'error', title: 'Edición de Agente'});
    }
});

app.get('/agentes_nuevo', async (req, res) => {
    const id_rol = parseInt(req.query.id_rol, 10);
    const id_oficina = parseInt(req.query.id_oficina, 10);
    try {
        let query3 = '';
        let result3 = '';
        let datos_oficina = '';
    
        if (id_rol == 1)
        {
            query3 = `select id_oficina, oficina from v_Oficinas where marca_baja = false`;
            result3 = await db.handlerSQLRead(query3);
            datos_oficina = result3.rows;
        } else {
            query3 = `select id_oficina, oficina from v_Oficinas where marca_baja = false and id_oficina = ${id_oficina}`;
            result3 = await db.handlerSQLRead(query3);
            datos_oficina = result3.rows;
        }

        const query4 = `select id_plataforma, plataforma from v_Plataformas where marca_baja = false`;
        const result4 = await db.handlerSQLRead(query4);
        const datos_plataforma = result4.rows;


        res.render('agentes_nuevo', { message: 'ok', title: 'Creación de Agente', datos_oficina : datos_oficina, datos_plataforma : datos_plataforma });
    }
    catch (error) {
        res.render('agentes_nuevo', { message: 'error', title: 'Creación de Agente'});
    }
});

app.get('/usuarios', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('usuarios', { message: 'error de sesión', title: 'Usuarios'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query2 = `select    id_usuario,` +
                                `usuario,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja,` +
                                `id_rol,` +
                                `nombre_rol,` +
                                `id_oficina,` +
                                `oficina, ` +
                                `recibe_retiro ` +
                        `from v_Usuarios`;
        if (result.rows[0].id_rol > 1) {
            query2 = query2 + ` where id_oficina = ${id_oficina} and id_rol >= ${id_rol}`;
            if (result.rows[0].id_rol == 3) {
                query2 = query2 + ` and id_usuario = ${id_usuario}`;
            }
        }
        query2 = query2 + ` order by oficina, marca_baja, nombre_rol, usuario`;
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('usuarios', { message: 'No hay Usuarios', title: 'Usuarios', id_rol : id_rol, id_usuario : id_usuario});
            return;
        }
        const datos = result2.rows;
        res.render('usuarios', { message: 'ok', title: 'Usuarios', datos: datos, id_rol : id_rol, id_usuario : id_usuario});
    }
    catch (error) {
        res.render('usuarios', { message: 'error', title: 'Usuarios'});
    }
});

app.get('/usuarios_editar', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_rol = parseInt(req.query.id_rol, 10);
    //console.log(`select * from v_Sesion_Bot where orden = 1 and id_operador = ${id_operador};`);
    let query2 = `select    id_usuario,` +
                            `usuario,` +
                            `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                            `marca_baja,` +
                            `id_rol,` +
                            `nombre_rol,` +
                            `id_oficina,` +
                            `oficina, ` +
                            `recibe_retiro ` +
                    `from v_Usuarios where id_usuario = ${id_usuario}`;
    //const query = `select * from Obtener_Usuario('${username}')`;
    //console.log(query2);
    const result2 = await db.handlerSQLRead(query2);
    if (result2.rows.length == 0) {
        res.render('usuarios_editar', { message: 'Usuario No encontrado', title: 'Edición de Usuario'});
        return;
    }

    //const datos = result2.rows;
    const datos = [
        {   id_usuario: result2.rows[0].id_usuario,
            usuario: result2.rows[0].usuario, 
            fecha_hora_creacion : result2.rows[0].fecha_hora_creacion,
            marca_baja : result2.rows[0].marca_baja, 
            id_rol : result2.rows[0].id_rol,
            nombre_rol : result2.rows[0].nombre_rol,
            id_oficina : result2.rows[0].id_oficina,
            oficina : result2.rows[0].oficina,
            recibe_retiro : result2.rows[0].recibe_retiro}
    ];

    let query3 = '';
    let result3 = '';
    let datos_oficina = '';

    if (id_rol == 1)
    {
        query3 = `select id_oficina, oficina from v_Oficinas where marca_baja = false`;
        result3 = await db.handlerSQLRead(query3);
        datos_oficina = result3.rows;
    } else {
        datos_oficina = [
            {   id_oficina : result2.rows[0].id_oficina,
                oficina : result2.rows[0].oficina}
        ];
    }

    //console.log(datos);
    res.render('usuarios_editar', { message: 'ok', title: 'Edición de Usuario', datos: datos, id_rol : id_rol, datos_oficina : datos_oficina });
});

app.get('/usuarios_nuevo', async (req, res) => {
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query3 = `select id_oficina, oficina from v_Oficinas where marca_baja = false`;
        const result3 = await db.handlerSQLRead(query3);
        const datos_oficina = result3.rows;

        res.render('usuarios_nuevo', { message: 'ok', title: 'Creación de Usuario', datos_oficina : datos_oficina, id_rol : id_rol });
    }
    catch (error) {
        res.render('usuarios_nuevo', { message: 'error', title: 'Creación de Usuario'});
    }
});

app.get('/usuarios_historial', async (req, res) => {
    const id_usuario = parseInt(req.query.id_usuario, 10);
    try {
        let query3 = `select    usuario,` +
                                `nombre_rol,` +
                                `oficina,` +
                                `ip,` +
                                `TO_CHAR(fecha_hora_creacion, 'YYYY/MM/DD HH24:MI:SS') as fecha_hora_creacion,` +
                                `fecha_hora_cierre ` +
                        `from v_Usuarios_Sesiones Where id_usuario = ${id_usuario} ` +
                        `and (fecha_hora_creacion > CURRENT_DATE - INTERVAL '${dias_historial} days') ` +
                        `order by fecha_hora_creacion desc`;
        let registros = 0;
        const result3 = await db.handlerSQLRead(query3);
        if (result3.rows.length == 0) {
            res.render('usuarios_historial', { message: 'No hay Historial', title: 'Historial de Sesiones de Usuario', registros : registros});
            return;
        }
        registros = result3.rows.length;
        datos = result3.rows;
        res.render('usuarios_historial', { message: 'ok', title: 'Historial de Sesiones de Usuario', datos : datos, registros : registros });
    }
    catch (error) {
        res.render('usuarios_historial', { message: 'No hay Historial', title: 'Historial de Sesiones de Usuario'});
    }
});

app.get('/configuracion_oficinas', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.render('configuracion_oficinas', { message: 'error de sesión', title: 'Configuracion de Oficinas'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        //console.log(`select * from v_Sesion_Bot where orden = 1 and id_operador = ${id_operador};`);
        let query2 = `select    id_oficina,` +
                                `oficina,` +
                                `contacto_whatsapp,` +
                                `contacto_telegram,` +
                                `bono_carga_1,` +
                                `bono_carga_perpetua,` +
                                `minimo_carga,` +
                                `minimo_retiro,` +
                                `minimo_espera_retiro,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja ` +
                        `from v_Oficinas`;
        if (result.rows[0].id_rol > 1) {
            query2 = query2 + ` where id_oficina = ${id_oficina}`;
        }
        query2 = query2 + ` order by marca_baja, oficina`;
        //const query = `select * from Obtener_Usuario('${username}')`;
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('configuracion_oficinas', { message: 'No hay Oficinas', title: 'Configuracion de Oficinas'});
            return;
        }
        const datos = result2.rows;
        res.render('configuracion_oficinas', { message: 'ok', title: 'Configuracion de Oficinas', datos: datos, id_rol : id_rol });
    }
    catch (error) {
        res.render('configuracion_oficinas', { message: 'error', title: 'Configuracion de Oficinas'});
    }
});

app.get('/configuracion_oficinas_editar', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_oficina = parseInt(req.query.id_oficina, 10);
    //console.log(`select * from v_Sesion_Bot where orden = 1 and id_operador = ${id_operador};`);
    let query2 = `select    id_oficina,` +
                            `oficina,` +
                            `contacto_whatsapp,` +
                            `contacto_telegram,` +
                            `bono_carga_1,` +
                            `bono_carga_perpetua,` +
                            `minimo_carga,` +
                            `minimo_retiro,` +
                            `minimo_espera_retiro,` +
                            `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                            `marca_baja ` +
                    `from v_Oficinas where id_oficina = ${id_oficina}`;
    //const query = `select * from Obtener_Usuario('${username}')`;
    //console.log(query2);
    const result2 = await db.handlerSQLRead(query2);
    if (result2.rows.length == 0) {
        res.render('configuracion_oficinas_editar', { message: 'Oficina No encontrada', title: 'Edición de Oficina'});
        return;
    }
    const datos = result2.rows;
    res.render('configuracion_oficinas_editar', { message: 'ok', title: 'Edición de Oficina', datos: datos });
});

app.get('/configuracion_oficinas_nueva', async (req, res) => {
    res.render('configuracion_oficinas_nueva', { title: 'Creación de Oficina' });
});

app.get('/cuentas_cobro', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    //console.log('Cuentas Cobro');
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        //console.log('Paso 1');
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('cuentas_cobro', { message: 'error de sesión', title: 'Configuracion de Cuentas de Cobro'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query2 = `select    id_cuenta_bancaria,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_billetera,` +
                                `billetera,` +
                                `usuario_noti,` +
                                `id_agente,` +
                                `agente,` +
                                `nombre,` +
                                `alias,` +
                                `cbu,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja ` +
                        `from Cuenta_Bancaria_Operador(${id_rol}, ${id_oficina});`;
        /*if (id_rol > 1) {
            query2 = query2 + ` where id_oficina = ${id_oficina}`;
        }
        query2 = query2 + ` order by marca_baja, nombre, alias, cbu`;*/
        //console.log('Paso 2');
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('cuentas_cobro', { message: 'No hay Cuentas de Cobro', title: 'Configuracion de Cuentas de Cobro', id_oficina : id_oficina, id_rol : id_rol});
            return;
        }
        const datos = result2.rows;

        res.render('cuentas_cobro', { message: 'ok', title: 'Configuracion de Cuentas de Cobro', id_oficina : id_oficina, datos : datos, id_rol : id_rol });
    }
    catch (error) {
        res.render('cuentas_cobro', { message: 'error', title: 'Configuracion de Cuentas de Cobro'});
    }
});

app.get('/cuentas_cobro_descargas', async (req, res) => {
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('cuentas_cobro_descargas', { message: 'error de sesión', title: 'Descargas de Cuentas de Cobro'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query3 = `select    id_cuenta_bancaria,` +
                                `id_oficina,` +
                                `oficina,` +
                                `nombre,` +
                                `alias,` +
                                `cbu,` +
                                `monto_cargas,` +
                                `cantidad_cargas,` +
                                `marca_baja ` +
                        `from Cuenta_Bancaria_Activa_Operador(${id_rol}, ${id_oficina})`;
        /*if (id_rol > 1) {
            query3 = query3 + ` where id_oficina = ${id_oficina}`;
        }
        query3 = query3 + ` order by orden`;*/

        const result3 = await db.handlerSQLRead(query3);
        if (result3.rows.length == 0) {
            res.render('cuentas_cobro_descargas', { message: 'No hay Cuentas de Cobro', title: 'Descargas de Cuentas de Cobro'});
            return;
        }
        datos_cobros = result3.rows;
        
        res.render('cuentas_cobro_descargas', { message: 'ok', title: 'Descargas de Cuentas de Cobro', datos_cobros : datos_cobros });
    }
    catch (error) {
        res.render('cuentas_cobro_descargas', { message: 'error', title: 'Descargas de Cuentas de Cobro'});
    }
});

app.get('/cuentas_cobro_eliminar', async (req, res) => {
    try 
    {
        // Obtener los parámetros de la URL
        const id_cuenta = parseInt(req.query.id_cuenta, 10);
        //console.log(datos);
        res.render('cuentas_cobro_eliminar', { message: 'ok', title: 'Eliminación de Cuentas', id_cuenta : id_cuenta });
    }
    catch (error) {
        res.render('cuentas_cobro_eliminar', { message: 'error', title: 'Eliminación de Cuentas'});
    }
});

app.get('/cuentas_cobro_descargas_confirmar', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_cuenta_bancaria = parseInt(req.query.id_cuenta_bancaria, 10);
    try {
        const query = `select    id_cuenta_bancaria,` +
                                // `id_oficina,` +
                                // `oficina,` +
                                // `nombre,` +
                                // `alias,` +
                                // `cbu,` +
                                `monto_cargas ` +
                                // `cantidad_cargas,` +
                                // `marca_baja ` +
                            `from Cuenta_Bancaria_Subtotales(${id_cuenta_bancaria});`;
                            /*`from v_Cuenta_Bancaria_Activa `+
                            `where id_cuenta_bancaria = ${id_cuenta_bancaria}`;*/

        const result = await db.handlerSQLRead(query);
        const monto_cargas = result.rows[0].monto_cargas;
        res.render('cuentas_cobro_descargas_confirmar', { message: 'ok', title: 'Descarga de Cuenta Bancaria', id_cuenta_bancaria : id_cuenta_bancaria, monto_cargas : monto_cargas});
    }
    catch (error) {
        res.render('cuentas_cobro_descargas_confirmar', { message: 'Error en la Cuenta', title: 'Descarga de Cuenta Bancaria'});
    }
});

app.get('/cuentas_cobro_descargas_historial', async (req, res) => {
    const id_cuenta_bancaria = parseInt(req.query.id_cuenta_bancaria, 10);
    try {
        let query3 = `select    nombre,` +
                                `alias,` +
                                `cbu,` +
                                `id_usuario,` +
                                `usuario,` +
                                `TO_CHAR(fecha_hora_descarga, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_descarga,` +
                                `cargas_monto,` +
                                `cargas_cantidad ` +
                        `from v_Cuentas_Bancarias_Descargas Where id_cuenta_bancaria = ${id_cuenta_bancaria} ` +
                        `and (fecha_hora_descarga > CURRENT_DATE - INTERVAL '${dias_historial} days') ` +
                        `order by id_cuenta_bancaria_descarga desc;`;

        const result3 = await db.handlerSQLRead(query3);
        if (result3.rows.length === 0) {
            res.render('cuentas_cobro_descargas_historial', { message: 'No hay Historial', title: 'Historial de Cuenta Bancaria'});
            return;
        }
        datos = result3.rows;
        const nombre = result3.rows[0].nombre;
        const alias = result3.rows[0].alias;
        const cbu = result3.rows[0].cbu;

        res.render('cuentas_cobro_descargas_historial', { message: 'ok', title: 'Historial de Cuenta Bancaria', nombre : nombre, alias : alias, cbu : cbu, datos : datos });
    }
    catch (error) {
        res.render('cuentas_cobro_descargas_historial', { message: 'No hay Historial', title: 'Historial de Cuenta Bancaria'});
    }
});

app.get('/cuentas_cobro_nueva', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_oficina = parseInt(req.query.id_oficina, 10);
    try {
        const query = `select oficina from oficina where id_oficina = ${id_oficina} `;
        const result = await db.handlerSQLRead(query);
        const oficina = result.rows[0].oficina;
        
        const query2 = `select id_billetera, billetera from billetera where marca_baja = false`; // and id_billetera = 1`;
        const result2 = await db.handlerSQLRead(query2);
        const datos_billeteras = result2.rows;
        
        const query3 = `select * from (select 0 as id_usuario, 'Sin Asingar' as usuario ` +
                        `Union Select  id_usuario, ` +
                                    `concat(usuario, ' (', cantidad_cuentas::varchar(10) , ' Cuentas Asingadas)') as usuario ` +
                        `from v_Usuarios_Cuentas_Bancarias ` +
                        `where id_oficina = ${id_oficina}) u order by 1`;
        //console.log(query2);
        const result3 = await db.handlerSQLRead(query3);
        const datos_usuarios = result3.rows;

        const query4 = `select id_agente, agente_usuario from (select 0 as id_agente, 'Sin Asingar' as agente_usuario ` +
                        `Union Select id_agente, ` +
                            `concat(agente_usuario, ' (', cantidad_cuentas::varchar(10) , ' Cuentas Asingadas)') as agente_usuario ` +
                        `from v_Agentes_Cuentas_Bancarias ` +
                        `where id_oficina = ${id_oficina}) u order by 1 desc`;
        //console.log(query4);
        const result4 = await db.handlerSQLRead(query4);
        const datos_agentes = result4.rows;

        res.render('cuentas_cobro_nueva', { message: 'ok', title: 'Creación de Cuenta de Cobro', id_oficina : id_oficina, oficina : oficina, datos_billeteras : datos_billeteras, datos_usuarios : datos_usuarios, datos_agentes : datos_agentes });
    }
    catch (error) {
        res.render('cuentas_cobro_nueva', { message: 'error', title: 'Creación de Cuenta de Cobro'});
    }
});

app.get('/cuentas_cobro_editar', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_cuenta = parseInt(req.query.id_cuenta, 10);
    try {
        //console.log(`select * from v_Sesion_Bot where orden = 1 and id_operador = ${id_operador};`);

        const query = `select    id_cuenta_bancaria,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_billetera,` +
                                `billetera,` +
                                `id_agente,` +
                                `agente,` +
                                `id_usuario_noti,` +
                                `nombre,` +
                                `alias,` +
                                `cbu,` +
                                `access_token,` +
                                `public_key,` +
                                `client_id,` +
                                `client_secret,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja ` +
                        `from Cuenta_Bancaria_Operador_2(${id_cuenta},0) ORDER BY marca_baja, nombre, alias, cbu`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.render('cuentas_cobro_editar', { message: 'Cuentas de Cobro no encontrada', title: 'Edición de Cuenta de Cobro'});
            return;
        }
        const datos = result.rows;

        const query2 = `select id_billetera, ` +
                            `billetera ` +
                    `from billetera where marca_baja = false`; // and id_billetera = 1`;
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        const datos_billeteras = result2.rows;

        const id_oficina = result.rows[0].id_oficina;
        const query3 = `select id_usuario, usuario from (select 0 as id_usuario, 'Sin Asingar' as usuario ` +
                        `Union Select  id_usuario, ` +
                                    `concat(usuario, ' (', cantidad_cuentas::varchar(10) , ' Cuentas Asingadas)') as usuario ` +
                        `from v_Usuarios_Cuentas_Bancarias ` +
                        `where id_oficina = ${id_oficina}) u order by 1 desc`;
        //console.log(query3);
        const result3 = await db.handlerSQLRead(query3);
        const datos_usuarios = result3.rows;

        const query4 = `select id_agente, agente_usuario from (select 0 as id_agente, 'Sin Asingar' as agente_usuario ` +
                        `Union Select id_agente, ` +
                            `concat(agente_usuario, ' (', cantidad_cuentas::varchar(10) , ' Cuentas Asingadas)') as agente_usuario ` +
                        `from v_Agentes_Cuentas_Bancarias ` +
                        `where id_oficina = ${id_oficina}) u order by 1 desc`;
        //console.log(query4);
        const result4 = await db.handlerSQLRead(query4);
        const datos_agentes = result4.rows;

        res.render('cuentas_cobro_editar', { message: 'ok', title: 'Edición de Cuenta de Cobro', datos: datos, datos_billeteras : datos_billeteras, datos_usuarios : datos_usuarios, datos_agentes : datos_agentes });
    }
    catch (error) {
        res.render('cuentas_cobro_editar', { message: 'error', title: 'Edición de Cuenta de Cobro'});
    }
});

app.get('/cuentas_cobro_editar_operador', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_cuenta = parseInt(req.query.id_cuenta, 10);
    try {
        //console.log(`Cuenta = ${id_cuenta}`);
        const query = `select    id_cuenta_bancaria,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_billetera,` +
                                `billetera,` +
                                `id_agente,` +
                                `agente,` +
                                `id_usuario_noti,` +
                                `nombre,` +
                                `alias,` +
                                `cbu,` +
                                `access_token,` +
                                `public_key,` +
                                `client_id,` +
                                `client_secret,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_creacion,` +
                                `marca_baja ` +
                        `from Cuenta_Bancaria_Operador_2(${id_cuenta},0) ORDER BY marca_baja, nombre, alias, cbu`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.render('cuentas_cobro_editar_operador', { message: 'Cuentas de Cobro no encontrada', title: 'Edición de Cuenta de Cobro'});
            return;
        }
        const datos = result.rows;

        //const query2 = `select id_billetera, ` +
        //                    `billetera ` +
        //            `from billetera where marca_baja = false`; // and id_billetera = 1`;
        //console.log(query2);
        //const result2 = await db.handlerSQLRead(query2);
        //const datos_billeteras = result2.rows;

        const id_oficina = result.rows[0].id_oficina;
        const query3 = `select id_usuario, usuario from (select 0 as id_usuario, 'Sin Asingar' as usuario ` +
                        `Union Select  id_usuario, ` +
                                    `concat(usuario, ' (', cantidad_cuentas::varchar(10) , ' Cuentas Asingadas)') as usuario ` +
                        `from v_Usuarios_Cuentas_Bancarias ` +
                        `where id_oficina = ${id_oficina}) u order by 1 desc`;
        //console.log(query3);
        const result3 = await db.handlerSQLRead(query3);
        const datos_usuarios = result3.rows;

        const query4 = `select id_agente, agente_usuario from (select 0 as id_agente, 'Sin Asingar' as agente_usuario ` +
                        `Union Select id_agente, ` +
                            `concat(agente_usuario, ' (', cantidad_cuentas::varchar(10) , ' Cuentas Asingadas)') as agente_usuario ` +
                        `from v_Agentes_Cuentas_Bancarias ` +
                        `where id_oficina = ${id_oficina}) u order by 1 desc`;
        //console.log(query4);
        const result4 = await db.handlerSQLRead(query4);
        const datos_agentes = result4.rows;
        
        res.render('cuentas_cobro_editar_operador', { message: 'ok', title: 'Edición de Cuenta de Cobro', datos: datos, datos_usuarios : datos_usuarios, datos_agentes : datos_agentes });
    }
    catch (error) {
        res.render('cuentas_cobro_editar_operador', { message: 'error', title: 'Edición de Cuenta de Cobro'});
    }
});

app.get('/monitoreo_notificaciones_anular', async (req, res) => {
    const id_notificacion = parseInt(req.query.id_notificacion, 10);
    res.render('monitoreo_notificaciones_anular', { title: 'Anulación de Notificacion de Cobro', id_notificacion : id_notificacion });
});

app.get('/monitoreo_notificaciones', async (req, res) => {
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('monitoreo_notificaciones', { message: 'error de sesión', title: 'Monitoreo de Transferencias'});
            return;
        }
        //console.log(1);
        const id_oficina = result.rows[0].id_oficina;
        let query2 = `select    id_notificacion,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_cuenta_bancaria,` +
                                `billetera,` +
                                `nombre,` +
                                `TO_CHAR(fecha_hora, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora,` +
                                `TO_CHAR(carga_monto, '999,999,999,999') as carga_monto,` +
                                `carga_usuario,` +
                                `id_origen,` +
                                `anulada,` +
                                `id_notificacion_carga,` +
                                `id_operacion_carga,` +
                                `marca_procesado,` +
                                `TO_CHAR(fecha_hora_procesado, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_procesado,` +
                                `TO_CHAR(importe, '999,999,999,999') as importe,` +
                                `TO_CHAR(bono, '999,999,999,999') as bono,` +
                                `id_cliente,` +
                                `TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_solicitud,` +
                                `cliente_usuario,` +
                                `agente_usuario,` +
                                `plataforma,` +
                                `REPLACE(REPLACE(url_admin, 'https://', ''), '.com', '') as url_admin ` +
                        `from Notificaciones_Cargas_Operador(1, ${id_rol}, ${id_oficina}, ${id_usuario});`;
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        //console.log(3);
        if (result2.rows.length == 0) {
            console.log(3.1);
            res.render('monitoreo_notificaciones', { message: 'No hay Actividad', title: 'Monitoreo de Transferencias', id_oficina : id_oficina});
            return;
        }
        const datos = result2.rows;
        //console.log(4);
        res.render('monitoreo_notificaciones', { message: 'ok', title: 'Monitoreo de Transferencias', id_oficina : id_oficina, datos : datos });
    }
    catch (error) {
        res.render('monitoreo_notificaciones', { message: 'error', title: 'Monitoreo de Transferencias'});
    }
});

app.get('/monitoreo_landingweb_retiro', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_operacion = parseInt(req.query.id_operacion, 10);
    const id_oficina = parseInt(req.query.id_oficina, 10);
    try {
        const query = `select    id_cliente,` +
                                `cliente_usuario,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_operacion,` +
                                `codigo_operacion,` +
                                `id_estado,` +
                                `estado,` +
                                `TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion,` +
                                `retiro_importe,` +
                                `TO_CHAR(retiro_importe, '999,999,999,999') as retiro_importe_formato,` +
                                `retiro_titular, ` +
                                `retiro_cbu ` +
        //`from v_Clientes_Operaciones where id_operacion = ${id_operacion}`;
        `from Clientes_Operaciones_Operador_Operacion_Retiro(${id_operacion});`;

        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.render('monitoreo_landingweb_retiro', { message: 'Operacion no encontrada', title: 'Verificación de Retiro'});
            return;
        }
        const datos = result.rows;

        let fecha_ult_carga_usuario = '';
        let monto_ult_carga_usuario = 0;
        const query3 = `select TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion, importe ` +
                        `from Clientes_Operaciones_Operador_Ultima_Carga_Usuario(${datos[0].id_cliente});`;
        //console.log(query3);
        const result3 = await db.handlerSQLRead(query3);
        if (result3.rows.length > 0) {
            fecha_ult_carga_usuario = result3.rows[0].fecha_hora_operacion;
            monto_ult_carga_usuario = result3.rows[0].importe;
        } else {
            fecha_ult_carga_usuario = '(Sin Registro)';
        }

        res.render('monitoreo_landingweb_retiro', { message: 'ok', 
                                                    title: 'Verificación de Retiro', 
                                                    datos: datos,
                                                    fecha_ult_carga_usuario : fecha_ult_carga_usuario,
                                                    monto_ult_carga_usuario : monto_ult_carga_usuario});
    }
    catch (error) {
        res.render('monitoreo_landingweb_retiro', { message: 'error', title: 'Verificación de Retiro'});
    }
});

app.get('/monitoreo_landingweb_carga', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_operacion = parseInt(req.query.id_operacion, 10);
    const id_oficina = parseInt(req.query.id_oficina, 10);
    try {
        const query = `select    id_cliente,` +
                                `cliente_usuario,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_operacion,` +
                                `codigo_operacion,` +
                                `id_estado,` +
                                `estado,` +
                                `TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion,` +
                                `carga_importe,` +
                                `carga_bono, ` +
                                `TO_CHAR(carga_importe, '999,999,999,999') as carga_importe_2,` +
                                `TO_CHAR(carga_bono, '999,999,999,999') as carga_bono_2,` +
                                `carga_titular, ` +
                                `carga_id_cuenta_bancaria ` +
        // `from v_Clientes_Operaciones where id_operacion = ${id_operacion}`;
        `from Clientes_Operaciones_Operador_Operacion_Carga(${id_operacion});`;
        
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.render('monitoreo_landingweb_carga', { message: 'Operacion no encontrada', title: 'Verificación de Carga'});
            return;
        }
        const datos = result.rows;
        const id_cliente = result.rows[0].id_cliente;
        const id_oficina_operacion = result.rows[0].id_oficina;

        const query2 = `select   id_cuenta_bancaria,` +
                                `id_cuenta_bancaria || '->' || nombre || ' - ' || ' - (Asignada a: ' || usuario_noti || ')' as cuenta_bancaria ` +
                                //`id_cuenta_bancaria || '->' || nombre || ' - ' || alias || ' - ' || cbu || ' - (Asignada a: ' || usuario_noti || ')' as cuenta_bancaria ` +
        `from Cuenta_Bancaria_Operador_2(0,${id_oficina_operacion}) order by nombre`;
        const result2 = await db.handlerSQLRead(query2);
        const datos_cuentas = result2.rows;

        let fecha_ult_carga_titular = '';
        /*const query3 = `select TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion ` +
                        `from v_Clientes_Operaciones where id_oficina = ${id_oficina_operacion} ` +
                        `and id_estado = 2 and id_accion in (1,5) ` +
                        `and carga_importe = ${datos[0].carga_importe} ` +
                        `and LOWER(carga_titular) = LOWER('${datos[0].carga_titular}') ` +
                        `order by fecha_hora_operacion desc limit 1`;*/
        const query3 = `select TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion ` +
                        `from Clientes_Operaciones_Operador_Ultima_Carga_TitularMonto(${id_oficina_operacion},` +
                        `${datos[0].carga_importe}, '${datos[0].carga_titular}');`;
        //console.log(query3);
        const result3 = await db.handlerSQLRead(query3);
        if (result3.rows.length > 0) {
            fecha_ult_carga_titular = result3.rows[0].fecha_hora_operacion;
        } else {
            fecha_ult_carga_titular = '(Sin Registro)';
        }

        let fecha_ult_carga_usuario = '';
        let monto_ult_carga_usuario = 0;
        const query5 = `select TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion, importe ` +
                        `from Clientes_Operaciones_Operador_Ultima_Carga_Usuario(${id_cliente});`;
        //console.log(query3);
        const result5 = await db.handlerSQLRead(query5);
        if (result5.rows.length > 0) {
            fecha_ult_carga_usuario = result5.rows[0].fecha_hora_operacion;
            monto_ult_carga_usuario = result5.rows[0].importe;
        } else {
            fecha_ult_carga_usuario = '(Sin Registro)';
        }

        const query4 = `select id_cliente_usuario_referente, cliente_usuario_referente, cantidad_cargas ` +
                        `from Clientes_Cargas_Operador(${id_cliente})`;
        //console.log(query4);
        const result4 = await db.handlerSQLRead(query4);
        let datos_referente = '';
        if (result4.rows.length > 0) {
            datos_referente = result4.rows[0];
        }

        res.render('monitoreo_landingweb_carga', { message: 'ok', 
                                                    title: 'Verificación de Carga', 
                                                    datos: datos, 
                                                    datos_cuentas : datos_cuentas, 
                                                    fecha_ult_carga_titular : fecha_ult_carga_titular, 
                                                    fecha_ult_carga_usuario : fecha_ult_carga_usuario,
                                                    monto_ult_carga_usuario : monto_ult_carga_usuario,
                                                    datos_referente : datos_referente });
    }
    catch (error) {
        res.render('monitoreo_landingweb_carga', { message: 'error', title: 'Verificación de Carga'});
    }
});

app.get('/monitoreo_landingweb_fijo', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    let id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        //console.log('Paso 1');
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('monitoreo_landingweb_fijo', { message: 'error de sesión', title: 'Monitoreo LandingWeb'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        if (id_rol == 3) {
            id_rol = 2;
        }
        let query2 = `select    cliente_usuario,` +
                                //`id_cliente,` +
                                //`id_agente,` +
                                `agente_usuario,` +
                                //`id_plataforma,` +
                                `plataforma,` +
                                //`id_oficina,` +
                                `oficina,` +
                                `id_operacion,` +
                                `codigo_operacion,` +
                                `id_estado,` +
                                `estado,` +
                                `usuario, ` +
                                `id_accion,` +
                                `accion,` +
                                `cliente_confianza,` +
                                `aceptadas,` +
                                `totales,` +
                                `TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion,` +
                                `TO_CHAR(fecha_hora_proceso, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_proceso,` +
                                `TO_CHAR(retiro_importe, '999,999,999,999') as retiro_importe_formato,` +
                                `retiro_importe,` +
                                `retiro_cbu,` +
                                `retiro_titular,` +
                                `TO_CHAR(carga_importe + carga_bono, '999,999,999,999') as carga_importe_total_formato,` +
                                `TO_CHAR(carga_importe, '999,999,999,999') as carga_importe_formato,` +
                                `TO_CHAR(carga_bono, '999,999,999,999') as carga_bono_formato,` +
                                `carga_importe,` +
                                `carga_bono, ` +
                                `carga_titular, ` +
                                `carga_observaciones, ` +
                                `carga_cuenta_bancaria_nombre, ` +
                                `id_notificacion, ` +
                                `id_origen_notificacion ` +
                                `from Clientes_Operaciones_Operador(2, ${id_rol}, ${id_oficina}, ${id_usuario}) `;
        //console.log('Paso 2');
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('monitoreo_landingweb_fijo', { message: 'No hay Actividad', title: 'Historial LandingWeb', id_oficina : id_oficina});
            return;
        }
        const datos = result2.rows;

        //console.log('Paso 3');
        res.render('monitoreo_landingweb_fijo', { message: 'ok', title: 'Historial LandingWeb', id_oficina : id_oficina, datos : datos });
    }
    catch (error) {
        res.render('monitoreo_landingweb_fijo', { message: 'error', title: 'Historial LandingWeb'});
    }
});

app.get('/monitoreo_landingweb', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    const id_rol = parseInt(req.query.id_rol, 10);
    try {
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        //console.log('Paso 1');
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('monitoreo_landingweb', { message: 'error de sesión', title: 'Monitoreo LandingWeb'});
            return;
        }
        const id_oficina = result.rows[0].id_oficina;
        let query2 = `select    cliente_usuario,` +
                                //`id_cliente,` +
                                //`id_agente,` +
                                `agente_usuario,` +
                                //`id_plataforma,` +
                                `plataforma,` +
                                //`id_oficina,` +
                                `oficina,` +
                                `id_operacion,` +
                                `codigo_operacion,` +
                                `id_estado,` +
                                `estado,` +
                                `usuario, ` +
                                `id_accion,` +
                                `accion,` +
                                `cliente_confianza,` +
                                `aceptadas,` +
                                `totales,` +
                                `TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion,` +
                                `TO_CHAR(fecha_hora_proceso, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_proceso,` +
                                `TO_CHAR(retiro_importe, '999,999,999,999') as retiro_importe_formato,` +
                                `retiro_importe,` +
                                `retiro_cbu,` +
                                `retiro_titular,` +
                                `TO_CHAR(carga_importe + carga_bono, '999,999,999,999') as carga_importe_total_formato,` +
                                `TO_CHAR(carga_importe, '999,999,999,999') as carga_importe_formato,` +
                                `TO_CHAR(carga_bono, '999,999,999,999') as carga_bono_formato,` +
                                `carga_importe,` +
                                `carga_bono, ` +
                                `carga_titular, ` +
                                `carga_observaciones, ` +
                                `carga_cuenta_bancaria_nombre, ` +
                                `id_notificacion, ` +
                                `id_origen_notificacion ` +
                        `from Clientes_Operaciones_Operador(1, ${id_rol}, ${id_oficina}, ${id_usuario}) `;
/*                        `from v_Clientes_Operaciones where marca_baja = false ` + 
                        `and (fecha_hora_operacion > NOW() - INTERVAL '${horas_historial_operaciones} hour' or id_estado = 1) `;
        if (id_rol > 1) {
            query2 = query2 + `and id_oficina = ${id_oficina} `;
        }
        if (id_rol == 3) {
            query2 = query2 + `and (id_usuario_noti = ${id_usuario} or id_usuario_noti = 0) `;
        }
        query2 = query2 + `order by id_operacion desc;`;*/

        //console.log('Paso 2');
        //console.log(query2);
        const result2 = await db.handlerSQLRead(query2);
        if (result2.rows.length == 0) {
            res.render('monitoreo_landingweb', { message: 'No hay Actividad', title: 'Monitoreo LandingWeb', id_oficina : id_oficina});
            return;
        }
        const datos = result2.rows;

        //console.log('Paso 3');
        res.render('monitoreo_landingweb', { message: 'ok', title: 'Monitoreo LandingWeb', id_oficina : id_oficina, datos : datos });
    }
    catch (error) {
        res.render('monitoreo_landingweb', { message: 'error', title: 'Monitoreo LandingWeb'});
    }
});

app.get('/monitoreo_landingweb_detalle', async (req, res) => {
    try {
        // Obtener los parámetros de la URL
        const id_operacion = parseInt(req.query.id_operacion, 10);
        const query = `select    id_cliente,` +
                                `cliente_usuario,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `id_plataforma,` +
                                `plataforma,` +
                                `id_oficina,` +
                                `oficina,` +
                                `id_operacion,` +
                                `codigo_operacion,` +
                                `id_estado,` +
                                `estado,` +
                                `id_accion,` +
                                `accion,` +
                                `usuario,` +
                                `TO_CHAR(fecha_hora_operacion, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_operacion,` +
                                `TO_CHAR(fecha_hora_proceso, 'DD/MM/YYYY HH24:MI:SS') as fecha_hora_proceso,` +
                                `retiro_importe,` +
                                `retiro_cbu, ` +
                                `retiro_titular, ` +
                                `retiro_observaciones, ` +
                                `carga_importe,` +
                                `carga_bono, ` +
                                `carga_titular, ` +
                                `carga_cuenta_bancaria, ` +
                                `carga_observaciones ` +
                        `from v_Clientes_Operaciones where id_operacion = ${id_operacion}`;
        //console.log('Paso 2');
        //console.log(query2);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length == 0) {
            res.render('monitoreo_landingweb_detalle', { message: 'No hay Detalle', title: 'Operacion Detalle'});
            return;
        }
        const datos = result.rows;

        //console.log('Paso 3');
        res.render('monitoreo_landingweb_detalle', { message: 'ok', title: 'Operacion Detalle', datos : datos });
    }
    catch (error) {
        res.render('monitoreo_landingweb_detalle', { message: 'error', title: 'Operacion Detalle'});
    }
});

// Páginas Generales
app.post('/anula_notificacion/:id_notificacion/:id_usuario', async (req, res) => {
    try {    
            const { id_notificacion, id_usuario } = req.params;
            const query = `select Registrar_Anulacion_Carga(${id_notificacion}, ${id_usuario})`;
            await db.handlerSQL(query);
            res.status(201).json({ message: 'Notificación Anulada' });
        }
    catch (error) {
        //console.log('baja cliente nook');
        res.status(500).json({ message: 'Error al Anular Notificacion' });
    }
});

app.post('/alerta_usuarios_clientes/:id_cliente/:id_usuario', async (req, res) => {
    try {
        let { id_cliente, id_usuario } = req.params;
        const query = `select id_cliente from Alerta_Cliente_Usuario(${id_cliente}, ${id_usuario})`;
        //console.log(query);
        const result = await db.handlerSQL(query);
        result.rows[0].id_cliente;
        if (result.rows[0].id_cliente > 0) {
            res.status(201).json({ message: `ok`});
        } else {
            res.status(201).json({ message: `nook`});
        }
    } catch (error) {
        res.status(500).json({ message: 'error' });
    }
});

app.post('/enviar_mensaje_masivo/:id_oficina/:id_usuario/:mensaje', async (req, res) => {
    try {
        let { id_oficina, id_usuario, mensaje } = req.params;
        const query = `select * from Insertar_Mensaje_Masivo(${id_oficina}, ${id_usuario}, '${mensaje.trim()}')`;
        const result = await db.handlerSQL(query);
        const cantidad_mensajes = result.rows[0].cantidad_mensajes;
        res.status(201).json({ codigo: 1 , message: `Envío de Mensaje Masvio Existoso a ${cantidad_mensajes} Usuarios!`});
    } catch (error) {
        res.status(500).json({ codigo: 2 , message: 'Error al Enviar Mensaje Masvio (Servidor)' });
    }
});

app.post('/cambiar_password/:id_cliente/:id_usuario/:password', async (req, res) => {
    try {
        let { id_cliente, id_usuario, password } = req.params;
        const query = `select    id_cliente,` +
                                `id_cliente_ext,` +
                                `id_cliente_db,` +
                                `cliente_usuario,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma ` +
                        //`from v_Clientes where id_cliente = ${id_cliente}`;
                        `from Obtener_Datos_Cliente2(${id_cliente})`;
        const result = await db.handlerSQLRead(query);

        const cliente_usuario = result.rows[0].cliente_usuario;
        const id_cliente_ext = result.rows[0].id_cliente_ext;
        const id_cliente_db = result.rows[0].id_cliente_db;
        const agente_nombre = result.rows[0].agente_usuario;
        const agente_password = result.rows[0].agente_password;
        /*console.log(`Cliente = ${cliente_usuario}`);
        console.log(`id_cliente_ext = ${id_cliente_ext}`);
        console.log(`id_cliente_db = ${id_cliente_db}`);
        console.log(`password = ${password.trim()}`);
        console.log(`agente_nombre = ${agente_nombre}`);
        console.log(`agente_password = ${agente_password}`);*/
        let resultado = '';
        if (result.rows[0].id_plataforma == 1 || result.rows[0].id_plataforma == 2) {
            const cambio_password = require('./scrap_bot3/contraseña.js');
            resultado = await cambio_password(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), password.trim(), agente_nombre, agente_password);
        }
        //console.log(`Resultado carga = ${resultado}`);
        if (resultado == 'ok')
        {
            const query2 = `select * from Modificar_Cliente_Password(${id_cliente}, ${id_usuario}, '${password.trim()}')`;
            await db.handlerSQL(query2);
            res.status(201).json({ codigo: 1 , message: `Cambio de contraseña Existoso!`});
        } else if (resultado === 'error') {
            res.status(500).json({ codigo: 2 , message: 'Error al Cambiar Contraseña (Servidor<1>)' });
        }
    } catch (error) {
        res.status(500).json({ codigo: 3 , message: 'Error al Cambiar Contraseña (Servidor<2>)' });
    }
});

app.post('/cargar_retiro_manual/:id_cliente/:id_usuario/:monto_importe/:cbu/:titular/:observacion', async (req, res) => {
    try {
        let { id_cliente, id_usuario, monto_importe, cbu, titular, observacion } = req.params;
        observacion = observacion.replace('<<','/');
        titular = titular.replace('<<','/');
        const query = `select    id_cliente,` +
                                `id_cliente_ext,` +
                                `id_cliente_db,` +
                                `cliente_usuario,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma ` +
                        //`from v_Clientes where id_cliente = ${id_cliente}`;
                        `from Obtener_Datos_Cliente2(${id_cliente})`;
        const result = await db.handlerSQLRead(query);

        const cliente_usuario = result.rows[0].cliente_usuario;
        const id_cliente_ext = result.rows[0].id_cliente_ext;
        const id_cliente_db = result.rows[0].id_cliente_db;
        const agente_nombre = result.rows[0].agente_usuario;
        const agente_password = result.rows[0].agente_password;
        
        let resultado = '';
        if (result.rows[0].id_plataforma == 1 || result.rows[0].id_plataforma == 2) {
            const retiro_manual3 = require('./scrap_bot3/retirar.js');
            resultado = await retiro_manual3(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_importe), agente_nombre, agente_password);
        }
        
        if (resultado == 'ok')
        {
            const query3 = `select * from Registrar_Retiro_Manual(${id_cliente}, ${id_usuario}, ${monto_importe}, '${cbu}', '${titular}', '${observacion}')`;
            const result3 = await db.handlerSQLDirect(query3);
            const codigo_operacion = result3.rows[0].codigo_operacion;
            if (codigo_operacion != 0) {
                res.status(201).json({ codigo: 1 , message: `Retiro Registrado con Éxito! Código de Operación: ${codigo_operacion}`});
            } else {
                res.status(201).json({ codigo: 2 , message: `Error en la Operación de Retiro!`});
            }
        } else if (resultado === 'error') {
            res.status(500).json({ codigo: 3 , message: 'Error al Retirar Fichas' });
        } else if (resultado === 'en_espera') {
            res.status(500).json({ codigo: 4 , message: 'Servidor con Demora. Por favor, volver a intentar en unos segundos' });
        } else if (resultado === 'faltante') {
            res.status(201).json({ codigo: 5 , message: 'Saldo Insuficiente!' });
        }   
    } catch (error) {
        res.status(500).json({ codigo: 6 , message: 'Error al Registrar Retiro!' });
    }
});

app.post('/cargar_cobro_manual/:id_cliente/:id_usuario/:monto_importe/:monto_bono/:titular/:id_cuenta_bancaria/:observacion', async (req, res) => {
    try {
        let { id_cliente, id_usuario, monto_importe, monto_bono, titular, id_cuenta_bancaria, observacion } = req.params;
        observacion = observacion.replace('<<','/');
        titular = titular.replace('<<','/');
        const query = `select    id_cliente,` +
                                `id_cliente_ext,` +
                                `id_cliente_db,` +
                                `cliente_usuario,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma ` +
                        //`from v_Clientes where id_cliente = ${id_cliente}`;
                        `from Obtener_Datos_Cliente2(${id_cliente})`;
        const result = await db.handlerSQLRead(query);
 
        const cliente_usuario = result.rows[0].cliente_usuario;
        const id_cliente_ext = result.rows[0].id_cliente_ext;
        const id_cliente_db = result.rows[0].id_cliente_db;
        const agente_nombre = result.rows[0].agente_usuario;
        const agente_password = result.rows[0].agente_password;
        const monto_total = Number(monto_importe) + Number(monto_bono);

        let resultado = '';
        if (result.rows[0].id_plataforma == 1 || result.rows[0].id_plataforma == 2) {
            const carga_manual3 = require('./scrap_bot3/cargar_manual.js');
            resultado = await carga_manual3(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_total), agente_nombre, agente_password);
        }
        //console.log(`Resultado carga = ${resultado}`);
        if (resultado == 'ok') 
        {
            const query3 = `select * from Registrar_Carga_Manual(${id_cliente}, ${id_usuario}, ${monto_importe}, ${monto_bono}, '${titular}', ${id_cuenta_bancaria}, '${observacion}')`;
            const result3 = await db.handlerSQL(query3);
            const codigo_operacion = result3.rows[0].codigo_operacion;
            if (codigo_operacion != 0) {
                res.status(201).json({ message: `Carga Registrada con Éxito! Código de Operación: ${codigo_operacion}`});
            } else {
                res.status(201).json({ message: `Error en la Operación de Carga!`});
            }
        } else if (resultado === 'error') {
            res.status(500).json({ message: 'Error al Cargar Fichas' });
        } else if (resultado === 'en_espera') {
            res.status(500).json({ message: 'Servidor con Demora. Por favor, volver a intentar en unos segundos' });
        }        
    } catch (error) {
        res.status(500).json({ message: 'Error al Registrar Cobro!' });
    }
});

app.post('/bloqueo_cliente/:id_usuario/:id_cliente/:bloqueo', async (req, res) => {
    const { id_usuario, id_cliente, bloqueo } = req.params;
    try {
        let query = '';
        let mensaje = '';
        if (bloqueo == 1) {
            query = `select Bloqueo_Cliente(${id_usuario}, ${id_cliente}, true)`;
            mensaje = 'Cliente Bloqueado Exitosamente!';
        } else {
            query = `select Bloqueo_Cliente(${id_usuario}, ${id_cliente}, false)`;
            mensaje = 'Cliente Desbloqueado Exitosamente!';
        }
        const result = await db.handlerSQL(query);
        res.status(201).json({ message: mensaje });
    } catch (error) {
        res.status(500).json({ message: 'Error al Modificar Cliente!' });
    }
});

app.post('/modificar_token/:id_registro_token/:id_usuario/:observaciones/:activo/:bono_carga_1/:bono_carga_1_porcentaje', async (req, res) => {
    const { id_registro_token, id_usuario, observaciones, activo, bono_carga_1, bono_carga_1_porcentaje } = req.params;
    try {
        const query = `select Modificar_Token_Agente(${id_registro_token}, ${id_usuario}, '${observaciones}', ${bono_carga_1}, ${activo}, ${bono_carga_1_porcentaje})`;
        await db.handlerSQL(query);
        res.status(201).json({ message: 'Token Modificado Exitosamente!' });
    } catch (error) {
        res.status(500).json({ message: 'Error al Modificar Token!' });
    }
});

app.post('/modificar_datos_registro/:id_cliente/:id_usuario/:telefono/:email', async (req, res) => {
    const { id_cliente, id_usuario, telefono, email } = req.params;
    try {
        const query = `select Modificar_Cliente_Registro(${id_cliente}, ${id_usuario}, '${telefono}', '${email}')`;
        const result = await db.handlerSQL(query);
        res.status(201).json({ message: 'Datos de Registro Modificados Exitosamente!' });
    } catch (error) {
        res.status(500).json({ message: 'Error al Modificar Datos de Registro!' });
    }
});

app.post('/modificar_agente/:id_agente/:id_usuario/:password/:estado/:oficina/:id_plataforma/:bono_carga_1/:bono_creacion/:bono_carga_1_porcentaje', async (req, res) => {
    const { id_agente, id_usuario, password, estado, oficina, id_plataforma, bono_carga_1, bono_creacion, bono_carga_1_porcentaje } = req.params;
    try {
        const query = `select Modificar_Agente(${id_agente}, ${id_usuario}, '${password}', ${estado}, ${oficina}, ${id_plataforma}, ${bono_carga_1}, ${bono_creacion}, ${bono_carga_1_porcentaje})`;
        const result = await db.handlerSQL(query);
        res.status(201).json({ message: 'Agente Modificado Exitosamente!' });
    } catch (error) {
        res.status(500).json({ message: 'Error al Modificar Agente!' });
    }
});

app.post('/modificar_usuario/:id_usuario_modi/:id_usuario/:password/:estado/:rol/:oficina/:retiro', async (req, res) => {
    const { id_usuario_modi, id_usuario, password, estado, rol, oficina, retiro } = req.params;
    try {
        const hashedPassword = await bcrypt.hash(password, 10);
        const query = `select Modificar_Usuario(${id_usuario_modi}, ${id_usuario}, '${hashedPassword}', ${estado}, ${rol}, ${oficina}, ${retiro})`;
        const result = await db.handlerSQL(query);
        res.status(201).json({ message: 'Usuario Modificado Exitosamente!' });
    } catch (error) {
        res.status(500).json({ message: 'Error al Modificar Usuario!' });
    }
});

app.post('/registrar_usuario/:id_usuario/:usuario/:password/:id_rol/:id_oficina', async (req, res) => {
    const { id_usuario, usuario, password, id_rol, id_oficina } = req.params;
    try {
        const queryChek = `select * from Obtener_Usuario('${usuario}', false)`;
        const result1 = await db.handlerSQL(queryChek);
        if (result1.rows.length > 0) {
            res.status(201).json({ message: 'El usuario ya existe!' });
            return;
        }

        const hashedPassword = await bcrypt.hash(password, 10);

        const query = `select Insertar_Usuario(${id_usuario}, '${usuario}', '${hashedPassword}', ${id_rol}, ${id_oficina})`;
        const result2 = await db.handlerSQL(query);

        res.status(201).json({ message: 'Usuario registrado exitosamente.' });
    } catch (error) {
        res.status(500).json({ message: 'Error al registrar usuario.' });
    }
});

app.post('/registrar_token/:id_usuario/:id_agente/:observaciones/:bono_carga_1/:bono_carga_1_porcentaje', async (req, res) => {
    const { id_usuario, id_agente, observaciones, bono_carga_1, bono_carga_1_porcentaje } = req.params;
    try {
        const query = `select * from Insertar_Token_Agente(${id_usuario}, ${id_agente}, '${observaciones}', ${bono_carga_1}, ${bono_carga_1_porcentaje})`;
        const result = await db.handlerSQL(query);
        const id_registro_token = result.rows[0].id_registro_token;
        //console.log(query);
        if (id_registro_token > 0) {
            res.status(201).json({ message: `Token Número ${id_registro_token} registrado exitosamente.` });
        } else {
            res.status(201).json({ message: 'Error al Insertar Token (Servidor)' });
        }        
    } catch (error) {
        res.status(500).json({ message: 'Error al Registrar Token (Servidor)' });
    }
});

app.post('/registrar_agente/:id_usuario/:usuario/:password/:id_oficina/:id_plataforma/:bono_carga_1/:bono_creacion/:bono_carga_1_porcentaje', async (req, res) => {
    const { id_usuario, usuario, password, id_oficina, id_plataforma, bono_carga_1, bono_creacion, bono_carga_1_porcentaje } = req.params;
    try {
        const queryChek = `select * from v_Agentes where agente_usuario = '${usuario}' and marca_baja = false;`;
        const result1 = await db.handlerSQLRead(queryChek);
        //console.log(queryChek);
        if (result1.rows.length > 0) {
            res.status(201).json({ message: 'El Agente ya existe!' });
            return;
        }
        const query = `select Insertar_Agente(${id_usuario}, '${usuario}', '${password}', ${id_oficina}, ${id_plataforma}, ${bono_carga_1}, ${bono_creacion}, ${bono_carga_1_porcentaje})`;
        //console.log(query);
        await db.handlerSQL(query);

        res.status(201).json({ message: 'Agente registrado exitosamente.' });
    } catch (error) {
        res.status(500).json({ message: 'Error al registrar Agente.' });
    }
});

app.post('/cargar_retiro/:id_operacion/:id_usuario/:monto', async (req, res) => {
    try {
        const { id_operacion, id_usuario, monto } = req.params;
        const query2 = `select * from Modificar_Cliente_Retiro(${id_operacion}, 2, ${monto}, ${id_usuario})`;
        await db.handlerSQLDirect(query2);
        res.status(201).json({ codigo : 1, message: `Retiro de Fichas Registrado Exitosamente!` });
        /*const query = `select    cliente_usuario,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma ` +
        `from v_Clientes_Operaciones where id_operacion = ${id_operacion}`;
        const result = await db.handlerSQL(query);

        const cliente_usuario = result.rows[0].cliente_usuario;
        const agente_nombre = result.rows[0].agente_usuario;
        const agente_password = result.rows[0].agente_password;

        let resultado = '';
        if (result.rows[0].id_plataforma == 1) {
            const retiro_manual3 = require('./scrap_bot3/retirar.js');
            resultado = await retiro_manual3(cliente_usuario.trim(), String(monto), agente_nombre, agente_password);
        }
        if (resultado == 'ok') 
        {
            const query2 = `select * from Modificar_Cliente_Retiro(${id_operacion}, 2, ${monto}, ${id_usuario})`;
            await db.handlerSQL(query2);
            res.status(201).json({ codigo : 1, message: `Retiro de Fichas Registrado Exitosamente!` });
        } else if (resultado == 'error') {
            res.status(500).json({ codigo : 2, message: 'Error al Retirar Fichas' });
        } else if (resultado == 'en_espera') {
            res.status(500).json({ codigo : 3, message: 'Servidor con Demora. Por favor, volver a intentar en unos segundos' });
        } else if (resultado == 'faltante') {
            res.status(201).json({ codigo : 4, message: 'Saldo Insuficiente!' });
        }*/
    } catch (error) {
        res.status(500).json({ message: 'Error en el Retiro' });
    }
});

app.post('/cargar_cobro/:id_operacion/:id_usuario/:monto/:bono/:id_cuenta_bancaria', async (req, res) => {
    try {
        const { id_operacion, id_usuario, monto, bono, id_cuenta_bancaria } = req.params;
        let query = `select    id_operacion_carga,` +
                                `procesando,` +
                                `id_cliente,` +
                                `cliente_usuario,` +
                                `id_cliente_ext,` +
                                `id_cliente_db,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma ` +
        `from Verificar_Operacion_Carga_Manual(${id_operacion});`;
        let result = await db.handlerSQLDirect(query);
        
        const id_operacion_carga = result.rows[0].id_operacion_carga;
        const procesando = result.rows[0].procesando;
        const cliente_usuario = result.rows[0].cliente_usuario.trim();
        const id_cliente_ext = result.rows[0].id_cliente_ext;
        const id_cliente_db = result.rows[0].id_cliente_db;
        const agente_nombre = result.rows[0].agente_usuario;
        const agente_password = result.rows[0].agente_password;
        const monto_carga = Number(monto.trim());
        const monto_bono = Number(bono.trim());
        const monto_total = monto_carga + monto_bono;

        /*console.log(`Operacion Carga = ${result.rows[0].id_operacion_carga}`);
        console.log(`Procesando = ${procesando}`);
        console.log(`Agente Nombre = ${agente_nombre}`);
        console.log(`Agente Password = ${agente_password}`);
        console.log(`Cliente Usuario = ${cliente_usuario}`);
        console.log(`Cliente id_cliente_ext = ${id_cliente_ext}`);
        console.log(`Cliente id_cliente_db = ${id_cliente_db}`);
        console.log(`Monto = ${String(monto_total)}`);*/

        let resultado = '';
        if (procesando) {
            res.status(500).json({ codigo : 4, message: 'La Solicitud ya está en Proceso de Aplicación' });
        } else {
            if (result.rows[0].id_plataforma == 1 || result.rows[0].id_plataforma == 2) {
                const carga_manual3 = require('./scrap_bot3/cargar_manual.js');
                resultado = await carga_manual3(id_cliente_ext, id_cliente_db, cliente_usuario, String(monto_total), agente_nombre, agente_password);
            }
            //console.log(`Resultado carga = ${resultado}`);
            if (resultado == 'ok') 
            {
                const query2 = `select * from Modificar_Cliente_Carga(${id_operacion}, 2, ${monto_carga}, ${monto_bono}, ${id_cuenta_bancaria}, ${id_usuario})`;
                await db.handlerSQLDirect(query2);
                res.status(201).json({ codigo : 1, message: `Carga de Fichas Registrada Exitosamente!` });
            } else if (resultado === 'error') {
                query = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                result = await db.handlerSQLDirect(query);
                res.status(500).json({ codigo : 2, message: 'Error al Cargar Fichas' });
            } else if (resultado === 'en_espera') {
                query = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                result = await db.handlerSQLDirect(query);
                res.status(500).json({ codigo : 3, message: 'Servidor con Demora. Por favor, volver a intentar en unos segundos' });
            }
        }
    } catch (error) {
        res.status(500).json({ message: 'Error al Cargar Cobro' });
    }
});

app.post('/cargar_bono_referido/:id_cliente/:id_usuario/:bono/:id_cuenta_bancaria/:id_operacion', async (req, res) => {
    try {
        const { id_cliente, id_usuario, bono, id_cuenta_bancaria, id_operacion } = req.params;
        const query = `select    cliente_usuario,` +
                                `id_cliente_ext,` +
                                `id_cliente_db,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `bono_creacion,` +
                                `id_plataforma ` +
        //`from v_Clientes where id_cliente = ${id_cliente}`;
        `from Obtener_Datos_Cliente2(${id_cliente})`;
        const result = await db.handlerSQLDirect(query);

        const cliente_usuario = result.rows[0].cliente_usuario;
        const id_cliente_ext = result.rows[0].id_cliente_ext;
        const id_cliente_db = result.rows[0].id_cliente_db;
        const agente_nombre = result.rows[0].agente_usuario;
        const agente_password = result.rows[0].agente_password;
        const bono_creacion = result.rows[0].bono_creacion;
        //Bono que recibió el referido en la primer carga:
        //const monto_total = Number(bono.trim());
        //Bono por creación de usuario:
        const monto_total = Number(bono_creacion);

        let resultado = '';
        if ((result.rows[0].id_plataforma == 1 || result.rows[0].id_plataforma == 2) && (monto_total > 0)) {
            const carga_manual3 = require('./scrap_bot3/cargar.js');
            resultado = await carga_manual3(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_total), agente_nombre, agente_password);
        }
        //console.log(`Resultado carga = ${resultado}`);
        if (resultado == 'ok') 
        {
            const query2 = `select * from Cargar_Bono_Referido(${id_cliente}, ${id_usuario}, ${monto_total}, ${id_cuenta_bancaria}, ${id_operacion})`;
            await db.handlerSQLDirect(query2);
            res.status(201).json({ codigo : 1, message: `Carga de Bono Referido Exitosa!` });
        } else if (resultado === 'error') {
            res.status(500).json({ codigo : 2, message: 'Error al Cargar Fichas Bono Referente' });
        } else if (resultado === 'en_espera') {
            res.status(500).json({ codigo : 3, message: 'Servidor con Demora. Por favor, volver a intentar en unos segundos' });
        }
    } catch (error) {
        res.status(500).json({ message: 'Error al Cargar Fichas Bono Referente' });
    }
});

app.post('/rechazar_cobro/:id_operacion/:id_usuario', async (req, res) => {
    try {
        const { id_operacion, id_usuario } = req.params;
        const query2 = `select * from Modificar_Cliente_Carga_Rechazo(${id_operacion}, 3, ${id_usuario})`;
        await db.handlerSQL(query2);
        res.status(201).json({ codigo : 1, message: `Acción Procesada.` });
    } catch (error) {
        res.status(500).json({ codigo : 2, message: 'Error al Rechazar Cobro.' });
    }
});

app.post('/rechazar_retiro/:id_operacion/:id_usuario/:id_cliente', async (req, res) => {
    try {
        const { id_operacion, id_usuario, id_cliente } = req.params;
        const query2 = `select * from Modificar_Cliente_Retiro(${id_operacion}, 3, 0, ${id_usuario})`;
        await db.handlerSQL(query2);

        const mensaje_rechazo = `La solicitud de retiro fué rechazada debido a que ` +
                                `se encontró un error en los datos ingresados. ` +
                                `Por favor ingresar CBU y Titular. Muchas Gracias!!`;
        const query3 = `select * from Insertar_Cliente_Chat(${id_cliente}, false, '${mensaje_rechazo}', ${id_usuario});`;
        await db.handlerSQL(query3);

        res.status(201).json({ codigo : 1, message: `Retiro Rechazado Exitosamente.` });
    } catch (error) {
        res.status(500).json({ codigo : 2, message: 'Error al Rechazar Retiro de Fichas.' });
    }
});

app.post('/login', async (req, res) => {
    try {    
        const { username, password, version, ipAddress_2 } = req.body;
        const ipAddress = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        const query = `select * from Obtener_Usuario('${username}', true)`;
        const result = await db.handlerSQL(query);
        if (result.rows.length == 0) {
            res.status(401).json({ message: 'Nombre de usuario incorrecto.' });
            return;
        }
        const user = result.rows[0];
        const isPasswordValid = await bcrypt.compare(password, user.password);
        
        if (isPasswordValid) {
            req.session.id_usuario = user.id_usuario; // Guardar el ID del usuario en la sesión
            const id_token = generarToken(30);
            const consulta2 = `select Modificar_Usuario_Token(${req.session.id_usuario}, '${id_token}', '${ipAddress}')`;
            await db.handlerSQL(consulta2);

            res.status(201).json({ message: 'ok', id_rol: user.id_rol, id_usuario: user.id_usuario, id_token : id_token, id_oficina: user.id_oficina });
        } else {
            res.status(401).json({ message: 'Contraseña incorrecta.' });
        }
    } catch (error) {
        console.error('Error al buscar usuario:', error);
        res.status(500).json({ message: 'Error al iniciar sesión.' });
    }
});

app.post('/login_token', async (req, res) => {
    try {    
            const { id_usuario, id_token } = req.body;
            const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
            const result = await db.handlerSQLRead(query);
            if (result.rows.length === 0) {
                res.status(401).json({ message: 'Token o usuario incorrecto.' });
                return;
            }
            const user = result.rows[0];
            res.status(201).json({ message: 'ok', id_rol: user.id_rol });
        }
    catch (error) {
        res.status(500).json({ message: 'token invalido' });
    }
});

app.get('/logout', async (req, res) => {
    // Obtener los parámetros de la URL
    const id_usuario = parseInt(req.query.id_usuario, 10);
    const id_token = req.query.id_token;
    //req.session.destroy();
    //console.log(`Finalización de sesión usuario ${id_usuario} (${id_rol}): '${id_token}'`);
    const cierre = `Select Cerrar_Sesion_Usuario(${id_usuario}, '${id_token}')`;
    await db.handlerSQL(cierre);
});

app.post('/crear_cuenta_bancaria/:id_oficina/:id_usuario/:nombre/:alias/:cbu/:estado/:billetera/:usuario_noti/:id_agente/:access_token/:public_key/:client_id/:client_secret', async (req, res) => {
    try {    
            const { id_oficina, id_usuario, nombre, alias, cbu, estado, billetera, usuario_noti, id_agente, access_token, public_key, client_id, client_secret } = req.params;
            let alias_aux  = '';
            if (alias !== 'XXXXX') {
                alias_aux = alias;
            }
            const query = `select * from Crear_Cuenta_Bancaria(${id_oficina}, ${id_usuario}, '${nombre}', '${alias_aux}', '${cbu}' , ${estado}, ${billetera}, ${usuario_noti}, ${id_agente}, '${access_token}', '${public_key}', '${client_id}', '${client_secret}')`;
            //console.log(query);
            const result = await db.handlerSQL(query);
            if (result.rows[0].id_cuenta_bancaria == 0) {
                res.status(401).json({ message: `Cuenta ${alias_aux} - ${cbu} ya existe` });
            } else {
                res.status(201).json({ message: `Cuenta ${alias_aux} - ${cbu} creada` });
            }
        }
    catch (error) {
        //console.log('crear cta bancaria nook');
        res.status(500).json({ message: 'Error al Crear Cuenta Bancaria' });
    }
});
       
app.post('/descargar_cuenta_bancaria/:id_usuario/:id_cuenta_bancaria/:monto_descarga', async (req, res) => {
    try {    
            const { id_usuario, id_cuenta_bancaria, monto_descarga } = req.params;
            const query = `select * from Descargar_Cuenta_Bancaria(${id_usuario}, ${id_cuenta_bancaria}, ${monto_descarga})`;
            //console.log(query);
            const result = await db.handlerSQL(query);
            const r_cbu = result.rows[0].cbu;
            const r_cantidad = result.rows[0].cargas_cantidad;
            const r_monto = result.rows[0].cargas_monto;
            if (r_cantidad == 0) {
                res.status(401).json({ message: `Cuenta CBU ${r_cbu} NO Tiene Cargas!` });
            } else {
                res.status(201).json({ message: `Cuenta CBU ${r_cbu} Descargó $ ${monto_descarga}, Restante en la cuenta de $ ${r_monto}`});
            }
        }
    catch (error) {
        //console.log('crear cta bancaria nook');
        res.status(500).json({ message: 'Error al Descargar Cuenta Bancaria' });
    }
});

app.post('/eliminar_cuenta_bancaria/:id_cuenta_bancaria/:id_usuario', async (req, res) => {
    try {    
            const { id_usuario, id_cuenta_bancaria } = req.params;
            let alertaInactivas = 0;
            //console.log(`Estado = ${estado}`);
            
            const query0 = `select count(*) as cantidad ` +
                            `from cuenta_bancaria ` +
                            `where id_oficina in  ` +
                            `(select id_oficina from  ` +
                            `cuenta_bancaria where id_cuenta_bancaria = ${id_cuenta_bancaria}) ` +
                            `and marca_baja = false and id_cuenta_bancaria != ${id_cuenta_bancaria}`;
            //console.log(query0);
            const result0 = await db.handlerSQLRead(query0);
            //console.log(result0.rows[0].cantidad);
            if (result0.rows[0].cantidad == 0) {
                alertaInactivas = 1;
            }   
            //console.log(alertaInactivas);
            if (alertaInactivas == 0) {
                const query = `select * from Eliminar_Cuenta_Bancaria(${id_usuario}, ${id_cuenta_bancaria})`;
                //console.log(query);
                const result = await db.handlerSQL(query);
                res.status(201).json({ message: `Cuenta ${id_cuenta_bancaria} eliminada` });
            } else {
                res.status(401).json({ message: `¡Siempre debe quedar al menos una Cuenta Activa en la Oficina!` });
            }
        }
    catch (error) {
        res.status(500).json({ message: 'Error al Eliminar Cuenta Bancaria' });
    }
});

app.post('/modificar_cuenta_bancaria_operador/:id_usuario/:id_cuenta_bancaria/:estado/:usuario_noti/:id_agente', async (req, res) => {
    try {    
            const { id_usuario, id_cuenta_bancaria, estado, usuario_noti, id_agente } = req.params;
            /*let alias_aux  = '';
            if (alias != 'XXXXX') {
                alias_aux = alias;
            }*/
            let alertaInactivas = 0;
            //console.log(`Estado = ${estado}`);
            if (estado == 'true') {
                const query0 = `select count(*) as cantidad ` +
                                `from cuenta_bancaria ` +
                                `where id_oficina in  ` +
                                `(select id_oficina from  ` +
                                `cuenta_bancaria where id_cuenta_bancaria = ${id_cuenta_bancaria}) ` +
                                `and marca_baja = false and id_cuenta_bancaria != ${id_cuenta_bancaria}`;
                //console.log(query0);
                const result0 = await db.handlerSQLRead(query0);
                //console.log(result0.rows[0].cantidad);
                if (result0.rows[0].cantidad == 0) {
                    alertaInactivas = 1;
                }
            } 
            //console.log(alertaInactivas);
            if (alertaInactivas == 0) {
                //const query = `select * from Modificar_Cuenta_Bancaria(${id_usuario}, ${id_cuenta_bancaria}, '${nombre}', '${alias_aux}', '${cbu}', ${estado}, ${billetera}, ${usuario_noti}, ${id_agente}, '${access_token}', '${public_key}', '${client_id}', '${client_secret}')`;
                const query = `select * from Modificar_Cuenta_Bancaria_Operador(${id_usuario}, ${id_cuenta_bancaria}, ${estado}, ${usuario_noti}, ${id_agente})`;
                //console.log(query);
                const result = await db.handlerSQL(query);
                if (result.rows[0].id_cuenta_bancaria == 0) {
                    res.status(401).json({ message: `Cuenta ${id_cuenta_bancaria} ya existe` });
                } else {
                    res.status(201).json({ message: `Cuenta ${id_cuenta_bancaria} modificada` });
                }
            } else {
                res.status(401).json({ message: `¡Siempre debe quedar al menos una Cuenta Activa en la Oficina!` });
            }
        }
    catch (error) {
        //console.log('modif cta bancaria nook');
        res.status(500).json({ message: 'Error al Modificar Cuenta Bancaria' });
    }
});

app.post('/modificar_cuenta_bancaria/:id_usuario/:id_cuenta_bancaria/:nombre/:alias/:cbu/:estado/:billetera/:usuario_noti/:id_agente/:access_token/:public_key/:client_id/:client_secret', async (req, res) => {
    try {    
            const { id_usuario, id_cuenta_bancaria, nombre, alias, cbu, estado, billetera, usuario_noti, id_agente, access_token, public_key, client_id, client_secret } = req.params;
            let alias_aux  = '';
            if (alias != 'XXXXX') {
                alias_aux = alias;
            }
            let alertaInactivas = 0;
            //console.log(`Estado = ${estado}`);
            if (estado == 'true') {
                const query0 = `select count(*) as cantidad ` +
                                `from cuenta_bancaria ` +
                                `where id_oficina in  ` +
                                `(select id_oficina from  ` +
                                `cuenta_bancaria where id_cuenta_bancaria = ${id_cuenta_bancaria}) ` +
                                `and marca_baja = false and id_cuenta_bancaria != ${id_cuenta_bancaria}`;
                //console.log(query0);
                const result0 = await db.handlerSQLRead(query0);
                //console.log(result0.rows[0].cantidad);
                if (result0.rows[0].cantidad == 0) {
                    alertaInactivas = 1;
                }
            } 
            //console.log(alertaInactivas);
            if (alertaInactivas == 0) {
                const query = `select * from Modificar_Cuenta_Bancaria(${id_usuario}, ${id_cuenta_bancaria}, '${nombre}', '${alias_aux}', '${cbu}', ${estado}, ${billetera}, ${usuario_noti}, ${id_agente}, '${access_token}', '${public_key}', '${client_id}', '${client_secret}')`;
                //console.log(query);
                const result = await db.handlerSQL(query);
                if (result.rows[0].id_cuenta_bancaria == 0) {
                    res.status(401).json({ message: `Cuenta ${alias_aux} - ${cbu} ya existe` });
                } else {
                    res.status(201).json({ message: `Cuenta ${alias_aux} - ${cbu} modificada` });
                }
            } else {
                res.status(401).json({ message: `¡Siempre debe quedar al menos una Cuenta Activa en la Oficina!` });
            }
        }
    catch (error) {
        //console.log('modif cta bancaria nook');
        res.status(500).json({ message: 'Error al Modificar Cuenta Bancaria' });
    }
});
 
app.post('/modificar_oficina/:id_usuario/:id_oficina/:oficina/:contactoWhatsapp/:contactoTelegram/:estado/:bonoPrimeraCarga/:bonoCargaPerpetua/:minimoCarga/:minimoRetiro/:minimoEsperaRetiro', async (req, res) => {
    try {    
            let { id_usuario, id_oficina , oficina, contactoWhatsapp, contactoTelegram, estado, bonoPrimeraCarga, bonoCargaPerpetua, minimoCarga, minimoRetiro, minimoEsperaRetiro } = req.params;
            contactoTelegram = contactoTelegram.replace('<<','/');
            const query = `select Modificar_Oficina(${id_oficina}, ${id_usuario}, '${oficina}', '${contactoWhatsapp}', '${contactoTelegram}', ${estado}, ${bonoPrimeraCarga}, ${bonoCargaPerpetua}, ${minimoCarga}, ${minimoRetiro}, ${minimoEsperaRetiro})`;
            //console.log(query);
            const result = await db.handlerSQL(query);
            res.status(201).json({ message: 'Oficina Modificada' });
        }
    catch (error) {
        //console.log('baja cliente nook');
        res.status(500).json({ message: 'Error al Modificar Oficina' });
    }
});

app.post('/crear_oficina/:id_usuario/:oficina/:contactoWhatsapp/:contactoTelegram/:estado/:bonoPrimeraCarga/:bonoCargaPerpetua/:minimoCarga/:minimoRetiro/:minimoEsperaRetiro', async (req, res) => {
    try {    
            let { id_usuario , oficina, contactoWhatsapp, contactoTelegram, estado, bonoPrimeraCarga, bonoCargaPerpetua, minimoCarga, minimoRetiro, minimoEsperaRetiro } = req.params;
            //console.log('llego');
            contactoTelegram = contactoTelegram.replace('<<','/');
            const query = `select * from Insertar_Oficina(${id_usuario}, '${oficina}', '${contactoWhatsapp}', '${contactoTelegram}', ${estado}, ${bonoPrimeraCarga}, ${bonoCargaPerpetua}, ${minimoCarga}, ${minimoRetiro}, ${minimoEsperaRetiro})`;
            //console.log(query);
            const result = await db.handlerSQL(query);
            if (result.rows[0].id_oficina == 0) {
                res.status(401).json({ message: `Oficina ${oficina} ya existe` });
            } else {
                res.status(201).json({ message: `Oficina ${oficina} ya creada` });
            }
        }
    catch (error) {
        //console.log('baja cliente nook');
        res.status(500).json({ message: 'Error al Crear Oficina' });
    }
});

function generarToken(length) {
    const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let token = '';
    for (let i = 0; i < length; i++) {
      const indice = Math.floor(Math.random() * caracteres.length);
      token += caracteres.charAt(indice);
    }
    return token;
}

/*Archivos Adjuntos*/
// Configuración de multer para la gestión de archivos
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads/') // Ruta donde se almacenarán los archivos
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname) // Nombre del archivo almacenado en el servidor
    }
})
  
const upload = multer({ 
    storage: storage,
    limits: {
        fileSize: 20 * 1024 * 1024 // Límite de 20MB
    },
    fileFilter: function (req, file, cb) {
        const extname = path.extname(file.originalname).toLowerCase();
        if (extname === '.jpg' || extname === '.png' || extname === '.jpeg' || extname === '.bmp' || extname === '.pdf') {
            cb(null, true);
        } else {
            cb(new Error('Solo se permiten archivos .jpg, .jpeg, .bmp, .png o .pdf'));
        }
}
});
  
// Ruta para subir un archivo
app.post('/upload', upload.single('file'), async (req, res) => {
    const id_cliente = req.body.id_cliente;
    const id_usuario = req.body.id_usuario;
    const nombre_guardado = req.body.nombre_guardado;
    const nombre_original = req.body.nombre_original;
    
    //console.log(`Cliente: ${id_cliente} - Archivo ${nombre_original} - Guardado como ${nombre_guardado} - Usuario: ${id_usuario}`);
    const query= `select Insertar_Cliente_Chat_Adjunto(${id_cliente}, false, '${nombre_original}', '${nombre_guardado}', ${id_usuario})`;
    await db.handlerSQL(query);
    res.status(201).json({ resultado: 'ok', mensaje: 'Archivo enviado exitosamente' });
});
  
// Manejador de errores
app.use((err, req, res, next) => {
    if (err instanceof multer.MulterError) {
        res.status(400).send('Error al subir archivo: ' + err.message);
    }
});

// Directorio donde se encuentran los archivos a servir
const directorioArchivos = path.join(__dirname, '../uploads/');

// Endpoint para servir archivos
app.get('/descargar/:nombreArchivo', (req, res) => {
  const nombreArchivo = req.params.nombreArchivo;
  const rutaArchivo = path.join(directorioArchivos, nombreArchivo);
  res.download(rutaArchivo);
});

app.get('/imagen/:nombreArchivo', (req, res) => {
    const nombreArchivo = req.params.nombreArchivo;
    const rutaImagen = path.join(directorioArchivos, nombreArchivo);
    res.sendFile(rutaImagen);
});

app.get('/descargar_detalle/:id_token/:id_usuario/:fecha/:agente_usuario/:oficina', async (req, res) => {
    try {        
        const id_usuario = parseInt(req.params.id_usuario, 10);
        const id_token = req.params.id_token;
        const fecha = req.params.fecha;
        const agente_usuario = req.params.agente_usuario;
        const oficina = req.params.oficina;
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.status(401).send('Error al generar el archivo CSV');
            return;
        }
        const consulta = `SELECT * FROM rpt_Clientes_Operaciones_Completo('${fecha}', '${agente_usuario}', '${oficina}');`;
        //console.log(consulta);
        const resultado = await db.handlerSQLRead(consulta);
        //console.log(resultado.rows[0]);
        // Convertir los datos obtenidos a CSV
        const json2csv = new Parser();
        const csv = json2csv.parse(resultado.rows);
        // Configurar encabezados para la descarga
        res.header('Content-Type', 'text/csv');
        res.attachment(`detalle_${fecha}_${oficina}_${agente_usuario}.csv`);
        res.send(csv);
    }
    catch (error) {
        res.status(500).send('Error al generar el archivo CSV');
    }
});

app.get('/descargar_usuarios_clientes_ip/:id_token/:id_usuario', async (req, res) => {
    try {        
        const id_usuario = parseInt(req.params.id_usuario, 10);
        const id_token = req.params.id_token;
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.status(401).send('Error al generar el archivo CSV');
            return;
        }
        const consulta = `select ip,` +
                            `id_cliente_bloqueante, ` +
                            `cliente_usuario_bloqueante, ` +
                            `agente_usuario_bloqueante, ` +
                            `oficina_bloqueante, ` +
                            `id_cliente_afectado, ` +
                            `cliente_usuario_afectado, ` +
                            `agente_usuario_afectado, ` +
                            `oficina_afectada ` +
                        `from Clientes_Bloqueantes_IP();`;
        //console.log(consulta);
        const resultado = await db.handlerSQLRead(consulta);
        //console.log(resultado.rows[0]);
        // Convertir los datos obtenidos a CSV
        const json2csv = new Parser();
        const csv = json2csv.parse(resultado.rows);
        // Configurar encabezados para la descarga
        res.header('Content-Type', 'text/csv');
        res.attachment(`usuarios_clientes_bloqueantes.csv`);
        res.send(csv);
    }
    catch (error) {
        res.status(500).send('Error al generar el archivo CSV');
    }
});

app.get('/descargar_detalle_operador/:id_token/:id_usuario/:fecha/:usuario/:oficina', async (req, res) => {
    try {        
        const id_usuario = parseInt(req.params.id_usuario, 10);
        const id_token = req.params.id_token;
        const fecha = req.params.fecha;
        const usuario = req.params.usuario;
        const oficina = req.params.oficina;
        const query = `select * from Obtener_Usuario_Token(${id_usuario},'${id_token}')`;
        //console.log(query);
        const result = await db.handlerSQLRead(query);
        if (result.rows.length === 0) {
            res.status(401).send('Error al generar el archivo CSV');
            return;
        }
        const consulta = `SELECT * FROM rpt_Operaciones_Operadores('${fecha}', '${usuario}', '${oficina}', 0);`;
        //console.log(consulta);
        const resultado = await db.handlerSQLRead(consulta);
        //console.log(resultado.rows[0]);
        // Convertir los datos obtenidos a CSV
        const json2csv = new Parser();
        const csv = json2csv.parse(resultado.rows);
        // Configurar encabezados para la descarga
        res.header('Content-Type', 'text/csv');
        res.attachment(`detalle_${fecha}_${oficina}_${usuario}.csv`);
        res.send(csv);
    }
    catch (error) {
        res.status(500).send('Error al generar el archivo CSV');
    }
});

var router = express.Router();  

app.use('/api', router);

router.post('/login_test', async (req, res) => {
    try {    
        const { username, password } = req.body;
        const query = `select * from Obtener_Usuario('${username}', true)`;
        const result = await db.handlerSQL(query);
        if (result.rows.length == 0) {
            res.status(401).json({ message: 'Nombre de usuario incorrecto.' });
             return;
        }
        const user = result.rows[0];
        const isPasswordValid = await bcrypt.compare(password, user.password);
        
        if (isPasswordValid) {
            res.status(201).json({ message: 'Login Ok' });
        } else {
            res.status(401).json({ message: 'Contraseña incorrecta.' });
        }
    } catch (error) {
        console.error('Error al buscar usuario:', error);
        res.status(500).json({ message: 'Error al iniciar sesión.' });
    }
});

// Ruta para recibir notificaciones IPN/webhook
/*router.post('/webhook', async (req, res) => {
    res.sendStatus(200);  // Responder a Mercado Pago que recibiste la notificación
    console.log('Webhook recibido:', req.body);

    const mercadopago = require('mercadopago');
    //mercadopago.configurations.setAccessToken('APP_USR-5827283266183224-051716-3c0bd335a059446611c107428e849f37-203158624');
    // Configurar el token de acceso
    mercadopago.configurations = {
        access_token: 'APP_USR-5827283266183224-051716-3c0bd335a059446611c107428e849f37-203158624'
    };
    // Procesar la notificación
    const payment = req.body;

    // Aquí puedes manejar la notificación según el tipo de evento
    if (payment.type === 'payment') {
        // Lógica para manejar pagos
        console.log('Pago recibido:', payment.data);
        const pago = await mercadopago.Payment.findById(payment.data.id);
        console.log('Detalles del pago:', pago);
    }
});
*/

router.post('/tucash', async (req, res) => {
    try 
    {
        res.status(200).json({ message: 'Correcta Recepción de Notificacion TuCash.' });
        //console.log('Req Completo:', req);
		//console.log('Headers:', req.headers);
        //console.log('Content-Type:', req.get('Content-Type'));
        let id_notificacion_carga = 0;
		const transaccion = req.body.TransactionId;
		const fecha_hora = req.body.IssueDate;
        const operacion_monto = String(Math.ceil(Number(req.body.Amount) / 100));
		const cvu_envia = String(req.body.SenderCvu).trim();
		const titular_envia = String(req.body.SenderFullName).trim().toLowerCase();
		const cuil_envia = String(req.body.SenderTributaryIdNumber).trim();
		const cvu_recibe = String(req.body.ReceiverCvu).trim();
        //await db.insertLogMessage(`TuCash -> ${transaccion} - ${fecha_hora} - ${cvu_recibe} - ${cvu_envia} - ${titular_envia} - ${cuil_envia} - ${operacion_monto}`);
        const query = `select * from Cuenta_Bancaria_TuCash('${cvu_recibe}');`;
        const result = await db.handlerSQLRead(query);
        if (result.rows) {
            const titulo = 'Envió : ' + titular_envia + ' - Monto : ' + String(operacion_monto);
            const notificacion = 'CUIL : ' + cuil_envia + ' - CVU : ' + cvu_envia;
            const idnotificacionorigen = transaccion;
            //const id_oficina = result.rows[0].id_oficina;
            const id_usuario = result.rows[0].id_usuario_ultima_modificacion;
            const id_cuenta_bancaria = result.rows[0].id_cuenta_bancaria;

            const query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 3)`;
            const result1 = await db.handlerSQL(query1);
            if (result1.rows.length > 0) 
            {
                const id_notificacion = result1.rows[0].id_notificacion;
                if (id_notificacion > 0) {
                    const query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${titular_envia}',${operacion_monto})`;
                    const result2 = await db.handlerSQL(query2);
                    id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                }
                if (id_notificacion_carga > 0) {
                    await verificarNotificacionCarga(id_notificacion_carga, id_usuario, id_cuenta_bancaria);
                }
            }
        } else {
            await db.insertLogMessage(`TuCash Cuenta Inválida -> ${transaccion} - ${fecha_hora} - ${cvu_recibe} - ${cvu_envia} - ${titular_envia} - ${cuil_envia} - ${operacion_monto}`);
        }
    } catch (error) {
		//console.log(`Error al recibir notificacion TuCash. ${error}`);
        await db.insertLogMessage(`Error al recibir notificacion TuCash. ${error}`);
    }
});

router.post('/mp_api_desa', async (req, res) => {
    try 
    {
        res.status(200).json({ message: 'Correcta Recepción de Notificacion MP.' });
    } catch (error) {
        await db.insertLogMessage(`Error al recibir notificacion MP Desa. ${error}`);
        res.status(400).json({ message: 'Error al recibir notificacion MP Desa.' });
    }
});

router.post('/mp_api', async (req, res) => {
    let id_cuenta_bancaria = 0;
    let url = '';
    let accessToken = '';
    try 
    {
        res.status(200).json({ message: 'Correcta Recepción de Notificacion MP.' });
        //console.log('Headers:', req.headers);
        //console.log('Content-Type:', req.get('Content-Type'));
        //db.insertLogMessage(`Notificacion - ${ejecucion_servidor_numero} - ${id_notificacion}`);
        id_cuenta_bancaria = parseInt(req.query.id_cuenta, 10);
        //console.log(`Notificacion de - ${id_cuenta_bancaria}`);
        //console.log('Recuros:', req.body.resource);
        const jsonString = JSON.stringify(req.body);
        let id_notificacion = 0;
        let id_notificacion_carga = 0;                        
        let query1 = '';
        let result1 = '';                     
        let query2 = '';
        let result2 = '';
        if (!req.body.resource) {
            db.insertLogMessage(`Error en recepción de Recurso!!`);
        } else {
			let idPago = '';
			if (String(req.body.resource).includes("/collections/notifications/")) {
				idPago = String(req.body.resource).split('/collections/notifications/')[1];
			} else {
				idPago = req.body.resource;
			}
			url = `https://api.mercadopago.com/v1/payments/${idPago}`;

            let id_oficina = 0;
            let headers = {};
            let respuesta = {};
    
            let operacion_monto = '';
            let operacion_usuario = '';
            let titulo = '';
            let notificacion = '';
            let idnotificacionorigen = '';
            let jsonStringCollection = '';
			
            //const query = `select * from v_Cuenta_Bancaria_Mercado_Pago where id_cuenta_bancaria = ${id_cuenta_bancaria};`;
            const query = `select * from Cuenta_Bancaria_Mercado_Pago(${id_cuenta_bancaria});`;
			//console.log(query);
            const result = await db.handlerSQLRead(query);
            if (result.rows) {
                accessToken = result.rows[0].access_token;
            } 
			//console.log(accessToken);
			//console.log(url);
            if (accessToken != '')
            {
                headers = {
                    Authorization: `Bearer ${accessToken}`,
                };
                respuesta = await axios.get(url, { headers });
				//console.log(respuesta);
                if (respuesta.status == 200) 
                {
                    if (respuesta.data.payment_type_id == 'account_money' && 
                        respuesta.data.payment_method_id == 'account_money' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;
    
                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            if (id_notificacion > 0) {
                                query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                                result2 = await db.handlerSQL(query2);
                                id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            }
                        }
                    } else if (respuesta.data.payment_type_id == 'account_money' && 
                                respuesta.data.payment_method_id == 'debin_transfer' && 
                                respuesta.data.operation_type == 'money_transfer') 
                    {                        
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        titulo = titulo + ' - Origen : Cuenta Asociada';
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;
    
                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    }
                    else if (respuesta.data.payment_type_id == 'credit_card' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        if (respuesta.data.status == 'approved')
                        {
                            operacion_monto = String(respuesta.data.transaction_amount);
							if (respuesta.data.payer.identification.type == 'DNI') {
								operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
							} else {
								operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
							}
                            operacion_usuario = operacion_usuario.toLowerCase();
                            titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                            titulo = titulo + ' - Origen : ' + respuesta.data.payment_method_id;
                            notificacion = 'PayerId : ' + respuesta.data.payer.id;
                            idnotificacionorigen = idPago;
                            id_oficina = result.rows[0].id_oficina;
                            id_usuario = result.rows[0].id_usuario;

                            query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                            result2 = await db.handlerSQL(query1);
                            if (result2.rows.length > 0) 
                            {
                                id_notificacion = result2.rows[0].id_notificacion;
                                query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                                result2 = await db.handlerSQL(query2);
                                id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            }
                        } else {
                            //jsonStringCollection = JSON.stringify(respuesta.data.collection);
                            //db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Pago TC Rechazado: ${jsonStringCollection}`); 
                            db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Pago TC Rechazado: ${url}`); 
                        }
                    } else if (respuesta.data.payment_type_id == 'digital_currency' && 
                        respuesta.data.payment_method_id == 'consumer_credits' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        titulo = titulo + '- Origen : En Cuotas MP';
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;

                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    } else if (respuesta.data.payment_type_id == 'bank_transfer' && 
                        respuesta.data.payment_method_id == 'debin_transfer' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        titulo = titulo + '- Origen : Cuenta Vinculada';
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;

                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1= await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    } /*else {
                        //jsonStringCollection = JSON.stringify(respuesta.data.collection);
                        //db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Colección Inesperada : ${jsonStringCollection}`);
                        db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Colección Inesperada : ${url}`);
                    }*/
                    if (id_notificacion_carga > 0) {
                        await verificarNotificacionCarga(id_notificacion_carga, id_usuario, id_cuenta_bancaria);
                    }
                } else {
                    db.insertLogMessage(`Error en Respuesta: ${respuesta.status}`);
                }
            } else {
                await db.insertLogMessage(`Acces Token no encontrado para cuenta: ${id_cuenta_bancaria}`);
                await db.insertLogMessage(`Recibido de MP: ${jsonString}`);
            }
        }
    } catch (error) {
		//console.log(`Error al recibir notificacion MP. ${error}`);
        //await db.insertLogMessage(`Error al recibir notificacion MP (Cuenta ${id_cuenta_bancaria}) ${error}`);
        await db.insertLogMessage(`Error al recibir notificacion MP ${id_cuenta_bancaria}: - ${error} - ${url} - ${accessToken}`);
        //res.status(400).json({ message: 'Error al recibir notificacion MP.' });
    }
});

router.post('/recibomp', async (req, res) => {
    let id_cuenta_bancaria = 0;
    let url = '';
    let accessToken = '';
    try 
    {
        res.status(200).json({ message: 'Correcta Recepción de Notificacion MP.' });
        //console.log('Headers:', req.headers);
        //console.log('Content-Type:', req.get('Content-Type'));
        //db.insertLogMessage(`Notificacion - ${ejecucion_servidor_numero} - ${id_notificacion}`);
        id_cuenta_bancaria = parseInt(req.query.id_cuenta, 10);
        //console.log(`Notificacion de - ${id_cuenta_bancaria}`);
        //console.log('Recuros:', req.body.resource);
        const jsonString = JSON.stringify(req.body);
        let id_notificacion = 0;
        let id_notificacion_carga = 0;                        
        let query1 = '';
        let result1 = '';                     
        let query2 = '';
        let result2 = '';
        if (!req.body.resource) {
            db.insertLogMessage(`Error en recepción de Recurso!!`);
        } else {
			let idPago = '';
			if (String(req.body.resource).includes("/collections/notifications/")) {
				idPago = String(req.body.resource).split('/collections/notifications/')[1];
			} else {
				idPago = req.body.resource;
			}
			url = `https://api.mercadopago.com/v1/payments/${idPago}`;

            let id_oficina = 0;
            let headers = {};
            let respuesta = {};
    
            let operacion_monto = '';
            let operacion_usuario = '';
            let titulo = '';
            let notificacion = '';
            let idnotificacionorigen = '';
            let jsonStringCollection = '';
			
            //const query = `select * from v_Cuenta_Bancaria_Mercado_Pago where id_cuenta_bancaria = ${id_cuenta_bancaria};`;
            const query = `select * from Cuenta_Bancaria_Mercado_Pago(${id_cuenta_bancaria});`;
			//console.log(query);
            const result = await db.handlerSQLRead(query);
            if (result.rows) {
                accessToken = result.rows[0].access_token;
            } 
			//console.log(accessToken);
			//console.log(url);
            if (accessToken != '')
            {
                headers = {
                    Authorization: `Bearer ${accessToken}`,
                };
                respuesta = await axios.get(url, { headers });
				//console.log(respuesta);
                if (respuesta.status == 200) 
                {
                    if (respuesta.data.payment_type_id == 'account_money' && 
                        respuesta.data.payment_method_id == 'account_money' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;
    
                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            if (id_notificacion > 0) {
                                query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                                result2 = await db.handlerSQL(query2);
                                id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            }
                        }
                    } else if (respuesta.data.payment_type_id == 'account_money' && 
                                respuesta.data.payment_method_id == 'debin_transfer' && 
                                respuesta.data.operation_type == 'money_transfer') 
                    {                        
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        titulo = titulo + ' - Origen : Cuenta Asociada';
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;
    
                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    }
                    else if (respuesta.data.payment_type_id == 'credit_card' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        if (respuesta.data.status == 'approved')
                        {
                            operacion_monto = String(respuesta.data.transaction_amount);
							if (respuesta.data.payer.identification.type == 'DNI') {
								operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
							} else {
								operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
							}
                            operacion_usuario = operacion_usuario.toLowerCase();
                            titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                            titulo = titulo + ' - Origen : ' + respuesta.data.payment_method_id;
                            notificacion = 'PayerId : ' + respuesta.data.payer.id;
                            idnotificacionorigen = idPago;
                            id_oficina = result.rows[0].id_oficina;
                            id_usuario = result.rows[0].id_usuario;

                            query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                            result2 = await db.handlerSQL(query1);
                            if (result2.rows.length > 0) 
                            {
                                id_notificacion = result2.rows[0].id_notificacion;
                                query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                                result2 = await db.handlerSQL(query2);
                                id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            }
                        } else {
                            //jsonStringCollection = JSON.stringify(respuesta.data.collection);
                            //db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Pago TC Rechazado: ${jsonStringCollection}`); 
                            db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Pago TC Rechazado: ${url}`); 
                        }
                    } else if (respuesta.data.payment_type_id == 'digital_currency' && 
                        respuesta.data.payment_method_id == 'consumer_credits' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        titulo = titulo + '- Origen : En Cuotas MP';
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;

                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    } else if (respuesta.data.payment_type_id == 'bank_transfer' && 
                        respuesta.data.payment_method_id == 'debin_transfer' && 
                        respuesta.data.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.transaction_details.total_paid_amount);
						if (respuesta.data.payer.identification.type == 'DNI') {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number);
						} else {
							operacion_usuario = respuesta.data.payer.email + ' ' + String(respuesta.data.payer.identification.number).slice(2, -1);
						}
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.issuer_id;
                        titulo = titulo + '- Origen : Cuenta Vinculada';
                        notificacion = 'PayerId : ' + respuesta.data.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;

                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1= await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    } /*else {
                        //jsonStringCollection = JSON.stringify(respuesta.data.collection);
                        //db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Colección Inesperada : ${jsonStringCollection}`);
                        db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Colección Inesperada : ${url}`);
                    }*/
                    if (id_notificacion_carga > 0) {
                        await verificarNotificacionCarga(id_notificacion_carga, id_usuario, id_cuenta_bancaria);
                    }
                } else {
                    db.insertLogMessage(`Error en Respuesta: ${respuesta.status}`);
                }
            } else {
                await db.insertLogMessage(`Acces Token no encontrado para cuenta: ${id_cuenta_bancaria}`);
                await db.insertLogMessage(`Recibido de MP: ${jsonString}`);
            }
        }
    } catch (error) {
		//console.log(`Error al recibir notificacion MP. ${error}`);
        //await db.insertLogMessage(`Error al recibir notificacion MP (Cuenta ${id_cuenta_bancaria}) ${error}`);
        await db.insertLogMessage(`Error al recibir notificacion MP ${id_cuenta_bancaria}: - ${error} - ${url} - ${accessToken}`);
        //res.status(400).json({ message: 'Error al recibir notificacion MP.' });
    }
});

router.post('/mp_api_ant', async (req, res) => {
    try 
    {
        res.status(200).json({ message: 'Correcta Recepción de Notificacion MP.' });
        //console.log('Headers:', req.headers);
        //console.log('Content-Type:', req.get('Content-Type'));
        //db.insertLogMessage(`Notificacion - ${ejecucion_servidor_numero} - ${id_notificacion}`);
        const id_cuenta_bancaria = parseInt(req.query.id_cuenta, 10);
        //console.log(`Notificacion de - ${id_cuenta_bancaria}`);
        //console.log('Recuros:', req.body.resource);
        const jsonString = JSON.stringify(req.body);
        let id_notificacion = 0;
        let id_notificacion_carga = 0;                        
        let query1 = '';
        let result1 = '';                     
        let query2 = '';
        let result2 = '';
        if (!req.body.resource) {
            db.insertLogMessage(`Error en recepción de Recurso!!`);
        } else {
            const url = req.body.resource;
            const idPago = url.split('/collections/notifications/')[1];
            let accessToken = '';
            let id_oficina = 0;
            let headers = {};
            let respuesta = {};
    
            let operacion_monto = '';
            let operacion_usuario = '';
            let titulo = '';
            let notificacion = '';
            let idnotificacionorigen = '';
            let jsonStringCollection = '';
			
            const query = `select * from v_Cuenta_Bancaria_Mercado_Pago where id_cuenta_bancaria = ${id_cuenta_bancaria};`;
			//console.log(query);
            const result = await db.handlerSQLRead(query);
            if (result.rows) {
                accessToken = result.rows[0].access_token;
            } 
			//console.log(accessToken);
			//console.log(url);
            if (accessToken != '')
            {
                headers = {
                    Authorization: `Bearer ${accessToken}`,
                };
                respuesta = await axios.get(url, { headers });
				//console.log(respuesta);
                if (respuesta.status == 200) 
                {
                    if (respuesta.data.collection.payment_type == 'account_money' && 
                        respuesta.data.collection.payment_method_id == 'account_money' && 
                        respuesta.data.collection.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.collection.total_paid_amount);
                        operacion_usuario = respuesta.data.collection.payer.email + ' ' + respuesta.data.collection.payer.nickname;
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.collection.issuer_id;
                        notificacion = 'PayerId : ' + respuesta.data.collection.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;
    
                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            if (id_notificacion > 0) {
                                query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                                result2 = await db.handlerSQL(query2);
                                id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            }
                        }
                    } else if (respuesta.data.collection.payment_type == 'account_money' && 
                                respuesta.data.collection.payment_method_id == 'debin_transfer' && 
                                respuesta.data.collection.operation_type == 'money_transfer') 
                    {                        
                        operacion_monto = String(respuesta.data.collection.total_paid_amount);
                        operacion_usuario = respuesta.data.collection.payer.email + ' ' + respuesta.data.collection.payer.nickname;
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.collection.issuer_id;
                        titulo = titulo + ' - Origen : Cuenta Asociada';
                        notificacion = 'PayerId : ' + respuesta.data.collection.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;
    
                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    }
                    else if (respuesta.data.collection.payment_type == 'credit_card' && 
                        respuesta.data.collection.operation_type == 'money_transfer') 
                    {
                        if (respuesta.data.collection.status == 'approved')
                        {
                            operacion_monto = String(respuesta.data.collection.transaction_amount);
                            operacion_usuario = respuesta.data.collection.payer.email + ' ' + respuesta.data.collection.payer.nickname;
                            operacion_usuario = operacion_usuario.toLowerCase();
                            titulo = 'IsUsserId : ' + respuesta.data.collection.issuer_id;
                            titulo = titulo + ' - Origen : ' + respuesta.data.collection.payment_method_id;
                            notificacion = 'PayerId : ' + respuesta.data.collection.payer.id;
                            idnotificacionorigen = idPago;
                            id_oficina = result.rows[0].id_oficina;
                            id_usuario = result.rows[0].id_usuario;

                            query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                            result2 = await db.handlerSQL(query1);
                            if (result2.rows.length > 0) 
                            {
                                id_notificacion = result2.rows[0].id_notificacion;
                                query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                                result2 = await db.handlerSQL(query2);
                                id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            }
                        } else {
                            jsonStringCollection = JSON.stringify(respuesta.data.collection);
                            //db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Pago TC Rechazado: ${jsonStringCollection}`); 
                            db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Pago TC Rechazado: ${url}`); 
                        }
                    } else if (respuesta.data.collection.payment_type == 'digital_currency' && 
                        respuesta.data.collection.payment_method_id == 'consumer_credits' && 
                        respuesta.data.collection.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.collection.total_paid_amount);
                        operacion_usuario = respuesta.data.collection.payer.email + ' ' + respuesta.data.collection.payer.nickname;
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.collection.issuer_id;
                        titulo = titulo + '- Origen : En Cuotas MP';
                        notificacion = 'PayerId : ' + respuesta.data.collection.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;

                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1 = await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    } else if (respuesta.data.collection.payment_type == 'bank_transfer' && 
                        respuesta.data.collection.payment_method_id == 'debin_transfer' && 
                        respuesta.data.collection.operation_type == 'money_transfer') 
                    {
                        operacion_monto = String(respuesta.data.collection.total_paid_amount);
                        operacion_usuario = respuesta.data.collection.payer.email + ' ' + respuesta.data.collection.payer.nickname;
                        operacion_usuario = operacion_usuario.toLowerCase();
                        titulo = 'IsUsserId : ' + respuesta.data.collection.issuer_id;
                        titulo = titulo + '- Origen : Cuenta Vinculada';
                        notificacion = 'PayerId : ' + respuesta.data.collection.payer.id;
                        idnotificacionorigen = idPago;
                        id_oficina = result.rows[0].id_oficina;
                        id_usuario = result.rows[0].id_usuario;

                        query1 = `select * from Registrar_Notificacion(${id_usuario}, ${id_cuenta_bancaria}, '${titulo}','${notificacion}','${idnotificacionorigen}', 1)`;
                        result1= await db.handlerSQL(query1);
                        if (result1.rows.length > 0) 
                        {
                            id_notificacion = result1.rows[0].id_notificacion;
                            query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${operacion_usuario}',${operacion_monto})`;
                            result2 = await db.handlerSQL(query2);
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                        }
                    } /*else {
                        //jsonStringCollection = JSON.stringify(respuesta.data.collection);
                        //db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Colección Inesperada : ${jsonStringCollection}`);
                        db.insertLogMessage(`Cuenta ${id_cuenta_bancaria} Colección Inesperada : ${url}`);
                    }*/
                    if (id_notificacion_carga > 0) {
                        await verificarNotificacionCarga(id_notificacion_carga, id_usuario, id_cuenta_bancaria);
                    }
                } else {
                    db.insertLogMessage(`Error en Respuesta: ${respuesta.status}`);
                }
            } else {
                await db.insertLogMessage(`Acces Token no encontrado para cuenta: ${id_cuenta_bancaria}`);
                await db.insertLogMessage(`Recibido de MP: ${jsonString}`);
            }
        }
    } catch (error) {
		//console.log(`Error al recibir notificacion MP. ${error}`);
        await db.insertLogMessage(`Error al recibir notificacion MP. ${error}`);
        //await db.insertLogMessage(`Error al recibir notificacion MP ${id_cuenta_bancaria}: - ${error} - ${url} - ${accessToken}`);
        //res.status(400).json({ message: 'Error al recibir notificacion MP.' });
    }
});
 
const verificarNotificacionCarga = async (id_notificacion_carga, id_usuario, id_cuenta_bancaria) => {
    try {
        const query1 = `select * from Verificar_Notificacion_Carga(${id_notificacion_carga})`;
        //console.log(query1);
        const result1 = await db.handlerSQLDirect(query1);
        if (result1.rows[0].id_operacion_carga > 0) 
        {
            const monto_total = result1.rows[0].monto;
            const bono = result1.rows[0].bono;
            const id_operacion = result1.rows[0].id_operacion;
            let id_cuenta_bancaria_ope = result1.rows[0].id_cuenta_bancaria;
            const id_cliente = result1.rows[0].id_cliente;
            const cliente_usuario = result1.rows[0].usuario;
            const id_cliente_ext = result1.rows[0].id_cliente_ext;
            const id_cliente_db = result1.rows[0].id_cliente_db;
            const agente_usuario = result1.rows[0].agente_usuario;
            const agente_password = result1.rows[0].agente_password;
            const id_operacion_carga = result1.rows[0].id_operacion_carga;
            const cantidad_cargas = result1.rows[0].cantidad_cargas;
            const referente_id_cliente = result1.rows[0].referente_id_cliente;
            const referente_usuario = result1.rows[0].referente_usuario;
            const referente_id_cliente_ext = result1.rows[0].referente_id_cliente_ext;
            const referente_id_cliente_db = result1.rows[0].referente_id_cliente_db;
            const id_plataforma = result1.rows[0].id_plataforma;

            if (id_cuenta_bancaria > 0) {
                id_cuenta_bancaria_ope = id_cuenta_bancaria;
            }

            let resultado = '';
            if (id_plataforma == 1 || id_plataforma == 2) {
                const carga_manual1 = require('./scrap_bot3/cargar.js');
                resultado = await carga_manual1(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_total), agente_usuario, agente_password);
            }
            //console.log(`Resultado carga = ${resultado}`);
            if (resultado == 'ok') 
            {
                const query3 = `select * from Confirmar_Notificacion_Carga(${id_notificacion_carga}, ${id_operacion_carga}, ${id_operacion}, ${id_usuario}, ${id_cuenta_bancaria_ope})`;
                await db.handlerSQLDirect(query3);
                //console.log(`Confirmar = ${query3}`);
                //console.log(`Cantidad cargas = ${cantidad_cargas}`);
                if (cantidad_cargas == 0 && referente_id_cliente > 0) {
                    //console.log(`referente_id_cliente = ${referente_id_cliente}`);
                    //console.log(`referente_id_cliente_ext = ${referente_id_cliente_ext}`);
                    //console.log(`referente_id_cliente_db = ${referente_id_cliente_db}`);
                    //console.log(`referente_usuario = ${referente_usuario}`);
                    //console.log(`bono = ${bono}`);
                    //resultado = await carga_manual3(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_total), agente_usuario, agente_password);
                    resultado = '';
                    if (id_plataforma == 1 || id_plataforma == 2) {
                        //console.log(`id_plataforma = ${id_plataforma}`);
                        const carga_manual2 = require('./scrap_bot3/cargar.js');
                        resultado = await carga_manual2(referente_id_cliente_ext, referente_id_cliente_db, referente_usuario.trim(), String(bono), agente_usuario, agente_password);
                        //console.log(resultado);
                    }
                    if (resultado == 'ok')
                    {
                        const query2 = `select * from Cargar_Bono_Referido(${referente_id_cliente}, ${id_usuario}, ${bono}, ${id_cuenta_bancaria_ope}, ${id_operacion})`;
                        //console.log(`Referido = ${query2}`);
                        await db.handlerSQLDirect(query2);
                    } else {
                        await db.insertLogMessage(`Error en la Carga Referente: ${resultado} - [${id_notificacion_carga}, ${id_operacion_carga}]`);
                    }
                }
            } else if (resultado == 'en_espera') {

                const redisClient = createClient({
                    url: 'redis://192.168.200.76:6379',
                    password: 'paneles0110'
                });
                await redisClient.connect();
                const cod_elemento_redis = `proc_002_${id_notificacion_carga}`;
                const nuevo_elemento_redis = {
                    id_notificacion_carga : id_notificacion_carga,
                    id_operacion_carga : id_operacion_carga,
                    id_cuenta_bancaria : id_cuenta_bancaria,
                    id_usuario : id_usuario
                }; 
                await redisClient.set(cod_elemento_redis, JSON.stringify(nuevo_elemento_redis));
                await redisClient.disconnect();
                const query4 = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                await db.handlerSQLDirect(query4);

            } else {
                await db.insertLogMessage(`Error en la Carga: ${resultado} - [${id_notificacion_carga}, ${id_operacion_carga}]`);
            }
            return resultado;
        }
    } catch (error) {
        return 'error';
    }
};

router.post('/obtenerNotificacion', async (req, res) => {
    res.status(201).json({ message: `Notificación Recibida en Servidor` });
    let { id_usuario, aplicacion, titulo, notificacion, idnotificacionorigen } = req.body;
    if (id_usuario == null || id_usuario == undefined) {
        id_usuario = 0;
    }
    try {
        let id_notificacion = 0;
        let id_notificacion_carga = 0;
        let id_cuenta_bancaria = 0;
        let monto = '';
        let enviado_por = '';
        let enviado_mp = false;
        const query1 = `select * from Registrar_Notificacion_PanelesNoti(${id_usuario},'${aplicacion}','${titulo}','${notificacion}','${idnotificacionorigen}', 2)`;
        //console.log(query1);
        const result1 = await db.handlerSQLDirect(query1);
        if (result1.rows.length > 0) {
            id_notificacion = result1.rows[0].id_notificacion;
            id_cuenta_bancaria = result1.rows[0].id_cuenta_bancaria;
            //db.insertLogMessage(`Notificacion - ${ejecucion_servidor_numero} - ${id_notificacion}`);
            //console.log(aplicacion);
            if (aplicacion == 'com.mercadopago.wallet' && id_notificacion > 0) {
                //console.log(titulo);
                if (titulo.startsWith('Recibiste $')) {
                    //console.log(notificacion);
                    //console.log(monto);
                    if (notificacion.includes(' te envió dinero y ya está disponible en tu cuenta.')) {
                        enviado_por = notificacion.replace(' te envió dinero y ya está disponible en tu cuenta.', '').trim();
                    }
                    if (notificacion.includes(' te envió dinero desde su cuenta de ')) {
                        enviado_por = notificacion.split(' te envió dinero desde su cuenta de ')[0].trim();
                    }
                    if (notificacion.includes(' te envió dinero, ¡invertilo y generá rendimientos!')) {
                        enviado_por = notificacion.split(' te envió dinero, ¡invertilo y generá rendimientos!')[0].trim();
                    }
                    if (notificacion.includes(' te envió dinero y ya está generando rendimientos en tu cuenta.')) {
                        enviado_por = notificacion.split(' te envió dinero y ya está generando rendimientos en tu cuenta.')[0].trim();
                    }
                    if (notificacion.includes(' desde su cuenta de Mercado Pago.')) {
                        enviado_mp = true;
                        enviado_por = notificacion.replace(' desde su cuenta de Mercado Pago.', '');
                        enviado_por = enviado_por.replace('De ', '').trim();
                    }
                    /*API MP sin funcionamiento: Comentar IF*/
                    if (!enviado_mp) {
                        monto = titulo.split('$')[1].trim();
                        monto = monto.replace('.','');
                        monto = monto.replace(',','.');
                        enviado_por = enviado_por.replace(/\s+/g, ' ');
                        enviado_por = enviado_por.replace(',', ' ');
                        enviado_por = enviado_por.toLowerCase();
                        //console.log(enviado_por);
                        const query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${enviado_por}',${monto})`;
                        const result2 = await db.handlerSQLDirect(query2);
                        if (result2.rows.length > 0) { 
                            id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                            //res.status(201).json({ message: `Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}` });
                        }
                    }
                }
            } else if (aplicacion == 'ar.openbank.modelbank' && id_notificacion > 0) {
                //console.log(titulo);
                if (titulo.trim() == '¡Recibiste una transferencia!') {
                    //console.log(notificacion);
                    aux = notificacion.trim().split(' te mandó $');
                    monto = aux[1].slice(0, -1);
                    monto = monto.replace('.','');
                    monto = monto.replace(',','.');
                    enviado_por = aux[0].toLowerCase();
                    enviado_por = enviado_por.replace(/\s+/g, ' ');
                    enviado_por = enviado_por.replace(',', ' ');
                    //console.log(monto);
                    //console.log(enviado_por);
                    const query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${enviado_por}',${monto})`;
                    const result2 = await db.handlerSQLDirect(query2);
                    if (result2.rows.length > 0) { 
                        id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                    }
                    //console.log(`Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}`);
                    //res.status(201).json({ message: `Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}` });
                }    
            } else if (aplicacion == 'com.brubank' && id_notificacion > 0) {
                //console.log(titulo);
                if (notificacion.includes(' te envió $ ')) {
                    //console.log(notificacion);
                    aux = notificacion.trim().split(' te envió $ ');
                    monto = aux[1].slice(0, -2).trim();
                    monto = monto.replace('.','');
                    monto = monto.replace(',','.');
                    enviado_por = aux[0].toLowerCase().trim();
                    enviado_por = enviado_por.replace(/\s+/g, ' ');
                    enviado_por = enviado_por.replace(',', ' ');
                    //console.log(monto);
                    //console.log(enviado_por);
                    const query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${enviado_por}',${monto})`;
                    const result2 = await db.handlerSQLDirect(query2);
                    if (result2.rows.length > 0) { 
                        id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                    }
                    //console.log(`Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}`);
                    //res.status(201).json({ message: `Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}` });
                } /*else {
                    //console.log(`Notificación creada de Mercado Pago: ${id_notificacion}`);
                    res.status(201).json({ message: `Notificación creada de Bru Bank: ${id_notificacion}` });
                }*/
            } else if (aplicacion == 'com.telepagos' && id_notificacion > 0) {
                //console.log(titulo);
                if (notificacion.includes('Recibiste $')) {
                    //console.log(notificacion);
                    aux = notificacion.replace('Recibiste','');
                    aux = aux.replace('$','');
                    aux = aux.trim();
                    aux = aux.split(' de ');
                    monto = aux[0].replace('.','');
                    monto = monto.replace(',','.');
                    enviado_por = aux[1].toLowerCase();
                    enviado_por = enviado_por.slice(0, -1);
                    enviado_por = enviado_por.replace(/\s+/g, ' ');
                    enviado_por = enviado_por.replace(',', ' ');
                    //console.log(monto);
                    //console.log(enviado_por);
                    const query2 = `select * from Registrar_Notificacion_Carga(${id_notificacion},'${enviado_por}',${monto})`;
                    const result2 = await db.handlerSQLDirect(query2);
                    if (result2.rows.length > 0) { 
                        id_notificacion_carga = result2.rows[0].id_notificacion_carga;
                    }
                    //console.log(`Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}`);
                    //res.status(201).json({ message: `Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}` });
                }
            }
            //console.log(`Notificación y Carga Detectada! ${id_notificacion} - ${id_notificacion_carga}`);
            if (id_notificacion_carga > 0) {
                await verificarNotificacionCarga(id_notificacion_carga, id_usuario, id_cuenta_bancaria);
            }
        }
    } catch (error) {
        db.insertLogMessage(`PanelesNoti Error: ${id_usuario} - ${aplicacion} - ${titulo} - ${notificacion} - ${idnotificacionorigen}`);
        //res.status(201).json({ message: `Error al recibir Notificación en Servidor` });
    }
});

router.post('/login_noti', async (req, res) => {
    try {
        const { username, password, version } = req.body;
        /*API MP sin funcionamiento:*/
        //if (version == 'web' || version == 'v5.5')
        /**/
        if (version == 'web' || version == 'v5')
        {
            const query = `select * from Obtener_Usuario_Noti('${username}')`;
            //console.log(query);
            const result = await db.handlerSQL(query);
            if (result.rows.length === 0) {
                res.status(401).json({ message: 'Nombre de Usuario Incorrecto ó Sin Cuenta Asignada.' });
                return;
            }
            //console.log(result.rows[0].cantidad_cuentas);
            /*if (result.rows[0].cantidad_cuentas > 1) {
                res.status(401).json({ message: 'El Usuario debe tener sólo 1 Cuenta Asignada por Billetera.' });
                return;
            }*/
            const user = result.rows[0];
            const isPasswordValid = await bcrypt.compare(password, user.password);
            if (isPasswordValid) {
                res.status(201).json({ message: user.id_usuario });
            } else {
                res.status(401).json({ message: 'Contraseña incorrecta.' });
            }
        } else {
            res.status(402).json({ message: 'Por Favor Actualizar App PanelesNoti' });
        }
    } catch (error) {
        //console.error('Error al buscar usuario:', error);
        res.status(500).json({ message: 'Error al iniciar sesión.' });
    }
});

router.post('/appsdecobro', async (req, res) => {
    try {    
        const query = `select * from Obtener_AppsCobro();`;
        const result = await db.handlerSQLRead(query);
        res.status(201).json(result.rows);
    } catch (error) {
        res.status(500).json({ message: 'Error al consultar aplicaciones de cobro.' });
    }
});

async function colectorMem() {
    if (global.gc) {
        global.gc();
    }
};

const intervalId_01 = setInterval(colectorMem, 60 * 1000); // (30.000 ms = 30 segundos)
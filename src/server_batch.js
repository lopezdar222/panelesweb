const path = require('path');
const db = require(__dirname + '/db');
const https = require('https');
const fs = require('fs');
const { createClient } = require('@redis/client');
let anulando_notificaciones = 0;
let registrando_concurrentes = 0;
let verificando_transferencias = 0;
let destrabando_solicitudes = 0;
const esperaRegistro = 30;
const esperaEnProceso = 120;
const esperaTransferencias = 5;
let iteracionesActivo = 0;
//url: 'redis://192.168.200.76:6379', //ip lan
//url: 'redis://200.58.106.199:6379', //ip publica
const urlRedis = 'redis://192.168.200.76:6379';
const passRedis = 'paneles0110';
///////////////////////////////////////////////////////////////////////////////
db.pruebaConexion();
const cargar = require('./scrap_bot3/cargar.js');

async function anular_Notificaciones_Landing() {
    if (anulando_notificaciones == 0)
    {
        //console.log('Procesa Anulacion');
        anulando_notificaciones = 1;
        try {
            const query = `SELECT  id_notificacion ` +
                            `FROM v_Notificaciones_Cargas ` +
                            `WHERE ((fecha_hora < NOW() - INTERVAL '60 minutes' AND id_origen = 1) ` +
                            `OR (fecha_hora < NOW() - INTERVAL '60 minutes' AND id_origen = 2)) ` +
                            `AND anulada = false ` +
                            `AND id_cuenta_bancaria > 1 ` +
                            `AND id_operacion_carga IS NULL ` +
                            `AND marca_procesado = false ` +
                            `ORDER BY fecha_hora LIMIT 1`;
            //console.log(query);
            const result = await db.handlerSQL(query);
            if (result.rows.length > 0) {
                //console.log(`Anula: ${result.rows[0].id_notificacion}`);
                const query2 = `SELECT Registrar_Anulacion_Carga(${result.rows[0].id_notificacion}, 1)`;
                await db.handlerSQLDirect(query2);
            }            
            anulando_notificaciones = 0;
            if (global.gc) {
                global.gc();
            }
        } catch (error) {
            anulando_notificaciones = 0;
            //console.error('Error al Anular Notificaciones', error);
            throw error;
        }
    }
};

async function destrabar_Solicitudes() {
    if (destrabando_solicitudes == 0)
    {
        //console.log('Procesa Anulacion');
        destrabando_solicitudes = 1;
        try {
            const query = `UPDATE operacion_carga AS opc ` +
                            `SET procesando = false ` +
                            `FROM operacion op ` +
                            `WHERE (opc.id_operacion = op.id_operacion ` +
                            `AND op.fecha_hora_creacion < NOW() - INTERVAL '2 minutes' ` +
                            `AND op.id_estado = 1)`;
            //console.log(query);
            const result = await db.handlerSQLDirect(query);
            destrabando_solicitudes = 0;
        } catch (error) {
            destrabando_solicitudes = 0;
            //console.error('Error al Anular Notificaciones', error);
            throw error;
        }
    }
};

async function registrar_Concurrencia() {
    if (registrando_concurrentes == 0)
    {
        //console.log('Registra Concurrencia');
        registrando_concurrentes = 1;
        try {
            const redisClient = createClient({
                url : urlRedis,
                password: passRedis
            });
			await redisClient.connect();
			let res = '';
            let cod_elemento = `proc_001`;
            let reply = await redisClient.get(cod_elemento);
            let data = JSON.parse(reply);
            
            let query = `INSERT INTO concurrencia_registro (fecha_hora, id_proceso, activos, en_espera, tope) ` +
                            `VALUES (Now(), ${data.id_proceso}, ${data.activos}, ${data.en_espera}, ${data.tope});`;
            //console.log(query);
            await db.handlerSQL(query);

            if(data.activos < 0) {
                data.activos = 0;
                data.en_espera = 0;
                await redisClient.set(cod_elemento, JSON.stringify(data));
            }
            if(data.activos >= data.tope) {
                iteracionesActivo++;
            } else {
                iteracionesActivo=0;
            }
            if(2 < iteracionesActivo) {
                reply = await redisClient.get(cod_elemento);
                data = JSON.parse(reply);
                data.activos = 0;
                await redisClient.set(cod_elemento, JSON.stringify(data));
                iteracionesActivo=0;
            }

            let keys = await redisClient.keys('apli*');
            if (keys.length > 0) {
                for (const key of keys) {
                    reply = await redisClient.get(key);
                    data = JSON.parse(reply);
                    await redisClient.del(key);
                    if ('resultado' in data) {
                        res = data.resultado;
                    } else {
                        res = 'error';
                    }
                    let query = `INSERT INTO concurrencia_registro_aplicadas (fecha_hora, id_proceso, id_notificacion_carga, resultado) ` +
                                `VALUES (Now(), 1, ${data.id_notificacion_carga}, '${res}');`;
                    //console.log(query);
                    await db.handlerSQL(query);
                }
            }
            
            await redisClient.disconnect();
            registrando_concurrentes = 0;
        } catch (error) {
            registrando_concurrentes = 0;
            //console.error('Error al Registrar Concurrencia', error);
            throw error;
        }
    }
};

async function verificar_Transferencias() {
    if (verificando_transferencias == 0)
    {
        //console.log('Registra Concurrencia');
        verificando_transferencias = 1;
        try {
            const redisClient = createClient({
                url : urlRedis,
                password: passRedis
            });
			await redisClient.connect();
            let reply = null;
            let tranData = null;
            let tranDataProc = {
                resultado: '',
                id_notificacion_carga: 0
            };        
            let query1 = '';
            let keys = await redisClient.keys('proc_002_*');
            if (keys.length > 0) {
                for (const key of keys) {
                    reply = await redisClient.get(key);
                    tranData = JSON.parse(reply);
                    await redisClient.del(key);
                    //console.log('Desde Paneles');
                    //console.log(tranData.id_notificacion_carga);
                    tranDataProc.id_notificacion_carga = tranData.id_notificacion_carga;
                    query1 = `update operacion_carga set procesando = false where id_operacion_carga = ${tranData.id_operacion_carga}`;
                    await db.handlerSQLDirect(query1);
                    tranDataProc.resultado = await verificarNotificacionCarga(tranData.id_notificacion_carga, tranData.id_usuario, tranData.id_cuenta_bancaria, redisClient);
                    await redisClient.set(`apli_002_${tranData.id_notificacion_carga}`, JSON.stringify(tranDataProc));
                }
            }
            keys = await redisClient.keys('proc_003_*');
            if (keys.length > 0) {
                for (const key of keys) {
                    reply = await redisClient.get(key);
                    tranData = JSON.parse(reply);
                    //console.log(`Elimina proc_003_${tranData.id_notificacion_carga}`);
                    await redisClient.del(key);
                    //console.log('Desde Landing');
                    //console.log(tranData.id_operacion);
                    tranDataProc.id_notificacion_carga = tranData.id_notificacion_carga;
                    query1 = `update operacion_carga set procesando = false where id_operacion = ${tranData.id_operacion}`;
                    await db.handlerSQLDirect(query1);
                    tranDataProc.resultado = await verificarOperacionCarga(tranData.id_operacion, redisClient);
                    //console.log(`Carga apli_003_${tranData.id_notificacion_carga}`);
                    await redisClient.set(`apli_003_${tranData.id_notificacion_carga}`, JSON.stringify(tranDataProc));
                };
            }
            verificando_transferencias = 0;
        } catch (error) {
            verificando_transferencias = 0;
            //console.error('Error al Verificar Transferencias', error);
            throw error;
        }
    }
};

async function verificarOperacionCarga (id_operacion, redisClient) {
    try {
        const query1 = `select * from Verificar_Operacion_Carga(${id_operacion})`;
        const result1 = await db.handlerSQLDirect(query1);
        let resultado = 'nook';
        //console.log(`Verificar = ${query1}`);
        if (result1.rows[0].id_operacion_carga > 0) 
        {
            const monto_total = result1.rows[0].monto;
            const bono = result1.rows[0].bono;
            const id_notificacion_carga = result1.rows[0].id_notificacion_carga;
            const id_usuario = result1.rows[0].id_usuario;
            const id_cuenta_bancaria = result1.rows[0].id_cuenta_bancaria;
            const id_cliente = result1.rows[0].id_cliente;
            const cliente_usuario = result1.rows[0].usuario;
            const id_cliente_ext = result1.rows[0].id_cliente_ext;
            const id_cliente_db = result1.rows[0].id_cliente_db;
            const agente_usuario = result1.rows[0].agente_usuario;
            const agente_password = result1.rows[0].agente_password;
            const id_operacion_carga = result1.rows[0].id_operacion_carga;
            const cantidad_cargas = result1.rows[0].cantidad_cargas;
            const referente_id_cliente = result1.rows[0].referente_id_cliente;
            const referente_usuario = result1.rows[0].referente_usuario;
            const referente_id_cliente_ext = result1.rows[0].referente_id_cliente_ext;
            const referente_id_cliente_db = result1.rows[0].referente_id_cliente_db;
            const id_plataforma = result1.rows[0].id_plataforma;

            if (id_plataforma == 1 || id_plataforma == 2) {
                //const carga_manual1 = require('./scrap_bot3/cargar.js');
                //console.log(`id_cliente_ext = ${id_cliente_ext}`);
                //console.log(`id_cliente_db = ${id_cliente_db}`);
                //console.log(`agente = ${agente_usuario}`);
                //console.log(`agente pass = ${agente_password}`);
                //console.log(`usuario = ${cliente_usuario.trim()}`);
                //console.log(`monto = ${String(monto_total)}`);
                //resultado = await cargar(id_cliente_ext, id_cliente_db, nombre, 600 + i, agent_user, agent_pass);
                resultado = await cargar(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), monto_total, agente_usuario, agente_password);
            }
            //console.log(`Resultado carga = ${resultado}`);
            if (resultado == 'ok')
            {
                const query3 = `select * from Confirmar_Notificacion_Carga(${id_notificacion_carga}, ${id_operacion_carga}, ${id_operacion}, ${id_usuario}, ${id_cuenta_bancaria})`;
                await db.handlerSQLDirect(query3);
                //console.log(`Confirmar = ${query3}`);
                //console.log(`Cantidad cargas = ${cantidad_cargas}`);
                if (cantidad_cargas == 0 && referente_id_cliente > 0) {
                    //console.log(`referente_id_cliente = ${referente_id_cliente}`);
                    //console.log(`referente_id_cliente_ext = ${referente_id_cliente_ext}`);
                    //console.log(`referente_id_cliente_db = ${referente_id_cliente_db}`);
                    //console.log(`referente_usuario = ${referente_usuario}`);
                    //console.log(`bono = ${bono}`);
                    let resultadoRef = '';
                    if (id_plataforma == 1 || id_plataforma == 2) {
                        //const carga_manual2 = require('./scrap_bot3/cargar.js');
                        resultadoRef = await cargar(referente_id_cliente_ext, referente_id_cliente_db, referente_usuario.trim(), String(bono), agente_usuario, agente_password);
                        //console.log(resultado);
                    }
                    if (resultadoRef == 'ok')
                    {
                        const query2 = `select * from Cargar_Bono_Referido(${referente_id_cliente}, ${id_usuario}, ${bono}, ${id_cuenta_bancaria}, ${id_operacion})`;
                        //console.log(`Referido = ${query2}`);
                        await db.handlerSQLDirect(query2);
                    } else {
                        await db.insertLogMessage(`Error en la Carga Referente: ${resultadoRef} - [${id_notificacion_carga}, ${id_operacion_carga}]`);
                    }
                }
            } else if (resultado == 'en_espera') {

                //console.log(`En espera...`);
                const cod_elemento_redis = `proc_003_${id_notificacion_carga}`;
                const nuevo_elemento_redis = {
                    id_notificacion_carga : id_notificacion_carga,
                    id_operacion_carga : id_operacion_carga,
                    id_operacion : id_operacion
                };
                await redisClient.set(cod_elemento_redis, JSON.stringify(nuevo_elemento_redis));
                const query4 = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                await db.handlerSQLDirect(query4);

            } else {
                await db.insertLogMessage(`Error en la Carga: ${resultado} - [${id_notificacion_carga}, ${id_operacion_carga}]`);
            }
            return resultado;
        }
    } catch (error) {
        return 'error';
    }
};

async function verificarNotificacionCarga (id_notificacion_carga, id_usuario, id_cuenta_bancaria, redisClient) {
    try {
        const query1 = `select * from Verificar_Notificacion_Carga(${id_notificacion_carga})`;
        //console.log(query1);
        const result1 = await db.handlerSQLDirect(query1);
        if (result1.rows[0].id_operacion_carga > 0) 
        {
            const monto_total = result1.rows[0].monto;
            const bono = result1.rows[0].bono;
            const id_operacion = result1.rows[0].id_operacion;
            let id_cuenta_bancaria_ope = result1.rows[0].id_cuenta_bancaria;
            const id_cliente = result1.rows[0].id_cliente;
            const cliente_usuario = result1.rows[0].usuario;
            const id_cliente_ext = result1.rows[0].id_cliente_ext;
            const id_cliente_db = result1.rows[0].id_cliente_db;
            const agente_usuario = result1.rows[0].agente_usuario;
            const agente_password = result1.rows[0].agente_password;
            const id_operacion_carga = result1.rows[0].id_operacion_carga;
            const cantidad_cargas = result1.rows[0].cantidad_cargas;
            const referente_id_cliente = result1.rows[0].referente_id_cliente;
            const referente_usuario = result1.rows[0].referente_usuario;
            const referente_id_cliente_ext = result1.rows[0].referente_id_cliente_ext;
            const referente_id_cliente_db = result1.rows[0].referente_id_cliente_db;
            const id_plataforma = result1.rows[0].id_plataforma;

            if (id_cuenta_bancaria > 0) {
                id_cuenta_bancaria_ope = id_cuenta_bancaria;
            }

            let resultado = '';
            if (id_plataforma == 1 || id_plataforma == 2) {
                //const carga_manual1 = require('./scrap_bot3/cargar.js');
                resultado = await cargar(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_total), agente_usuario, agente_password);
            }
            //console.log(`Resultado carga = ${resultado}`);
            if (resultado == 'ok') 
            {
                const query3 = `select * from Confirmar_Notificacion_Carga(${id_notificacion_carga}, ${id_operacion_carga}, ${id_operacion}, ${id_usuario}, ${id_cuenta_bancaria_ope})`;
                await db.handlerSQLDirect(query3);
                //console.log(`Confirmar = ${query3}`);
                //console.log(`Cantidad cargas = ${cantidad_cargas}`);
                if (cantidad_cargas == 0 && referente_id_cliente > 0) {
                    //console.log(`referente_id_cliente = ${referente_id_cliente}`);
                    //console.log(`referente_id_cliente_ext = ${referente_id_cliente_ext}`);
                    //console.log(`referente_id_cliente_db = ${referente_id_cliente_db}`);
                    //console.log(`referente_usuario = ${referente_usuario}`);
                    //console.log(`bono = ${bono}`);
                    //resultado = await carga_manual3(id_cliente_ext, id_cliente_db, cliente_usuario.trim(), String(monto_total), agente_usuario, agente_password);
                    resultado = '';
                    if (id_plataforma == 1 || id_plataforma == 2) {
                        //console.log(`id_plataforma = ${id_plataforma}`);
                        //const carga_manual2 = require('./scrap_bot3/cargar.js');
                        resultado = await cargar(referente_id_cliente_ext, referente_id_cliente_db, referente_usuario.trim(), String(bono), agente_usuario, agente_password);
                        //console.log(resultado);
                    }
                    if (resultado == 'ok')
                    {
                        const query2 = `select * from Cargar_Bono_Referido(${referente_id_cliente}, ${id_usuario}, ${bono}, ${id_cuenta_bancaria_ope}, ${id_operacion})`;
                        //console.log(`Referido = ${query2}`);
                        await db.handlerSQLDirect(query2);
                    } else {
                        await db.insertLogMessage(`Error en la Carga Referente: ${resultado} - [${id_notificacion_carga}, ${id_operacion_carga}]`);
                    }
                }
            } else if (resultado == 'en_espera') {

                const cod_elemento_redis = `proc_002_${id_notificacion_carga}`;
                const nuevo_elemento_redis = {
                    id_notificacion_carga : id_notificacion_carga,
                    id_operacion_carga : id_operacion_carga,
                    id_cuenta_bancaria : id_cuenta_bancaria,
                    id_usuario : id_usuario
                };
                await redisClient.set(cod_elemento_redis, JSON.stringify(nuevo_elemento_redis));
                const query4 = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                await db.handlerSQLDirect(query4);

            } else {
                await db.insertLogMessage(`Error en la Carga: ${resultado} - [${id_notificacion_carga}, ${id_operacion_carga}]`);
            }
            return resultado;
        }
    } catch (error) {
        return 'error';
    }
};

const intervalId_01 = setInterval(anular_Notificaciones_Landing, esperaRegistro * 1000); // (30000 ms = 30 segundos)

const intervalId_02 = setInterval(registrar_Concurrencia, esperaRegistro * 1000); // (30000 ms = 30 segundos)

const intervalId_03 = setInterval(verificar_Transferencias, esperaTransferencias * 1000); // (30000 ms = 30 segundos)

const intervalId_04 = setInterval(destrabar_Solicitudes, esperaEnProceso * 1000);
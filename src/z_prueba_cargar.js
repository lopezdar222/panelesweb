const path = require('path');
const db = require(__dirname + '/db');
let tiempoEspera = 0;

const cargar = require('./scrap_bot3/cargar.js');

async function probarEndpoint(mensaje, i) {
    try {
        const id_cliente_ext = 5004001269;
        const id_cliente_db = 9;
        const nombre = 'dario010';
        const agent_user = 'botcasino365';
        const agent_pass = 'hola123';
        let resultado = '';
        console.log(mensaje);
        while (resultado != 'ok') {
            console.log(`Proceso${i} - Iniciando Carga`);
            resultado = await cargar(id_cliente_ext, id_cliente_db, nombre, 600 + i, agent_user, agent_pass);
            console.log(`Proceso${i} - Resultado: ${resultado}`);
        }
	} catch (error) {
        console.error('Error al ejecutar crear:', error);
    }
}

function numeroEnteroAleatorioEntre(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function ejecutarPrueba(mensaje, tiempoEspera, i) {
    setTimeout(() => {
        probarEndpoint(mensaje, i);
    }, tiempoEspera);
}

i = 0;
while (i < 20) {
    tiempoEspera = tiempoEspera + numeroEnteroAleatorioEntre(0, 750);
    ejecutarPrueba(`Lanzamiento de Proceso a los ${tiempoEspera / 1000} segundos!`, tiempoEspera, i);
    i++;
}
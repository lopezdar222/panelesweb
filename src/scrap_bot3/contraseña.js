const axios = require('axios');

async function contraseña(id_cliente_ext, id_cliente_db, nombre, contra, agent_user, agent_pass ) {
    try {
        //console.log('Cambio contraseña...');
        const url = `https://wallet-uat.emaraplay.com/bot/user/password`;
        // Datos que deseas enviar en la petición POST
        const data = {
            agent : {
                username : agent_user,
                password : agent_pass
            },
            /*user : {
                username : nombre,
                newPassword : contra
            },*/
            user : {
                id : id_cliente_ext,
                db : id_cliente_db,
                newPassword : contra
            }
        };

        // Configuración de la petición
        const config = {
          method: 'post',
          url: url,
          data: data
        };
        //console.log(data);
        // Realizar la petición POST
        const response = await axios(config);
        //console.log(response.data);
        if (response.data.error == true) {
            return 'error';
        } else {
            return 'ok';
        }
    } catch (error) {
        console.log(`----------\nSucedio un error en la creación del usuario ${nombre}.\n${error}\n----------\n`);
        return 'error';
    }
};

module.exports = contraseña;
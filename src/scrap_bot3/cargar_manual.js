const axios = require('axios');
const { createClient } = require('@redis/client');

async function cargar(id_cliente_ext, id_cliente_db, nombre, monto, agent_user, agent_pass) {
    try {
        const redisClient = createClient({
            //url: 'redis://localhost:6379'
            //url: 'redis://192.168.200.76:6379',
            url: 'redis://200.58.106.199:6379',
            password: 'paneles0110'
        });
        await redisClient.connect();
        const cod_elemento = `proc_001`;
        let reply = '';
        let estado = '';
        reply = await redisClient.get(cod_elemento);
        estado = JSON.parse(reply);
        estado.activos++;
        await redisClient.set(cod_elemento, JSON.stringify(estado));
        await redisClient.disconnect();
        
        const url = `https://wallet-uat.emaraplay.com/bot/user/deposit`;
        // Datos que deseas enviar en la petición POST
        const data = {
            agent : {
                username : agent_user,
                password : agent_pass
            },
            user : {
                id : id_cliente_ext,
                db : id_cliente_db
            },
            /*user : {
                username : nombre
            },*/
            amount : monto
        };
        console.log(data);
        
        // Configuración de la petición
        const config = {
          method: 'post',
          url: url,
          data: data
        };

        // Realizar la petición POST
        const response = await axios(config);

        console.log(response);
        await redisClient.connect();
        reply = await redisClient.get(cod_elemento);
        estado = JSON.parse(reply);
        estado.activos--;
        await redisClient.set(cod_elemento, JSON.stringify(estado));
        await redisClient.disconnect();
        //console.log('Finalizando...');
        //console.log(estado);

        if (response.data.error == false) {
            return 'ok';
        } else {
            return 'error';
        }
    } catch (error) {
        return 'error';
    }
};

module.exports = cargar;
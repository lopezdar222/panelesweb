const axios = require('axios');
const { createClient } = require('@redis/client');

async function cargar(id_cliente_ext, id_cliente_db, nombre, monto, agent_user, agent_pass) {
    try {
        const redisClient = createClient({
            //url: 'redis://localhost:6379'
            //url: 'redis://192.168.200.76:6379',
            url: 'redis://200.58.106.199:6379',
            password: 'paneles0110'
        });
        const cod_elemento = `proc_001`;
        let reply = '';
        let estado = '';
        await redisClient.connect();
        /*const espera_concurrencia = 1000;
        const ciclos_espera_tope = 5;
        let ciclos_espera = 0;
        let concurrentes = true;
        while (concurrentes) {
            reply = await redisClient.get(cod_elemento);
            estado = JSON.parse(reply);
            //console.log('Comenzando...');
            //console.log(estado);
            if (estado.activos + 1 > estado.tope) {
                if (ciclos_espera == 0) {
                    reply = await redisClient.get(cod_elemento);
                    estado = JSON.parse(reply);
                    estado.en_espera++;
                    await redisClient.set(cod_elemento, JSON.stringify(estado));
                }
            } else {
                reply = await redisClient.get(cod_elemento);
                estado = JSON.parse(reply);
                estado.activos++;
                await redisClient.set(cod_elemento, JSON.stringify(estado));
                concurrentes = false;
            }
            if (concurrentes && ciclos_espera == ciclos_espera_tope) {
                reply = await redisClient.get(cod_elemento);
                estado = JSON.parse(reply);
                estado.en_espera--;
                await redisClient.set(cod_elemento, JSON.stringify(estado));
                await redisClient.disconnect();
                return 'en_espera';
            } else if (concurrentes) {
                ciclos_espera++;
                await new Promise(res => setTimeout(res, espera_concurrencia));
            }
            //reply = await redisClient.get(cod_elemento);
            //estado = JSON.parse(reply);
            //console.log('En Proceso...');
            //console.log(`Iteración: ${ciclos_espera} - Concurrentes: ${estado.en_espera}`);
            //console.log(estado);
        }
        if (ciclos_espera > 0) {
            reply = await redisClient.get(cod_elemento);
            estado = JSON.parse(reply);
            estado.en_espera--;
            await redisClient.set(cod_elemento, JSON.stringify(estado));
        }
        await redisClient.disconnect();*/
        
        reply = await redisClient.get(cod_elemento);
        estado = JSON.parse(reply);
        estado.activos++;
        await redisClient.set(cod_elemento, JSON.stringify(estado));
        await redisClient.disconnect();

        const url = `https://wallet-uat.emaraplay.com/bot/user/deposit`;
        // Datos que deseas enviar en la petición POST
        const data = {
            agent : {
                username : agent_user,
                password : agent_pass
            },
            user : {
                id : id_cliente_ext,
                db : id_cliente_db
            },
            /*user : {
                username : nombre
            },*/
            amount : monto
        };
        
        // Configuración de la petición
        const config = {
          method: 'post',
          url: url,
          data: data
        };

        // Realizar la petición POST
        const response = await axios(config);

        //console.log(response);
        await redisClient.connect();
        reply = await redisClient.get(cod_elemento);
        estado = JSON.parse(reply);
        estado.activos--;
        await redisClient.set(cod_elemento, JSON.stringify(estado));
        await redisClient.disconnect();
        //console.log('Finalizando...');
        //console.log(estado);

        if (response.data.error == false) {
            return 'ok';
        } else {
            return 'error';
        }
    } catch (error) {
        return 'error';
    }
};

module.exports = cargar;
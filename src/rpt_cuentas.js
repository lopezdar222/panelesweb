const path = require('path');
const db = require(__dirname + '/db');
const axios = require('axios');

async function rpt_Cuentas() {
    try {
        let query = `select Reporte_Cuentas_Bancarias_Actualiza()`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

rpt_Cuentas();
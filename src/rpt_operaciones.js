const path = require('path');
const db = require(__dirname + '/db');
const { format, subDays } = require('date-fns');

async function rpt_Operaciones() {
    try {
        //console.log('Ejecutando rpt_Operaciones...');
        let fecha_proceso = format(Date(), 'yyyy-MM-dd');
        let query = `select Insertar_Rpt_Operaciones('${fecha_proceso}')`;
        //console.log(query);
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function rpt_Operaciones_Diario() {
    try {
        //console.log('Ejecutando rpt_Operaciones_Diario...');
        let query = `select Mantenimiento_Clientes_Operaciones_Diario()`;
        //console.log(query);
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function main() {
    await rpt_Operaciones();
    await rpt_Operaciones_Diario();
}

main().catch(err => {
    console.error('Error al ejecutar rpt_operaciones:', err);
});
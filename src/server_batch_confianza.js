const path = require('path');
const db = require(__dirname + '/db');
const https = require('https');
const fs = require('fs');
//const { createClient } = require('@redis/client');
let verificando = 0;
const espera = 5;
const aplica_confianza = 95;
const aplica_cantidad = 100;
const aplica_monto = 50000;
const carga_manual3 = require('./scrap_bot3/cargar_manual.js');
///////////////////////////////////////////////////////////////////////////////
db.pruebaConexion();

async function verificar_Solicitudes() {
    if (verificando == 0)
    {
        verificando = 1;
        try {
            const query = `select cliente_usuario,` +
                        `id_operacion,` +
                        `carga_id_cuenta_bancaria,` +
                        `carga_importe,` +
                        `carga_bono ` +
                `from Clientes_Operaciones_Operador(1, 1, 1, 1)` +
                `where id_estado = 1 ` + 
                `and fecha_hora_operacion < NOW() - INTERVAL '5 minutes' ` + 
                `and id_oficina = 2 ` + 
                `and id_accion = 1 ` +  
                `and carga_importe <= ${aplica_monto} ` + 
                `and cliente_confianza >= ${aplica_confianza} ` + 
                `and totales >= ${aplica_cantidad} ` + 
                `and id_cliente not in (select op.id_cliente  ` + 
                `from operacion op join operacion_carga oc  ` + 
                `on (op.id_operacion = oc.id_operacion  ` + 
                `and op.id_estado = 2 ` + 
                `and op.fecha_hora_ultima_modificacion >= NOW() - INTERVAL '1 hours' ` + 
                `and op.id_usuario_ultima_modificacion = 1)) ` + 
                `order by fecha_hora_operacion limit 1;`;
            
            const result = await db.handlerSQLRead(query);

            if (result.rows.length == 1) {
                const id_operacion = Number(result.rows[0].id_operacion);
                const monto_carga = Number(result.rows[0].carga_importe);
                const monto_bono = Number(result.rows[0].carga_bono);
                const monto_total = monto_carga + monto_bono;
                const id_cuenta_bancaria = Number(result.rows[0].carga_id_cuenta_bancaria);
                //console.log(`Carga a: ${result.rows[0].cliente_usuario}, Monto: ${monto_total}`);

                const query2 = `select id_operacion_carga,` +
                                `procesando,` +
                                `id_cliente,` +
                                `cliente_usuario,` +
                                `id_cliente_ext,` +
                                `id_cliente_db,` +
                                `id_agente,` +
                                `agente_usuario,` +
                                `agente_password,` +
                                `id_plataforma ` +
                `from Verificar_Operacion_Carga_Manual(${id_operacion});`;
                const result2 = await db.handlerSQL(query2);
                
                const id_operacion_carga = result2.rows[0].id_operacion_carga;
                const procesando = String(result2.rows[0].procesando).trim();
                const cliente_usuario = result2.rows[0].cliente_usuario.trim();
                const id_cliente_ext = result2.rows[0].id_cliente_ext;
                const id_cliente_db = result2.rows[0].id_cliente_db;
                const agente_nombre = result2.rows[0].agente_usuario;
                const agente_password = result2.rows[0].agente_password;
                const id_plataforma = result2.rows[0].id_plataforma

                /*console.log(`Operacion = ${id_operacion}`);
                console.log(`Procesando = ${procesando}`);
                console.log(`Agente Nombre = ${agente_nombre}`);
                console.log(`Agente Password = ${agente_password}`);
                console.log(`Cliente Usuario = ${cliente_usuario}`);
                console.log(`Cliente id_cliente_ext = ${id_cliente_ext}`);
                console.log(`Cliente id_cliente_db = ${id_cliente_db}`);
                console.log(`Monto = ${String(monto_total)}`);
                console.log(`ID Cuenta Bancaria = ${id_cuenta_bancaria}`);
                console.log(`Plataforma = ${id_plataforma}`);*/

                let resultado = '';
                let query3 = '';
                if (procesando == 'false') {
                    if (id_plataforma == 1 || id_plataforma == 2) {
                        resultado = await carga_manual3(id_cliente_ext, id_cliente_db, cliente_usuario, String(monto_total), agente_nombre, agente_password);
                    }
                    //console.log(`Resultado carga = ${resultado}`);
                    if (resultado == 'ok') 
                    {
                        query3 = `select * from Modificar_Cliente_Carga(${id_operacion}, 2, ${monto_carga}, ${monto_bono}, ${id_cuenta_bancaria}, 1)`;
                        await db.handlerSQL(query3);
                        await db.insertLogMessage(`Aplicación Confianza Exitosa : ${id_operacion}`);
                        //console.log(`Aplicación Confianza Exitosa : ${id_operacion}`);
                    } else if (resultado == 'error') {
                        query3 = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                        await db.handlerSQL(query3);
                        await db.insertLogMessage(`Aplicación Confianza Con Error : ${id_operacion}`);
                        //console.log(`Aplicación Confianza Con Error : ${id_operacion}`);
                    } else if (resultado == 'en_espera') {
                        query3 = `update operacion_carga set procesando = false where id_operacion_carga = ${id_operacion_carga};`;
                        await db.handlerSQL(query3);
                        await db.insertLogMessage(`Aplicación Confianza En Espera : ${id_operacion}`);
                        //console.log(`Aplicación Confianza En Espera : ${id_operacion}`);
                    }
                } else {
                    await db.insertLogMessage(`Aplicación Confianza Procesando : ${id_operacion} (${procesando})`);
                    //console.log(`Aplicación Confianza Procesando : ${id_operacion} (${procesando})`);
                }
            }
            verificando = 0;
        } catch (error) {
            verificando = 0;
            //console.error('Error al Verificar Transferencias', error);
            throw error;
        }
    }
};
const intervalId_01 = setInterval(verificar_Solicitudes, espera * 1000); // (30000 ms = 30 segundos)

/*async function main() {
    while (true) {
        verificando_transferencias = 0;
        await verificar_Transferencias();
        await new Promise(resolve => setTimeout(resolve, esperaTransferencias * 1000));
    }
}

main();*/
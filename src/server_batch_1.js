const path = require('path');
const db = require(__dirname + '/db');
const https = require('https');
const fs = require('fs');
const { createClient } = require('@redis/client');
let anulando_notificaciones = 0;
let registrando_concurrentes = 0;
let registrando_concurrentes_2 = 0;
let destrabando_solicitudes = 0;
const esperaRegistro = 30;
const esperaAnulacion = 60;
const esperaEnProceso = 120;
let iteracionesActivo = 0;
//url: 'redis://192.168.200.76:6379', //ip lan
//url: 'redis://200.58.106.199:6379', //ip publica
const urlRedis = 'redis://192.168.200.76:6379';
const passRedis = 'paneles0110';
///////////////////////////////////////////////////////////////////////////////
db.pruebaConexion();

async function anular_Notificaciones_Landing() {
    if (anulando_notificaciones == 0)
    {
        //console.log('Procesa Anulacion');
        anulando_notificaciones = 1;
        try {
            let query = `SELECT Registrar_Anulacion_Carga_Masiva();`;
            await db.handlerSQL(query);

            anulando_notificaciones = 0;
            if (global.gc) {
                global.gc();
            }
        } catch (error) {
            anulando_notificaciones = 0;
            //console.error('Error al Anular Notificaciones', error);
            throw error;
        }
    }
};

async function destrabar_Solicitudes() {
    if (destrabando_solicitudes == 0)
    {
        destrabando_solicitudes = 1;
        try {
            //console.log('Destraba Solicitud');
            let query = `UPDATE operacion_carga AS opc ` +
                            `SET procesando = false ` +
                            `FROM operacion op ` +
                            `WHERE (opc.id_operacion = op.id_operacion ` +
                            `AND op.fecha_hora_creacion < NOW() - INTERVAL '2 minutes' ` +
                            `AND op.id_estado = 1)`;
            //console.log(query);
            await db.handlerSQL(query);

            //console.log('Destraba Notificación');
            query = `UPDATE notificacion_carga AS nc ` +
            `SET procesando = false ` +
            `FROM notificacion n ` +
            `WHERE (nc.id_notificacion = n.id_notificacion ` +
            `AND n.fecha_hora < NOW() - INTERVAL '10 minutes' ` +
            `AND n.anulada = false ` +
            `AND nc.marca_procesado = false ` +
            `AND nc.procesando = true)`;
            //console.log(query);
            await db.handlerSQL(query);
            destrabando_solicitudes = 0;
        } catch (error) {
            destrabando_solicitudes = 0;
            throw error;
        }
    }
};

async function registrar_Concurrencia() {
    if (registrando_concurrentes == 0)
    {
        //console.log('Registra Concurrencia');
        registrando_concurrentes = 1;
        try {
            const redisClient = createClient({
                url : urlRedis,
                password: passRedis
            });
			await redisClient.connect();
			let res = '';
            let cod_elemento = `proc_001`;
            let reply = await redisClient.get(cod_elemento);
            let data = JSON.parse(reply);
            
            let query = `INSERT INTO concurrencia_registro (fecha_hora, id_proceso, activos, en_espera, tope) ` +
                            `VALUES (Now(), ${data.id_proceso}, ${data.activos}, ${data.en_espera}, ${data.tope});`;
            //console.log(query);
            await db.handlerSQL(query);

            if(data.activos < 0) {
                data.activos = 0;
                data.en_espera = 0;
                await redisClient.set(cod_elemento, JSON.stringify(data));
            }
            if(data.activos >= data.tope) {
                iteracionesActivo++;
            } else {
                iteracionesActivo=0;
            }
            if(2 < iteracionesActivo) {
                reply = await redisClient.get(cod_elemento);
                data = JSON.parse(reply);
                data.activos = 0;
                await redisClient.set(cod_elemento, JSON.stringify(data));
                iteracionesActivo=0;
            }

            let keys = await redisClient.keys('apli*');
            if (keys.length > 0) {
                for (const key of keys) {
                    reply = await redisClient.get(key);
                    data = JSON.parse(reply);
                    await redisClient.del(key);
                    if ('resultado' in data) {
                        res = data.resultado;
                    } else {
                        res = 'error';
                    }
                    let query = `INSERT INTO concurrencia_registro_aplicadas (fecha_hora, id_proceso, id_notificacion_carga, resultado) ` +
                                `VALUES (Now(), 1, ${data.id_notificacion_carga}, '${res}');`;
                    //console.log(query);
                    await db.handlerSQL(query);
                }
            }
            
            await redisClient.disconnect();
            registrando_concurrentes = 0;
        } catch (error) {
            registrando_concurrentes = 0;
            //console.error('Error al Registrar Concurrencia', error);
            throw error;
        }
    }
};

async function registrar_Concurrencia_2() {
    if (registrando_concurrentes_2 == 0)
    {
        //console.log('Registra Concurrencia');
        registrando_concurrentes_2 = 1;
        try {
            const redisClient = createClient({
                url : urlRedis,
                password: passRedis
            });
			await redisClient.connect();
			let res = '';
            let cod_elemento = `proc_002`;
            let reply = await redisClient.get(cod_elemento);
            let data = JSON.parse(reply);
            
            let query = `INSERT INTO concurrencia_registro (fecha_hora, id_proceso, activos, en_espera, tope) ` +
                            `VALUES (Now(), ${data.id_proceso}, ${data.activos}, ${data.en_espera}, ${data.tope});`;
            //console.log(query);
            await db.handlerSQL(query);


            if(data.activos < 0) {
                data.activos = 0;
                data.en_espera = 0;
                await redisClient.set(cod_elemento, JSON.stringify(data));
            }
            /*if(data.activos >= data.tope) {
                iteracionesActivo++;
            } else {
                iteracionesActivo=0;
            }
            if(2 < iteracionesActivo) {
                reply = await redisClient.get(cod_elemento);
                data = JSON.parse(reply);
                data.activos = 0;
                await redisClient.set(cod_elemento, JSON.stringify(data));
                iteracionesActivo=0;
            }*/
            await redisClient.disconnect();
            registrando_concurrentes_2 = 0;
        } catch (error) {
            registrando_concurrentes_2 = 0;
            //console.error('Error al Registrar Concurrencia', error);
            throw error;
        }
    }
};

const intervalId_01 = setInterval(anular_Notificaciones_Landing, esperaAnulacion * 1000); // (30000 ms = 30 segundos)

const intervalId_02 = setInterval(registrar_Concurrencia, esperaRegistro * 1000); // (30000 ms = 30 segundos)

const intervalId_03 = setInterval(registrar_Concurrencia_2, esperaRegistro * 1000); // (30000 ms = 30 segundos)

const intervalId_04 = setInterval(destrabar_Solicitudes, esperaEnProceso * 1000);
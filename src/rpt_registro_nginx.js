const path = require('path');
const db = require(__dirname + '/db');
const fs = require('fs');

async function rpt_Registro_Nginx(filePath) {
    try {
        // Leer el archivo línea por línea
        const data = fs.readFileSync(filePath, 'utf8');
        const lines = data.split('\n');
        let query = '';

        for (let line of lines) {
            if (line.trim() === '') continue; // Omitir líneas vacías

            const [timestamp, valueStr] = line.split(' - ');
            const value = parseInt(valueStr, 10);
            query = `Insert into rpt_peticiones_nginx (id_servidor, fecha_hora, peticiones) values (4, '${timestamp}', ${value})`;
            await db.handlerSQL(query);
            console.log(query);
        }
    }
    catch (error) {
        console.log('Error', error);
    }
};

//rpt_Registro_Nginx('nginx.log');
rpt_Registro_Nginx('/home/dario/panelesweb/nginx.log');
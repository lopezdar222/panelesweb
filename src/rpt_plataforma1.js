const path = require('path');
const db = require(__dirname + '/db');
const axios = require('axios');
const { format, subDays } = require('date-fns');

function getYesterdayDate() {
    const yesterday = subDays(new Date(), 1);
    return format(yesterday, 'yyyy-MM-dd');
}

async function rpt_Plataforma_1() {
    try {
        const fecha_proceso = getYesterdayDate();
        let query = '';
        query = `Delete from rpt_plataforma_club where id_plataforma = 1 and fecha = '${fecha_proceso}'`;
        await db.handlerSQL(query);

        const url = `https://wallet-uat.emaraplay.com/bot/report/txs`;
        // Datos que deseas enviar en la petición POST
        //Roby	50092934 
        //guayaquilisla1	10164951
        //chajamarisla1	10301940
        //venezuelaplata 10295322
        const masters = [10111963, 50092934, 10164951, 10301940, 10295322, 10696534, 50478444, 10129829];
        for (let i = 0; i < masters.length; i++) {
            let postData = {
                agent:{
                    username:"takitaki",
                    password:"Juanambar18."
                },
                report:{
                    master : masters[i],
                    date : String(fecha_proceso)
                }
            };
            //console.log(`Procesando maestro ${masters[i]}`);
            let response = await axios.post(url, postData);
            let datos = response.data;

            //console.log(`Datos ${datos}`);
            // Procesar e insertar los datos en PostgreSQL
            for (const record of datos) {
                query = `SELECT Insertar_Rpt_Plataforma_Club(1,` +
                                                            `'${fecha_proceso}', ` +
                                                            `'${record.alias}', ` +
                                                            `${record.summary.total.operaciones}, ` +
                                                            `${record.summary.total.cantidad}, ` +
                                                            `${record.summary.cargas.operaciones}, ` +
                                                            `${record.summary.cargas.cantidad}, ` +
                                                            `${record.summary.descargas.operaciones}, ` +
                                                            `${record.summary.descargas.cantidad}); `;
                //console.log(query);
                await db.handlerSQL(query);
            }
        }
        query = `SELECT Insertar_Rpt_Plataforma_Club_Totales(1, '${fecha_proceso}'); `;
        //console.log(query);
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

rpt_Plataforma_1();
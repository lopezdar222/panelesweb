const path = require('path');
const db = require(__dirname + '/db');
const axios = require('axios');
const { format, subDays } = require('date-fns');

function getYesterdayDate() {
    const yesterday = subDays(new Date(), 1);
    return format(yesterday, 'yyyy-MM-dd');
}

async function mante_Cargas() {
    try {
        const fecha_proceso = getYesterdayDate();
        const query = `Select Mantenimiento_Operaciones_Cargas('${fecha_proceso}')`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function mante_Retiros() {
    try {
        const fecha_proceso = getYesterdayDate();
        const query = `Select Mantenimiento_Operaciones_Retiros('${fecha_proceso}')`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function mante_Resto() {
    try {
        const fecha_proceso = getYesterdayDate();
        const query = `Select Mantenimiento_Operaciones_Resto('${fecha_proceso}')`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function mante_Notificaciones() {
    try {
        const fecha_proceso = getYesterdayDate();
        const query = `Select Mantenimiento_Notificaciones('${fecha_proceso}')`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function mante_Clientes_Operaciones_Hist() {
    try {
        const query = `Select Mantenimiento_Clientes_Operaciones_Hist()`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function mante_Clientes_Sesiones_Hist() {
    try {
        const fecha_proceso = getYesterdayDate();
        const query = `Select Mantenimiento_Cliente_Sesion('${fecha_proceso}')`;
        await db.handlerSQL(query);
    }
    catch (error) {
        console.log('Error', error);
    }
};

async function main() {
    await mante_Cargas();
    await mante_Retiros();
    await mante_Resto();
    await mante_Notificaciones();
    await mante_Clientes_Operaciones_Hist();
    await mante_Clientes_Sesiones_Hist();
}

main().catch(err => {
    console.error('Error al ejecutar mantenimiento_tablas:', err);
});

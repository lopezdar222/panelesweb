const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const bcrypt = require('bcrypt');
const db = require(__dirname + '/db');
const ejs = require('ejs');
const axios = require('axios');
const { Console } = require('console');
///////////////////////////////////////////////////////////////////////////////
const fs = require('fs'); 
const qrcode = require('qrcode');
const { Client, LocalAuth } = require('whatsapp-web.js');
///////////////////////////////////////////////////////////////////////////////
const app = express();
const PORT = process.env.PORT || 3001;
///////////////////////////////////////////////////////////////////////////////
db.pruebaConexion();
//dbMongo.pruebaConexion();
app.listen(PORT, () => {
    db.insertLogMessage(`Servidor en funcionamiento en el puerto ${PORT}`);
});
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());
app.use(express.json()); // Para analizar JSON
app.use(express.urlencoded({ extended: true })); // Para analizar datos de formularios URL-encoded
const ejecucion_servidor = true;
let pahtChromium = '';
if (ejecucion_servidor) {
    pahtChromium = '/usr/bin/chromium-browser';
} else {
    pahtChromium = 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe';
}
const esperaRecupero = 60;
let procesandoEsperaRecupero = 0;
const ejecucion_servidor_numero = 1;
const clientsArray = {};
const clientsArraySesion = {};
/*API MP sin funcionamiento:*/
// Configuración de sesiones
app.use(session({
    secret: 'tu-secreto-seguro',
    resave: false,
    saveUninitialized: true
}));

// Rutas
app.get('/', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

//Rutas Whatsapp
app.post('/add-session/:id_oficina/:id_usuario', async (req, res) => {
    try {
        const { id_oficina, id_usuario } = req.params;

        let claves = Object.keys(clientsArray);
        if (claves.includes(id_oficina)) {
            res.status(409).json({ message: 'Oficina en Recupero' });
            return;
        }

        clientsArray[id_oficina] = new Client({
            puppeteer: { headless: true, 
                        args: ['--no-sandbox', '--disable-setuid-sandbox'],
                        executablePath: pahtChromium}
            //,authStrategy: new LocalAuth({ clientId: id_oficina })
        });

        clientsArray[id_oficina].on('qr', (qrCodeData) => {
            //console.log(`Evento .on con QR de qr_${phoneNumber}.png`);
            qrcode.toDataURL(qrCodeData, (err, url) => {
                if (err) {
                    //db.insertLogMessage(`qr Code: Error!`);
                    res.status(500).send('Error al generar el QR');
                    return;
                } else {
                    // Convertir la URL de datos a una imagen y guardarla
                    const imageData = url.split(';base64,').pop();   
                    let qrImagePath = '';
                    if (ejecucion_servidor) {
                        qrImagePath = __dirname.replace('src', 'public/qr') + `/qr_${id_oficina}.png`;
                    } else {
                        qrImagePath = __dirname.replace('src', 'public\\qr') + `\\qr_${id_oficina}.png`;
                    }
                    const qrImageName = `qr_${id_oficina}.png`;
                    try {
                        fs.promises.writeFile(qrImagePath, imageData, { encoding: 'base64' });
                        //console.log(`Código QR guardado en ${qrImagePath}`);
                        res.status(200).json({ nombreImagen: qrImageName });
                    } catch (error) {
                        res.status(500).send('Error al guardar el QR como imagen');
                        console.log(`Error al guardar el QR como imagen en ${qrImagePath}`);
                    }
                    return;
                }
            });
        });

        clientsArray[id_oficina].on('authenticated', () => {
            db.insertLogMessage(`Cliente ${id_oficina} autenticado`);
            //console.log(`Cliente ${id_oficina} autenticado`);
        });

        clientsArray[id_oficina].on('ready', async () => {
            const me = await clientsArray[id_oficina].info;
            const numeroTelefono = me.wid.user;
            //db.insertLogMessage(`Cliente ${id_oficina} listo. Numero: ${numeroTelefono}`);
            //console.log(`Cliente ${id_oficina} listo. Numero: ${numeroTelefono}. Usuario: ${id_usuario}`);
            const query = `select id_sesion_recupero_whatsapp from Insertar_Sesion_Recupero_Whatsapp(${id_usuario}, ${id_oficina}, '${numeroTelefono}')`;
            const result = await db.handlerSQL(query);
            clientsArraySesion[id_oficina]=result.rows[0].id_sesion_recupero_whatsapp;
        });

        clientsArray[id_oficina].initialize();

        /*clientsArray[id_oficina].on('disconnected', (reason) => {
            console.log(`Cliente ${id_oficina} desconectado`, reason);
        });*/

        // Manejador para los mensajes entrantes (se ejecuta de forma asíncrona)
        clientsArray[id_oficina].on('message', async (message) => {
            // Guardar el mensaje en la base de datos
            db.insertLogMessage(`[${message.to}] - Mensaje recibido de ${message.from}: ${message.body}`);
            //console.log(`[${message.to}] - Mensaje recibido de ${message.from}: ${message.body}`);
        });

    } catch (error) {
        db.insertLogMessage(`Error starting session: ${error}`);
        res.status(500).send(`Error starting session: ${error}`);
    }
});

app.post('/end-session/:id_oficina', async (req, res) => {
    try {
        const { id_oficina } = req.params;

        //console.log('llegó!');

        let claves = Object.keys(clientsArray);
        if (!claves.includes(id_oficina)) {
            res.status(200).json({ message: 'Oficina Libre' });
            return;
        }

        //console.log('llegó 2!');

        clientsArray[id_oficina].destroy();
        delete clientsArray[id_oficina];
        const id_sesion_recupero = clientsArraySesion[id_oficina];
        delete clientsArraySesion[id_oficina];
        
        //console.log('llegó 3!');

        const query = `select Finalizar_Sesion_Recupero_Whatsapp(${id_sesion_recupero})`;
        await db.handlerSQL(query);
        res.status(200).json({ message: 'Sesión Finalizada' });
        return;
    } catch (error) {
        db.insertLogMessage(`Error ending session: ${error}`);
        res.status(500).send(`Error ending session: ${error}`);
    }
});

// Proceso automático de Recuperos:
async function enviar_Mensaje_Recupero() {
    //console.log('Entra en Proceso Recupero');
    let cliente_mensaje = '';
    let mensajeConBoton = '';
    let randomIndex = 0;
    let numero_telefono = '';
    let id_contactos_recupero = 0;
    let id_token = '';
    let query = '';
    let result = '';
    const saludos = ["Hola!!", "Buenas!!", "¿Cómo estás?!", "¿Cómo andás?!", "Qué tal!!", "Buenas Noches!!", "Hermosa Noche!!"];
    if (procesandoEsperaRecupero == 0)
    {
        try {
            procesandoEsperaRecupero = 1;
            for (let id_oficina in clientsArraySesion) {
                //console.log(`Oficina ${id_oficina}...:`);

                query = `select cr.id_contactos_recupero, ` +
                                `cr.numero_telefono, ` +
                                `rt.id_token ` +
                `from contactos_recupero cr join registro_token rt on (cr.id_registro_token = rt.id_registro_token) ` +
                `where cr.id_oficina = ${id_oficina} and cr.id_estado = 1 order by 1 limit 1`;
                result = await db.handlerSQLRead(query);
                //console.log(query);

                id_contactos_recupero = result.rows[0].id_contactos_recupero;
                numero_telefono = result.rows[0].numero_telefono + '@c.us';
                id_token = result.rows[0].id_token;

                randomIndex = Math.floor(Math.random() * saludos.length);
                
                cliente_mensaje = saludos[randomIndex] + ` ¡Te hablamos de 365! Bloquearon nuestros WhatsApp.`;
                cliente_mensaje = cliente_mensaje + `\n🤖 Te invitamos a probar nuestra Automatización 🤖`;    
                cliente_mensaje = cliente_mensaje + `\n🤩 Contás con un REGALO de $3.000.- Respondé que Si, si querés obtenerlo 🤩`;                      
                cliente_mensaje = cliente_mensaje + `\n😁 Podés Registrarte en este enlace \nhttps://red365club.com/html/registro.html?token=${id_token}`;

                /*cliente_mensaje = saludos[randomIndex] + ` 🤖¡Recordá que estamos cargando como siempre las 24hs con pagos `;
                cliente_mensaje = cliente_mensaje + `en el acto!!\n🤩 Y además, contás con un BONO de $3.000.- sobre tu carga 🤩`;                                
                cliente_mensaje = cliente_mensaje + `\n😁 Podés Registrarte en este enlace\n https://red365club.com/html/registro.html?token=${id_token}`;*/

                await clientsArray[id_oficina].sendMessage(numero_telefono, cliente_mensaje);
                //console.log(`Numero ${numero_telefono} - Mensaje: ${cliente_mensaje}`);

                /*await clientsArray[id_oficina].sendMessage(numero_telefono, `*********************************`);

                mensajeConBoton = {
                    text: `😁 Podés Registrarte en este enlace`,
                    buttons: [
                      {
                        type: 'urlButton',
                        url: `https://red365club.com/html/registro.html?token=${id_token}`,
                        title: 'Registrarme'
                      }
                    ]
                };

                await clientsArray[id_oficina].sendMessage(numero_telefono, mensajeConBoton);
                await clientsArray[id_oficina].sendMessage(numero_telefono, `*********************************`);

                cliente_mensaje = saludos[randomIndex] + ` 🤖¡Recordá que estamos cargando como siempre las 24hs con pagos `;
                cliente_mensaje = cliente_mensaje + `en el acto!!\n🤩 Y además, contás con un BONO de $3.000.- sobre tu carga 🤩`;                                
                cliente_mensaje = cliente_mensaje + `\n<a href="https://red365club.com/html/registro.html?token=${id_token}">😁 Podés Registrarte en este enlace</a>`;
                await clientsArray[id_oficina].sendMessage(numero_telefono, cliente_mensaje, {parse_mode: 'Markdown'});
                await clientsArray[id_oficina].sendMessage(numero_telefono, `*********************************`);*/

                query = `update contactos_recupero ` +
                        `set fecha_hora_enviado = now(), id_estado = 2 ` +
                        `where id_contactos_recupero = ${id_contactos_recupero}`;
                await db.handlerSQL(query);        
                //console.log(query);
            }
            procesandoEsperaRecupero = 0;
        } catch (error) {
            procesandoEsperaRecupero = 0;
            throw error;
        }
    }
};

const intervalId_01 = setInterval(enviar_Mensaje_Recupero, esperaRecupero * 1000); // (30000 ms = 30 segundos)

async function colectorMem() {
    if (global.gc) {
        global.gc();
    }
};

const intervalId_02 = setInterval(colectorMem, 60 * 1000); // (30.000 ms = 30 segundos)
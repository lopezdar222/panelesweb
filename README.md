## Instalaciones:

* Instalar Nodejs y PostgreSQL

* hacer clic en Inicio, escribir “Windows PowerShell”, hacer clic con el botón derecho encima de la aplicación y finalmente hacer clic en “Ejecutar como administrador”.<br>
Get-ExecutionPolicy -List ​<br>
Set-ExecutionPolicy RemoteSigned -Scope CurrentUser <br>

* npm install -g nodemon <br>

* npm install -g pm2 <br>

* https://platzi.com/clases/1759-fundamentos-node/25188-herramientas-para-ser-mas-felices-nodemon-y-pm2/ <br>

* sudo apt install redis-server

* En la carpeta de proyecto ejecutar:

npm init
npm install express bcrypt body-parser express-session pg connect-pg-simple ip ejs axios multer
npm install ws date-fns json2csv
npm install @redis/client@latest
npm install puppeteer puppeteer-extra puppeteer-extra-plugin-stealth qrcode-terminal whatsapp-web.js qrcode

sudo apt install -y chromium-browser
sudo apt install -y xdg-utils

## Python

mkdir reportes
sudo apt install python3-pip -y
pip --version
python3 --version
sudo apt install libpq-dev python3-dev
pip install psycopg2
pip install pandas seaborn notebook jupyterlab 
pip install requests folium
pip install spacy
python3 -m spacy download es_core_news_sm
pip install nltk

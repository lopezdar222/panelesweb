#!/bin/bash

# Nombre del servicio de PostgreSQL
SERVICE="postgresql"

# Ruta al archivo de log
LOGFILE="/home/dario/panelesweb/postgresql_restarts.log"

# Verifica el estado del servicio
STATUS=$(systemctl is-active $SERVICE)
SUBSTATUS=$(systemctl show -p SubState --value $SERVICE)

if [ "$STATUS" == "active" ] && [ "$SUBSTATUS" == "exited" ]; then
    echo "$SERVICE está activo pero en estado 'exited'. Reiniciando el servicio..."
    # Reinicia el servicio
    sudo systemctl restart $SERVICE

    # Verifica si el servicio se reinició correctamente
    if systemctl is-active --quiet $SERVICE; then
        echo "$SERVICE se reinició correctamente."
        # Registro en el archivo de log
        echo "$(date +'%Y-%m-%d %H:%M:%S') - $SERVICE se reinició correctamente." >> $LOGFILE
    else
        echo "Error: $SERVICE no se pudo reiniciar."
        # Registro en el archivo de log
        echo "$(date +'%Y-%m-%d %H:%M:%S') - Error: $SERVICE no se pudo reiniciar." >> $LOGFILE
    fi
elif [ "$STATUS" != "active" ]; then
    echo "$SERVICE no está activo. Reiniciando el servicio..."
    # Reinicia el servicio
    sudo systemctl restart $SERVICE

    # Verifica si el servicio se reinició correctamente
    if systemctl is-active --quiet $SERVICE; then
        echo "$SERVICE se reinició correctamente."
        # Registro en el archivo de log
        echo "$(date +'%Y-%m-%d %H:%M:%S') - $SERVICE se reinició correctamente." >> $LOGFILE
    else
        echo "Error: $SERVICE no se pudo reiniciar."
        # Registro en el archivo de log
        echo "$(date +'%Y-%m-%d %H:%M:%S') - Error: $SERVICE no se pudo reiniciar." >> $LOGFILE
    fi
else
    echo "$SERVICE está activo y funcionando correctamente."
fi


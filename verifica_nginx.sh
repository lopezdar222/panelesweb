#!/bin/bash

# Archivo de registro donde se guardar� el resultado
LOG_FILE="/home/dario/panelesweb/nginx.log"

# Obtener el n�mero de l�neas en el archivo de acceso de Nginx
LINE_COUNT=$(wc -l /var/log/nginx/access.log | awk '{print $1}')

# Obtener la marca de tiempo actual
TIMESTAMP=$(date '+%Y-%m-%d %H:%M:%S')

# Guardar el resultado en el archivo de registro
echo "$TIMESTAMP - $LINE_COUNT" > $LOG_FILE

node /home/dario/panelesweb/src/rpt_registro_nginx.js

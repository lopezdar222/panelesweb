import psycopg2
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import datetime
import requests
import folium

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="panelesweb",
    user="postgres",
    password="paneles0110",
    #host="localhost"
    host="panelesweb-cluster.cluster-ro-cjgme8oigakn.us-east-1.rds.amazonaws.com"
)

# del df_acciones_cargas_total
ruta_imagenes = '/home/dario/panelesweb/reportes/'

dia_inicio_periodo = '2025-01-01'
# Convertir la cadena a un objeto datetime
fecha_limite = pd.to_datetime(dia_inicio_periodo).date()
top_clientes = 15

# Obtener la fecha y hora actual
datetime_actual = datetime.datetime.now()

mes_hoy = '0' + str(datetime_actual.month)
mes_hoy = mes_hoy[-2:]
dia_de_hoy = str(datetime_actual.year) + '-' + str(datetime_actual.month).zfill(2) + '-' + str(datetime_actual.day).zfill(2)

valor_tope_monto_cargas = 15000
valor_tope_demora_cargas = 150

def nombre_accion(id_origen):
    if id_origen == 0:
        return 'Carga Aceptada Manual'
    elif id_origen == -1:
        return 'Carga Automática Por Confianza'
    elif id_origen == 1:
        return 'Carga Automática MercadoPago'
    elif id_origen == 2:
        return 'Carga Automática PanelesNoti'
    elif id_origen == 2:
        return 'Carga Automática TuCash'
    else:
        return 'Carga No Identificada'
    
# REPORTE PETICIONES HISTORICO
sql_peticiones_hist_1 = "SELECT * FROM rpt_Peticiones_Nginx_Historico where id_servidor = 1 order by horario;"
df_peticiones_hist_1 = pd.read_sql(sql_peticiones_hist_1, conn)
df_peticiones_hist_1['desviacion'] = df_peticiones_hist_1['desviacion'].fillna(0)

sql_peticiones_hist_2 = "SELECT * FROM rpt_Peticiones_Nginx_Historico where id_servidor = 2 order by horario;"
df_peticiones_hist_2 = pd.read_sql(sql_peticiones_hist_2, conn)
df_peticiones_hist_2['desviacion'] = df_peticiones_hist_2['desviacion'].fillna(0)

plt.figure(figsize=(15, 10))
plt.plot(df_peticiones_hist_1['horario'], df_peticiones_hist_1['promedio'], label='Promedio')
plt.fill_between(df_peticiones_hist_1['horario'], 
                 df_peticiones_hist_1['promedio'] - df_peticiones_hist_1['desviacion'], 
                 df_peticiones_hist_1['promedio'] + df_peticiones_hist_1['desviacion'], 
                 color='gray', alpha=0.2, label='Desviación Estándar')
plt.ylim(0, 2000)                 
plt.xlabel('Horario')
plt.ylabel('Peticiones')
plt.title('Promedio y Desviación Estándar de Peticiones por Horario al Servidor PanelesLanding')
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_peticiones_prom_desv_s1.png', bbox_inches='tight')
# plt.show()
plt.clf()
del df_peticiones_hist_1

plt.figure(figsize=(15, 10))
plt.plot(df_peticiones_hist_2['horario'], df_peticiones_hist_2['promedio'], label='Promedio')
plt.fill_between(df_peticiones_hist_2['horario'], 
                 df_peticiones_hist_2['promedio'] - df_peticiones_hist_2['desviacion'], 
                 df_peticiones_hist_2['promedio'] + df_peticiones_hist_2['desviacion'], 
                 color='gray', alpha=0.2, label='Desviación Estándar')
plt.ylim(0, 2000)
plt.xlabel('Horario')
plt.ylabel('Peticiones')
plt.title('Promedio y Desviación Estándar de Peticiones por Horario al Servidor Landing')
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_peticiones_prom_desv_s2.png', bbox_inches='tight')
# plt.show()
plt.clf()

del df_peticiones_hist_2

# REPORTE EVOLUTIVO DE CARGAS
sql_query_2 = "SELECT fecha, id_oficina, oficina, id_accion, accion, id_origen_notificacion, sum(cantidad) as cantidad FROM rpt_Acciones_Diarias"
sql_query_2 = sql_query_2 + " WHERE fecha < '" + dia_de_hoy + "' AND fecha >= '" + dia_inicio_periodo + "'"
sql_query_2 = sql_query_2 + " AND id_accion in (1,5)"
sql_query_2 = sql_query_2 + " GROUP BY fecha, id_oficina, oficina, id_accion, accion, id_origen_notificacion"
sql_query_2 = sql_query_2 + " ORDER BY fecha, id_oficina, oficina, id_accion, accion, id_origen_notificacion"

df_acciones_cargas = pd.read_sql(sql_query_2, conn)
df_acciones_cargas['accion'] = df_acciones_cargas.apply(lambda row: row['accion'] if row['id_accion'] > 1 else nombre_accion(row['id_origen_notificacion']), axis=1)
df_acciones_cargas['fecha'] = pd.to_datetime(df_acciones_cargas['fecha'])
df_acciones_cargas_todas = df_acciones_cargas[df_acciones_cargas['id_oficina'] == 0]
df_acciones_cargas = df_acciones_cargas[df_acciones_cargas['id_oficina'] > 0]

# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
oficina = df_acciones_cargas_todas.oficina.unique()[0]
df_acciones_cargas_total = df_acciones_cargas_todas.groupby(['fecha'])['cantidad'].sum().reset_index()
ax1 = sns.lineplot(data=df_acciones_cargas_todas[df_acciones_cargas_todas.accion == 'Carga Manual'], x='fecha', y='cantidad', label='Carga Manual')
ax2 = sns.lineplot(data=df_acciones_cargas_todas[df_acciones_cargas_todas.accion == 'Carga Aceptada Manual'], x='fecha', y='cantidad', label='Carga Aceptada Manual')
ax3 = sns.lineplot(data=df_acciones_cargas_todas[df_acciones_cargas_todas.accion == 'Carga Automática MercadoPago'], x='fecha', y='cantidad', label='Carga Automática MercadoPago')
ax4 = sns.lineplot(data=df_acciones_cargas_todas[df_acciones_cargas_todas.accion == 'Carga Automática PanelesNoti'], x='fecha', y='cantidad', label='Carga Automática PanelesNoti')
ax5 = sns.lineplot(data=df_acciones_cargas_todas[df_acciones_cargas_todas.accion == 'Carga Automática Por Confianza'], x='fecha', y='cantidad', label='Carga Automática Por Confianza')
ax6 = sns.lineplot(data=df_acciones_cargas_total, x='fecha', y='cantidad', label='Cargas Totales')

# Ajustar el diseño del gráfico
plt.title('Evolución de Cargas de Fichas - Oficina: ' + oficina)
plt.xlabel('Fecha')
plt.ylabel('Cantidad de Fichas')
plt.legend(title='Variables')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')
ax4.set_xticklabels(ax4.get_xticklabels(), rotation=45, ha='right')
ax5.set_xticklabels(ax5.get_xticklabels(), rotation=45, ha='right')
ax6.set_xticklabels(ax5.get_xticklabels(), rotation=45, ha='right')

plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_cargas_evolucion.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()

for id_oficina in df_acciones_cargas.id_oficina.unique():
    # Crear el gráfico de líneas utilizando Seaborn
    plt.figure(figsize=(15, 10))
    df_acciones_cargas_ofi = df_acciones_cargas[df_acciones_cargas['id_oficina'] == id_oficina]
    oficina = df_acciones_cargas_ofi.oficina.unique()[0]
    df_acciones_cargas_total = df_acciones_cargas_ofi.groupby(['fecha'])['cantidad'].sum().reset_index()
    ax1 = sns.lineplot(data=df_acciones_cargas_ofi[df_acciones_cargas_ofi.accion == 'Carga Manual'], x='fecha', y='cantidad', label='Carga Manual')
    ax2 = sns.lineplot(data=df_acciones_cargas_ofi[df_acciones_cargas_ofi.accion == 'Carga Aceptada Manual'], x='fecha', y='cantidad', label='Carga Aceptada Manual')
    ax3 = sns.lineplot(data=df_acciones_cargas_ofi[df_acciones_cargas_ofi.accion == 'Carga Automática MercadoPago'], x='fecha', y='cantidad', label='Carga Automática MercadoPago')
    ax4 = sns.lineplot(data=df_acciones_cargas_ofi[df_acciones_cargas_ofi.accion == 'Carga Automática PanelesNoti'], x='fecha', y='cantidad', label='Carga Automática PanelesNoti')
    ax5 = sns.lineplot(data=df_acciones_cargas_total, x='fecha', y='cantidad', label='Cargas Totales')
    
    # Ajustar el diseño del gráfico
    plt.title('Evolución de Cargas de Fichas - Oficina: ' + oficina)
    plt.xlabel('Fecha')
    plt.ylabel('Cantidad de Fichas')
    plt.legend(title='Variables')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    # Rotar los labels del eje y a 45 grados
    ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
    ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
    ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')
    ax4.set_xticklabels(ax4.get_xticklabels(), rotation=45, ha='right')
    ax5.set_xticklabels(ax5.get_xticklabels(), rotation=45, ha='right')
    
    plt.grid(True)
    
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_cargas_evolucion.png', bbox_inches='tight')
    
    # Mostrar el gráfico
    # plt.show()
    plt.clf()

del df_acciones_cargas
del df_acciones_cargas_todas
del df_acciones_cargas_ofi
del df_acciones_cargas_total

# REPORTE EVOLUTIVO DE NUEVOS USUARIOS
sql_query = "SELECT TO_CHAR(cli.fecha_hora_proceso, 'YYYY-MM-DD') AS fecha, "
sql_query = sql_query + "cli.accion, "
sql_query = sql_query + "COUNT(*) AS cantidad "
sql_query = sql_query + "FROM (SELECT fecha_hora_proceso, accion FROM clientes_operaciones_diario WHERE id_accion IN (7,8) AND id_estado = 2 "
sql_query = sql_query + "AND TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') >= '" + dia_inicio_periodo + "' UNION "
sql_query = sql_query + "SELECT fecha_hora_proceso, accion FROM clientes_operaciones_hist WHERE id_accion IN (7,8) AND id_estado = 2 "
sql_query = sql_query + "AND TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') >= '" + dia_inicio_periodo + "') cli "
sql_query = sql_query + "GROUP BY TO_CHAR(cli.fecha_hora_proceso, 'YYYY-MM-DD'), cli.accion"

df_acciones_1 = pd.read_sql(sql_query, conn)

df_acciones_1['fecha'] = pd.to_datetime(df_acciones_1['fecha'])

# Pivotear los datos para que cada categoría sea una columna
df_pivot_acciones = df_acciones_1.pivot_table(index='fecha', columns='accion', values='cantidad', fill_value=0)

# Crear gráficos separados pero con el mismo eje x
fig, axs = plt.subplots(nrows=len(df_pivot_acciones.columns), sharex=True, figsize=(8, 6))

# Iterar sobre las categorías para generar gráficos individuales
for i, accion in enumerate(df_pivot_acciones.columns):
    axs[i].plot(df_pivot_acciones.index, df_pivot_acciones[accion], label=f'{accion}')
    axs[i].fill_between(df_pivot_acciones.index, df_pivot_acciones[accion], alpha=0.3)  # Línea apilada
    axs[i].set_ylabel(f'{accion}')
    axs[i].legend(loc='upper left')
    axs[i].grid(True)

# Configurar el eje X compartido
axs[-1].set_xlabel('Fecha')
# Rotar las etiquetas del eje x
for ax in axs:
    ax.tick_params(axis='x', rotation=45)
    
plt.suptitle('Nuevos Usuarios', fontsize=14)
plt.tight_layout(rect=[0, 0, 1, 0.95])  # Ajustar para el título

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_clientes_nuevos.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()

# REPORTE EVOLUTIVO DE SOLICITUDES RECHAZADAS
sql_query = "SELECT TO_CHAR(cli.fecha_hora_proceso, 'YYYY-MM-DD') AS fecha, "
sql_query = sql_query + "cli.accion, "
sql_query = sql_query + "COUNT(*) AS cantidad "
sql_query = sql_query + "FROM (SELECT fecha_hora_proceso, accion FROM clientes_operaciones_diario WHERE id_estado = 3 "
sql_query = sql_query + "AND TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') >= '" + dia_inicio_periodo + "' UNION "
sql_query = sql_query + "SELECT fecha_hora_proceso, accion FROM clientes_operaciones_hist WHERE id_estado = 3 "
sql_query = sql_query + "AND TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') >= '" + dia_inicio_periodo + "') cli "
sql_query = sql_query + "GROUP BY TO_CHAR(cli.fecha_hora_proceso, 'YYYY-MM-DD'), cli.accion"

df_acciones_2 = pd.read_sql(sql_query, conn)

df_acciones_2['fecha'] = pd.to_datetime(df_acciones_2['fecha'])

# Pivotear los datos para que cada categoría sea una columna
df_pivot_acciones = df_acciones_2.pivot_table(index='fecha', columns='accion', values='cantidad', fill_value=0)

# Crear gráficos separados pero con el mismo eje x
fig, axs = plt.subplots(nrows=len(df_pivot_acciones.columns), sharex=True, figsize=(8, 6))

# Iterar sobre las categorías para generar gráficos individuales
for i, accion in enumerate(df_pivot_acciones.columns):
    axs[i].plot(df_pivot_acciones.index, df_pivot_acciones[accion], label=f'{accion}')
    axs[i].fill_between(df_pivot_acciones.index, df_pivot_acciones[accion], alpha=0.3)  # Línea apilada
    axs[i].set_ylabel(f'{accion}')
    axs[i].legend(loc='upper left')
    axs[i].grid(True)

# Configurar el eje X compartido
axs[-1].set_xlabel('Fecha')
# Rotar las etiquetas del eje x
for ax in axs:
    ax.tick_params(axis='x', rotation=45)
    
plt.suptitle('Solicitudes Rechazadas', fontsize=14)
plt.tight_layout(rect=[0, 0, 1, 0.95])  # Ajustar para el título

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_solicitudes_rechazadas.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()

# REPORTE EVOLUTIVO DE CANTIDAD DE CLIENTES EN OPERACIONES
sql_query = "SELECT TO_CHAR(cli.fecha_hora_proceso, 'YYYY-MM-DD') AS fecha, "
sql_query = sql_query + "cli.accion, "
sql_query = sql_query + "COUNT(DISTINCT id_cliente) AS cantidad "
sql_query = sql_query + "FROM (SELECT fecha_hora_proceso, accion, id_cliente FROM clientes_operaciones_diario WHERE id_accion IN (1,2,5,6) AND id_estado = 2 "
sql_query = sql_query + "AND TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') >= '" + dia_inicio_periodo + "' UNION "
sql_query = sql_query + "SELECT fecha_hora_proceso, accion, id_cliente FROM clientes_operaciones_hist WHERE id_accion IN (1,2,5,6) AND id_estado = 2 "
sql_query = sql_query + "AND TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') >= '" + dia_inicio_periodo + "') cli "
sql_query = sql_query + "GROUP BY TO_CHAR(cli.fecha_hora_proceso, 'YYYY-MM-DD'), cli.accion"

df_acciones_3 = pd.read_sql(sql_query, conn)

df_acciones_3['fecha'] = pd.to_datetime(df_acciones_3['fecha'])

# Pivotear los datos para que cada categoría sea una columna
df_pivot_acciones = df_acciones_3.pivot_table(index='fecha', columns='accion', values='cantidad', fill_value=0)

# Crear gráficos separados pero con el mismo eje x
fig, axs = plt.subplots(nrows=len(df_pivot_acciones.columns), sharex=True, figsize=(8, 6))

# Iterar sobre las categorías para generar gráficos individuales
for i, accion in enumerate(df_pivot_acciones.columns):
    axs[i].plot(df_pivot_acciones.index, df_pivot_acciones[accion], label=f'{accion}')
    axs[i].fill_between(df_pivot_acciones.index, df_pivot_acciones[accion], alpha=0.3)  # Línea apilada
    axs[i].set_ylabel(f'{accion}')
    axs[i].legend(loc='upper left')
    axs[i].grid(True)

# Configurar el eje X compartido
axs[-1].set_xlabel('Fecha')
# Rotar las etiquetas del eje x
for ax in axs:
    ax.tick_params(axis='x', rotation=45)
    
plt.suptitle('Cantidad de Clientes en las Operaciones', fontsize=14)
plt.tight_layout(rect=[0, 0, 1, 0.95])  # Ajustar para el título

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_clientes_en_operaciones.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()

del df_pivot_acciones
del df_acciones_1
del df_acciones_2
del df_acciones_3

# REPORTE BOXPLOT DE IMPORTES
sql_query_3 = "SELECT id_oficina,oficina,fecha_hora_operacion,EXTRACT(EPOCH FROM (fecha_hora_proceso - fecha_hora_operacion)) AS demora,carga_importe,carga_bono"
sql_query_3 = sql_query_3 + " FROM v_Clientes_Operaciones_Hist WHERE fecha_hora_operacion < '" + dia_de_hoy + "'"
sql_query_3 = sql_query_3 + " AND fecha_hora_operacion >= '" + dia_inicio_periodo + "'"
sql_query_3 = sql_query_3 + " AND id_accion in (1,5) AND es_carga = 1 AND id_estado = 2"

df_cargas = pd.read_sql(sql_query_3, conn)

df_cargas['carga_importe'].dropna(inplace=True)
df_cargas['demora'].dropna(inplace=True)
df_cargas['carga_bono'].fillna(0, inplace=True)

mostradas = df_cargas[(df_cargas['carga_importe'] > 0) & (df_cargas['carga_importe'] <= valor_tope_monto_cargas)].count()[0] / df_cargas[df_cargas['carga_importe'] > 0].count()[0]
mostradas = round(mostradas * 100, 2)

# Crear el boxplot con seaborn
# plt.figure(figsize=(8, 6))
sns.boxplot(data=df_cargas[df_cargas['carga_importe'] > 0], y='carga_importe')
plt.title('Boxplot de Importe de Cargas Hasta ' + str(valor_tope_monto_cargas) + ' (' + str(mostradas) + '% de los casos)') 
plt.ylabel('Importe')
plt.ylim(0, valor_tope_monto_cargas)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_boxplot_cargas.png', bbox_inches='tight')
# Mostrar el boxplot
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    df_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina]
    oficina = df_cargas_ofi.oficina.unique()[0]
    mostradas = df_cargas_ofi[(df_cargas_ofi['carga_importe'] > 0) & (df_cargas_ofi['carga_importe'] <= valor_tope_monto_cargas)].count()[0] / df_cargas_ofi[df_cargas_ofi['carga_importe'] > 0].count()[0]
    mostradas = round(mostradas * 100, 2)
    
    # Crear el boxplot con seaborn
    # plt.figure(figsize=(8, 6))
    sns.boxplot(data=df_cargas_ofi[df_cargas_ofi['carga_importe'] > 0], y='carga_importe')
    plt.title(oficina + ' - Boxplot de Importe de Cargas Hasta ' + str(valor_tope_monto_cargas) + ' (' + str(mostradas) + '% de los casos)') 
    plt.ylabel('Importe')
    plt.ylim(0, valor_tope_monto_cargas)
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_boxplot_cargas.png', bbox_inches='tight')
    # Mostrar el boxplot
    # plt.show()
    plt.clf()

# REPORTE BOXPLOT DE DEMORAS
# Crear el boxplot con seaborn
# plt.figure(figsize=(8, 6))
mostradas = df_cargas[df_cargas['demora'] < valor_tope_demora_cargas].count()[0] / df_cargas.count()[0]
mostradas = round(mostradas * 100, 2)
sns.boxplot(data=df_cargas, y='demora')
plt.title('Boxplot de Demora de Cargas Hasta ' + str(valor_tope_demora_cargas) + ' (' + str(mostradas) + '% de los casos)') 
plt.ylabel('Demora (en Segundos)')
plt.ylim(0, valor_tope_demora_cargas)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_boxplot_demoras.png', bbox_inches='tight')
# Mostrar el boxplot
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    df_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina]
    oficina = df_cargas_ofi.oficina.unique()[0]
    mostradas = df_cargas_ofi[df_cargas_ofi['demora'] < valor_tope_demora_cargas].count()[0] / df_cargas_ofi.count()[0]
    mostradas = round(mostradas * 100, 2)
    sns.boxplot(data=df_cargas_ofi, y='demora')
    plt.title(oficina + ' - Boxplot de Demora de Cargas Hasta ' + str(valor_tope_demora_cargas) + ' (' + str(mostradas) + '% de los casos)') 
    plt.ylabel('Demora (en Segundos)')
    plt.ylim(0, valor_tope_demora_cargas)
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_boxplot_demoras.png', bbox_inches='tight')
    # Mostrar el boxplot
    # plt.show()
    plt.clf()

# REPORTE HISTOGRAMA DE IMPORTES
# Crear el histograma con seaborn
# plt.figure(figsize=(8, 6))
sns.histplot(data=df_cargas[(df_cargas['carga_importe'] > 0) & (df_cargas['carga_importe'] <= valor_tope_monto_cargas)], x='carga_importe', bins=20, kde=True)
plt.title('Histograma de Distribución de Montos de Cargas Hasta ' + str(valor_tope_monto_cargas))
plt.xlabel('Importe')
plt.ylabel('Frecuencia')
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_histograma_cargas.png', bbox_inches='tight')
# Mostrar el boxplot
plt.clf()
#plt.show()

for id_oficina in df_cargas.id_oficina.unique():
    df_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina]
    oficina = df_cargas_ofi.oficina.unique()[0]
    # Crear el histograma con seaborn
    # plt.figure(figsize=(8, 6))
    sns.histplot(data=df_cargas_ofi[(df_cargas_ofi['carga_importe'] > 0) & (df_cargas_ofi['carga_importe'] <= valor_tope_monto_cargas)], x='carga_importe', bins=20, kde=True)
    plt.title(oficina + ' - Histograma de Distribución de Montos de Cargas Hasta ' + str(valor_tope_monto_cargas))
    plt.xlabel('Importe')
    plt.ylabel('Frecuencia')
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_histograma_cargas.png', bbox_inches='tight')
    # Mostrar el boxplot
    plt.clf()
    #plt.show()

#REPORTE DE PROPORCIÓN DE MONTOS Y BONOS
# Calcular las sumas de los campos 'monto' y 'bono'
suma_monto = df_cargas['carga_importe'].sum()
suma_bono = df_cargas['carga_bono'].sum()
# Crear una lista con las sumas calculadas
sumas = [suma_monto, suma_bono]
# Etiquetas para las porciones de la torta
labels = ['Monto', 'Bono']
# Colores para cada porción
colors = ['blue', 'orange']
# Crear el gráfico de torta
# plt.figure(figsize=(8, 6))
plt.pie(sumas, labels=labels, colors=colors, autopct='%1.1f%%', startangle=140)
plt.title('Proporción de Monto y Bono')
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_monto_bono.png', bbox_inches='tight')
# Mostrar el gráfico
plt.clf()
#plt.show()

for id_oficina in df_cargas.id_oficina.unique():
    df_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina]
    oficina = df_cargas_ofi.oficina.unique()[0]
    # Calcular las sumas de los campos 'monto' y 'bono'
    suma_monto = df_cargas_ofi['carga_importe'].sum()
    suma_bono = df_cargas_ofi['carga_bono'].sum()
    # Crear una lista con las sumas calculadas
    sumas = [suma_monto, suma_bono]
    # Etiquetas para las porciones de la torta
    labels = ['Monto', 'Bono']
    # Colores para cada porción
    colors = ['blue', 'orange']
    # Crear el gráfico de torta
    # plt.figure(figsize=(8, 6))
    plt.pie(sumas, labels=labels, colors=colors, autopct='%1.1f%%', startangle=140)
    plt.title(oficina + ' - Proporción de Monto y Bono')
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_monto_bono.png', bbox_inches='tight')
    # Mostrar el gráfico
    # plt.show()
    plt.clf()

# Obtener el número de día de la semana (0 para lunes, 1 para martes, ..., 6 para domingo)
df_cargas['dia_semana'] = df_cargas['fecha_hora_operacion'].dt.dayofweek

# Obtener la hora del día (en formato de 24 horas)
df_cargas['hora'] = df_cargas['fecha_hora_operacion'].dt.hour

#REPORTE HISTOGRAMA DE CARGAS POR HORA
# Crear el histograma con Seaborn
# plt.figure(figsize=(10, 6))
sns.histplot(data=df_cargas, x='hora', bins=24, kde=True)  # 'bins=24' para tener un bin por cada hora del día
plt.title('Histograma de Distribución de Cargas Por Hora')
plt.xlabel('Hora del día')
plt.ylabel('Frecuencia')
plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_cargas_hora.png', bbox_inches='tight')
# Mostrar el histograma
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    df_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina]
    oficina = df_cargas_ofi.oficina.unique()[0]
    # Crear el histograma con Seaborn
    # plt.figure(figsize=(10, 6))
    sns.histplot(data=df_cargas_ofi, x='hora', bins=24, kde=True)  # 'bins=24' para tener un bin por cada hora del día
    plt.title(oficina + ' - Histograma de Distribución de Cargas Por Hora')
    plt.xlabel('Hora del día')
    plt.ylabel('Frecuencia')
    plt.grid(True)
    
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_cargas_hora.png', bbox_inches='tight')
    # Mostrar el histograma
    # plt.show()
    plt.clf()

df_cargas['date_only'] = df_cargas['fecha_hora_operacion'].dt.date

#REPORTE BOXPLOT DE CARGAS POR DÍA DE LA SEMANA
# Crear el boxplot con seaborn
# plt.figure(figsize=(8, 6))
sns.boxplot(x='dia_semana', y='carga_importe', data=df_cargas.groupby(['dia_semana','date_only'])['carga_importe'].count().reset_index())
plt.title('Boxplot de Cargas por Día de la Semana')
plt.ylabel('Cantidad de Cargas')
plt.xlabel('Día de la Semana (0 para lunes, 6 para domingo)')
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_cargas_dia.png', bbox_inches='tight')
# Mostrar el boxplot
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    df_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina]
    oficina = df_cargas_ofi.oficina.unique()[0]

    # Crear el boxplot con seaborn
    # plt.figure(figsize=(8, 6))
    sns.boxplot(x='dia_semana', y='carga_importe', data=df_cargas_ofi.groupby(['dia_semana','date_only'])['carga_importe'].count().reset_index())
    plt.title(oficina + ' - Boxplot de Cargas por Día de la Semana')
    plt.ylabel('Cantidad de Cargas')
    plt.xlabel('Día de la Semana (0 para lunes, 6 para domingo)')
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_cargas_dia.png', bbox_inches='tight')
    # Mostrar el boxplot
    # plt.show()
    plt.clf()

df_agrupado_cargas = df_cargas.groupby(['date_only'])['carga_importe'].sum().reset_index()
df_cargas['carga_total'] = df_cargas['carga_importe'] + df_cargas['carga_bono']
df_agrupado_cargas_bonos = df_cargas.groupby(['date_only'])['carga_total'].sum().reset_index()

sql_query_4 = "SELECT id_oficina,oficina,fecha_hora_operacion,EXTRACT(EPOCH FROM (fecha_hora_proceso - fecha_hora_operacion)) AS demora,retiro_importe"
sql_query_4 = sql_query_4 + " FROM v_Clientes_Operaciones_Hist WHERE fecha_hora_operacion < '" + dia_de_hoy + "'"
sql_query_4 = sql_query_4 + " AND fecha_hora_operacion >= '" + dia_inicio_periodo + "'"
sql_query_4 = sql_query_4 + " AND id_accion in (2,6) AND es_retiro = 1 AND id_estado = 2"

df_retiros = pd.read_sql(sql_query_4, conn)

df_retiros['date_only'] = df_retiros['fecha_hora_operacion'].dt.date

df_agrupado_retiros = df_retiros.groupby(['date_only'])['retiro_importe'].sum().reset_index()

#REPORTE MONTOS DE CARGAS, BONOS Y RETIROS
# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
ax1 = sns.lineplot(data=df_agrupado_retiros, x='date_only', y='retiro_importe', label='Retiros')
ax2 = sns.lineplot(data=df_agrupado_cargas_bonos, x='date_only', y='carga_total', label='Cargas + Bonos')
ax3 = sns.lineplot(data=df_agrupado_cargas, x='date_only', y='carga_importe', label='Cargas')

# Ajustar el diseño del gráfico
plt.title('Cargas, Bonos y Retiros')
plt.xlabel('Fecha')
plt.ylabel('Monto')
plt.legend(title='Variables')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')
plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_cargas_retiros_bonos.png', bbox_inches='tight')
# Mostrar el gráfico
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    oficina = df_cargas[df_cargas['id_oficina'] == id_oficina].oficina.unique()[0]    
    df_agrupado_cargas_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina].groupby(['date_only'])['carga_importe'].sum().reset_index()
    df_agrupado_cargas_bonos_ofi = df_cargas[df_cargas['id_oficina'] == id_oficina].groupby(['date_only'])['carga_total'].sum().reset_index()
    df_agrupado_retiros_ofi = df_retiros[df_retiros['id_oficina'] == id_oficina].groupby(['date_only'])['retiro_importe'].sum().reset_index()
    
    # Crear el gráfico de líneas utilizando Seaborn
    plt.figure(figsize=(15, 10))
    ax1 = sns.lineplot(data=df_agrupado_retiros_ofi, x='date_only', y='retiro_importe', label='Retiros')
    ax2 = sns.lineplot(data=df_agrupado_cargas_bonos_ofi, x='date_only', y='carga_total', label='Cargas + Bonos')
    ax3 = sns.lineplot(data=df_agrupado_cargas_ofi, x='date_only', y='carga_importe', label='Cargas')
    
    # Ajustar el diseño del gráfico
    plt.title(oficina + ' - Cargas, Bonos y Retiros')
    plt.xlabel('Fecha')
    plt.ylabel('Monto')
    plt.legend(title='Variables')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    # Rotar los labels del eje y a 45 grados
    ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
    ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
    ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')
    plt.grid(True)
    
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_cargas_retiros_bonos.png', bbox_inches='tight')
    # Mostrar el gráfico
    # plt.show()
    plt.clf()

#REPORTE DE PROMEDIO Y DESVIACIÓN ESTÁNDAR DE PRECIO
df_agrupado_cargas = df_cargas.groupby('date_only').agg({'carga_importe': ['mean', 'std']})
df_agrupado_cargas.reset_index(inplace=True)
plt.figure(figsize=(15, 10))
plt.plot(df_agrupado_cargas['date_only'], df_agrupado_cargas['carga_importe']['mean'], label='Promedio')
plt.fill_between(df_agrupado_cargas['date_only'], 
                 df_agrupado_cargas['carga_importe']['mean'] - df_agrupado_cargas['carga_importe']['std'], 
                 df_agrupado_cargas['carga_importe']['mean'] + df_agrupado_cargas['carga_importe']['std'], 
                 color='gray', alpha=0.2, label='Desviación Estándar')
plt.xlabel('Fecha')
plt.ylabel('Monto')
plt.title('Promedio y Desviación Estándar de Importe de Cargas por Fecha')
plt.ylim(0, valor_tope_monto_cargas * 3)
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_cargas_prom_desv.png', bbox_inches='tight')
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    oficina = df_cargas[df_cargas['id_oficina'] == id_oficina].oficina.unique()[0]  
    df_agrupado_cargas = df_cargas[df_cargas['id_oficina'] == id_oficina].groupby('date_only').agg({'carga_importe': ['mean', 'std']})
    df_agrupado_cargas.reset_index(inplace=True)
    
    plt.figure(figsize=(15, 10))
    plt.plot(df_agrupado_cargas['date_only'], df_agrupado_cargas['carga_importe']['mean'], label='Promedio')
    plt.fill_between(df_agrupado_cargas['date_only'], 
                     df_agrupado_cargas['carga_importe']['mean'] - df_agrupado_cargas['carga_importe']['std'], 
                     df_agrupado_cargas['carga_importe']['mean'] + df_agrupado_cargas['carga_importe']['std'], 
                     color='gray', alpha=0.2, label='Desviación Estándar')
    plt.xlabel('Fecha')
    plt.ylabel('Monto')
    plt.ylim(0, valor_tope_monto_cargas * 3)
    plt.title(oficina + ' - Promedio y Desviación Estándar de Importe de Cargas por Fecha')
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_cargas_prom_desv.png', bbox_inches='tight')
    # plt.show()
    plt.clf()

#REPORTE DE PROMEDIO Y DESVIACIÓN ESTÁNDAR DE DEMORA
# Filtrar los datos para incluir solo las fechas a partir del '2024-06-01'
df_agrupado_demoras_total = df_cargas[df_cargas['date_only'] >= fecha_limite]

df_agrupado_demoras = df_agrupado_demoras_total.groupby('date_only').agg({'demora': ['mean', 'std']})
df_agrupado_demoras.reset_index(inplace=True)

plt.figure(figsize=(15, 10))
plt.plot(df_agrupado_demoras['date_only'], df_agrupado_demoras['demora']['mean'], label='Promedio')
plt.fill_between(df_agrupado_demoras['date_only'], 
                 df_agrupado_demoras['demora']['mean'] - df_agrupado_demoras['demora']['std'], 
                 df_agrupado_demoras['demora']['mean'] + df_agrupado_demoras['demora']['std'], 
                 color='gray', alpha=0.2, label='Desviación Estándar')
plt.xlabel('Fecha')
plt.ylabel('Demora en Segundos')
plt.title('Promedio y Desviación Estándar de Demora (en Segundos) por Fecha')
#plt.ylim(0)
plt.ylim(0, valor_tope_demora_cargas * 5)
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_demoras_prom_desv.png', bbox_inches='tight')
# plt.show()
plt.clf()

for id_oficina in df_cargas.id_oficina.unique():
    oficina = df_cargas[df_cargas['id_oficina'] == id_oficina].oficina.unique()[0]
    df_agrupado_demoras_ofi = df_agrupado_demoras_total[df_agrupado_demoras_total['id_oficina'] == id_oficina].groupby('date_only').agg({'demora': ['mean', 'std']})
    df_agrupado_demoras_ofi.reset_index(inplace=True)

    plt.figure(figsize=(15, 10))
    plt.plot(df_agrupado_demoras_ofi['date_only'], df_agrupado_demoras_ofi['demora']['mean'], label='Promedio')
    plt.fill_between(df_agrupado_demoras_ofi['date_only'], 
                     df_agrupado_demoras_ofi['demora']['mean'] - df_agrupado_demoras_ofi['demora']['std'], 
                     df_agrupado_demoras_ofi['demora']['mean'] + df_agrupado_demoras_ofi['demora']['std'], 
                     color='gray', alpha=0.2, label='Desviación Estándar')
    plt.xlabel('Fecha')
    plt.ylabel('Demora en Segundos')
    plt.title(oficina + ' - Promedio y Desviación Estándar de Demora (en Segundos) por Fecha')
    plt.ylim(0)
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_demoras_prom_desv.png', bbox_inches='tight')
    # plt.show()
    plt.clf()

conn.close()

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="panelesweb",
    user="postgres",
    password="paneles0110",
    #host="panelesweb-cluster.cluster-cjgme8oigakn.us-east-1.rds.amazonaws.com"
    host="192.168.200.182",
    port=6432
)
#REPORTES DE CLIENTES
sql_clientes = "SELECT * FROM Reporte_Clientes();"
df_clientes = pd.read_sql(sql_clientes, conn)

sql_clientes = "SELECT r.id_cliente,r.cliente_usuario,r.id_agente,r.agente_usuario,r.id_oficina,r.oficina,"
sql_clientes = sql_clientes + "CASE WHEN r.datos_telefono = 1 THEN 'Completo' ELSE 'Faltante' END AS estado_datos_telefono,"
sql_clientes = sql_clientes + "CASE WHEN r.datos_email = 1 THEN 'Completo' ELSE 'Faltante' END AS estado_datos_email,"
sql_clientes = sql_clientes + "CASE WHEN r.datos_nombre = 1 THEN 'Completo' ELSE 'Faltante' END AS estado_datos_nombre,"
sql_clientes = sql_clientes + "r.en_sesion,r.marca_baja,r.bloqueado,r.afectado_ip,r.id_registro_token,r.cantidad_cargas,"
sql_clientes = sql_clientes + "r.total_importe_cargas,r.total_importe_bonos,r.ultima_carga,EXTRACT(DAY FROM CURRENT_DATE - r.ultima_carga) AS dias_ultima_carga,"
sql_clientes = sql_clientes + "r.cantidad_retiros,r.total_importe_retiros,r.ultimo_retiro FROM rpt_clientes r;"

df_clientes = pd.read_sql(sql_clientes, conn)

def nombre_estado(bloqueado, afectado_ip, dias, cantidad_cargas):
    if bloqueado:
        return 'Bloqueado'
    elif afectado_ip:
        return 'Bloqueado por IP'
    elif dias <= 1:
        return 'Cargó en el último día'
    elif dias < 8:
        return 'Cargó en la última semana'
    elif dias < 31:
        return 'Cargó en el último mes'
    elif cantidad_cargas == 0:
        return 'Nunca Cargó'
    else:
        return 'Resto'
    
df_clientes['estado'] = df_clientes.apply(lambda row: nombre_estado(row['bloqueado'], row['afectado_ip'], row['dias_ultima_carga'], row['cantidad_cargas']), axis=1)

# Crear el gráfico de torta cargas
s_acciones = df_clientes[df_clientes['id_oficina'] > 1].groupby(['estado']).count().id_cliente
plt.figure(figsize=(8, 8))
plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
titulo = 'Todas'
plt.title(titulo + ' - Estado de Clientes - Total : ' + str(s_acciones.sum()))
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_clientes_torta.png', bbox_inches='tight')
# plt.show()

for oficina in df_clientes.id_oficina.unique():
    s_acciones = df_clientes[df_clientes['id_oficina']  == oficina].groupby(['estado']).count().id_cliente
    plt.figure(figsize=(8, 8))
    plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
    titulo = df_clientes[df_clientes.id_oficina == oficina].oficina.unique()[0]
    plt.title(titulo + ' - Estado de Clientes - Total : ' + str(s_acciones.sum()))
    plt.savefig(ruta_imagenes  + 'oficina_' + str(oficina) + '/rpt_clientes_torta.png', bbox_inches='tight')
    # plt.show()

# Crear el gráfico de torta
s_acciones = df_clientes[df_clientes['id_oficina'] > 1].groupby(['estado_datos_telefono']).count().id_cliente
plt.figure(figsize=(8, 8))
plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
titulo = 'Todas'
plt.title(titulo + ' - Completitud Datos de Teléfono')
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_clientes_torta_datos_telefono.png', bbox_inches='tight')
#plt.show()

for oficina in df_clientes.id_oficina.unique():
    s_acciones = df_clientes[df_clientes['id_oficina']  == oficina].groupby(['estado_datos_telefono']).count().id_cliente
    plt.figure(figsize=(8, 8))
    plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
    titulo = df_clientes[df_clientes.id_oficina == oficina].oficina.unique()[0]
    plt.title(titulo + ' - Completitud Datos de Telefono')
    plt.savefig(ruta_imagenes  + 'oficina_' + str(oficina) + '/rpt_clientes_torta_datos_telefono.png', bbox_inches='tight')
    #plt.show()

s_acciones = df_clientes[df_clientes['id_oficina'] > 1].groupby(['estado_datos_nombre']).count().id_cliente
plt.figure(figsize=(8, 8))
plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
titulo = 'Todas'
plt.title(titulo + ' - Completitud Datos de Nombre')
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_clientes_torta_datos_nombre.png', bbox_inches='tight')
#plt.show()

for oficina in df_clientes.id_oficina.unique():
    s_acciones = df_clientes[df_clientes['id_oficina']  == oficina].groupby(['estado_datos_email']).count().id_cliente
    plt.figure(figsize=(8, 8))
    plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
    titulo = df_clientes[df_clientes.id_oficina == oficina].oficina.unique()[0]
    plt.title(titulo + ' - Completitud Datos de Email')
    plt.savefig(ruta_imagenes  + 'oficina_' + str(oficina) + '/rpt_clientes_torta_datos_email.png', bbox_inches='tight')
    #plt.show()

s_acciones = df_clientes[df_clientes['id_oficina'] > 1].groupby(['estado_datos_email']).count().id_cliente
plt.figure(figsize=(8, 8))
plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
titulo = 'Todas'
plt.title(titulo + ' - Completitud Datos de Email')
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_clientes_torta_datos_email.png', bbox_inches='tight')
#plt.show()

for oficina in df_clientes.id_oficina.unique():
    s_acciones = df_clientes[df_clientes['id_oficina']  == oficina].groupby(['estado_datos_nombre']).count().id_cliente
    plt.figure(figsize=(8, 8))
    plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
    titulo = df_clientes[df_clientes.id_oficina == oficina].oficina.unique()[0]
    plt.title(titulo + ' - Completitud Datos de Nombre')
    plt.savefig(ruta_imagenes  + 'oficina_' + str(oficina) + '/rpt_clientes_torta_datos_nombre.png', bbox_inches='tight')
    #plt.show()

# Ordena el DataFrame por 'cantidad_cargas' en orden descendente y toma el top 10
df_top_clientes = df_clientes.sort_values(by='cantidad_cargas', ascending=False).head(top_clientes)

# Crea el gráfico de barras
plt.figure(figsize=(10, 6))
plt.bar(df_top_clientes['cliente_usuario'], df_top_clientes['cantidad_cargas'], color='skyblue')
plt.xlabel('Cliente')
plt.ylabel('Cantidad de Cargas')
plt.title('Top ' + str(top_clientes) + ' Clientes con Más Cantidad de Cargas')
plt.xticks(rotation=45)
plt.tight_layout()  # Ajusta el diseño para evitar recortes de etiquetas
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_top_clientes_importes.png', bbox_inches='tight')

# Muestra el gráfico
# plt.show()

# Ordena el DataFrame por 'cantidad_cargas' en orden descendente y toma el top 10
df_top_clientes = df_clientes.sort_values(by='total_importe_cargas', ascending=False).head(top_clientes)

# Crea el gráfico de barras
plt.figure(figsize=(10, 6))
plt.bar(df_top_clientes['cliente_usuario'], df_top_clientes['total_importe_cargas'], color='green')
plt.xlabel('Cliente')
plt.ylabel('Importe de Cargas')
plt.title('Top ' + str(top_clientes) + ' Clientes con Más Importe de Cargas')
plt.xticks(rotation=45)
plt.tight_layout()  # Ajusta el diseño para evitar recortes de etiquetas
plt.savefig(ruta_imagenes  + 'oficina_todas/rpt_top_clientes_cargas.png', bbox_inches='tight')

# Muestra el gráfico
# plt.show()

for oficina in df_clientes.id_oficina.unique():
    # Ordena el DataFrame por 'cantidad_cargas' en orden descendente y toma el top top_clientes
    df_top_clientes = df_clientes[df_clientes['id_oficina']  == oficina].sort_values(by='cantidad_cargas', ascending=False).head(top_clientes)
    
    # Crea el gráfico de barras
    plt.figure(figsize=(10, 6))
    plt.bar(df_top_clientes['cliente_usuario'], df_top_clientes['cantidad_cargas'], color='skyblue')
    plt.xlabel('Cliente')
    plt.ylabel('Cantidad de Cargas')
    titulo = df_clientes[df_clientes.id_oficina == oficina].oficina.unique()[0]
    plt.title(titulo + ' - Top ' + str(top_clientes) + ' Clientes con Más Cantidad de Cargas')
    plt.xticks(rotation=45)
    plt.tight_layout()  # Ajusta el diseño para evitar recortes de etiquetas
    plt.savefig(ruta_imagenes  + 'oficina_' + str(oficina) + '/rpt_top_clientes_cargas.png', bbox_inches='tight')
    
    # Muestra el gráfico
    # plt.show()

for oficina in df_clientes.id_oficina.unique():
    # Ordena el DataFrame por 'cantidad_cargas' en orden descendente y toma el top top_clientes
    df_top_clientes = df_clientes[df_clientes['id_oficina']  == oficina].sort_values(by='total_importe_cargas', ascending=False).head(top_clientes)
    
    # Crea el gráfico de barras
    plt.figure(figsize=(10, 6))
    plt.bar(df_top_clientes['cliente_usuario'], df_top_clientes['total_importe_cargas'], color='green')
    plt.xlabel('Cliente')
    plt.ylabel('Importe de Cargas')
    titulo = df_clientes[df_clientes.id_oficina == oficina].oficina.unique()[0]
    plt.title(titulo + ' - Top ' + str(top_clientes) + ' Clientes con Más Importe de Cargas')
    plt.xticks(rotation=45)
    plt.tight_layout()  # Ajusta el diseño para evitar recortes de etiquetas
    plt.savefig(ruta_imagenes  + 'oficina_' + str(oficina) + '/rpt_top_clientes_importes.png', bbox_inches='tight')
    
    # Muestra el gráfico
    # plt.show()

conn.close()

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="panelesweb",
    user="postgres",
    password="paneles0110",
    #host="panelesweb-cluster.cluster-cjgme8oigakn.us-east-1.rds.amazonaws.com"
    #host="192.168.200.182",
    #port=6432
    host="panelesweb-cluster.cluster-ro-cjgme8oigakn.us-east-1.rds.amazonaws.com"
)

#PROMEDIO Y DESVIACION DE CHAT POR OFICINA Y TODAS
sql_chat_hist = "SELECT * FROM Reporte_Chat_Historico('" + dia_inicio_periodo + "') ORDER BY horario;"
df_chat_hist = pd.read_sql(sql_chat_hist, conn)

df_chat_hist['desviacion'] = df_chat_hist['desviacion'].fillna(0)
df_chat_hist_operador = df_chat_hist[(df_chat_hist.id_oficina > 0) & (df_chat_hist.origen == 'Operador')]
df_chat_hist_cliente = df_chat_hist[(df_chat_hist.id_oficina > 0) & (df_chat_hist.origen == 'Cliente')]
df_chat_hist_operador_todas = df_chat_hist[(df_chat_hist.id_oficina == 0) & (df_chat_hist.origen == 'Operador')]
df_chat_hist_cliente_todas = df_chat_hist[(df_chat_hist.id_oficina == 0) & (df_chat_hist.origen == 'Cliente')]

plt.figure(figsize=(15, 10))
plt.plot(df_chat_hist_operador_todas['horario'], df_chat_hist_operador_todas['promedio'], label='Promedio')
plt.fill_between(df_chat_hist_operador_todas['horario'], 
                 df_chat_hist_operador_todas['promedio'] - df_chat_hist_operador_todas['desviacion'], 
                 df_chat_hist_operador_todas['promedio'] + df_chat_hist_operador_todas['desviacion'], 
                 color='gray', alpha=0.2, label='Desviación Estándar')
plt.xlabel('Horario')
plt.ylabel('Mensajes')
plt.title('Promedio y Desviación Estándar de Mensajes De Operador por Horario')
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_chat_prom_desv_ope.png', bbox_inches='tight')
#plt.show()

for id_oficina in df_chat_hist_operador.id_oficina.unique():
    df_chat_hist_operador_ofi = df_chat_hist_operador[(df_chat_hist_operador.id_oficina == id_oficina)]
    plt.figure(figsize=(15, 10))
    plt.plot(df_chat_hist_operador_ofi['horario'], df_chat_hist_operador_ofi['promedio'], label='Promedio')
    plt.fill_between(df_chat_hist_operador_ofi['horario'], 
                     df_chat_hist_operador_ofi['promedio'] - df_chat_hist_operador_ofi['desviacion'], 
                     df_chat_hist_operador_ofi['promedio'] + df_chat_hist_operador_ofi['desviacion'], 
                     color='gray', alpha=0.2, label='Desviación Estándar')
   
    plt.xlabel('Horario')
    plt.ylabel('Mensajes')
    oficina = df_chat_hist_operador_ofi[(df_chat_hist_operador_ofi.id_oficina == id_oficina)].oficina.unique()[0]
    # Ajustar el diseño del gráfico
    titulo = oficina + ' Promedio y Desviación Estándar de Mensajes De Operador por Horario'
    plt.title(titulo)
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_chat_prom_desv_ope.png', bbox_inches='tight')
    # plt.show()
    
plt.figure(figsize=(15, 10))
plt.plot(df_chat_hist_cliente_todas['horario'], df_chat_hist_cliente_todas['promedio'], label='Promedio')
plt.fill_between(df_chat_hist_cliente_todas['horario'], 
                 df_chat_hist_cliente_todas['promedio'] - df_chat_hist_cliente_todas['desviacion'], 
                 df_chat_hist_cliente_todas['promedio'] + df_chat_hist_cliente_todas['desviacion'], 
                 color='gray', alpha=0.2, label='Desviación Estándar')
plt.xlabel('Horario')
plt.ylabel('Mensajes')
plt.title('Promedio y Desviación Estándar de Mensajes De Cliente por Horario')
plt.legend()
plt.xticks(rotation=45)
plt.grid(True)
# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_chat_prom_desv_cli.png', bbox_inches='tight')
# plt.show()

for id_oficina in df_chat_hist_cliente.id_oficina.unique():
    df_chat_hist_cliente_ofi = df_chat_hist_cliente[(df_chat_hist_cliente.id_oficina == id_oficina)]
    plt.figure(figsize=(15, 10))
    plt.plot(df_chat_hist_cliente_ofi['horario'], df_chat_hist_cliente_ofi['promedio'], label='Promedio')
    plt.fill_between(df_chat_hist_cliente_ofi['horario'], 
                     df_chat_hist_cliente_ofi['promedio'] - df_chat_hist_cliente_ofi['desviacion'], 
                     df_chat_hist_cliente_ofi['promedio'] + df_chat_hist_cliente_ofi['desviacion'], 
                     color='gray', alpha=0.2, label='Desviación Estándar')
   
    plt.xlabel('Horario')
    plt.ylabel('Mensajes')
    oficina = df_chat_hist_cliente_ofi[(df_chat_hist_cliente_ofi.id_oficina == id_oficina)].oficina.unique()[0]
    # Ajustar el diseño del gráfico
    titulo = oficina + ' Promedio y Desviación Estándar de Mensajes De Cliente por Horario'
    plt.title(titulo)
    plt.legend()
    plt.xticks(rotation=45)
    plt.grid(True)
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_chat_prom_desv_cli.png', bbox_inches='tight')
    #plt.show()

#PALABRAS MÁS MENCIONADAS
import nltk
import spacy
import re

sql_mensajes = "select * from rpt_Chat_Detalle where fecha_hora_creacion >= '" + dia_inicio_periodo + "';"
df_mensajes = pd.read_sql(sql_mensajes, conn)

stopwords = nltk.corpus.stopwords.words('spanish')
stopwords.append('hola')
stopwords.append('buenas')
stopwords.append('noches')
stopwords.append('si')
stopwords.append('mil')
stopwords.append('q')
stopwords.remove('por')
nlp = spacy.load('es_core_news_sm')
# Este proceso demora bastante!
mensajes_list=[]
n = 2
for mensaje in df_mensajes.mensaje:
    # Pasamos todo a minúsculas
    mensaje=mensaje.lower()
    # Vamos a reemplzar los caracteres que no sean leras por espacios
    mensaje=re.sub("[^a-záéíóú]"," ",str(mensaje))
    # Tokenizamos para separar las palabras
    mensaje=nltk.word_tokenize(mensaje)
    # Sacamos las Stopwords
    mensaje = [palabra for palabra in mensaje if not palabra in stopwords]
    # Conseguir Ngramas
    ngramas = list(nltk.ngrams(mensaje, n))
    frases = [' '.join(bigram) for bigram in ngramas]
    mensaje = mensaje + frases
    if 'por' in mensaje:
        mensaje.remove('por')
    if 'favor' in mensaje:
        mensaje.remove('favor')
    # Eliminamos las palabras de emenos de 3 letras
    # mensaje = [palabra for palabra in mensaje if len(palabra)>2]
    # Aplico Lematización
    # mensaje = [nlp(palabra) for palabra in mensaje]
    mensajes_list.append(mensaje)    
    
df_mensajes["mensaje_stem"] = mensajes_list

def genera_grafico(titulo, mensajes_list, ubicacion):
    lista_plana = [str(item) for sublista in mensajes_list for item in sublista]
    mensajes_list_freq = nltk.FreqDist(lista_plana)
    df_mensajes_list_freq = pd.DataFrame(list(mensajes_list_freq.items()), columns = ["Palabra","Frecuencia"])
    df_mensajes_list_freq.sort_values('Frecuencia',ascending=False, inplace = True)
    plt.figure(figsize = (15,8))
    plot = sns.barplot(x  = df_mensajes_list_freq.iloc[:30].Palabra, y = df_mensajes_list_freq.iloc[:30].Frecuencia)
    plt.title(titulo)
    for item in plot.get_xticklabels():
        item.set_rotation(90)
    plt.savefig(ubicacion, bbox_inches='tight')
    ##plt.show()
    
df_mensajes_filtro = df_mensajes[df_mensajes.origen == 'Operador'].mensaje_stem
ubicacion = ruta_imagenes  + 'oficina_todas' + '/rpt_mensajes_operador.png'
genera_grafico('Todas las Oficinas - Palabras frecuentes en mensajes de Operador', df_mensajes_filtro, ubicacion)

df_mensajes_filtro = df_mensajes[df_mensajes.origen == 'Cliente'].mensaje_stem
ubicacion = ruta_imagenes  + 'oficina_todas' + '/rpt_mensajes_cliente.png'
genera_grafico('Todas las Oficinas - Palabras frecuentes en mensajes de Cliente', df_mensajes_filtro, ubicacion)

for id_oficina in df_mensajes.id_oficina.unique():
    df_mensajes_filtro = df_mensajes[(df_mensajes.origen == 'Operador') & (df_mensajes.id_oficina == id_oficina) ].mensaje_stem
    ubicacion = ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_mensajes_operador.png'
    oficina = df_mensajes[(df_mensajes.id_oficina == id_oficina)].oficina.unique()[0]
    genera_grafico(oficina + ' - Palabras frecuentes en mensajes de Operador', df_mensajes_filtro, ubicacion)
    
for id_oficina in df_mensajes.id_oficina.unique():
    df_mensajes_filtro = df_mensajes[(df_mensajes.origen == 'Cliente') & (df_mensajes.id_oficina == id_oficina) ].mensaje_stem
    ubicacion = ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_mensajes_cliente.png'
    oficina = df_mensajes[(df_mensajes.id_oficina == id_oficina)].oficina.unique()[0]
    genera_grafico(oficina + ' - Palabras frecuentes en mensajes de Cliente', df_mensajes_filtro, ubicacion)

conn.close()

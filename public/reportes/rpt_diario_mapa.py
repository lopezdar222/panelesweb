import psycopg2
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import requests
import folium

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="panelesweb",
    user="postgres",
    password="paneles0110",
    host="panelesweb-cluster.cluster-ro-cjgme8oigakn.us-east-1.rds.amazonaws.com",
    port="5432"
)

def get_ips_from_db_cli(conn):
    cur = conn.cursor()
    consulta = "select ip from cliente_sesion where ip not in ('', '0.0.0.0', '::1')"
    consulta = consulta + " and ip not in (select ip from ip_lista_negra) and ip not in (select ip from ip_ubicacion)"
    consulta = consulta + " order by fecha_hora_creacion desc limit 10"
    cur.execute(consulta)
    rows = cur.fetchall()
    cur.close()
    return [row[0] for row in rows]

def get_ips_from_db_usr(conn):
    cur = conn.cursor()
    consulta = "select ip from usuario_sesion where ip not in ('', '0.0.0.0', '::1')"
    consulta = consulta + " and ip not in (select ip from ip_lista_negra) and ip not in (select ip from ip_ubicacion)"
    consulta = consulta + " order by fecha_hora_creacion desc limit 10"
    cur.execute(consulta)
    rows = cur.fetchall()
    cur.close()
    return [row[0] for row in rows]
    
def get_geolocation(ip):
    try:
        response = requests.get(f"https://ipinfo.io/{ip}/json")
        response.raise_for_status()  # Esto lanzará una excepción para códigos de estado HTTP 4xx/5xx
        data = response.json()
        if 'loc' in data:
            lat, lon = map(float, data['loc'].split(','))
            return lat, lon
        else:
            print(f"No 'loc' field found in response for IP: {ip}")
            return None, None
    except requests.exceptions.RequestException as e:
        print(f"Request failed for IP: {ip} - {e}")
        return None, None
    except ValueError as e:
        print(f"Error parsing response for IP: {ip} - {e}")
        return None, None
        
ips_cli = get_ips_from_db_cli(conn)
ips_usr = get_ips_from_db_usr(conn)

conn.close()

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="panelesweb",
    user="postgres",
    password="paneles0110",
    #host="panelesweb-cluster.cluster-cjgme8oigakn.us-east-1.rds.amazonaws.com"
    host="192.168.200.182",
    port=6432
)

cursor = conn.cursor()
insert_query = "insert into ip_ubicacion(ip, latitud, longitud) values (%s, %s, %s)"

for ip in ips_cli:
    lat, lon = get_geolocation(ip)
    
    if lat and lon:
        values_to_insert = (ip, lat, lon)
    else:
        values_to_insert = (ip, 0, 0)
    
    cursor.execute(insert_query, values_to_insert)
    conn.commit()

for ip in ips_usr:
    lat, lon = get_geolocation(ip)
    
    if lat and lon:
        values_to_insert = (ip, lat, lon)
    else:
        values_to_insert = (ip, 0, 0)
    
    cursor.execute(insert_query, values_to_insert)
    conn.commit()
    
conn.close()
import psycopg2
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import datetime
import folium

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="panelesweb",
    user="postgres",
    password="paneles0110",
    # host="localhost"
    host="panelesweb-cluster.cluster-ro-cjgme8oigakn.us-east-1.rds.amazonaws.com"
)

# del df_acciones_cargas_total
ruta_imagenes = '/home/dario/panelesweb/reportes/'

dia_inicio_periodo = '2024-12-01'

# Obtener la fecha y hora actual
datetime_actual = datetime.datetime.now()

mes_hoy = '0' + str(datetime_actual.month)
mes_hoy = mes_hoy[-2:]
dia_de_hoy = str(datetime_actual.year) + '-' + str(datetime_actual.month).zfill(2) + '-' + str(datetime_actual.day).zfill(2)

def nombre_accion(id_origen):
    if id_origen == 0:
        return 'Carga Aceptada Manual'
    elif id_origen == -1:
        return 'Carga Automática Por Confianza'
    elif id_origen == 1:
        return 'Carga Automática MercadoPago'
    elif id_origen == 2:
        return 'Carga Automática PanelesNoti'
    elif id_origen == 3:
        return 'Carga Automática TuCash'
    else:
        return 'Carga No Identificada'
    
# REPORTE CONCURRENCIA
sql_concurrencia = "SELECT * FROM concurrencia_registro WHERE id_proceso = 1 and fecha_hora >= NOW() - INTERVAL '24 hours'"
# sql_concurrencia = "SELECT * FROM concurrencia_registro WHERE id_proceso = 1 and fecha_hora >= '2024-08-30 21:13:00'" # NOW() - INTERVAL '24 hours'"
# Ejecutar la consulta y cargar los resultados en un DataFrame de pandas
df_concurrencia = pd.read_sql(sql_concurrencia, conn)


# sql_concurrencia_demora = "SELECT date_trunc('minute', fecha_hora) AS fecha_hora, COUNT(*) AS aplicadas "
# sql_concurrencia_demora = sql_concurrencia_demora + "FROM concurrencia_registro_aplicadas "
# sql_concurrencia_demora = sql_concurrencia_demora + "WHERE id_proceso = 1 AND resultado = 'ok' "
# sql_concurrencia_demora = sql_concurrencia_demora + "AND fecha_hora >= NOW() - INTERVAL '24 hours' GROUP BY date_trunc('minute', fecha_hora)"
# df_concurrencia_demora = pd.read_sql(sql_concurrencia_demora, conn)

df_concurrencia_error = "SELECT date_trunc('minute', fecha_hora) AS fecha_hora, COUNT(*) AS errores "
df_concurrencia_error = df_concurrencia_error + "FROM v_Console_Logs "
df_concurrencia_error = df_concurrencia_error + "WHERE mensaje_log like 'Error en la Carga:%' "
df_concurrencia_error = df_concurrencia_error + "AND fecha_hora >= NOW() - INTERVAL '24 hours' "
df_concurrencia_error = df_concurrencia_error + "GROUP BY date_trunc('minute', fecha_hora);"

df_concurrencia_error = pd.read_sql(df_concurrencia_error, conn)

# Crear un rango de fechas y horas con todas las posibles para el día
fecha_inicio = df_concurrencia['fecha_hora'].min().normalize()  # Normaliza al principio del día
fecha_fin = fecha_inicio + pd.DateOffset(days=1) - pd.DateOffset(minutes=1)

# Generar un rango de tiempo con intervalos de una hora para completar las 24 horas
rango_completo = pd.date_range(start=fecha_inicio, end=fecha_fin, freq='H')

# Crear un nuevo DataFrame basado en el rango completo
df_error_completo = pd.DataFrame({'fecha_hora': rango_completo, 'errores' : 0})

# Fusionar ambos DataFrames para completar las horas faltantes
df_concurrencia_error_final = pd.concat([df_error_completo, df_concurrencia_error])

df_concurrencia_error_final = df_concurrencia_error_final.sort_values(by='fecha_hora', ascending=False)


# Crear subtramas
# fig, (ax1, ax2, ax3) = plt.subplots(3, figsize=(10, 6))
fig, (ax1, ax2) = plt.subplots(2, figsize=(10, 6))

# Trazar la primera gráfica de línea en la primera subtrama
sns.lineplot(data=df_concurrencia, x='fecha_hora', y='activos', ax=ax1, label='Cargas En Curso', color='green')
# Trazar la primera gráfica de línea en la primera subtrama
# sns.lineplot(data=df_concurrencia, x='fecha_hora', y='en_espera', ax=ax2, label='Cargas En Espera', color='blue')
# Trazar la segunda gráfica de línea en la segunda subtrama
# sns.lineplot(data=df_concurrencia_demora, x='fecha_hora', y='aplicadas', ax=ax3, label='Aplicadas con Espera', color='red')
sns.lineplot(data=df_concurrencia_error_final, x='fecha_hora', y='errores', ax=ax2, label='Cargas Con Error', color='red')

# Agregar título al gráfico completo
fig.suptitle('Concurrencia de Procesos de Cargas de Fichas')

ax1.set_ylim(bottom=0)
ax2.set_ylim(bottom=0)
# ax3.set_ylim(bottom=0)

# Etiquetas de ejes
ax1.set_xlabel('Fecha - Hora')
ax1.set_ylabel('# Procesos')
ax2.set_xlabel('Fecha - Hora')
ax2.set_ylabel('# Procesos')
# ax3.set_xlabel('Fecha - Hora')
# ax3.set_ylabel('# Procesos')

# Ajustar diseño y mostrar
plt.tight_layout()
ax1.grid(True)
ax2.grid(True)
# ax3.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_concurrencias.png', bbox_inches='tight')
# Mostrar el gráfico
# plt.show()
plt.clf()
del df_concurrencia
del df_error_completo
del df_concurrencia_error
del df_concurrencia_error_final

# REPORTE CONCURRENCIA DE ESCRITURAS EN BASE DE DATOS
sql_concurrencia_bd = "SELECT * FROM concurrencia_registro WHERE id_proceso = 2 and fecha_hora >= NOW() - INTERVAL '24 hours' and activos > 0"
df_concurrencia_bd = pd.read_sql(sql_concurrencia_bd, conn)

# Crear subtramas
# fig, (ax1, ax2, ax3) = plt.subplots(3, figsize=(10, 6))
fig, (ax1) = plt.subplots(1, figsize=(10, 6))

# Trazar la primera gráfica de línea en la primera subtrama
sns.lineplot(data=df_concurrencia_bd, x='fecha_hora', y='activos', ax=ax1, label='Escrituras en Base de Datos', color='green')

# Agregar título al gráfico completo
fig.suptitle('Concurrencia de Procesos de Escritura en Base de Datos')

# ax1.set_ylim(bottom=0)

# Etiquetas de ejes
ax1.set_xlabel('Fecha - Hora')
ax1.set_ylabel('# Procesos')

# Ajustar diseño y mostrar
# plt.tight_layout()
ax1.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_concurrencias_bd.png', bbox_inches='tight')
# Mostrar el gráfico
# plt.show()
plt.clf()
del df_concurrencia_bd

# REPORTE SOCKETS
sql_sockets = "SELECT * FROM rpt_Registro_Sesiones_Sockets_Ult24hs"
df_sockets = pd.read_sql(sql_sockets, conn)

sql_sockets_total = "SELECT 'Todas'	as oficina,fecha_hora,SUM(cantidad) AS cantidad FROM rpt_Registro_Sesiones_Sockets_Ult24hs GROUP BY fecha_hora"
df_sockets_total = pd.read_sql(sql_sockets_total, conn)

# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
ax1 = sns.lineplot(data=df_sockets_total, x='fecha_hora', y='cantidad', label='Todas')

# Ajustar el diseño del gráfico
plt.title('Sesiones Activas de Usuario')
plt.xlabel('Fecha')
plt.ylabel('Cantidad')
plt.legend(title='Oficina')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')

plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas/rpt_sockets.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()

for id_oficina in df_sockets.id_oficina.unique():
    # Crear el gráfico de líneas utilizando Seaborn
    plt.figure(figsize=(15, 10))
    ax1 = sns.lineplot(data=df_sockets[df_sockets.id_oficina == id_oficina], x='fecha_hora', y='cantidad', label=df_sockets[df_sockets.id_oficina == id_oficina].oficina.unique()[0])
    
    # Ajustar el diseño del gráfico
    plt.title('Sesiones Activas de Usuario')
    plt.xlabel('Fecha')
    plt.ylabel('Cantidad')
    plt.legend(title='Oficina')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    # Rotar los labels del eje y a 45 grados
    ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
    
    plt.grid(True)
    
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes + 'oficina_' + str(id_oficina) + '/rpt_sockets.png', bbox_inches='tight')
    
    # Mostrar el gráfico
    # plt.show()
    plt.clf()

del df_sockets
del df_sockets_total

# REPORTE PETICIONES
sql_peticiones = "SELECT * FROM rpt_Peticiones_Nginx_Ult24hs WHERE peticiones > 0;"
df_peticiones = pd.read_sql(sql_peticiones, conn)

# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
ax1 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 1], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 1].servidor.unique()[0])
ax2 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 2], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 2].servidor.unique()[0])

# Ajustar el diseño del gráfico
plt.title('Peticiones Servidores Nginx')
plt.xlabel('Fecha')
plt.ylabel('Cantidad')
plt.legend(title='Servidor')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')

plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas' + '/rpt_peticiones_nginx.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()
del df_peticiones

# REPORTE PETICIONES OPERADOR
sql_peticiones = "SELECT * FROM rpt_Peticiones_Operador_Nginx_Ult24hs WHERE peticiones > 0;"
df_peticiones = pd.read_sql(sql_peticiones, conn)

# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
ax1 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 1], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 1].servidor.unique()[0])
ax2 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 3], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 3].servidor.unique()[0])
ax3 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 4], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 4].servidor.unique()[0])
ax4 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 5], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 5].servidor.unique()[0])
ax5 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 6], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 6].servidor.unique()[0])

# Ajustar el diseño del gráfico
plt.title('Peticiones Servidores Nginx')
plt.xlabel('Fecha')
plt.ylabel('Cantidad')
plt.legend(title='Servidor')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')
ax4.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')
ax5.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')

plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas' + '/rpt_peticiones_operador.png', bbox_inches='tight')

# REPORTE PETICIONES NOTIFICACIONES
sql_peticiones = "SELECT * FROM rpt_Peticiones_Notificaciones_Nginx_Ult24hs WHERE peticiones > 0;"
df_peticiones = pd.read_sql(sql_peticiones, conn)

# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
ax1 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 7], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 7].servidor.unique()[0])
ax2 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 8], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 8].servidor.unique()[0])
ax3 = sns.lineplot(data=df_peticiones[df_peticiones.id_servidor == 9], x='fecha_hora', y='peticiones', label=df_peticiones[df_peticiones.id_servidor == 9].servidor.unique()[0])

# Ajustar el diseño del gráfico
plt.title('Peticiones Servidores Nginx')
plt.xlabel('Fecha')
plt.ylabel('Cantidad')
plt.legend(title='Servidor')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
ax2.set_xticklabels(ax2.get_xticklabels(), rotation=45, ha='right')
ax3.set_xticklabels(ax3.get_xticklabels(), rotation=45, ha='right')

plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas' + '/rpt_peticiones_notificaciones.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()
plt.clf()
del df_peticiones

# REPORTE ACCIONES DIARIO
sql_query_1 = "SELECT * FROM rpt_Acciones_Diarias where fecha = '" + dia_de_hoy + "';"
# Ejecutar la consulta y cargar los resultados en un DataFrame de pandas
df_acciones = pd.read_sql(sql_query_1, conn)

df_acciones['accion'] = df_acciones.apply(lambda row: row['accion'] if row['id_accion'] > 1 else nombre_accion(row['id_origen_notificacion']), axis=1)
df_acciones_todas = df_acciones[df_acciones['id_oficina'] == 0]
df_acciones = df_acciones[df_acciones['id_oficina'] > 0]

# Crear el barplot con Seaborn
plt.figure(figsize=(10, 10))
df_acciones_todas_agrupado = df_acciones_todas.groupby(['accion'])['cantidad'].sum().reset_index()
ax = sns.barplot(data=df_acciones_todas_agrupado, x='accion', y='cantidad')

# Añadir las etiquetas de texto dentro de las barras
for p in ax.patches:
    ax.annotate(format(p.get_height(), '.2f'), 
                (p.get_x() + p.get_width() / 2., p.get_height()), 
                ha = 'center', va = 'center', 
                xytext = (0, 9), 
                textcoords = 'offset points')

# Ajustar el diseño del gráfico
titulo = 'Actividad del día ' + dia_de_hoy + ' (hasta ' + str(datetime_actual.hour) + ':' + str(datetime_actual.minute) + ')'
plt.title(titulo)
plt.xlabel('Acción')
plt.ylabel('Cantidad')
plt.grid(True)
# Rotar los labels del eje y a 45 grados
ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')

# Guardar la imagen en disco
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_actividad_dia.png', bbox_inches='tight')
# plt.show()
plt.clf()

# Crear el gráfico de torta cargas
s_acciones = df_acciones_todas[df_acciones_todas['id_accion'].isin([1, 5])].groupby(['accion'])['cantidad'].sum()
plt.figure(figsize=(8, 8))
plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
plt.title(titulo + ' - Proporción de Medios de Cargas - Total Cargas : ' + str(s_acciones.sum()))
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_actividad_torta_dia_cargas.png', bbox_inches='tight')
# plt.show()
plt.clf()

# Crear el gráfico de torta plataformas
s_plataformas = df_acciones_todas[df_acciones_todas['id_accion'].isin([1, 5])].groupby(['plataforma'])['cantidad'].sum()
plt.figure(figsize=(8, 8))
plt.pie(s_plataformas, labels=s_plataformas.index, autopct='%1.1f%%', startangle=140)
plt.title(titulo + ' - Proporción de Cargas en Plataformas')
plt.savefig(ruta_imagenes  + 'oficina_todas' + '/rpt_actividad_torta_dia_plataformas.png', bbox_inches='tight')
# plt.show()
plt.clf()

for id_oficina in df_acciones.id_oficina.unique():
    df_acciones_ofi = df_acciones[df_acciones['id_oficina'] == id_oficina]
    # Crear el barplot con Seaborn
    plt.figure(figsize=(10, 10))
    df_acciones_ofi_agrupado = df_acciones_ofi.groupby(['accion'])['cantidad'].sum().reset_index()
    oficina = df_acciones_ofi.oficina.unique()[0]
    ax = sns.barplot(data=df_acciones_ofi_agrupado, x='accion', y='cantidad')
    # Añadir las etiquetas de texto dentro de las barras
    for p in ax.patches:
        ax.annotate(format(p.get_height(), '.2f'), 
                    (p.get_x() + p.get_width() / 2., p.get_height()), 
                    ha = 'center', va = 'center', 
                    xytext = (0, 9), 
                    textcoords = 'offset points')
    
    # Ajustar el diseño del gráfico
    titulo = oficina + ' - Actividad del día ' + dia_de_hoy + ' (hasta ' + str(datetime_actual.hour) + ':' + str(datetime_actual.minute) + ')'
    plt.title(titulo)
    plt.xlabel('Categoría')
    plt.ylabel('Cantidad')
    plt.grid(True)
    # Rotar los labels del eje y a 45 grados
    ax.set_xticklabels(ax.get_xticklabels(), rotation=45, ha='right')
    
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_actividad_dia.png', bbox_inches='tight')
    # plt.show()
    plt.clf()
    
    # Crear el gráfico de torta cargas
    s_acciones = df_acciones_ofi[df_acciones_ofi['id_accion'].isin([1, 5])].groupby(['accion'])['cantidad'].sum()
    plt.figure(figsize=(8, 8))
    plt.pie(s_acciones, labels=s_acciones.index, autopct='%1.1f%%', startangle=140)
    plt.title(titulo + ' - Proporción de Medios de Cargas - Total Cargas : ' + str(s_acciones.sum()))
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_actividad_torta_dia_cargas.png', bbox_inches='tight')
    # plt.show()
    plt.clf()
    
    # Crear el gráfico de torta plataformas
    s_plataformas = df_acciones_todas[df_acciones_todas['id_accion'].isin([1, 5])].groupby(['plataforma'])['cantidad'].sum()
    plt.figure(figsize=(8, 8))
    plt.pie(s_plataformas, labels=s_plataformas.index, autopct='%1.1f%%', startangle=140)
    plt.title(titulo + ' - Proporción de Cargas en Plataformas')
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_actividad_torta_dia_plataformas.png', bbox_inches='tight')
    # plt.show()
    plt.clf()     

del df_acciones
del df_acciones_todas
del df_acciones_todas_agrupado
del df_acciones_ofi
del df_acciones_ofi_agrupado
del s_acciones
del s_plataformas

# Crear el gráfico de Chat de las últimas 24hs
sql_chat = "SELECT * FROM rpt_Chat_Ult24hs;"
df_chat = pd.read_sql(sql_chat, conn)

df_chat_todas = df_chat[(df_chat.id_oficina == 0)]
df_chat_oficina = df_chat[(df_chat.id_oficina > 0)]

# Crear el gráfico de líneas utilizando Seaborn
plt.figure(figsize=(15, 10))
ax1 = sns.lineplot(data=df_chat_todas[(df_chat_todas.origen == 'Operador')], x='fecha_hora', y='cantidad', label='Operador')
ax2 = sns.lineplot(data=df_chat_todas[(df_chat_todas.origen == 'Cliente')], x='fecha_hora', y='cantidad', label='Cliente')

# Ajustar el diseño del gráfico
plt.title('Cantidad de Mensajes')
plt.xlabel('Fecha')
plt.ylabel('Cantidad')
plt.legend(title='Origen')
plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
# Rotar los labels del eje y a 45 grados
ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
ax2.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')

plt.grid(True)

# Guardar la imagen en disco
plt.savefig(ruta_imagenes + 'oficina_todas' + '/rpt_chat_ult24.png', bbox_inches='tight')

# Mostrar el gráfico
# plt.show()

for id_oficina in df_chat_oficina.id_oficina.unique():
    # Crear el gráfico de líneas utilizando Seaborn
    plt.figure(figsize=(15, 10))
    ax1 = sns.lineplot(data=df_chat_oficina[(df_chat_oficina.id_oficina == id_oficina) & (df_chat_oficina.origen == 'Operador')], x='fecha_hora', y='cantidad', label='Operador')
    ax2 = sns.lineplot(data=df_chat_oficina[(df_chat_oficina.id_oficina == id_oficina) & (df_chat_oficina.origen == 'Cliente')], x='fecha_hora', y='cantidad', label='Cliente')

    oficina = df_chat_oficina[(df_chat_oficina.id_oficina == id_oficina)].oficina.unique()[0]
    # Ajustar el diseño del gráfico
    titulo = oficina + ' Cantidad de Mensajes'
    plt.title(titulo)
    plt.xlabel('Fecha')
    plt.ylabel('Cantidad')
    plt.legend(title='Origen')
    plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left')
    # Rotar los labels del eje y a 45 grados
    ax1.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
    ax2.set_xticklabels(ax1.get_xticklabels(), rotation=45, ha='right')
    
    plt.grid(True)
    
    # Guardar la imagen en disco
    plt.savefig(ruta_imagenes  + 'oficina_' + str(id_oficina) + '/rpt_chat_ult24.png', bbox_inches='tight')
    # Mostrar el gráfico
    # plt.show()    

del df_chat
del df_chat_todas
del df_chat_oficina

#MAPA DE VISITAS A LA LANDING
sql_ip_ubicaciones = "SELECT DISTINCT u.ip, u.latitud, u.longitud, cl.cliente_usuario, o.oficina FROM ip_ubicacion u JOIN cliente_sesion cs"
sql_ip_ubicaciones = sql_ip_ubicaciones + " ON (u.ip = cs.ip AND u.ip NOT IN (select ip from ip_lista_negra)"
sql_ip_ubicaciones = sql_ip_ubicaciones + " AND fecha_hora_creacion >= NOW() - INTERVAL '6 hours')"
sql_ip_ubicaciones = sql_ip_ubicaciones + " JOIN cliente cl ON (cs.id_cliente = cl.id_cliente) JOIN agente ag ON (ag.id_agente = cl.id_agente)"
sql_ip_ubicaciones = sql_ip_ubicaciones + " JOIN oficina o ON (ag.id_oficina = o.id_oficina)"
df_IP_Uicaciones = pd.read_sql(sql_ip_ubicaciones, conn)

# Crear un mapa centrado en un punto medio
map_center = [df_IP_Uicaciones['latitud'].mean(), df_IP_Uicaciones['longitud'].mean()]
mymap = folium.Map(location=map_center, zoom_start=5)

for index, row in df_IP_Uicaciones.iterrows():
    folium.Marker(
        location=[row['latitud'], row['longitud']],
        popup=f"IP: {row['ip']}, Usuario : {row['cliente_usuario']}, Oficina : {row['oficina']}",
        icon=folium.Icon(color="blue", icon="info-sign")
    ).add_to(mymap)
    
mymap.save(ruta_imagenes + 'oficina_todas/ip_map.html')

del df_IP_Uicaciones

#MAPA DE INICIOS DE SESION DE USUARIOS
sql_ip_ubicaciones = "SELECT DISTINCT ipu.ip, ipu.latitud, ipu.longitud, u.usuario, o.oficina FROM ip_ubicacion ipu JOIN usuario_sesion us"
sql_ip_ubicaciones = sql_ip_ubicaciones + " ON (ipu.ip = us.ip AND ipu.ip NOT IN (select ip from ip_lista_negra)"
sql_ip_ubicaciones = sql_ip_ubicaciones + " AND us.fecha_hora_creacion >= NOW() - INTERVAL '6 hours')"
sql_ip_ubicaciones = sql_ip_ubicaciones + " JOIN usuario u ON (u.id_usuario = us.id_usuario)"
sql_ip_ubicaciones = sql_ip_ubicaciones + " JOIN oficina o ON (u.id_oficina = o.id_oficina)"
df_IP_Uicaciones = pd.read_sql(sql_ip_ubicaciones, conn)

# Crear un mapa centrado en un punto medio
map_center = [df_IP_Uicaciones['latitud'].mean(), df_IP_Uicaciones['longitud'].mean()]
mymap = folium.Map(location=map_center, zoom_start=5)

for index, row in df_IP_Uicaciones.iterrows():
    folium.Marker(
        location=[row['latitud'], row['longitud']],
        popup=f"IP: {row['ip']}, Usuario : {row['usuario']}, Oficina : {row['oficina']}",
        icon=folium.Icon(color="blue", icon="info-sign")
    ).add_to(mymap)
    
mymap.save(ruta_imagenes + 'oficina_todas/ip_map_usr.html')

del df_IP_Uicaciones

conn.close()
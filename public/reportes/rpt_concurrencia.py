import psycopg2
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import datetime

# Establece la conexión a la base de datos PostgreSQL
conn = psycopg2.connect(
    dbname="paneles",
    user="postgres",
    password="paneles0110",
    host="localhost"
)

sql_concurrencia = "SELECT Semaforo_Concurrencia_Registro();"

# Crear un cursor para ejecutar consultas
cur = conn.cursor()

# Ejecutar una consulta SQL
cur.execute(sql_concurrencia)

conn.commit()

# Cerrar el cursor y la conexión
cur.close()
conn.close()
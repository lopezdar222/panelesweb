DROP TABLE IF EXISTS usuario;

CREATE TABLE IF NOT EXISTS usuario
(
    id_usuario serial NOT NULL,
    usuario character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    id_rol integer NOT NULL,
    id_oficina integer NOT NULL,
	recibe_retiro boolean NOT NULL DEFAULT true,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_creacion timestamp NOT NULL,
    id_usuario_creacion integer NOT NULL,
    fecha_hora_ultima_modificacion timestamp,
    id_usuario_ultima_modificacion integer NOT NULL,
    PRIMARY KEY (id_usuario)
);
CREATE INDEX usuario_id_rol ON usuario (id_rol);
CREATE INDEX usuario_id_oficina ON usuario (id_oficina);

DROP TABLE IF EXISTS usuario_sesion;
CREATE TABLE IF NOT EXISTS usuario_sesion
(
    id_usuario_sesion serial NOT NULL,
    id_usuario integer NOT NULL,
    id_token character varying(30) NOT NULL DEFAULT '',
    ip character varying(30) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_cierre timestamp,
    cierre_abrupto boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_usuario_sesion)
);
CREATE INDEX usuario_sesion_id_usuario ON usuario_sesion (id_usuario);
CREATE INDEX usuario_sesion_ip ON usuario_sesion (ip);

DROP TABLE IF EXISTS console_logs; 
CREATE TABLE IF NOT EXISTS console_logs
(
    id serial NOT NULL,
    mensaje_log character varying(300),
    fecha_hora timestamp NOT NULL,
    PRIMARY KEY (id)
);

DROP TABLE IF EXISTS rol;
CREATE TABLE IF NOT EXISTS rol
(
    id_rol serial NOT NULL,
    nombre_rol character varying(50) NOT NULL,
    fecha_hora_creacion timestamp NOT NULL,
    marca_baja boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_rol)
);

DROP TABLE IF EXISTS oficina;
CREATE TABLE IF NOT EXISTS oficina
(
    id_oficina serial NOT NULL,
    oficina character varying(100) NOT NULL,
    contacto_whatsapp character varying(200) NOT NULL DEFAULT '',
    contacto_telegram character varying(200) NOT NULL DEFAULT '',
	bono_carga_1 integer NOT NULL DEFAULT 0,
	bono_carga_perpetua integer NOT NULL DEFAULT 0,
	minimo_carga integer NOT NULL DEFAULT 0,
	minimo_retiro integer NOT NULL DEFAULT 0,
	minimo_espera_retiro integer NOT NULL DEFAULT 24,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_creacion timestamp NOT NULL,
	id_usuario_creacion integer NOT NULL DEFAULT 0,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_oficina)
);

DROP TABLE IF EXISTS cuenta_bancaria;
CREATE TABLE IF NOT EXISTS cuenta_bancaria
(
    id_cuenta_bancaria serial NOT NULL,
	id_oficina integer not null,
    nombre character varying(200) NOT NULL,
    alias character varying(200) NOT NULL,
    cbu character varying(200) NOT NULL,
	id_billetera integer NOT NULL DEFAULT 1,
	id_usuario_noti integer NOT NULL DEFAULT 0,
	id_agente integer NOT NULL DEFAULT 0,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_creacion timestamp NOT NULL,
	id_usuario_creacion integer NOT NULL DEFAULT 0,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
	marca_eliminada boolean NOT NULL DEFAULT false
    PRIMARY KEY (id_cuenta_bancaria)
);
CREATE INDEX cuenta_bancaria_id_oficina ON cuenta_bancaria (id_oficina);
CREATE INDEX cuenta_bancaria_id_billetera ON cuenta_bancaria (id_billetera);

select * from cuenta_bancaria_hist where id_cuenta_bancaria = 1036 order by 1 desc

CREATE TABLE IF NOT EXISTS cuenta_bancaria_hist
(
    id_cuenta_bancaria_hist serial NOT NULL,
	id_cuenta_bancaria integer not null,
	id_oficina integer not null,
    nombre character varying(200) NOT NULL,
    alias character varying(200) NOT NULL,
    cbu character varying(200) NOT NULL,
	id_billetera integer NOT NULL DEFAULT 1,
	id_usuario_noti integer NOT NULL DEFAULT 0,
	id_agente integer NOT NULL DEFAULT 0,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
	marca_eliminada boolean NOT NULL DEFAULT false
);
CREATE INDEX cuenta_bancaria_hist_id_cuenta_bancaria ON cuenta_bancaria_hist (id_cuenta_bancaria);

DROP TABLE IF EXISTS billetera;
CREATE TABLE IF NOT EXISTS billetera
(
    id_billetera serial NOT NULL,
    billetera character varying(100) NOT NULL,
    notificacion_descripcion character varying(100) NOT NULL DEFAULT '',
    notificacion_activa boolean NOT NULL DEFAULT true,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_creacion timestamp NOT NULL,
	id_usuario_creacion integer NOT NULL DEFAULT 0,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_billetera)
);
--select * from cuenta_bancaria_descarga order by 1 desc

DROP TABLE IF EXISTS cuenta_bancaria_descarga;
CREATE TABLE IF NOT EXISTS cuenta_bancaria_descarga
(
    id_cuenta_bancaria_descarga serial NOT NULL,
	id_cuenta_bancaria integer NOT NULL,
	id_usuario integer not null,
    fecha_hora_descarga timestamp NOT NULL,
    cargas_monto numeric NOT NULL,
    cargas_cantidad integer NOT NULL,
    marca_baja boolean NOT NULL DEFAULT false,
	monto_restante numeric NOT NULL DEFAULT 0,
	ultima_descarga boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_cuenta_bancaria_descarga)
);
CREATE INDEX cuenta_bancaria_descarga_id_cuenta_bancaria ON cuenta_bancaria_descarga (id_cuenta_bancaria);
CREATE INDEX cuenta_bancaria_descarga_id_usuario ON cuenta_bancaria_descarga (id_usuario);
CREATE INDEX cuenta_bancaria_descarga_fecha_hora_descarga ON cuenta_bancaria_descarga (fecha_hora_descarga DESC);

DROP TABLE IF EXISTS plataforma;
CREATE TABLE IF NOT EXISTS plataforma
(
    id_plataforma serial NOT NULL,
    plataforma character varying(200) NOT NULL,
    url_admin character varying(200) NOT NULL,
    url_juegos character varying(200) NOT NULL,
	imagen character varying(200) NOT NULL DEFAULT '-',
    marca_baja boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_plataforma)
);

DROP TABLE IF EXISTS agente;
CREATE TABLE IF NOT EXISTS agente
(
    id_agente serial NOT NULL,
    agente_usuario character varying(200) NOT NULL,
    agente_password character varying(200) NOT NULL,
    id_plataforma integer NOT NULL,
    id_oficina integer NOT NULL,
    marca_baja boolean NOT NULL DEFAULT false,
	tokens_bono_creacion INTEGER NOT NULL DEFAULT 0,
	tokens_bono_carga_1 INTEGER NOT NULL DEFAULT 0,
	tokens_bono_carga_1_porcentaje BOOLEAN NOT NULL DEFAULT true,
    fecha_hora_creacion timestamp NOT NULL,
	id_usuario_creacion integer NOT NULL DEFAULT 0,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_agente)
);
CREATE INDEX agente_id_oficina ON agente (id_oficina);
CREATE INDEX agente_id_plataforma ON agente (id_plataforma);

DROP TABLE IF EXISTS cliente;
CREATE TABLE IF NOT EXISTS cliente
(
    id_cliente serial NOT NULL,
    cliente_usuario character varying(200) NOT NULL,
    cliente_password character varying(200) NOT NULL,
	id_cliente_ext BIGINT NOT NULL DEFAULT 0,
	id_cliente_db INTEGER NOT NULL DEFAULT 0,
    id_agente integer NOT NULL,
    en_sesion boolean NOT NULL DEFAULT false,
    marca_baja boolean NOT NULL DEFAULT false,
	bloqueado boolean NOT NULL DEFAULT false,
	id_registro_token integer NOT NULL DEFAULT 0,
	correo_electronico character varying(100) NOT NULL DEFAULT '',
	telefono character varying(100) NOT NULL DEFAULT '',
	cliente_nombre character varying(200) NOT NULL DEFAULT '',
	cliente_apellido character varying(200) NOT NULL DEFAULT ''
    fecha_hora_creacion timestamp NOT NULL,
	id_usuario_creacion integer NOT NULL DEFAULT 0,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_cliente)
);
CREATE INDEX cliente_id_agente ON cliente (id_agente);
--select * from cliente order by 1 limit 100

DROP TABLE IF EXISTS cliente_sesion;
CREATE TABLE IF NOT EXISTS cliente_sesion
(
    id_cliente_sesion serial NOT NULL,
    id_cliente integer NOT NULL,
    id_token character varying(100) NOT NULL DEFAULT '',
    ip character varying(100) NOT NULL DEFAULT '',
    moneda character varying(30),
    monto numeric,
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_cierre timestamp,
    cierre_abrupto boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_cliente_sesion)
);
CREATE INDEX cliente_sesion_id_cliente ON cliente_sesion (id_cliente);
CREATE INDEX cliente_sesion_ip ON cliente_sesion (ip);

DROP TABLE IF EXISTS cliente_sesion_hist;
CREATE TABLE IF NOT EXISTS cliente_sesion_hist
(
    id_cliente_sesion integer NOT NULL DEFAULT 0,
    id_cliente integer NOT NULL,
    id_token character varying(100) NOT NULL DEFAULT '',
    ip character varying(100) NOT NULL DEFAULT '',
    moneda character varying(30),
    monto numeric,
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_cierre timestamp,
    cierre_abrupto boolean NOT NULL DEFAULT false
);
CREATE INDEX cliente_sesion_hist_id_cliente ON cliente_sesion_hist (id_cliente);
CREATE INDEX cliente_sesion_hist_ip ON cliente_sesion_hist (ip);

DROP TABLE IF EXISTS cliente_sesion_transferencia;
CREATE TABLE IF NOT EXISTS cliente_sesion_transferencia
(
    id_cliente_sesion integer NOT NULL,
    transferido boolean NOT NULL DEFAULT false,
    fecha_hora timestamp NOT NULL
);

DROP TABLE IF EXISTS ip_lista_negra;
CREATE TABLE IF NOT EXISTS ip_lista_negra
(
    ip character varying(100) NOT NULL DEFAULT ''
);
CREATE INDEX ip_lista_negra_ip ON ip_lista_negra (ip);

DROP TABLE IF EXISTS ip_ubicacion;
CREATE TABLE IF NOT EXISTS ip_ubicacion
(
    ip character varying(100) NOT NULL DEFAULT '',
	latitud DOUBLE PRECISION NOT NULL DEFAULT 0,
	longitud DOUBLE PRECISION NOT NULL DEFAULT 0
);
CREATE INDEX ip_ubicacion_ip ON ip_ubicacion (ip);

DROP TABLE IF EXISTS registro_sesiones_sockets;
CREATE TABLE IF NOT EXISTS registro_sesiones_sockets
(
    id_registro_sesiones_sockets serial NOT NULL,
    fecha_hora timestamp NOT NULL,
    conexiones integer NOT NULL DEFAULT 0,
	id_oficina integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_registro_sesiones_sockets)
);

DROP TABLE IF EXISTS cliente_chat;
CREATE TABLE IF NOT EXISTS cliente_chat
(
    id_cliente_chat serial NOT NULL,
    id_cliente integer NOT NULL,
    mensaje character varying(200) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    enviado_cliente boolean NOT NULL DEFAULT true,
    visto_cliente boolean NOT NULL DEFAULT false,
    visto_operador boolean NOT NULL DEFAULT false,
    id_usuario integer NOT NULL DEFAULT 1,
	marca_baja boolean NOT NULL DEFAULT false,
	id_usuario_baja integer,
	fecha_hora_baja timestamp
    PRIMARY KEY (id_cliente_chat)
);
CREATE INDEX cliente_chat_id_cliente ON cliente_chat (id_cliente);
CREATE INDEX cliente_chat_fecha_hora_creacion ON cliente_chat USING BRIN(fecha_hora_creacion);

DROP TABLE IF EXISTS cliente_chat_adjunto;
CREATE TABLE IF NOT EXISTS cliente_chat_adjunto
(
    id_cliente_chat_adjunto serial NOT NULL,
    id_cliente integer NOT NULL,
    nombre_original character varying(200) NOT NULL DEFAULT '',
    nombre_guardado character varying(300) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    enviado_cliente boolean NOT NULL DEFAULT true,
    visto_cliente boolean NOT NULL DEFAULT false,
    visto_operador boolean NOT NULL DEFAULT false,
    id_usuario integer NOT NULL DEFAULT 1,
    PRIMARY KEY (id_cliente_chat_adjunto)
);
CREATE INDEX cliente_chat_adjunto_id_cliente ON cliente_chat_adjunto (id_cliente);

DROP TABLE IF EXISTS visita_sesion;
CREATE TABLE IF NOT EXISTS visita_sesion
(
    id_visita_sesion serial NOT NULL,
    id_token character varying(100) NOT NULL DEFAULT '',
	alias_sesion character varying(100) NOT NULL DEFAULT '',
    ip character varying(100) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_inicio_sesion timestamp DEFAULT NULL,
	fecha_hora_cierre timestamp DEFAULT NULL
    PRIMARY KEY (id_visita_sesion)
);
CREATE INDEX visita_sesion_ip ON visita_sesion (ip);

DROP TABLE IF EXISTS visita_sesion_chat;
CREATE TABLE IF NOT EXISTS visita_sesion_chat
(
    id_visita_sesion_chat serial NOT NULL,
    id_visita_sesion integer NOT NULL,
    mensaje character varying(200) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    enviado_cliente boolean NOT NULL DEFAULT true,
    visto_cliente boolean NOT NULL DEFAULT false,
    visto_operador boolean NOT NULL DEFAULT false,
    id_usuario integer NOT NULL DEFAULT 1,
	marca_baja boolean NOT NULL DEFAULT false,
	id_usuario_baja integer,
	fecha_hora_baja timestamp,
    PRIMARY KEY (id_visita_sesion_chat)
);
CREATE INDEX visita_sesion_chat_id_visita_sesion ON visita_sesion_chat (id_visita_sesion);
CREATE INDEX visita_sesion_chat_fecha_hora_creacion ON visita_sesion_chat USING BRIN(fecha_hora_creacion);

DROP TABLE IF EXISTS visita_sesion_chat_adjunto;
CREATE TABLE IF NOT EXISTS visita_sesion_chat_adjunto
(
    id_visita_sesion_chat_adjunto serial NOT NULL,
    id_visita_sesion integer NOT NULL,
    nombre_original character varying(200) NOT NULL DEFAULT '',
    nombre_guardado character varying(300) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    enviado_cliente boolean NOT NULL DEFAULT true,
    visto_cliente boolean NOT NULL DEFAULT false,
    visto_operador boolean NOT NULL DEFAULT false,
    id_usuario integer NOT NULL DEFAULT 1,
    PRIMARY KEY (id_visita_sesion_chat_adjunto)
);
CREATE INDEX visita_sesion_chat_adjunto_id_visita_sesion ON visita_sesion_chat_adjunto (id_visita_sesion);

DROP TABLE IF EXISTS registro_token;
CREATE TABLE IF NOT EXISTS registro_token
(
    id_registro_token serial NOT NULL,
    id_token character varying(60) NOT NULL DEFAULT '',
    de_agente boolean NOT NULL DEFAULT true,
    activo boolean NOT NULL DEFAULT true,
    id_usuario integer NOT NULL,
    ingresos integer NOT NULL,
    registros integer NOT NULL,
	bono_creacion INTEGER NOT NULL DEFAULT 0,
	bono_carga_1 INTEGER NOT NULL DEFAULT 0,
	bono_carga_1_porcentaje BOOLEAN NOT NULL DEFAULT true,
	observaciones character varying(200) NOT NULL DEFAULT '',
    fecha_hora_creacion timestamp NOT NULL,
    id_usuario_ultima_modificacion integer NOT NULL,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
    PRIMARY KEY (id_registro_token)
);
CREATE INDEX registro_token_id_usuario ON registro_token (id_usuario);

DROP TABLE IF EXISTS operacion;
CREATE TABLE IF NOT EXISTS operacion
(
    id_operacion serial NOT NULL,
    codigo_operacion integer NOT NULL,
    id_accion integer NOT NULL,
    id_cliente integer NOT NULL,
    id_estado integer NOT NULL,
    notificado boolean NOT NULL DEFAULT true,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_operacion)
);
CREATE INDEX operacion_id_accion ON operacion (id_accion);
CREATE INDEX operacion_id_cliente ON operacion (id_cliente);
CREATE INDEX operacion_id_estado ON operacion (id_estado);
CREATE INDEX operacion_fecha_hora_creacion ON operacion (fecha_hora_creacion DESC);
CREATE INDEX operacion_fecha_hora_ultima_modificacion ON operacion (fecha_hora_ultima_modificacion DESC);
CREATE INDEX operacion_id_usuario_ultima_modificacion ON operacion (id_usuario_ultima_modificacion);

DROP TABLE IF EXISTS operacion_hist;
CREATE TABLE IF NOT EXISTS operacion_hist
(
    id_operacion integer NOT NULL,
    codigo_operacion integer NOT NULL,
    id_accion integer NOT NULL,
    id_cliente integer NOT NULL,
    id_estado integer NOT NULL,
    notificado boolean NOT NULL DEFAULT true,
    marca_baja boolean NOT NULL DEFAULT false,
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 0
);
CREATE INDEX operacion_hist_id_operacion ON operacion_hist (id_operacion);
CREATE INDEX operacion_hist_id_accion ON operacion_hist (id_accion);
CREATE INDEX operacion_hist_id_cliente ON operacion_hist (id_cliente);
CREATE INDEX operacion_hist_id_estado ON operacion_hist (id_estado);
CREATE INDEX operacion_hist_fecha_hora_creacion ON operacion_hist (fecha_hora_creacion DESC);
CREATE INDEX operacion_hist_fecha_hora_ultima_modificacion ON operacion_hist (fecha_hora_ultima_modificacion DESC);
CREATE INDEX operacion_hist_id_usuario_ultima_modificacion ON operacion_hist (id_usuario_ultima_modificacion);

DROP TABLE IF EXISTS operacion_transferencia;
CREATE TABLE IF NOT EXISTS operacion_transferencia
(
    id_operacion integer NOT NULL,
    transferido boolean NOT NULL DEFAULT false,
    fecha_hora timestamp NOT NULL
);

DROP TABLE IF EXISTS operacion_carga;
CREATE TABLE IF NOT EXISTS operacion_carga
(
    id_operacion_carga serial NOT NULL,
    id_operacion integer NOT NULL,
    titular character varying(200) NOT NULL,
    importe numeric NOT NULL,
    bono numeric NOT NULL DEFAULT 0,
    id_cuenta_bancaria integer NOT NULL,
	sol_importe numeric NOT NULL DEFAULT 0,
	sol_bono numeric NOT NULL DEFAULT 0,
	sol_id_cuenta_bancaria integer NOT NULL DEFAULT 0,
	observaciones character varying(200) default '',
	procesando BOOLEAN NOT NULL DEFAULT false,
    PRIMARY KEY (id_operacion_carga)
);
CREATE INDEX operacion_carga_id_operacion ON operacion_carga (id_operacion);
CREATE INDEX operacion_carga_id_cuenta_bancaria ON operacion_carga (id_cuenta_bancaria);

DROP TABLE IF EXISTS operacion_carga_hist;
CREATE TABLE IF NOT EXISTS operacion_carga_hist
(
    id_operacion_carga integer NOT NULL,
    id_operacion integer NOT NULL,
    titular character varying(200) NOT NULL,
    importe numeric NOT NULL,
    bono numeric NOT NULL DEFAULT 0,
    id_cuenta_bancaria integer NOT NULL,
	sol_importe numeric NOT NULL DEFAULT 0,
	sol_bono numeric NOT NULL DEFAULT 0,
	sol_id_cuenta_bancaria integer NOT NULL DEFAULT 0,
	observaciones character varying(200) default '',
	procesando BOOLEAN NOT NULL DEFAULT false,
    PRIMARY KEY (id_operacion_carga)
);
CREATE INDEX operacion_carga_hist_id_operacion_carga ON operacion_carga_hist (id_operacion_carga);
CREATE INDEX operacion_carga_hist_id_operacion ON operacion_carga_hist (id_operacion);
CREATE INDEX operacion_carga_hist_id_cuenta_bancaria ON operacion_carga_hist (id_cuenta_bancaria);

DROP TABLE IF EXISTS operacion_retiro;
CREATE TABLE IF NOT EXISTS operacion_retiro
(
    id_operacion_retiro serial NOT NULL,
    id_operacion integer NOT NULL,
    cbu character varying(200) NOT NULL,
    titular character varying(200) NOT NULL,
    importe numeric NOT NULL,
	sol_importe numeric NOT NULL DEFAULT 0,
	observaciones character varying(200) default '',
	id_usuario integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_operacion_retiro)
);
CREATE INDEX operacion_retiro_id_operacion ON operacion_retiro (id_operacion);

DROP TABLE IF EXISTS clientes_operaciones_hist;
CREATE TABLE IF NOT EXISTS clientes_operaciones_hist
(
    id_cliente integer,
    cliente_usuario character varying(200),
	id_cliente_ext bigint,
	id_cliente_db integer,
    id_agente integer,
    agente_usuario character varying(200),
    agente_password character varying(200),
    id_plataforma integer,
    plataforma character varying(200),
    id_oficina integer,
    marca_baja boolean,
    oficina character varying(100),
    id_operacion integer,
    codigo_operacion integer,
    id_estado integer,
    estado character varying(200),
    id_accion integer,
    es_carga integer,
    es_retiro integer,
    accion character varying(200),
    usuario character varying(50),
	cliente_confianza numeric,
    fecha_hora_operacion timestamp,
	fecha_hora_proceso timestamp,
	retiro_importe numeric,
    retiro_cbu character varying(200),
	retiro_titular character varying(200),
	retiro_observaciones character varying(200),
	carga_importe numeric,
	carga_titular character varying(200),
	carga_id_cuenta_bancaria integer,
	carga_bono numeric,
	carga_observaciones character varying(200),
	carga_cuenta_bancaria character varying(200),
	carga_cuenta_bancaria_nombre character varying(200),
	carga_cuenta_bancaria_alias character varying(200),
	carga_cuenta_bancaria_cbu character varying(200),
	id_billetera integer,
	id_usuario_noti integer,
	id_notificacion integer,
	id_origen_notificacion integer,
	carga_automatica integer
);
CREATE INDEX clientes_operaciones_hist_id_cliente ON clientes_operaciones_hist (id_cliente);
CREATE INDEX clientes_operaciones_hist_id_agente ON clientes_operaciones_hist (id_agente);
CREATE INDEX clientes_operaciones_hist_id_oficina ON clientes_operaciones_hist (id_oficina);
CREATE INDEX clientes_operaciones_hist_id_operacion ON clientes_operaciones_hist (id_operacion);

DROP TABLE IF EXISTS clientes_operaciones_diario;
CREATE TABLE IF NOT EXISTS clientes_operaciones_diario
(
    id_cliente integer,
    cliente_usuario character varying(200),
	id_cliente_ext bigint,
	id_cliente_db integer,
    id_agente integer,
    agente_usuario character varying(200),
    agente_password character varying(200),
    id_plataforma integer,
    plataforma character varying(200),
    id_oficina integer,
    marca_baja boolean,
    oficina character varying(100),
    id_operacion integer,
    codigo_operacion integer,
    id_estado integer,
    estado character varying(200),
    id_accion integer,
    es_carga integer,
    es_retiro integer,
    accion character varying(200),
    usuario character varying(50),
	cliente_confianza numeric,
    fecha_hora_operacion timestamp,
	fecha_hora_proceso timestamp,
	retiro_importe numeric,
    retiro_cbu character varying(200),
	retiro_titular character varying(200),
	retiro_observaciones character varying(200),
	carga_importe numeric,
	carga_titular character varying(200),
	carga_id_cuenta_bancaria integer,
	carga_bono numeric,
	carga_observaciones character varying(200),
	carga_cuenta_bancaria character varying(200),
	carga_cuenta_bancaria_nombre character varying(200),
	carga_cuenta_bancaria_alias character varying(200),
	carga_cuenta_bancaria_cbu character varying(200),
	id_billetera integer,
	id_usuario_noti integer,
	id_notificacion integer,
	id_origen_notificacion integer,
	carga_automatica integer
);
CREATE INDEX clientes_operaciones_diario_id_cliente ON clientes_operaciones_diario (id_cliente);
CREATE INDEX clientes_operaciones_diario_id_agente ON clientes_operaciones_diario (id_agente);
CREATE INDEX clientes_operaciones_diario_id_oficina ON clientes_operaciones_diario (id_oficina);
CREATE INDEX clientes_operaciones_diario_id_operacion ON clientes_operaciones_diario (id_operacion);

DROP TABLE IF EXISTS accion;
CREATE TABLE IF NOT EXISTS accion
(
    id_accion serial NOT NULL,
    accion character varying(200) NOT NULL,
    marca_baja boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_accion)
);

DROP TABLE IF EXISTS estado;
CREATE TABLE IF NOT EXISTS estado
(
    id_estado serial NOT NULL,
    estado character varying(200) NOT NULL,
    marca_baja boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_estado)
);

DROP TABLE IF EXISTS cuenta_bancaria_mercado_pago;
CREATE TABLE IF NOT EXISTS cuenta_bancaria_mercado_pago
(
    id_cuenta_bancaria_mercado_pago serial NOT NULL,
	id_cuenta_bancaria integer NOT NULL,
    access_token varchar(200) NOT NULL DEFAULT '-',
    public_key varchar(200) NOT NULL DEFAULT '-',
    client_id varchar(200) NOT NULL DEFAULT '-',
    client_secret varchar(200) NOT NULL DEFAULT '-',
    marca_baja boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_cuenta_bancaria_mercado_pago)
);
CREATE INDEX cuenta_bancaria_mercado_pago_id_cuenta_bancaria ON cuenta_bancaria_mercado_pago (id_cuenta_bancaria);

DROP TABLE IF EXISTS concurrencia_registro;
CREATE TABLE IF NOT EXISTS concurrencia_registro
(
    id_concurrencia_registro serial NOT NULL,
    fecha_hora timestamp,
    id_proceso integer NOT NULL DEFAULT 0,
    activos integer NOT NULL DEFAULT 0,
    en_espera integer NOT NULL DEFAULT 0,
    tope integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_concurrencia_registro)
);

DROP TABLE IF EXISTS concurrencia_registro_aplicadas;
CREATE TABLE IF NOT EXISTS concurrencia_registro_aplicadas
(
    id_concurrencia_registro_aplicadas serial NOT NULL,
    fecha_hora timestamp,
    id_proceso integer NOT NULL DEFAULT 0,
    id_notificacion_carga integer NOT NULL DEFAULT 0,
    resultado varchar(50) NOT NULL DEFAULT '-',
    PRIMARY KEY (id_concurrencia_registro_aplicadas)
);

DROP TABLE IF EXISTS sesion_recupero_whatsapp;
CREATE TABLE IF NOT EXISTS sesion_recupero_whatsapp
(
    id_sesion_recupero_whatsapp serial NOT NULL,
    id_usuario integer NOT NULL,
	numero_telefono character varying(100) NOT NULL,
    id_estado integer NOT NULL,
    fecha_hora_creacion timestamp NOT NULL,
    fecha_hora_cierre timestamp,
    PRIMARY KEY (id_sesion_recupero_whatsapp)
);
CREATE INDEX sesion_recupero_whatsapp_id_usuario ON sesion_recupero_whatsapp (id_usuario);
CREATE INDEX sesion_recupero_whatsapp_numero_telefono ON sesion_recupero_whatsapp (numero_telefono);
--update sesion_recupero_whatsapp set id_estado = 2, fecha_hora_cierre = now() where id_estado = 1

/*Recupero especial de clientes de Pina*/
DROP TABLE IF EXISTS contactos_recupero;
CREATE TABLE IF NOT EXISTS contactos_recupero
(
    id_contactos_recupero serial NOT NULL,
	numero_telefono character varying(100) NOT NULL DEFAULT '-',
	descripcion character varying(500) NOT NULL DEFAULT '-',
    id_oficina integer NOT NULL DEFAULT 1,
    id_estado integer NOT NULL DEFAULT 1,
    fecha_hora_creacion timestamp,
    fecha_hora_enviado timestamp,
	id_registro_token integer NOT NULL DEFAULT 0,
    PRIMARY KEY (id_contactos_recupero)
);
INSERT INTO contactos_recupero (numero_telefono, descripcion, id_oficina, id_estado, fecha_hora_creacion)
VALUES	('5491131961299', 'Darío', 1, 1, now());
INSERT INTO contactos_recupero (numero_telefono, descripcion, id_oficina, id_estado, fecha_hora_creacion)
VALUES	('5492236602350', 'Guille', 1, 1, now());
INSERT INTO contactos_recupero (numero_telefono, descripcion, id_oficina, id_estado, fecha_hora_creacion)
VALUES	('5491130990068', 'Joaco', 1, 1, now());
INSERT INTO contactos_recupero (numero_telefono, descripcion, id_oficina, id_estado, fecha_hora_creacion)
VALUES	('5492235342256', 'Franco', 1, 1, now());
INSERT INTO contactos_recupero (numero_telefono, descripcion, id_oficina, id_estado, fecha_hora_creacion)
VALUES	('5491157477661', 'Mari', 1, 1, now());

update sesion_recupero_whatsapp set id_estado = 2, fecha_hora_cierre = now() where id_estado = 1;

select count(*) from contactos_recupero;
where id_contactos_recupero > 5
and id_contactos_recupero < 5006
order by 1;

UPDATE contactos_recupero SET id_estado = 1, fecha_hora_enviado = null where id_contactos_recupero IN (1,2,5);

UPDATE contactos_recupero SET numero_telefono = '5491131961299' WHERE id_contactos_recupero = 1;
UPDATE contactos_recupero SET id_registro_token = 99793 WHERE id_contactos_recupero = 1;
UPDATE contactos_recupero SET id_registro_token = 99794 WHERE id_contactos_recupero = 2;
UPDATE contactos_recupero SET id_registro_token = 99795 WHERE id_contactos_recupero = 3;
UPDATE contactos_recupero SET id_registro_token = 99796 WHERE id_contactos_recupero = 4;
UPDATE contactos_recupero SET id_registro_token = 99797 WHERE id_contactos_recupero = 5;
select * from registro_token where id_registro_token in (99793,99794,99795,99796,99797);

UPDATE contactos_recupero SET id_oficina = 2
WHERE id_contactos_recupero > 5
AND id_oficina = 1;

select * from agente order by 2
35	"oficina33"

INSERT INTO registro_token (id_token, de_agente, activo, id_usuario, ingresos, registros, observaciones, bono_carga_1, bono_carga_1_porcentaje, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
SELECT 	CONCAT('a-35-',substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)),
		true, true,
		35,
		0, 0,
		id_contactos_recupero::varchar,
		3000, false,
		now(), now(), 1
FROM 	contactos_recupero
WHERE id_estado = 1
AND id_contactos_recupero > 5005
order by descripcion;

select * from registro_token order by 1 desc limit 10

select * from usuario where usuario = 'bitpina'

UPDATE  contactos_recupero
SET		id_registro_token = rt.id_registro_token
FROM	registro_token rt
WHERE	contactos_recupero.id_contactos_recupero::varchar = rt.observaciones
AND contactos_recupero.id_estado = 1
AND contactos_recupero.id_contactos_recupero > 5005
AND contactos_recupero.id_contactos_recupero < 5006;

UPDATE  registro_token
SET		observaciones = cr.descripcion
FROM	contactos_recupero cr
WHERE	registro_token.id_registro_token = cr.id_registro_token
AND cr.id_estado = 1
AND cr.id_contactos_recupero > 5005
AND cr.id_contactos_recupero < 5006;

SELECT *
FROM 	contactos_recupero
WHERE id_estado = 1
AND id_contactos_recupero > 5005
AND id_contactos_recupero < 5006
order by descripcion;

select cr.id_contactos_recupero, 
cr.numero_telefono,
rt.id_token
from contactos_recupero cr join registro_token rt on (cr.id_registro_token = rt.id_registro_token)
where cr.id_oficina = 2 and cr.id_estado = 1 
order by 1 limit 1;

select count(*), max(rt.id_registro_token), min(rt.id_registro_token)
from contactos_recupero cr join registro_token rt 
	on (cr.id_registro_token = rt.id_registro_token)
where cr.id_oficina = 2 
and cr.id_estado = 1;

/*****************************************************************************/
insert into plataforma (plataforma, url_admin, url_juegos, marca_baja)
values ('Casino 365 Online', 'https://bo.casinoenvivo.club', 'https://www.casino365online.us', false);
insert into plataforma (plataforma, url_admin, url_juegos, marca_baja)
values ('Casino 365 Vip', 'https://bo.casinoenvivo.club', 'https://www.casino365vip.ws', false);
--select * from plataforma
--update plataforma set url_juegos = 'https://www.casino365online.us' where id_plataforma = 1;

insert into rol (nombre_rol, fecha_hora_creacion) values ('Administrador', NOW());
insert into rol (nombre_rol, fecha_hora_creacion) values ('Encargado', NOW());
insert into rol (nombre_rol, fecha_hora_creacion) values ('Operador', NOW());
--select * from rol;

insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Mercado Pago', 'com.mercadopago.wallet', true, false, now(), 1, now(), 1);
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Open Bank', 'ar.openbank.modelbank', true, false, now(), 1, now(), 1);
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Brubank', 'com.brubank', true, false, now(), 1, now(), 1);
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Telepagos', 'com.telepagos', true, false, now(), 1, now(), 1);
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Ualá', 'ar.uala', false, false, now(), 1, now(), 1);
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Belo', 'com.belo.android', false, false, now(), 1, now(), 1); 
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('Mercado Pago PY', 'com.mercadopago.wallet', true, false, now(), 1, now(), 1);
insert into billetera(billetera, notificacion_descripcion, notificacion_activa, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
values ('TuCash', 'tucash', false, false, now(), 1, now(), 1);
--select * from billetera;

--DROP FUNCTION Insertar_Usuario(in_id_usuario INTEGER, usr VARCHAR(50), pass VARCHAR(200), rol INTEGER, ofi INTEGER);
CREATE OR REPLACE FUNCTION Insertar_Usuario(in_id_usuario INTEGER, usr VARCHAR(50), pass VARCHAR(200), rol INTEGER, ofi INTEGER) RETURNS VOID AS $$
BEGIN
	INSERT INTO usuario (usuario, password, id_rol, id_oficina, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (usr, pass, rol, ofi, now(), in_id_usuario, now(), in_id_usuario);
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Insertar_Agente(in_id_usuario INTEGER, in_usuario VARCHAR(50), in_password VARCHAR(200), in_id_oficina INTEGER, in_id_plataforma INTEGER, in_bono_carga_1 INTEGER, in_bono_creacion INTEGER, in_bono_carga_1_porcentaje BOOLEAN)
CREATE OR REPLACE FUNCTION Insertar_Agente(in_id_usuario INTEGER, in_usuario VARCHAR(50), in_password VARCHAR(200), in_id_oficina INTEGER, in_id_plataforma INTEGER, in_bono_carga_1 INTEGER, in_bono_creacion INTEGER, in_bono_carga_1_porcentaje BOOLEAN) RETURNS VOID AS $$
BEGIN
	INSERT INTO agente (agente_usuario, agente_password, tokens_bono_carga_1, tokens_bono_creacion, tokens_bono_carga_1_porcentaje, id_plataforma, id_oficina, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (in_usuario, in_password, in_bono_carga_1, in_bono_creacion, in_bono_carga_1_porcentaje, in_id_plataforma, in_id_oficina, now(), in_id_usuario, now(), in_id_usuario);
END;
$$ LANGUAGE plpgsql;

select Insertar_Usuario(1, 'admin', '$2b$10$ekiX2TBOqH/a0h/CyCevJ./woR3sd0bY4hE2wkpjgDYxok.1C7FCa', 1, 1);
select Insertar_Usuario(1, 'dario', '$2b$10$rDzcPsghffuJwcHnYCnfkOjTy1DH6zwDL1of2RZAE4vnJOOfxd1Ya', 2, 1);
select Insertar_Usuario(1, 'dario_ope', '$2b$10$rDzcPsghffuJwcHnYCnfkOjTy1DH6zwDL1of2RZAE4vnJOOfxd1Ya', 3, 1);
select Insertar_Usuario(1, 'guille', '$2b$10$ekiX2TBOqH/a0h/CyCevJ./woR3sd0bY4hE2wkpjgDYxok.1C7FCa', 1, 1);
--select * from usuario;

CREATE OR REPLACE FUNCTION Modificar_Usuario(in_id_usuario_modi integer, in_id_usuario integer, pass VARCHAR(200), in_estado BOOLEAN, in_id_rol INTEGER, in_id_oficina INTEGER, in_retiro BOOLEAN) RETURNS VOID AS $$
BEGIN
	UPDATE usuario 
	SET password = pass,
		id_rol = in_id_rol, 
		id_oficina = in_id_oficina, 
		id_usuario_ultima_modificacion = in_id_usuario_modi,
		fecha_hora_ultima_modificacion = now(),
		marca_baja = in_estado,
		recibe_retiro = in_retiro
	WHERE id_usuario = in_id_usuario;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Insertar_Token_Agente(in_id_usuario INTEGER, in_id_agente INTEGER, in_observaciones VARCHAR(30), in_bono_carga_1 INTEGER);
CREATE OR REPLACE FUNCTION Insertar_Token_Agente(in_id_usuario INTEGER, in_id_agente INTEGER, in_observaciones VARCHAR(30), in_bono_carga_1 INTEGER, in_bono_carga_1_porcentaje BOOLEAN)
RETURNS TABLE (id_registro_token INTEGER) AS $$
DECLARE aux_id_registro_token INTEGER;
		aux_token VARCHAR(60);
BEGIN
	SELECT substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)
	INTO aux_token;

	INSERT INTO registro_token (id_token, de_agente, activo, id_usuario, ingresos, registros, observaciones, bono_carga_1, bono_carga_1_porcentaje, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (CONCAT('a-', in_id_agente::varchar, '-', aux_token), true, true, in_id_agente, 0, 0, in_observaciones, in_bono_carga_1, in_bono_carga_1_porcentaje, now(), now(), in_id_usuario)
	RETURNING registro_token.id_registro_token INTO aux_id_registro_token;

	IF aux_id_registro_token IS NULL THEN
		aux_id_registro_token := 0;
	END IF;

	RETURN QUERY SELECT aux_id_registro_token;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Datos_Cliente(in_id_cliente INTEGER);
CREATE OR REPLACE FUNCTION Obtener_Datos_Cliente(in_id_cliente INTEGER)
RETURNS TABLE (	cliente_usuario VARCHAR(200),
			   	cliente_apellido VARCHAR(100),
			  	cliente_nombre VARCHAR(200),
				correo_electronico VARCHAR(100),
			  	telefono VARCHAR(100),
			  	fecha_hora_creacion TIMESTAMP) AS $$
BEGIN
	RETURN QUERY SELECT cl.cliente_usuario,
						cl.cliente_apellido,
			  			cl.cliente_nombre,
						cl.correo_electronico,
						cl.telefono,
						cl.fecha_hora_creacion
	FROM cliente cl
	WHERE cl.id_cliente = in_id_cliente;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Datos_Cliente(54);

--DROP FUNCTION Obtener_Datos_Cliente2(in_id_cliente INTEGER);
CREATE OR REPLACE FUNCTION Obtener_Datos_Cliente2(in_id_cliente INTEGER)
RETURNS TABLE (	id_cliente INTEGER,
				id_cliente_ext BIGINT,
			    id_cliente_db INTEGER,
			   	cliente_usuario VARCHAR(200),
			   	id_agente INTEGER,
			   	agente_usuario VARCHAR(200),
			   	agente_password VARCHAR(200),
			   	bono_creacion INTEGER,
			    id_plataforma INTEGER,
			    plataforma VARCHAR(200),
			    id_oficina INTEGER,
			    oficina VARCHAR(200)) AS $$
BEGIN
	RETURN QUERY SELECT	cl.id_cliente,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.cliente_usuario,
        	cl.id_agente,
			ag.agente_usuario,
        	ag.agente_password,
        	COALESCE(rt.bono_creacion, 0) AS bono_creacion,
			ag.id_plataforma,
			pl.plataforma,
			ag.id_oficina,
			o.oficina
	FROM cliente cl JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN plataforma pl
			ON (ag.id_plataforma = pl.id_plataforma)
		JOIN oficina o
			ON (o.id_oficina = ag.id_oficina)
		LEFT JOIN registro_token rt
			ON (rt.de_agente = false
			   AND rt.id_usuario = cl.id_cliente)
	WHERE cl.id_cliente = in_id_cliente;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Datos_Cliente2(54);

/*DROP FUNCTION Modificar_Datos_Cliente(in_id_cliente integer, 
												   	in_cliente_apellido VARCHAR(100), 
												   	in_cliente_nombre VARCHAR(200),
													in_correo_electronico VARCHAR(100),
			  										in_telefono VARCHAR(100))*/
CREATE OR REPLACE FUNCTION Modificar_Datos_Cliente(in_id_cliente integer, 
												   	in_cliente_apellido VARCHAR(100), 
												   	in_cliente_nombre VARCHAR(200),
			  										in_telefono VARCHAR(100),
													in_correo_electronico VARCHAR(100))
RETURNS VOID AS $$
BEGIN
	UPDATE cliente
	SET	cliente_apellido = in_cliente_apellido, 
		cliente_nombre = in_cliente_nombre,
		correo_electronico = in_correo_electronico,
		telefono = in_telefono,
		id_usuario_ultima_modificacion = 1,
		fecha_hora_ultima_modificacion = now()
	WHERE id_cliente = in_id_cliente;
END;
$$ LANGUAGE plpgsql;
--select Modificar_Datos_Cliente(in_id_cliente integer, in_cliente_apellido, in_cliente_nombre, in_correo_electronico, in_telefono)

--DROP FUNCTION Obtener_Usuario_Token(in_id_usuario INTEGER, in_id_token VARCHAR(30));
CREATE OR REPLACE FUNCTION Obtener_Usuario_Token(in_id_usuario INTEGER, in_id_token VARCHAR(30))
RETURNS TABLE (id_usuario INTEGER, usuario VARCHAR(50), id_rol INTEGER, id_token VARCHAR(30), id_oficina INTEGER) AS $$
begin
	RETURN QUERY SELECT u.id_usuario, u.usuario, u.id_rol, us.id_token, u.id_oficina
	FROM usuario_sesion us JOIN usuario u
		ON (us.id_usuario = u.id_usuario
			AND us.id_usuario = in_id_usuario
			AND us.id_token = in_id_token
		   	AND fecha_hora_cierre IS NULL);
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Usuario_Token(2, 'ejemplo');

--DROP FUNCTION Insertar_Mensaje_Masivo(in_id_oficina INTEGER, in_id_usuario INTEGER, in_mensaje VARCHAR(200));
CREATE OR REPLACE FUNCTION Insertar_Mensaje_Masivo(in_id_oficina INTEGER, in_id_usuario INTEGER, in_mensaje VARCHAR(200))
RETURNS TABLE (cantidad_mensajes BIGINT) AS $$
BEGIN
	INSERT INTO cliente_chat (id_cliente, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
	SELECT cl.id_cliente, in_mensaje, now(), false, false, true, in_id_usuario
	FROM cliente cl JOIN agente ag
		ON (cl.id_agente = ag.id_agente
			AND ag.id_oficina = in_id_oficina
			AND cl.marca_baja = false);

	RETURN QUERY SELECT COUNT(*) AS cantidad_mensajes
	FROM cliente cl JOIN agente ag
		ON (cl.id_agente = ag.id_agente
			AND ag.id_oficina = in_id_oficina
			AND cl.marca_baja = false);
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Usuario_Token(id_usuario INTEGER, id_token VARCHAR(30), in_dir_ip VARCHAR(30));
CREATE OR REPLACE FUNCTION Modificar_Usuario_Token(in_id_usuario INTEGER, in_id_token VARCHAR(30), in_dir_ip VARCHAR(30)) RETURNS VOID AS $$
BEGIN
	
	UPDATE 	usuario_sesion
	SET 	fecha_hora_cierre = now(),
			cierre_abrupto = true
	WHERE	id_usuario = in_id_usuario
			AND id_token != in_id_token
			AND fecha_hora_cierre IS NULL;
			
	INSERT INTO usuario_sesion (id_usuario, id_token, ip, fecha_hora_creacion)
	VALUES	(in_id_usuario, in_id_token, in_dir_ip, now());
	
END;
$$ LANGUAGE plpgsql;
--select Modificar_Usuario_Token(2, 'ejemplo');

--DROP FUNCTION Cerrar_Sesion_Usuario(in_id_usuario INTEGER, in_id_token VARCHAR(30));
CREATE OR REPLACE FUNCTION Cerrar_Sesion_Usuario(in_id_usuario INTEGER, in_id_token VARCHAR(30)) RETURNS VOID AS $$
BEGIN
	UPDATE 	usuario_sesion
	SET 	fecha_hora_cierre = now()
	WHERE	id_usuario = in_id_usuario
			AND id_token = in_id_token;		
END;
$$ LANGUAGE plpgsql;

CREATE EXTENSION IF NOT EXISTS pgcrypto;

--DROP FUNCTION Confirmar_Sesion_Cliente_Id(in_id_cliente INTEGER, in_id_token VARCHAR(30), in_ip VARCHAR(100), in_monto NUMERIC, in_moneda VARCHAR(30))
CREATE OR REPLACE FUNCTION Confirmar_Sesion_Cliente_Id(in_id_cliente INTEGER, in_id_token VARCHAR(30), in_ip VARCHAR(100), in_monto NUMERIC, in_moneda VARCHAR(30))
RETURNS TABLE (id_cliente INTEGER) AS $$
DECLARE
    aux_id_cliente INTEGER;
    aux_id_agente INTEGER;
    aux_bloqueado BOOLEAN;
BEGIN
	IF NOT EXISTS (SELECT 1 FROM ip_lista_negra WHERE ip = in_ip)
					/*FROM cliente_sesion cls join cliente cl 
						ON (cls.id_cliente = cl.id_cliente
							AND cls.ip = in_ip
							AND cl.bloqueado = true))*/
	THEN 
	
		SELECT cl.id_cliente, cl.bloqueado, COALESCE(ag.id_agente, -1)
		INTO aux_id_cliente, aux_bloqueado, aux_id_agente
		FROM cliente cl LEFT JOIN agente ag
			ON (ag.id_agente = cl.id_agente
			   AND ag.marca_baja = false)
		WHERE cl.id_cliente = in_id_cliente;

		IF (aux_bloqueado) THEN
			aux_id_cliente := -1;
		END IF;	

		IF (aux_id_agente < 0) THEN
			aux_id_cliente := -3;
		END IF;	
	
		IF (aux_id_cliente > 0) THEN
			UPDATE cliente_sesion
			SET cierre_abrupto = true,
				fecha_hora_cierre = now()
			WHERE cliente_sesion.id_cliente = aux_id_cliente
				AND cliente_sesion.fecha_hora_cierre IS NULL;

			INSERT INTO cliente_sesion (id_cliente, id_token, ip, fecha_hora_creacion, monto, moneda)
			VALUES (aux_id_cliente, in_id_token, in_ip, now(), in_monto, in_moneda);

			UPDATE cliente
			SET en_sesion = true
			WHERE cliente.id_cliente = aux_id_cliente;
		END IF;
	ELSE
		aux_id_cliente := -2;
	END IF;
	
	RETURN QUERY SELECT aux_id_cliente;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Confirmar_Sesion_Cliente_Registro(in_agente VARCHAR(200), in_usuario VARCHAR(200), in_password VARCHAR(200), in_id_token VARCHAR(30), in_ip VARCHAR(100), in_monto NUMERIC, in_moneda VARCHAR(30), in_cliente_ext BIGINT, in_cliente_db INTEGER);
CREATE OR REPLACE FUNCTION Confirmar_Sesion_Cliente_Registro(in_agente VARCHAR(200), in_usuario VARCHAR(200), in_password VARCHAR(200), in_id_token VARCHAR(30), in_ip VARCHAR(100), in_monto NUMERIC, in_moneda VARCHAR(30), in_cliente_ext BIGINT, in_cliente_db INTEGER)
RETURNS TABLE (id_cliente INTEGER, id_oficina INTEGER) AS $$
DECLARE
    aux_id_cliente INTEGER;
    aux_id_agente INTEGER;
    aux_id_oficina INTEGER;
    aux_bloqueado BOOLEAN;
	aux_token VARCHAR(60);
	aux_bono_carga_1 INTEGER;
	aux_bono_creacion INTEGER;
BEGIN
	IF NOT EXISTS (SELECT 1 FROM ip_lista_negra WHERE ip = in_ip)
	/*IF NOT EXISTS (SELECT 1
					FROM cliente_sesion cls join cliente cl 
						ON (cls.id_cliente = cl.id_cliente
							AND cls.ip = in_ip
							AND cl.bloqueado = true))*/
	THEN 
		SELECT ag.id_agente, ag.tokens_bono_creacion, ag.tokens_bono_carga_1, ag.id_oficina
		INTO aux_id_agente, aux_bono_creacion, aux_bono_carga_1, aux_id_oficina
		FROM agente ag
		WHERE ag.agente_usuario = in_agente
		AND ag.marca_baja = false;
		
		IF aux_id_agente > 0 THEN
			
			SELECT	cl.id_cliente
			INTO aux_id_cliente
			FROM cliente cl
			WHERE 	lower(trim(cl.cliente_usuario)) = lower(trim(in_usuario))
			AND 	cl.id_agente = aux_id_agente
			AND 	cl.marca_baja = false;
			
			IF aux_id_cliente > 0 THEN
			
				INSERT INTO cliente_sesion (id_cliente, id_token, ip, fecha_hora_creacion, monto, moneda)
				VALUES (aux_id_cliente, in_id_token, in_ip, now(), in_monto, in_moneda);
			
				UPDATE cliente
				SET en_sesion = true, 
					cliente_password = in_password
				WHERE cliente.id_cliente = aux_id_cliente;
				
			ELSE	
				INSERT INTO cliente (cliente_usuario, cliente_password, id_agente, en_sesion, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion, id_cliente_ext, id_cliente_db)
				VALUES (lower(trim(in_usuario)), in_password, aux_id_agente, true, false, now(), 1,  now(), 1, in_cliente_ext, in_cliente_db)
				RETURNING cliente.id_cliente INTO aux_id_cliente;

				INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
				VALUES (1, 7, aux_id_cliente, 2, true, false, now(), now(), 1);

				SELECT substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)
				INTO aux_token;

				INSERT INTO registro_token (id_token, de_agente, activo, id_usuario, ingresos, registros, bono_creacion, bono_carga_1, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
				VALUES (CONCAT('c-', aux_id_cliente::varchar, '-', aux_token), false, true, aux_id_cliente, 0, 0, aux_bono_creacion, aux_bono_carga_1, now(), now(), 1);
			
			END IF;
			
		ELSE
			aux_id_cliente := -3;
		END IF;
	ELSE
		aux_id_cliente := -2;
	END IF;
	
	RETURN QUERY SELECT aux_id_cliente, aux_id_oficina;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Confirmar_Sesion_Cliente(in_agente VARCHAR(200), in_usuario VARCHAR(200), in_password VARCHAR(200), in_id_token VARCHAR(30), in_ip VARCHAR(100), in_monto NUMERIC, in_moneda VARCHAR(30));
CREATE OR REPLACE FUNCTION Confirmar_Sesion_Cliente(in_agente VARCHAR(200), in_usuario VARCHAR(200), in_password VARCHAR(200), in_id_token VARCHAR(30), in_ip VARCHAR(100), in_monto NUMERIC, in_moneda VARCHAR(30))
RETURNS TABLE (id_cliente INTEGER) AS $$
DECLARE
    aux_id_cliente INTEGER;
    aux_id_agente INTEGER;
    aux_bloqueado BOOLEAN;
	aux_token VARCHAR(60);
	aux_bono_carga_1 INTEGER;
	aux_bono_creacion INTEGER;
BEGIN
	IF NOT EXISTS (SELECT 1 FROM ip_lista_negra WHERE ip = in_ip)
	/*IF NOT EXISTS (SELECT 1
					FROM cliente_sesion cls join cliente cl 
						ON (cls.id_cliente = cl.id_cliente
							AND cls.ip = in_ip
							AND cl.bloqueado = true))*/
	THEN 
	
		IF EXISTS (SELECT 1
					FROM cliente cl join agente ag 
						ON (cl.id_agente = ag.id_agente
							AND lower(trim(cl.cliente_usuario)) = lower(trim(in_usuario))
							AND ag.agente_usuario = in_agente))
		THEN
		
			SELECT cl.id_cliente, cl.bloqueado, ag.id_agente
			INTO aux_id_cliente, aux_bloqueado, aux_id_agente
			FROM cliente cl join agente ag 
				ON (cl.id_agente = ag.id_agente
					AND cl.cliente_usuario = in_usuario
					AND ag.agente_usuario = in_agente);
					
			IF (aux_bloqueado) THEN
				aux_id_cliente := -1;
			ELSE			
				UPDATE cliente
				SET cliente_password = in_password,
					en_sesion = true
				WHERE cliente.id_cliente = aux_id_cliente;
			END IF;
			
		ELSE 
		
			SELECT id_agente, tokens_bono_creacion, tokens_bono_carga_1
			INTO aux_id_agente, aux_bono_creacion, aux_bono_carga_1
			FROM agente
			WHERE agente_usuario = in_agente;

			INSERT INTO cliente (cliente_usuario, cliente_password, id_agente, en_sesion, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
			VALUES (lower(trim(in_usuario)), in_password, aux_id_agente, true, false, now(), 1,  now(), 1)
			RETURNING cliente.id_cliente INTO aux_id_cliente;
			
			INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
			VALUES (1, 7, aux_id_cliente, 2, true, false, now(), now(), 1);

			SELECT substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)
			INTO aux_token;
			
			INSERT INTO registro_token (id_token, de_agente, activo, id_usuario, ingresos, registros, bono_creacion, bono_carga_1, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
			VALUES (CONCAT('c-', aux_id_cliente::varchar, '-', aux_token), false, true, aux_id_cliente, 0, 0, aux_bono_creacion, aux_bono_carga_1, now(), now(), 1);

		END IF;

		IF (aux_id_cliente > 0) THEN
			UPDATE cliente_sesion
			SET cierre_abrupto = true,
				fecha_hora_cierre = now()
			WHERE cliente_sesion.id_cliente = aux_id_cliente
				AND cliente_sesion.fecha_hora_cierre IS NULL;

			INSERT INTO cliente_sesion (id_cliente, id_token, ip, fecha_hora_creacion, monto, moneda)
			VALUES (aux_id_cliente, in_id_token, in_ip, now(), in_monto, in_moneda);
		END IF;
	ELSE
		aux_id_cliente := -2;
	END IF;
	
	RETURN QUERY SELECT aux_id_cliente;
END;
$$ LANGUAGE plpgsql;
--select * from cliente
--select * from cliente_sesion

--DROP FUNCTION Registrar_Cliente(in_id_agente INTEGER, in_usuario VARCHAR(200), in_password VARCHAR(200), in_correo_electronico VARCHAR(100), in_telefono VARCHAR(100), in_id_registro_token INTEGER, in_id_cliente_ext BIGINT, id_cliente_bd INTEGER)
CREATE OR REPLACE FUNCTION Registrar_Cliente(in_id_agente INTEGER, in_usuario VARCHAR(200), in_password VARCHAR(200), in_correo_electronico VARCHAR(100), in_telefono VARCHAR(100), in_id_registro_token INTEGER, in_id_cliente_ext BIGINT, in_id_cliente_db INTEGER)
RETURNS TABLE (id_cliente INTEGER) AS $$
DECLARE
    aux_id_cliente INTEGER;
	aux_token VARCHAR(60);
	aux_bono_carga_1 INTEGER;
	aux_bono_creacion INTEGER;
BEGIN
	INSERT INTO cliente (cliente_usuario, cliente_password, id_agente, en_sesion, marca_baja, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion, id_registro_token, correo_electronico, telefono, id_cliente_ext, id_cliente_db)
	VALUES (lower(trim(in_usuario)), in_password, in_id_agente, false, false, now(), 1,  now(), 1, in_id_registro_token, in_correo_electronico, in_telefono, in_id_cliente_ext, in_id_cliente_db)
	RETURNING cliente.id_cliente INTO aux_id_cliente;

	INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (1, 8, aux_id_cliente, 2, true, false, now(), now(), 1);

	SELECT substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)
	INTO aux_token;

	SELECT tokens_bono_creacion, tokens_bono_carga_1
	INTO aux_bono_creacion, aux_bono_carga_1
	FROM agente
	WHERE id_agente = in_id_agente;
			
	INSERT INTO registro_token (id_token, de_agente, activo, id_usuario, ingresos, registros, bono_creacion, bono_carga_1, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (CONCAT('c-', aux_id_cliente::varchar, '-', aux_token), false, true, aux_id_cliente, 0, 0, aux_bono_creacion, aux_bono_carga_1, now(), now(), 1);
	
	RETURN QUERY SELECT aux_id_cliente;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Cliente_Token(in_id_usuario INTEGER, in_id_token VARCHAR(30));
CREATE OR REPLACE FUNCTION Obtener_Cliente_Token(in_id_cliente INTEGER, in_id_token VARCHAR(30))
RETURNS TABLE (id_cliente INTEGER, 
				id_agente INTEGER, 
				cliente_usuario VARCHAR(100), 
			   	id_cliente_ext BIGINT,
			   	id_cliente_db INTEGER,
				monto NUMERIC, 
				moneda VARCHAR(30),
				id_oficina INTEGER,
				contacto_whatsapp VARCHAR(200),
				contacto_telegram VARCHAR(200),
				bono_carga_1 INTEGER,
			   	bono_carga_1_porcentaje BOOLEAN,
				bono_carga_perpetua INTEGER,
				minimo_carga INTEGER,
				minimo_retiro INTEGER,
				minimo_espera_retiro INTEGER,
				agente_usuario VARCHAR(200),
			   	agente_password VARCHAR(200),
				id_plataforma INTEGER,
				plataforma VARCHAR(200),
				url_juegos VARCHAR(200),
			  	token_cliente VARCHAR(60)) AS $$
BEGIN
	RETURN QUERY SELECT cl.id_cliente, 
						cl.id_agente, 
						cl.cliente_usuario, 
						cl.id_cliente_ext,
						cl.id_cliente_db,
						cls.monto, 
						cls.moneda,
						o.id_oficina,
						o.contacto_whatsapp,
						o.contacto_telegram,
						COALESCE(rtr.bono_carga_1, o.bono_carga_1) AS bono_carga_1,
						COALESCE(rtr.bono_carga_1_porcentaje, false) AS bono_carga_1_porcentaje,
						o.bono_carga_perpetua,
						o.minimo_carga,
						o.minimo_retiro,
						o.minimo_espera_retiro,
						ag.agente_usuario,
						ag.agente_password,
						pl.id_plataforma,
						pl.plataforma,
						pl.url_juegos,
						COALESCE(rt.id_token, 'qwerty') AS token_cliente
	FROM cliente cl JOIN agente ag
			ON (cl.id_agente = ag.id_agente
				AND cl.id_cliente = in_id_cliente
			   	AND cl.marca_baja = false)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina
			   	AND o.marca_baja = false)
		JOIN plataforma pl
			ON (ag.id_plataforma = pl.id_plataforma
			   AND pl.marca_baja = false)
		LEFT JOIN registro_token rt
			ON (rt.id_usuario = cl.id_cliente
			   AND rt.activo = true
			   AND rt.de_agente = false)
		LEFT JOIN registro_token rtr
			ON (rtr.id_registro_token = cl.id_registro_token
			   	AND rtr.activo = true)
		JOIN cliente_sesion cls
			ON (cl.id_cliente = cls.id_cliente
				AND cls.id_token = in_id_token
				AND cls.fecha_hora_cierre IS NULL);
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Cliente_Token(54, '9-c6a78420f497658fa2de94db6473d594ee356503e788a2757f8b632b1612a760');
--select * from Obtener_Cliente_Token(1, '5-bcea02d3958a7dc0a9f1aa589cda747b6886f429ab89c8bb0b20e54a4afe042b');

--DROP FUNCTION Obtener_Cliente_Alertas(in_id_cliente INTEGER);
CREATE OR REPLACE FUNCTION Obtener_Cliente_Alertas(in_id_cliente INTEGER)
RETURNS TABLE (	pendientes_mensajes INTEGER, 
				pendientes_operaciones INTEGER) AS $$
DECLARE
	aux_pendientes_mensajes INTEGER;
	aux_pendientes_operaciones INTEGER;
BEGIN
	SELECT	COUNT(ch.id_cliente_chat)
	INTO aux_pendientes_mensajes
	FROM cliente_chat ch
	WHERE ch.id_cliente = in_id_cliente
	AND ch.visto_cliente = false;
	
	IF aux_pendientes_mensajes IS NULL THEN
		aux_pendientes_mensajes := 0;
	END IF;
	
	SELECT	COUNT(op.id_operacion)
	INTO aux_pendientes_operaciones
	FROM operacion op
	WHERE op.id_cliente = in_id_cliente
	AND op.notificado = false
	AND op.marca_baja = false
	AND op.id_accion in (1, 2, 5, 6, 9);
	
	IF aux_pendientes_operaciones IS NULL THEN
		aux_pendientes_operaciones := 0;
	END IF;
	
	RETURN QUERY SELECT aux_pendientes_mensajes, aux_pendientes_operaciones;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Cliente_Alertas(4);

--DROP FUNCTION Finalizar_Cliente_Chat(in_id_cliente INTEGER, in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Finalizar_Cliente_Chat(in_id_cliente INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (id_cliente INTEGER) AS $$
BEGIN
	INSERT INTO cliente_chat (id_cliente, 
							  mensaje, 
							  fecha_hora_creacion, 
							  enviado_cliente, 
							  visto_cliente, 
							  visto_operador,
							  id_usuario,
							  id_usuario_baja,
							  fecha_hora_baja,
							  marca_baja)
	VALUES (in_id_cliente, 
			  'Finalizado', 
			  NOW(),
			  false, 
			  true, 
			  true,
			  in_id_usuario,
			  in_id_usuario,
			  NOW(),
			  true);
	
	RETURN query SELECT in_id_cliente;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Finalizar_Cliente_Visita_Chat(in_id_visita_sesion INTEGER, in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Finalizar_Cliente_Visita_Chat(in_id_visita_sesion INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (id_cliente INTEGER) AS $$
BEGIN
	INSERT INTO visita_sesion_chat (id_visita_sesion, 
							  mensaje, 
							  fecha_hora_creacion, 
							  enviado_cliente, 
							  visto_cliente, 
							  visto_operador,
							  id_usuario,
							  id_usuario_baja,
							  fecha_hora_baja,
							  marca_baja)
	VALUES (in_id_visita_sesion, 
			  'Finalizado', 
			  NOW(),
			  false, 
			  true, 
			  true,
			  in_id_usuario,
			  in_id_usuario,
			  NOW(),
			  true);
	
	RETURN query SELECT in_id_visita_sesion as id_cliente;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Cerrar_Sesion_Cliente(in_id_cliente INTEGER, in_id_token VARCHAR(30));
CREATE OR REPLACE FUNCTION Cerrar_Sesion_Cliente(in_id_cliente INTEGER, in_id_token VARCHAR(30)) RETURNS VOID AS $$
BEGIN
	UPDATE 	cliente_sesion
	SET 	fecha_hora_cierre = now()
	WHERE	id_cliente = in_id_cliente
			AND id_token = in_id_token;
	
	UPDATE 	cliente
	SET		en_sesion = false
	WHERE 	id_cliente = in_id_cliente;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Cerrar_Sesion_Cliente_Visita(in_id_visita_sesion INTEGER);
CREATE OR REPLACE FUNCTION Cerrar_Sesion_Cliente_Visita(in_id_visita_sesion INTEGER) RETURNS VOID AS $$
BEGIN
	UPDATE 	visita_sesion
	SET 	fecha_hora_cierre = now()
	WHERE	id_visita_sesion = in_id_visita_sesion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Token_Registro(in_id_token VARCHAR(60));
CREATE OR REPLACE FUNCTION Obtener_Token_Registro(in_id_token VARCHAR(60))
RETURNS TABLE (id_registro_token INTEGER) AS $$
DECLARE
    aux_id_registro_token INTEGER;
BEGIN	
	SELECT rt.id_registro_token
	INTO aux_id_registro_token
	FROM registro_token rt
	WHERE rt.id_token = in_id_token
	and rt.activo = true;
	
	IF aux_id_registro_token IS NOT NULL THEN
		UPDATE registro_token
		SET	ingresos = ingresos + 1
		WHERE registro_token.id_registro_token = aux_id_registro_token;
	ELSE
		aux_id_registro_token := 0;
	END IF;

	RETURN QUERY SELECT aux_id_registro_token;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Token_Registro('c-4-9473764e8de991edc3899a179f8af9c1ce3d6ba');
--select * from Obtener_Token_Registro('a-1-9473764e8de991edc3899a179f8af9c1ce3d6ba');

DROP FUNCTION Obtener_Token_Visita_Sesion_IP(in_ip VARCHAR(100));
CREATE OR REPLACE FUNCTION Obtener_Token_Visita_Sesion_IP(in_ip VARCHAR(100))
RETURNS TABLE (id_visita_sesion INTEGER, id_token VARCHAR(100)) AS $$
DECLARE
    aux_cant_sesiones_ip INTEGER;
    aux_id_visita_sesion INTEGER;
    aux_id_token VARCHAR(100);
BEGIN

	SELECT COUNT(DISTINCT vs.id_visita_sesion)
	INTO aux_cant_sesiones_ip
	FROM visita_sesion vs
	WHERE vs.ip = in_ip
	--AND vs.ip NOT IN ('201.235.221.173','181.228.49.61')
	AND vs.fecha_hora_creacion > NOW() - INTERVAL '72 hours';

	IF (aux_cant_sesiones_ip < 2000) THEN
		aux_id_token := CONCAT('vs-',substr(translate(encode(gen_random_bytes(70), 'base64'), '/+', 'ab'), 1, 70));
	
		INSERT INTO visita_sesion (id_token, ip, fecha_hora_creacion)
		VALUES (aux_id_token, in_ip, now())
		RETURNING visita_sesion.id_visita_sesion INTO aux_id_visita_sesion;
		
	ELSE
		aux_id_visita_sesion := 0;
		aux_id_token := '';
		
	END IF;

	RETURN QUERY SELECT aux_id_visita_sesion, aux_id_token;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Token_Visita_Sesion_IP('::1');

CREATE OR REPLACE FUNCTION Modificar_Visita_Sesion_Alias(in_id_visita_sesion integer, in_alias_sesion VARCHAR(100)) 
RETURNS VOID AS $$
BEGIN
	UPDATE visita_sesion 
	SET alias_sesion = in_alias_sesion,
		fecha_hora_inicio_sesion = now()
	WHERE id_visita_sesion = in_id_visita_sesion;
END;
$$ LANGUAGE plpgsql;

--la mía: '201.235.221.173','181.228.49.61'
CREATE OR REPLACE FUNCTION Obtener_Token_RegistroIP(in_id_token VARCHAR(60), in_ip VARCHAR(100))
RETURNS TABLE (id_registro_token INTEGER) AS $$
DECLARE
    aux_id_registro_token INTEGER;
    aux_cant_registros_ip INTEGER;
BEGIN
	SELECT COUNT(DISTINCT op.id_operacion)
	INTO aux_cant_registros_ip
	FROM operacion op JOIN cliente cl
			ON (op.id_cliente = cl.id_cliente)
		JOIN cliente_sesion cs
			ON (cl.id_cliente = cs.id_cliente
				AND cs.ip = in_ip
			   	AND cs.ip NOT IN ('201.235.221.173','181.228.49.61'))
	WHERE op.id_accion = 8 
			AND op.marca_baja = false
			AND op.id_estado = 2
			AND op.fecha_hora_creacion > NOW() - INTERVAL '72 hours';

	IF (aux_cant_registros_ip < 2) THEN
		SELECT rt.id_registro_token
		INTO aux_id_registro_token
		FROM registro_token rt
		WHERE rt.id_token = in_id_token
		and rt.activo = true;

		IF aux_id_registro_token IS NOT NULL THEN
			UPDATE registro_token
			SET	ingresos = ingresos + 1
			WHERE registro_token.id_registro_token = aux_id_registro_token;
		ELSE
			aux_id_registro_token := 0;
		END IF;
	ELSE
		aux_id_registro_token := -1;
	END IF;

	RETURN QUERY SELECT aux_id_registro_token;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Agente(in_id_agente integer, in_id_usuario integer, pass VARCHAR(200), in_estado BOOLEAN, in_id_oficina INTEGER, in_id_plataforma INTEGER, in_bono_carga_1 INTEGER, in_bono_creacion INTEGER, in_bono_carga_1_porcentaje BOOLEAN)
CREATE OR REPLACE FUNCTION Modificar_Agente(in_id_agente integer, in_id_usuario integer, pass VARCHAR(200), in_estado BOOLEAN, in_id_oficina INTEGER, in_id_plataforma INTEGER, in_bono_carga_1 INTEGER, in_bono_creacion INTEGER, in_bono_carga_1_porcentaje BOOLEAN) RETURNS VOID AS $$
BEGIN
	UPDATE agente 
	SET agente_password = pass,
		id_oficina = in_id_oficina, 
		id_plataforma = in_id_plataforma,
		tokens_bono_carga_1 = in_bono_carga_1,
		tokens_bono_creacion = in_bono_creacion,
		tokens_bono_carga_1_porcentaje = in_bono_carga_1_porcentaje,
		id_usuario_ultima_modificacion = in_id_usuario,
		fecha_hora_ultima_modificacion = now(),
		marca_baja = in_estado
	WHERE id_agente = in_id_agente;
	
	UPDATE registro_token AS rt
	SET 	bono_creacion = in_bono_creacion,
			bono_carga_1 = in_bono_carga_1,
			bono_carga_1_porcentaje = in_bono_carga_1_porcentaje,
			id_usuario_ultima_modificacion = in_id_usuario,
			fecha_hora_ultima_modificacion = now()
	FROM 	cliente cl
		WHERE (rt.id_usuario = cl.id_cliente
			AND rt.de_agente = false
			AND cl.id_agente = in_id_agente);
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Token_Agente(in_id_registro_token integer, in_id_usuario integer, in_observaciones VARCHAR(200), in_bono_carga_1 INTEGER, in_activo BOOLEAN, in_bono_carga_1_porcentaje BOOLEAN)
CREATE OR REPLACE FUNCTION Modificar_Token_Agente(in_id_registro_token integer, in_id_usuario integer, in_observaciones VARCHAR(200), in_bono_carga_1 INTEGER, in_activo BOOLEAN, in_bono_carga_1_porcentaje BOOLEAN) RETURNS VOID AS $$
BEGIN	
	UPDATE registro_token
	SET 	bono_carga_1 = in_bono_carga_1,
			bono_carga_1_porcentaje = in_bono_carga_1_porcentaje,
			observaciones = in_observaciones,
			activo = in_activo,
			id_usuario_ultima_modificacion = in_id_usuario,
			fecha_hora_ultima_modificacion = now()
	WHERE id_registro_token = in_id_registro_token;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Cliente_Registro(in_id_cliente integer, in_id_usuario integer, in_correo_electronico VARCHAR(100), in_telefono VARCHAR(100))
CREATE OR REPLACE FUNCTION Modificar_Cliente_Registro(in_id_cliente integer, in_id_usuario integer, in_telefono VARCHAR(100), in_correo_electronico VARCHAR(100)) RETURNS VOID AS $$
BEGIN
	UPDATE cliente 
	SET correo_electronico = in_correo_electronico,
		telefono = in_telefono, 
		id_usuario_ultima_modificacion = in_id_usuario,
		fecha_hora_ultima_modificacion = now()
	WHERE id_cliente = in_id_cliente;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Cliente_Password(in_id_cliente integer, in_id_usuario integer, in_password VARCHAR(100))
CREATE OR REPLACE FUNCTION Modificar_Cliente_Password(in_id_cliente integer, in_id_usuario integer, in_password VARCHAR(100)) RETURNS VOID AS $$
BEGIN
	UPDATE cliente 
	SET cliente_password = in_password,
		id_usuario_ultima_modificacion = in_id_usuario,
		fecha_hora_ultima_modificacion = now()
	WHERE id_cliente = in_id_cliente;
	
	IF (in_id_usuario > 1) THEN
		UPDATE cliente_sesion 
		SET	fecha_hora_cierre = now(),
			cierre_abrupto = true
		WHERE id_cliente = in_id_cliente
		AND fecha_hora_cierre IS NULL;
	END IF;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Usuario(usr VARCHAR(50), in_login BOOLEAN);
CREATE OR REPLACE FUNCTION Obtener_Usuario(usr VARCHAR(50), in_login BOOLEAN)
RETURNS TABLE (id_usuario INTEGER, usuario VARCHAR(50), password VARCHAR(200), id_rol INTEGER, id_oficina INTEGER) AS $$
BEGIN
	IF (in_login) THEN
		RETURN query select u.id_usuario, u.usuario, u.password, u.id_rol, u.id_oficina
		from usuario u join oficina o
			on (u.id_oficina = o.id_oficina)
		where u.usuario = usr
			and u.marca_baja = false
			and o.marca_baja = false;
	ELSE
		RETURN query select u.id_usuario, u.usuario, u.password, u.id_rol, u.id_oficina
		from usuario u join oficina o
			on (u.id_oficina = o.id_oficina)
		where u.usuario = usr;
	END IF;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Usuario('dario', true);
--select * from usuario order by 1 desc

--DROP FUNCTION Obtener_Usuario_Noti(usr VARCHAR(50));
CREATE OR REPLACE FUNCTION Obtener_Usuario_Noti(usr VARCHAR(50))
RETURNS TABLE (	id_usuario INTEGER, 
			   	usuario VARCHAR(50), 
			   	password VARCHAR(100), 
			   	id_rol INTEGER, 
			   	id_oficina INTEGER, 
			   	id_billetera INTEGER, 
			   	cantidad_cuentas INTEGER, 
			   	id_cuenta_bancaria INTEGER) AS $$
BEGIN
	RETURN QUERY SELECT u.id_usuario, 
						u.usuario, 
						u.password, 
						u.id_rol, 
						u.id_oficina,
						cb.id_billetera,
						COUNT(cb.id_cuenta_bancaria)::INTEGER	AS cantidad_cuentas,
						MIN(cb.id_cuenta_bancaria)				AS id_cuenta_bancaria
	FROM usuario u JOIN oficina o
				ON (u.id_oficina = o.id_oficina)
			JOIN cuenta_bancaria cb
				ON (cb.id_usuario_noti = u.id_usuario)
	WHERE u.usuario = usr
		AND u.marca_baja = false
		AND o.marca_baja = false
	GROUP BY u.id_usuario, 
		u.usuario, 
		u.password, 
		u.id_rol, 
		u.id_oficina,
		cb.id_billetera
	ORDER BY COUNT(cb.id_cuenta_bancaria) DESC
	LIMIT 1;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Insertar_Oficina(in_id_usuario INTEGER, in_oficina VARCHAR(200), in_contacto_whatsapp VARCHAR(200), in_contacto_telegram VARCHAR(200), in_bono_carga_1 INTEGER, in_bono_carga_perpetua INTEGER, in_minimo_carga INTEGER, in_minimo_retiro INTEGER, in_minimo_espera_retiro INTEGER) 
CREATE OR REPLACE FUNCTION Insertar_Oficina(in_id_usuario INTEGER, in_oficina VARCHAR(200), in_contacto_whatsapp VARCHAR(200), in_contacto_telegram VARCHAR(200), in_estado BOOLEAN, in_bono_carga_1 INTEGER, in_bono_carga_perpetua INTEGER, in_minimo_carga INTEGER, in_minimo_retiro INTEGER, in_minimo_espera_retiro INTEGER)
RETURNS TABLE (id_oficina INTEGER) AS $$
DECLARE
    aux_id_oficina INTEGER;
BEGIN

	IF NOT EXISTS(	select 1
					from oficina o
					where o.oficina = in_oficina) THEN					
		INSERT INTO oficina (oficina, contacto_whatsapp, contacto_telegram, marca_baja, bono_carga_1, bono_carga_perpetua, minimo_carga, minimo_retiro, minimo_espera_retiro, fecha_hora_creacion, id_usuario_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
		VALUES (in_oficina, in_contacto_whatsapp, in_contacto_telegram, in_estado, in_bono_carga_1, in_bono_carga_perpetua, in_minimo_carga, in_minimo_retiro, in_minimo_espera_retiro, now(), in_id_usuario, now(), in_id_usuario)
		RETURNING oficina.id_oficina INTO aux_id_oficina;		
	ELSE
		aux_id_oficina := 0;
	END IF;
	
	RETURN query SELECT aux_id_oficina;
END;
$$ LANGUAGE plpgsql;

SELECT * FROM Insertar_Oficina(1, 'Oficina Test', '5491133682345', '5491133682345', false, 20, 0, 100, 100, 0);
--SELECT * FROM oficina

CREATE OR REPLACE FUNCTION Modificar_Oficina(in_id_oficina INTEGER, in_id_usuario INTEGER, in_oficina VARCHAR(200), in_contacto_whatsapp VARCHAR(200), in_contacto_telegram VARCHAR(200), in_estado BOOLEAN, in_bono_carga_1 INTEGER, in_bono_carga_perpetua INTEGER, in_minimo_carga INTEGER, in_minimo_retiro INTEGER, in_minimo_espera_retiro INTEGER) RETURNS VOID AS $$
BEGIN

	UPDATE oficina set oficina = in_oficina, 
						contacto_whatsapp = in_contacto_whatsapp, 
						contacto_telegram = in_contacto_telegram,
						bono_carga_1 = in_bono_carga_1, 
						bono_carga_perpetua = in_bono_carga_perpetua,
						minimo_carga = in_minimo_carga, 
						minimo_retiro = in_minimo_retiro,
						minimo_espera_retiro = in_minimo_espera_retiro,
						fecha_hora_ultima_modificacion = now(),
						id_usuario_ultima_modificacion = in_id_usuario,
						marca_baja = in_estado
	WHERE id_oficina = in_id_oficina;
	
END;
$$ LANGUAGE plpgsql;
--select Modificar_Oficina(1, 1, 'Oficina Test', '5491133682345', '5491133682345', false, 20, 0, 100, 100, 0)
--SELECT * FROM oficina

--DROP FUNCTION Crear_Cuenta_Bancaria(in_id_oficina INTEGER, in_id_usuario INTEGER, in_nombre VARCHAR(200), in_alias VARCHAR(200), in_cbu VARCHAR(200), in_estado BOOLEAN, in_id_billetera INTEGER, in_id_usuario_noti INTEGER, in_access_token VARCHAR(200), in_public_key VARCHAR(200), in_client_id VARCHAR(200), in_client_secret VARCHAR(200))
CREATE OR REPLACE FUNCTION Crear_Cuenta_Bancaria(in_id_oficina INTEGER, in_id_usuario INTEGER, in_nombre VARCHAR(200), in_alias VARCHAR(200), in_cbu VARCHAR(200), in_estado BOOLEAN, in_id_billetera INTEGER, in_id_usuario_noti INTEGER, in_id_agente INTEGER, in_access_token VARCHAR(200), in_public_key VARCHAR(200), in_client_id VARCHAR(200), in_client_secret VARCHAR(200)) 
RETURNS TABLE (id_cuenta_bancaria INTEGER) AS $$
DECLARE
    aux_id_cuenta_bancaria INTEGER;
BEGIN
	IF NOT EXISTS(	select 1
					from cuenta_bancaria cb
					where cb.cbu = in_cbu) THEN
					-- where cb.alias = in_alias OR cb.cbu = in_cbu) THEN

		INSERT INTO cuenta_bancaria (id_oficina, 
									 nombre, 
									 alias, 
									 cbu,
									 id_billetera,
									 id_usuario_noti,
									 id_agente,
									 fecha_hora_creacion,
									 id_usuario_creacion,
									 fecha_hora_ultima_modificacion,
									 id_usuario_ultima_modificacion,
									 marca_baja)
		VALUES (in_id_oficina, in_nombre, in_alias, in_cbu, in_id_billetera, in_id_usuario_noti, in_id_agente, now(), in_id_usuario, now(), in_id_usuario, false)
		RETURNING cuenta_bancaria.id_cuenta_bancaria INTO aux_id_cuenta_bancaria;
		
		INSERT INTO cuenta_bancaria_mercado_pago (id_cuenta_bancaria, access_token, public_key, client_id, client_secret, marca_baja) 
		VALUES (aux_id_cuenta_bancaria, in_access_token, in_public_key, in_client_id, in_client_secret, false);

	ELSE
		aux_id_cuenta_bancaria := 0;
		
	END IF;
	
	RETURN query SELECT aux_id_cuenta_bancaria;
END;
$$ LANGUAGE plpgsql;
--select * from Crear_Cuenta_Bancaria(1, 1, 'Noelia Caleza', 'noelia.227.caleza.mp', '0000003100091595755417' , 1, false);
--select * from cuenta_bancaria;

--DROP FUNCTION Modificar_Cuenta_Bancaria(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_nombre VARCHAR(200), in_alias VARCHAR(200), in_cbu VARCHAR(200), in_estado BOOLEAN, in_id_billetera INTEGER, in_id_usuario_noti INTEGER, in_id_agente INTEGER, in_access_token VARCHAR(200), in_public_key VARCHAR(200), in_client_id VARCHAR(200), in_client_secret VARCHAR(200))
CREATE OR REPLACE FUNCTION Modificar_Cuenta_Bancaria(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_nombre VARCHAR(200), in_alias VARCHAR(200), in_cbu VARCHAR(200), in_estado BOOLEAN, in_id_billetera INTEGER, in_id_usuario_noti INTEGER, in_id_agente INTEGER, in_access_token VARCHAR(200), in_public_key VARCHAR(200), in_client_id VARCHAR(200), in_client_secret VARCHAR(200)) 
RETURNS TABLE (id_cuenta_bancaria INTEGER) AS $$
DECLARE
    aux_id_cuenta_bancaria INTEGER;
BEGIN

	IF NOT EXISTS(	select 1
					from cuenta_bancaria cb
					where 	((cb.alias = in_alias AND cb.alias!='') OR cb.cbu = in_cbu)
				 		AND cb.id_cuenta_bancaria != in_id_cuenta_bancaria) THEN

		UPDATE cuenta_bancaria 	set nombre = in_nombre, 
								alias = in_alias,
								cbu = in_cbu,
								marca_baja = in_estado,
								id_billetera = in_id_billetera,
								id_usuario_noti = in_id_usuario_noti,
								id_agente = in_id_agente,
								fecha_hora_ultima_modificacion = now(),
								id_usuario_ultima_modificacion = in_id_usuario
		WHERE cuenta_bancaria.id_cuenta_bancaria = in_id_cuenta_bancaria;

		aux_id_cuenta_bancaria := in_id_cuenta_bancaria;

		INSERT INTO cuenta_bancaria_hist (id_cuenta_bancaria,
										id_oficina,
    									nombre,
    									alias,
    									cbu,
										id_billetera,
										id_usuario_noti,
										id_agente,
    									marca_baja,
    									fecha_hora_ultima_modificacion,
										id_usuario_ultima_modificacion)
		SELECT	cn.id_cuenta_bancaria,
				cn.id_oficina,
				cn.nombre,
				cn.alias,
				cn.cbu,
				cn.id_billetera,
				cn.id_usuario_noti,
				cn.id_agente,
				cn.marca_baja,
				cn.fecha_hora_ultima_modificacion,
				cn.id_usuario_ultima_modificacion
		FROM cuenta_bancaria cn
		WHERE cn.id_cuenta_bancaria = in_id_cuenta_bancaria;
		
		IF (in_id_billetera = 1) THEN
			IF EXISTS(	SELECT 1
						FROM cuenta_bancaria_mercado_pago cbmp
						WHERE cbmp.id_cuenta_bancaria = in_id_cuenta_bancaria) THEN
				
				UPDATE cuenta_bancaria_mercado_pago
					SET access_token = in_access_token,
						public_key = in_public_key,
						client_id = in_client_id,
						client_secret = in_client_secret
					WHERE cuenta_bancaria_mercado_pago.id_cuenta_bancaria = in_id_cuenta_bancaria;
			ELSE
			
				INSERT INTO cuenta_bancaria_mercado_pago (id_cuenta_bancaria, access_token, public_key, client_id, client_secret, marca_baja) 
				VALUES (in_id_cuenta_bancaria, in_access_token, in_public_key, in_client_id, in_client_secret, false);
			
			END IF;
		END IF;	
	ELSE
		aux_id_cuenta_bancaria := 0;
	END IF;
	
	RETURN query SELECT aux_id_cuenta_bancaria;
END;
$$ LANGUAGE plpgsql;
--select * from Modificar_Cuenta_Bancaria(1, 1, 'Noelia Caleza', 'noelia.227.caleza.mp', '0000003100091595755417' , 1, false);

--DROP FUNCTION Modificar_Cuenta_Bancaria_Operador(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_estado BOOLEAN, in_id_usuario_noti INTEGER, in_id_agente INTEGER)
CREATE OR REPLACE FUNCTION Modificar_Cuenta_Bancaria_Operador(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_estado BOOLEAN, in_id_usuario_noti INTEGER, in_id_agente INTEGER) 
RETURNS TABLE (id_cuenta_bancaria INTEGER) AS $$
BEGIN
	UPDATE cuenta_bancaria 	set marca_baja = in_estado,
							id_usuario_noti = in_id_usuario_noti,
							id_agente = in_id_agente,
							fecha_hora_ultima_modificacion = now(),
							id_usuario_ultima_modificacion = in_id_usuario
	WHERE cuenta_bancaria.id_cuenta_bancaria = in_id_cuenta_bancaria;

	INSERT INTO cuenta_bancaria_hist (id_cuenta_bancaria,
									id_oficina,
									nombre,
									alias,
									cbu,
									id_billetera,
									id_usuario_noti,
									id_agente,
									marca_baja,
									fecha_hora_ultima_modificacion,
									id_usuario_ultima_modificacion)
	SELECT	cn.id_cuenta_bancaria,
			cn.id_oficina,
			cn.nombre,
			cn.alias,
			cn.cbu,
			cn.id_billetera,
			cn.id_usuario_noti,
			cn.id_agente,
			cn.marca_baja,
			cn.fecha_hora_ultima_modificacion,
			cn.id_usuario_ultima_modificacion
	FROM cuenta_bancaria cn
	WHERE cn.id_cuenta_bancaria = in_id_cuenta_bancaria;
	
	RETURN query SELECT in_id_cuenta_bancaria;
END;
$$ LANGUAGE plpgsql;
--select * from Modificar_Cuenta_Bancaria_Operador(3, 1, true, 0, 9)
--select * from Modificar_Cuenta_Bancaria_Operador(1, 1, 'Noelia Caleza', 'noelia.227.caleza.mp', '0000003100091595755417' , 1, false);

--DROP FUNCTION Eliminar_Cuenta_Bancaria(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER)
CREATE OR REPLACE FUNCTION Eliminar_Cuenta_Bancaria(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER)
RETURNS TABLE (id_cuenta_bancaria INTEGER) AS $$
BEGIN
	UPDATE cuenta_bancaria 	set marca_eliminada = true,
								marca_baja = true,
								fecha_hora_ultima_modificacion = now(),
								id_usuario_ultima_modificacion = in_id_usuario
	WHERE cuenta_bancaria.id_cuenta_bancaria = in_id_cuenta_bancaria;

	INSERT INTO cuenta_bancaria_hist (id_cuenta_bancaria,
									id_oficina,
									nombre,
									alias,
									cbu,
									id_billetera,
									id_usuario_noti,
									id_agente,
									marca_baja,
									fecha_hora_ultima_modificacion,
									id_usuario_ultima_modificacion,
									marca_eliminada)
	SELECT	cn.id_cuenta_bancaria,
			cn.id_oficina,
			cn.nombre,
			cn.alias,
			cn.cbu,
			cn.id_billetera,
			cn.id_usuario_noti,
			cn.id_agente,
			cn.marca_baja,
			cn.fecha_hora_ultima_modificacion,
			cn.id_usuario_ultima_modificacion,
			cn.marca_eliminada
	FROM cuenta_bancaria cn
	WHERE cn.id_cuenta_bancaria = in_id_cuenta_bancaria;
	
	RETURN query SELECT in_id_cuenta_bancaria;
END;
$$ LANGUAGE plpgsql;
--select * from Eliminar_Cuenta_Bancaria(3, 1)
--select * from Eliminar_Cuenta_Bancaria(1, 1);

/*
INSERT INTO cuenta_bancaria_hist (id_cuenta_bancaria,
								id_oficina,
								nombre,
								alias,
								cbu,
								id_billetera,
								id_usuario_noti,
								id_agente,
								marca_baja,
								fecha_hora_ultima_modificacion,
								id_usuario_ultima_modificacion)
SELECT	cn.id_cuenta_bancaria,
		cn.id_oficina,
		cn.nombre,
		cn.alias,
		cn.cbu,
		cn.id_billetera,
		cn.id_usuario_noti,
		cn.id_agente,
		cn.marca_baja,
		cn.fecha_hora_ultima_modificacion,
		cn.id_usuario_ultima_modificacion
FROM cuenta_bancaria cn
WHERE cn.id_cuenta_bancaria NOT IN (SELECT DISTINCT id_cuenta_bancaria FROM cuenta_bancaria_hist);
*/

--DROP FUNCTION Cuenta_Bancaria_Subtotales(in_id_cuenta_bancaria INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Subtotales(in_id_cuenta_bancaria INTEGER)
RETURNS TABLE (	id_cuenta_bancaria integer,
				cantidad_cargas bigint,
				monto_cargas numeric) AS $$
BEGIN
	RETURN QUERY
	SELECT	op.id_cuenta_bancaria,
			COUNT(op.importe) AS cantidad_cargas, 
			SUM(op.importe) + op.monto_restante AS monto_cargas
	FROM (
		SELECT	oc.id_cuenta_bancaria,
				oc.importe,
				COALESCE(cbd.id_cuenta_bancaria_descarga, 0) AS id_cuenta_bancaria_descarga,
				COALESCE(cbd.monto_restante, 0) AS monto_restante,
				DENSE_RANK() OVER (PARTITION BY oc.id_cuenta_bancaria
							ORDER BY COALESCE(cbd.id_cuenta_bancaria_descarga, 0) DESC) as orden
		FROM 	operacion_carga oc JOIN operacion op
					ON (op.id_operacion = oc.id_operacion
							AND op.marca_baja = false
							AND oc.id_cuenta_bancaria = in_id_cuenta_bancaria
							AND op.id_accion IN (1,5)  --accion de carga
							AND op.id_estado = 2) --estado aceptado
				LEFT JOIN cuenta_bancaria_descarga cbd
					ON (oc.id_cuenta_bancaria = cbd.id_cuenta_bancaria
						AND cbd.ultima_descarga = true)
		WHERE op.fecha_hora_ultima_modificacion >= COALESCE(cbd.fecha_hora_descarga, '1900-01-01 00:00:00')
		) op
	WHERE op.orden = 1
	GROUP BY op.id_cuenta_bancaria, op.monto_restante;
END;
$$ LANGUAGE plpgsql;
--select * from Cuenta_Bancaria_Subtotales(449);

CREATE OR REPLACE VIEW v_Cuenta_Bancaria_Subtotales AS
SELECT	op.id_cuenta_bancaria,
		COUNT(op.importe) AS cantidad_cargas, 
		SUM(op.importe) + op.monto_restante AS monto_cargas
FROM (
	SELECT	oc.id_cuenta_bancaria,
			oc.importe,
			COALESCE(cbd.id_cuenta_bancaria_descarga, 0) AS id_cuenta_bancaria_descarga,
			COALESCE(cbd.monto_restante, 0) AS monto_restante,
			DENSE_RANK() OVER (PARTITION BY oc.id_cuenta_bancaria
						ORDER BY COALESCE(cbd.id_cuenta_bancaria_descarga, 0) DESC) as orden
	FROM 	operacion_carga oc JOIN operacion op
				ON (op.id_operacion = oc.id_operacion
						AND op.marca_baja = false
						AND op.id_accion IN (1,5)  --accion de carga
						AND op.id_estado = 2) --estado aceptado
			LEFT JOIN cuenta_bancaria_descarga cbd
				ON (oc.id_cuenta_bancaria = cbd.id_cuenta_bancaria
					AND cbd.ultima_descarga = true)
	WHERE op.fecha_hora_ultima_modificacion >= COALESCE(cbd.fecha_hora_descarga, '1900-01-01 00:00:00')
	) op
WHERE op.orden = 1
GROUP BY op.id_cuenta_bancaria, op.monto_restante;

--select * from v_Cuenta_Bancaria_Subtotales order by 1
--SELECT * FROM v_Cuenta_Bancaria_Subtotales where id_cuenta_bancaria in (1,7,134)

--DROP FUNCTION Cuenta_Bancaria_Activa(in_id_cuenta_bancaria INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Activa(in_id_cuenta_bancaria INTEGER)
RETURNS TABLE (	id_oficina integer,
				oficina character varying(100),
			   	id_cuenta_bancaria integer,
				nombre character varying(200),
			   	alias character varying(200),
			   	cbu character varying(200),
				marca_baja boolean,
				id_billetera integer,
				cantidad_cargas bigint,
				monto_cargas numeric,
				orden bigint) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cb.id_oficina,
		o.oficina,
		cb.id_cuenta_bancaria,
		cb.nombre,
		cb.alias,
		cb.cbu,
		cb.marca_baja,
		cb.id_billetera,
		coalesce(cbc.cantidad_cargas, 0)	as cantidad_cargas,
		coalesce(cbc.monto_cargas, 0)		as monto_cargas,
		DENSE_RANK() OVER (PARTITION BY cb.id_oficina, o.oficina
						   ORDER BY coalesce(cbc.cantidad_cargas, 0), coalesce(cbc.monto_cargas, 0)) as orden
	FROM 	cuenta_bancaria cb join oficina o
				ON (cb.id_oficina = o.id_oficina
					AND cb.id_cuenta_bancaria = in_id_cuenta_bancaria
					AND cb.marca_baja = false)
			LEFT JOIN Cuenta_Bancaria_Subtotales(in_id_cuenta_bancaria) cbc
				on (cb.id_cuenta_bancaria = cbc.id_cuenta_bancaria);	
END;
$$ LANGUAGE plpgsql;
--select * from Cuenta_Bancaria_Activa(449);

--DROP VIEW v_Cuenta_Bancaria_Activa;
CREATE OR REPLACE VIEW v_Cuenta_Bancaria_Activa AS
SELECT 	cb.id_oficina,
		o.oficina,
		cb.id_cuenta_bancaria,
		cb.nombre,
		cb.alias,
		cb.cbu,
		cb.marca_baja,
		cb.id_billetera,
		coalesce(cbc.cantidad_cargas, 0)	as cantidad_cargas,
		coalesce(cbc.monto_cargas, 0)		as monto_cargas,
		DENSE_RANK() OVER (PARTITION BY cb.id_oficina, o.oficina
						   ORDER BY coalesce(cbc.cantidad_cargas, 0), coalesce(cbc.monto_cargas, 0)) as orden
FROM 	cuenta_bancaria cb join oficina o
			on (cb.id_oficina = o.id_oficina
			   	AND cb.marca_baja = false)
		LEFT JOIN v_Cuenta_Bancaria_Subtotales cbc
			on (cb.id_cuenta_bancaria = cbc.id_cuenta_bancaria);
--ORDER BY cb.id_oficina, coalesce(cbc.monto_cargas, 0), coalesce(cbc.cantidad_cargas, 0);

--DROP FUNCTION Cuenta_Bancaria_Activa_Operador(in_id_rol INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Activa_Operador(in_id_rol INTEGER, in_id_oficina INTEGER)
RETURNS TABLE (	id_oficina integer,
			  	oficina character varying(100),
			  	id_cuenta_bancaria integer,
			  	nombre character varying(200),
			  	alias character varying(200),
			  	cbu character varying(200),
			  	marca_baja boolean,
			  	id_billetera integer,
			  	cantidad_cargas bigint,
			  	monto_cargas numeric,
			  	orden bigint) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cb.id_oficina,
			o.oficina,
			cb.id_cuenta_bancaria,
			cb.nombre,
			cb.alias,
			cb.cbu,
			cb.marca_baja,
			cb.id_billetera,
			coalesce(cbc.cantidad_cargas, 0)	as cantidad_cargas,
			coalesce(cbc.monto_cargas, 0)		as monto_cargas,
			DENSE_RANK() OVER (PARTITION BY cb.id_oficina, o.oficina
							   ORDER BY coalesce(cbc.cantidad_cargas, 0), coalesce(cbc.monto_cargas, 0)) as orden
	FROM 	cuenta_bancaria cb join oficina o
				on (cb.id_oficina = o.id_oficina
					AND cb.marca_baja = false)
			LEFT JOIN v_Cuenta_Bancaria_Subtotales cbc
				on (cb.id_cuenta_bancaria = cbc.id_cuenta_bancaria)
	WHERE CASE WHEN in_id_rol < 2 THEN true
			ELSE CASE WHEN in_id_oficina = cb.id_oficina THEN true ELSE false END 
			END
	ORDER BY orden;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Cuenta_Bancaria_Activa_Cliente_Agente(in_id_billetera INTEGER, in_id_oficina INTEGER, in_id_agente INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Activa_Cliente_Agente(in_id_billetera INTEGER, in_id_oficina INTEGER, in_id_agente INTEGER)
RETURNS TABLE (	id_oficina integer,
			  	oficina character varying(100),
			  	id_cuenta_bancaria integer,
			  	nombre character varying(200),
			  	alias character varying(200),
			  	cbu character varying(200),
			  	marca_baja boolean,
			  	id_billetera integer,
			  	cantidad_cargas bigint,
			  	monto_cargas numeric,
			  	orden bigint) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cb.id_oficina,
			o.oficina,
			cb.id_cuenta_bancaria,
			cb.nombre,
			cb.alias,
			cb.cbu,
			cb.marca_baja,
			cb.id_billetera,
			coalesce(cbc.cantidad_cargas, 0)	as cantidad_cargas,
			coalesce(cbc.monto_cargas, 0)		as monto_cargas,
			DENSE_RANK() OVER (PARTITION BY cb.id_oficina, o.oficina
							   ORDER BY coalesce(cbc.cantidad_cargas, 0), coalesce(cbc.monto_cargas, 0)) as orden
	FROM 	cuenta_bancaria cb join oficina o
				ON (cb.id_oficina = o.id_oficina
					AND cb.marca_baja = false
					AND cb.id_oficina = in_id_oficina
					AND (cb.id_agente = in_id_agente OR cb.id_agente = 0)
					AND CASE WHEN in_id_billetera = 1 THEN cb.id_billetera = 1
				   		ELSE cb.id_billetera > 1 END)
			LEFT JOIN v_Cuenta_Bancaria_Subtotales cbc
				on (cb.id_cuenta_bancaria = cbc.id_cuenta_bancaria)
	ORDER BY orden LIMIT 1;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Cuenta_Bancaria_Activa_Cliente(in_id_billetera INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Activa_Cliente(in_id_billetera INTEGER, in_id_oficina INTEGER)
RETURNS TABLE (	id_oficina integer,
			  	oficina character varying(100),
			  	id_cuenta_bancaria integer,
			  	nombre character varying(200),
			  	alias character varying(200),
			  	cbu character varying(200),
			  	marca_baja boolean,
			  	id_billetera integer,
			  	cantidad_cargas bigint,
			  	monto_cargas numeric,
			  	orden bigint) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cb.id_oficina,
			o.oficina,
			cb.id_cuenta_bancaria,
			cb.nombre,
			cb.alias,
			cb.cbu,
			cb.marca_baja,
			cb.id_billetera,
			coalesce(cbc.cantidad_cargas, 0)	as cantidad_cargas,
			coalesce(cbc.monto_cargas, 0)		as monto_cargas,
			DENSE_RANK() OVER (PARTITION BY cb.id_oficina, o.oficina
							   ORDER BY coalesce(cbc.cantidad_cargas, 0), coalesce(cbc.monto_cargas, 0)) as orden
	FROM 	cuenta_bancaria cb join oficina o
				on (cb.id_oficina = o.id_oficina
					AND cb.marca_baja = false
					AND cb.id_oficina = in_id_oficina
					AND CASE WHEN in_id_billetera = 1 THEN cb.id_billetera = 1
				   		ELSE cb.id_billetera > 1 END)
			LEFT JOIN v_Cuenta_Bancaria_Subtotales cbc
				on (cb.id_cuenta_bancaria = cbc.id_cuenta_bancaria)
	ORDER BY orden LIMIT 1;
END;
$$ LANGUAGE plpgsql;

--SELECT * FROM v_Cuenta_Bancaria_Activa where id_oficina = 2
--SELECT * FROM v_Cuenta_Bancaria_Subtotales where id_cuenta_bancaria in (1,13,134)
--SELECT * FROM Cuenta_Bancaria_Activa_Cliente(1, 2)

--DROP FUNCTION Descargar_Cuenta_Bancaria(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_monto_descarga NUMERIC);
CREATE OR REPLACE FUNCTION Descargar_Cuenta_Bancaria(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_monto_descarga NUMERIC) 
RETURNS TABLE (cbu VARCHAR(200), cargas_monto NUMERIC, cargas_cantidad INTEGER) AS $$
DECLARE
    aux_cbu VARCHAR(200);
    aux_cargas_monto NUMERIC;
    aux_cargas_cantidad INTEGER;
    aux_monto_restante NUMERIC;
BEGIN	
	IF EXISTS (	SELECT 1 
			   	FROM v_Cuenta_Bancaria_Activa cb
				WHERE cb.id_cuenta_bancaria = in_id_cuenta_bancaria
			  	AND cb.cantidad_cargas > 0) THEN
	
		SELECT cb.cbu, cb.cantidad_cargas, cb.monto_cargas
		INTO aux_cbu, aux_cargas_cantidad, aux_cargas_monto
		FROM v_Cuenta_Bancaria_Activa cb
		WHERE cb.id_cuenta_bancaria = in_id_cuenta_bancaria;
		
		aux_monto_restante := aux_cargas_monto - in_monto_descarga;
		
		UPDATE cuenta_bancaria_descarga SET ultima_descarga = false 
		WHERE cuenta_bancaria_descarga.ultima_descarga = true
		AND cuenta_bancaria_descarga.id_cuenta_bancaria = in_id_cuenta_bancaria;
		
		INSERT INTO cuenta_bancaria_descarga (id_cuenta_bancaria, id_usuario, fecha_hora_descarga, cargas_cantidad, cargas_monto, marca_baja, monto_restante, ultima_descarga)
		VALUES (in_id_cuenta_bancaria, in_id_usuario, now(), aux_cargas_cantidad, in_monto_descarga, false, aux_monto_restante, true);
		
	ELSE
		aux_cbu := '';
		aux_cargas_monto := 0;
		aux_cargas_cantidad := 0;
		aux_monto_restante := 0;
		
	END IF;	
	
	RETURN query SELECT aux_cbu, aux_monto_restante, aux_cargas_cantidad;
END;
$$ LANGUAGE plpgsql;
-- select * from Descargar_Cuenta_Bancaria(1,8);

insert into estado(estado) values ('Pendiente');
insert into estado(estado) values ('Aceptado');
insert into estado(estado) values ('Rechazado');
--select * from estado order by 1;

insert into accion(accion) values ('Carga');
insert into accion(accion) values ('Retiro');
insert into accion(accion) values ('Cambio de Contraseña');
insert into accion(accion) values ('Desbloqueo de Usuario');
insert into accion(accion) values ('Carga Manual');
insert into accion(accion) values ('Retiro Manual');
insert into accion(accion) values ('Primer Ingreso');
insert into accion(accion) values ('Creación de Usuario');
insert into accion(accion) values ('Carga Bono Referido');
--select * from accion;

--DROP FUNCTION Obtener_Oficina();
CREATE OR REPLACE FUNCTION Obtener_Oficina()
RETURNS TABLE (id_oficina INTEGER, oficina VARCHAR(100)) AS $$
begin
	RETURN query 
	SELECT 	o.id_oficina, 
			o.oficina
	FROM oficina o;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Oficina();

--DROP FUNCTION Registrar_Cliente_Accion(in_id_cliente INTEGER, in_id_accion INTEGER, in_titular VARCHAR(200), in_importe NUMERIC, in_id_cuenta_bancaria INTEGER, in_cbu VARCHAR(200), in_bono INTEGER) 
CREATE OR REPLACE FUNCTION Registrar_Cliente_Accion(in_id_cliente INTEGER, in_id_accion INTEGER, in_titular VARCHAR(200), in_importe NUMERIC, in_id_cuenta_bancaria INTEGER, in_cbu VARCHAR(200), in_bono INTEGER) 
RETURNS TABLE (id_operacion INTEGER, codigo_operacion INTEGER) AS $$
DECLARE
    aux_id_operacion INTEGER;
    aux_codigo_operacion INTEGER;
    aux_codigo_operacion_hist INTEGER;
    aux_id_estado INTEGER;
    aux_id_oficina INTEGER;
    aux_id_usuario INTEGER;
    aux_id_usuario_modi INTEGER;
BEGIN	
	aux_id_oficina := 0;
	aux_id_usuario := 0;

	IF (in_id_accion IN (3,4)) THEN
		aux_id_estado := 2;
	ELSE
		aux_id_estado := 1;
	END IF;

	aux_id_usuario_modi := 1;

	IF (in_id_accion = 2) THEN
		SELECT ag.id_oficina
		INTO aux_id_oficina
		FROM agente ag JOIN cliente cl
		ON (ag.id_agente = cl.id_agente
		   AND cl.id_cliente = in_id_cliente);

		SELECT 	u.id_usuario --, COALESCE(COUNT(opr.id_operacion), 0) AS cant_pendientes
		INTO 	aux_id_usuario
		FROM	usuario u JOIN oficina o
					ON (u.id_oficina = o.id_oficina
					   AND u.marca_baja = false
					   AND u.recibe_retiro = true
					   AND u.id_rol = 3
					   AND o.id_oficina = aux_id_oficina)
				JOIN usuario_sesion us
					ON (u.id_usuario = us.id_usuario
					   AND us.fecha_hora_cierre IS NULL)
				LEFT JOIN operacion_retiro opr
					ON (opr.id_usuario = u.id_usuario)
				LEFT JOIN operacion op
					ON (op.id_operacion = opr.id_operacion
					   AND op.marca_baja = false
					   AND op.id_estado = 1)									   
		GROUP BY u.id_usuario, 
				u.usuario
		ORDER BY COALESCE(COUNT(op.id_operacion), 0), u.id_usuario
		LIMIT 1;
		
		IF (aux_id_usuario IS NULL) THEN
			aux_id_usuario := 0;
		ELSE
			aux_id_usuario_modi := aux_id_usuario;
		END IF;
		
	END IF;

	SELECT	COALESCE(COUNT(c.id_operacion), 0)
	INTO 	aux_codigo_operacion
	FROM operacion c
	WHERE c.id_cliente = in_id_cliente;
	
	SELECT	COALESCE(COUNT(c.id_operacion), 0)
	INTO 	aux_codigo_operacion_hist
	FROM operacion_hist c
	WHERE c.id_cliente = in_id_cliente;
	
	aux_codigo_operacion := aux_codigo_operacion + aux_codigo_operacion_hist + 1;
	
	INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (aux_codigo_operacion, in_id_accion, in_id_cliente, aux_id_estado, true, false, now(), now(), aux_id_usuario_modi)
	RETURNING operacion.id_operacion INTO aux_id_operacion;
	
	IF (in_id_accion = 1) THEN
		INSERT INTO operacion_carga (id_operacion, titular, importe, id_cuenta_bancaria, bono, sol_importe, sol_id_cuenta_bancaria, sol_bono)
		VALUES (aux_id_operacion, in_titular, in_importe, in_id_cuenta_bancaria, in_bono, in_importe, in_id_cuenta_bancaria, in_bono);
	END IF;
	
	IF (in_id_accion = 2) THEN
		INSERT INTO operacion_retiro (id_operacion, cbu, titular, importe, sol_importe, id_usuario)
		VALUES (aux_id_operacion, in_cbu, in_titular, in_importe, in_importe, aux_id_usuario);
	END IF;
	
	RETURN QUERY SELECT aux_id_operacion, aux_codigo_operacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Registrar_Carga_Manual(in_id_cliente INTEGER, in_id_usuario INTEGER, in_importe NUMERIC, in_bono INTEGER, in_titular VARCHAR(200), in_id_cuenta_bancaria INTEGER, in_observaciones VARCHAR(200))  
CREATE OR REPLACE FUNCTION Registrar_Carga_Manual(in_id_cliente INTEGER, in_id_usuario INTEGER, in_importe NUMERIC, in_bono INTEGER, in_titular VARCHAR(200), in_id_cuenta_bancaria INTEGER, in_observaciones VARCHAR(200)) 
RETURNS TABLE (id_operacion INTEGER, codigo_operacion INTEGER) AS $$
DECLARE
    aux_id_operacion INTEGER;
    aux_codigo_operacion INTEGER;
    aux_codigo_operacion_hist INTEGER;
BEGIN
	SELECT	COALESCE(COUNT(ope.id_operacion), 0)
	INTO 	aux_codigo_operacion
	FROM operacion ope
	WHERE ope.id_cliente = in_id_cliente;
	
	SELECT	COALESCE(COUNT(ope.id_operacion), 0)
	INTO 	aux_codigo_operacion_hist
	FROM operacion_hist ope
	WHERE ope.id_cliente = in_id_cliente;
	
	aux_codigo_operacion := aux_codigo_operacion + aux_codigo_operacion_hist + 1;
	
	INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (aux_codigo_operacion, 5, in_id_cliente, 2, false, false, now(), now(), in_id_usuario)
	RETURNING operacion.id_operacion INTO aux_id_operacion;

	INSERT INTO operacion_carga (id_operacion, titular, importe, id_cuenta_bancaria, bono, sol_importe, sol_id_cuenta_bancaria, sol_bono, observaciones)
	VALUES (aux_id_operacion, in_titular, in_importe, in_id_cuenta_bancaria, in_bono, in_importe, in_id_cuenta_bancaria, in_bono, in_observaciones);
	
	RETURN QUERY SELECT aux_id_operacion, aux_codigo_operacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Registrar_Retiro_Manual(in_id_cliente INTEGER, in_id_usuario INTEGER, in_importe NUMERIC, in_cbu VARCHAR(30), in_titular VARCHAR(200), in_observaciones VARCHAR(200))  
CREATE OR REPLACE FUNCTION Registrar_Retiro_Manual(in_id_cliente INTEGER, in_id_usuario INTEGER, in_importe NUMERIC, in_cbu VARCHAR(30), in_titular VARCHAR(200), in_observaciones VARCHAR(200)) 
RETURNS TABLE (id_operacion INTEGER, codigo_operacion INTEGER) AS $$
DECLARE
    aux_id_operacion INTEGER;
    aux_codigo_operacion INTEGER;
	aux_codigo_operacion_hist INTEGER;
BEGIN
	SELECT	COALESCE(COUNT(ope.id_operacion), 0)
	INTO 	aux_codigo_operacion
	FROM operacion ope
	WHERE ope.id_cliente = in_id_cliente;
	
	SELECT	COALESCE(COUNT(ope.id_operacion), 0)
	INTO 	aux_codigo_operacion_hist
	FROM operacion_hist ope
	WHERE ope.id_cliente = in_id_cliente;
	
	aux_codigo_operacion := aux_codigo_operacion + aux_codigo_operacion_hist + 1;
	
	INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (aux_codigo_operacion, 6, in_id_cliente, 2, false, false, now(), now(), in_id_usuario)
	RETURNING operacion.id_operacion INTO aux_id_operacion;

	INSERT INTO operacion_retiro (id_operacion, cbu, titular, importe, sol_importe, observaciones)
	VALUES (aux_id_operacion, in_cbu, in_titular, in_importe, in_importe, in_observaciones);
	
	RETURN QUERY SELECT aux_id_operacion, aux_codigo_operacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Cliente_Carga(in_id_operacion INTEGER, in_id_estado INTEGER, in_importe NUMERIC, in_bono NUMERIC, in_id_cuenta_bancaria INTEGER, in_id_usuario INTEGER) 
CREATE OR REPLACE FUNCTION Modificar_Cliente_Carga(in_id_operacion INTEGER, in_id_estado INTEGER, in_importe NUMERIC, in_bono NUMERIC, in_id_cuenta_bancaria INTEGER, in_id_usuario INTEGER) 
RETURNS VOID AS $$
BEGIN
	UPDATE operacion
	SET	fecha_hora_ultima_modificacion = NOW(),
		id_usuario_ultima_modificacion = in_id_usuario,
		id_estado = in_id_estado,
		notificado = false
	WHERE id_operacion = in_id_operacion;

	IF (in_id_estado = 2) THEN
		UPDATE operacion_carga
		SET	importe = in_importe,
			bono = in_bono,
			id_cuenta_bancaria = in_id_cuenta_bancaria,
			procesando = false
		WHERE id_operacion = in_id_operacion;
	END IF;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Cliente_Carga_Rechazo(in_id_operacion INTEGER, in_id_estado INTEGER, in_id_usuario INTEGER) 
CREATE OR REPLACE FUNCTION Modificar_Cliente_Carga_Rechazo(in_id_operacion INTEGER, in_id_estado INTEGER, in_id_usuario INTEGER) 
RETURNS VOID AS $$
DECLARE aux_procesando BOOLEAN;
DECLARE aux_id_estado INTEGER;
DECLARE lock_key BIGINT := 2;  -- Una clave única para el bloqueo advisory
BEGIN
    -- Intentar adquirir el bloqueo. pg_advisory_lock es una llamada bloqueante.
    PERFORM pg_advisory_lock(lock_key);
	
	-- Aquí pones el código que deseas ejecutar de manera exclusiva
    BEGIN
	
		SELECT	opc.procesando, op.id_estado
		INTO 	aux_procesando, aux_id_estado
		FROM 	operacion_carga opc JOIN operacion op
				ON (op.id_operacion = opc.id_operacion
					AND op.id_operacion = in_id_operacion);
		
		IF (aux_id_estado = 2) THEN
			aux_procesando := true;
		END IF;

		IF (NOT aux_procesando) THEN
			UPDATE operacion
			SET	fecha_hora_ultima_modificacion = NOW(),
				id_usuario_ultima_modificacion = in_id_usuario,
				id_estado = in_id_estado,
				notificado = false
			WHERE id_operacion = in_id_operacion;
		END IF;

    EXCEPTION
        WHEN OTHERS THEN
            -- Si ocurre algún error, liberamos el bloqueo para evitar deadlocks
            PERFORM pg_advisory_unlock(lock_key);
            RAISE;
    END;
	-- Liberar el bloqueo advisory
    PERFORM pg_advisory_unlock(lock_key);
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Modificar_Cliente_Retiro(in_id_operacion INTEGER, in_id_estado INTEGER, in_importe NUMERIC, in_bono NUMERIC, in_id_cuenta_bancaria INTEGER, in_id_usuario INTEGER) 
CREATE OR REPLACE FUNCTION Modificar_Cliente_Retiro(in_id_operacion INTEGER, in_id_estado INTEGER, in_importe NUMERIC, in_id_usuario INTEGER) 
RETURNS VOID AS $$
BEGIN	
	UPDATE operacion
	SET	fecha_hora_ultima_modificacion = NOW(),
		id_usuario_ultima_modificacion = in_id_usuario,
		id_estado = in_id_estado,
		notificado = false
	WHERE id_operacion = in_id_operacion;
	
	IF (in_id_estado = 2) THEN
		UPDATE operacion_retiro
		SET	importe = in_importe
		WHERE id_operacion = in_id_operacion;
	END IF;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Bloqueo_Cliente(in_id_usuario INTEGER, in_id_cliente INTEGER, in_bloqueado BOOLEAN) 
CREATE OR REPLACE FUNCTION Bloqueo_Cliente(in_id_usuario INTEGER, in_id_cliente INTEGER, in_bloqueado BOOLEAN) 
RETURNS VOID AS $$
BEGIN
	UPDATE cliente
	SET	fecha_hora_ultima_modificacion = NOW(),
		id_usuario_ultima_modificacion = in_id_usuario,
		bloqueado = in_bloqueado
	WHERE id_cliente = in_id_cliente;
	
	IF (in_bloqueado) THEN
	
		INSERT INTO ip_lista_negra (ip)
		SELECT DISTINCT cls.ip
        FROM cliente cl JOIN cliente_sesion cls
        	ON (cl.id_cliente = in_id_cliente 
				AND cls.id_cliente = cl.id_cliente
            	AND cls.ip NOT IN ('', '0.0.0.0', '::1')
			   	AND cls.ip NOT IN (SELECT lni.ip FROM ip_lista_negra lni));
	
		INSERT INTO ip_lista_negra (ip)
		SELECT DISTINCT cls.ip
        FROM cliente cl JOIN cliente_sesion_hist cls
        	ON (cl.id_cliente = in_id_cliente 
				AND cls.id_cliente = cl.id_cliente
            	AND cls.ip NOT IN ('', '0.0.0.0', '::1')
			   	AND cls.ip NOT IN (SELECT ipln.ip FROM ip_lista_negra ipln));
	
	ELSE
	
		DELETE FROM ip_lista_negra ipln
		WHERE (ipln.ip IN (SELECT DISTINCT cls.ip
							FROM cliente cl JOIN cliente_sesion cls
								ON (cl.id_cliente = in_id_cliente 
									AND cls.id_cliente = cl.id_cliente))
				OR
				ipln.ip IN (SELECT DISTINCT cls.ip
							FROM cliente cl JOIN cliente_sesion_hist cls
								ON (cl.id_cliente = in_id_cliente 
									AND cls.id_cliente = cl.id_cliente)))
			AND ipln.ip NOT IN (SELECT DISTINCT cls.ip
							FROM cliente cl JOIN cliente_sesion cls
								ON (cl.bloqueado = true
									AND cls.id_cliente = cl.id_cliente))
			AND ipln.ip NOT IN (SELECT DISTINCT cls.ip
							FROM cliente cl JOIN cliente_sesion_hist cls
								ON (cl.bloqueado = true
									AND cls.id_cliente = cl.id_cliente));
	END IF;

END;
$$ LANGUAGE plpgsql;
--select * from ip_lista_negra

--DROP FUNCTION Obtener_Cliente_Chat(in_id_cliente INTEGER, in_es_cliente BOOLEAN)
CREATE OR REPLACE FUNCTION Obtener_Cliente_Chat(in_id_cliente INTEGER, in_es_cliente BOOLEAN)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   enlace VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100)) AS $$
BEGIN
	IF (in_es_cliente) THEN
		UPDATE cliente_chat
		SET visto_cliente = true
		WHERE cliente_chat.id_cliente = in_id_cliente
		AND cliente_chat.visto_cliente = false;
		
		UPDATE cliente_chat_adjunto
		SET visto_cliente = true
		WHERE cliente_chat_adjunto.id_cliente = in_id_cliente
		AND cliente_chat_adjunto.visto_cliente = false;
	ELSE
		UPDATE cliente_chat
		SET visto_operador = true
		WHERE cliente_chat.id_cliente = in_id_cliente
		AND cliente_chat.visto_operador = false;
		
		UPDATE cliente_chat_adjunto
		SET visto_operador = true
		WHERE cliente_chat_adjunto.id_cliente = in_id_cliente
		AND cliente_chat_adjunto.visto_operador = false;
	END IF;
	
	RETURN QUERY SELECT clc.id_cliente_chat,
					clc.id_cliente,
					clc.mensaje,
					COALESCE((SELECT UNNEST(regexp_match(clc.mensaje,'(https?://[^\s]+)'))),'')::VARCHAR(200) as enlace,
					'' as nombre_original,
					'' as nombre_guardado,
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario
	FROM cliente_chat clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
		AND clc.marca_baja = false
	UNION
	SELECT clc.id_cliente_chat_adjunto as id_cliente_chat,
					clc.id_cliente, 
					'' as mensaje,
					''::VARCHAR(200) as enlace,
					clc.nombre_original, 
					clc.nombre_guardado, 
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario
	FROM cliente_chat_adjunto clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;
/*select id_cliente_chat,id_cliente, mensaje, 		
		enlace,
		nombre_original,
		TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,
		TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, 
		enviado_cliente, visto_cliente, 
		visto_operador, id_usuario, usuario, 
		TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) 
			OVER (ORDER BY fecha_hora_creacion) AS misma_fecha 
from Obtener_Cliente_Chat(4, true) order by 1 desc;*/

--DROP FUNCTION Obtener_Cliente_Chat_Operador(in_id_cliente INTEGER, in_id_rol INTEGER)
CREATE OR REPLACE FUNCTION Obtener_Cliente_Chat_Operador(in_id_cliente INTEGER, in_id_rol INTEGER)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
BEGIN
	IF (in_id_rol > 1) THEN
		UPDATE cliente_chat
		SET visto_operador = true
		WHERE cliente_chat.id_cliente = in_id_cliente
		AND cliente_chat.visto_operador = false;

		UPDATE cliente_chat_adjunto
		SET visto_operador = true
		WHERE cliente_chat_adjunto.id_cliente = in_id_cliente
		AND cliente_chat_adjunto.visto_operador = false;
	END IF;
	
	RETURN QUERY SELECT clc.id_cliente_chat,
					clc.id_cliente, 
					clc.mensaje,
					'' as nombre_original,
					'' as nombre_guardado,
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					clc.marca_baja
	FROM cliente_chat clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	UNION
	SELECT clc.id_cliente_chat_adjunto as id_cliente_chat,
					clc.id_cliente, 
					'' as mensaje,
					clc.nombre_original, 
					clc.nombre_guardado, 
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM cliente_chat_adjunto clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;
/*select id_cliente_chat,id_cliente, mensaje, nombre_original,
		TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') as fecha_mensaje,
		TO_CHAR(fecha_hora_creacion, 'HH24:MI') as horario_mensaje, 
		enviado_cliente, visto_cliente, 
		visto_operador, id_usuario, usuario, marca_baja,
		TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY') = LAG(TO_CHAR(fecha_hora_creacion, 'DD/MM/YYYY')) 
			OVER (ORDER BY fecha_hora_creacion) AS misma_fecha 
from Obtener_Cliente_Chat_Operador(1, 1);*/

--DROP FUNCTION Obtener_Cliente_Visita_Chat_Operador(in_id_visita_sesion INTEGER, in_id_rol INTEGER)
CREATE OR REPLACE FUNCTION Obtener_Cliente_Visita_Chat_Operador(in_id_visita_sesion INTEGER, in_id_rol INTEGER)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
BEGIN
	IF (in_id_rol > 1) THEN
		UPDATE visita_sesion_chat
		SET visto_operador = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_operador = false;

		UPDATE visita_sesion_chat_adjunto
		SET visto_operador = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_operador = false;
	END IF;
	
	RETURN QUERY SELECT vsc.id_visita_sesion_chat 	as id_cliente_chat,
					vsc.id_visita_sesion 			as id_cliente, 
					vsc.mensaje,
					'' as nombre_original,
					'' as nombre_guardado,
					vsc.fecha_hora_creacion, 
					vsc.enviado_cliente, 
					vsc.visto_cliente, 
					vsc.visto_operador,
					vsc.id_usuario,
					u.usuario,
					vsc.marca_baja
	FROM visita_sesion_chat vsc JOIN usuario u
			ON (vsc.id_usuario = u.id_usuario)
	WHERE vsc.id_visita_sesion = in_id_visita_sesion
	UNION
	SELECT vsc.id_visita_sesion_chat_adjunto 	as id_cliente_chat,
					vsc.id_visita_sesion		as id_cliente, 
					'' as mensaje,
					vsc.nombre_original, 
					vsc.nombre_guardado, 
					vsc.fecha_hora_creacion, 
					vsc.enviado_cliente, 
					vsc.visto_cliente, 
					vsc.visto_operador,
					vsc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM visita_sesion_chat_adjunto vsc JOIN usuario u
			ON (vsc.id_usuario = u.id_usuario)
	WHERE vsc.id_visita_sesion = in_id_visita_sesion
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Cliente_Chat_Eliminar(in_id_cliente_chat INTEGER, in_id_cliente INTEGER, in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Obtener_Cliente_Chat_Eliminar(in_id_cliente_chat INTEGER, in_id_cliente INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
BEGIN
	UPDATE cliente_chat
	SET marca_baja = true,
		fecha_hora_baja = NOW(),
		id_usuario_baja = in_id_usuario
	WHERE cliente_chat.id_cliente_chat = in_id_cliente_chat;

	UPDATE cliente_chat
	SET visto_operador = true
	WHERE cliente_chat.id_cliente = in_id_cliente
	AND cliente_chat.visto_operador = false;

	UPDATE cliente_chat_adjunto
	SET visto_operador = true
	WHERE cliente_chat_adjunto.id_cliente = in_id_cliente
	AND cliente_chat_adjunto.visto_operador = false;
	
	RETURN QUERY SELECT clc.id_cliente_chat,
					clc.id_cliente, 
					clc.mensaje,
					'' as nombre_original,
					'' as nombre_guardado,
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					clc.marca_baja
	FROM cliente_chat clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	UNION
	SELECT clc.id_cliente_chat_adjunto as id_cliente_chat,
					clc.id_cliente, 
					'' as mensaje,
					clc.nombre_original, 
					clc.nombre_guardado, 
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM cliente_chat_adjunto clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Cliente_Visita_Chat_Eliminar(in_id_visita_sesion_chat INTEGER, in_id_visita_sesion INTEGER, in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Obtener_Cliente_Visita_Chat_Eliminar(in_id_visita_sesion_chat INTEGER, in_id_visita_sesion INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
BEGIN
	UPDATE visita_sesion_chat
	SET marca_baja = true,
		fecha_hora_baja = NOW(),
		id_usuario_baja = in_id_usuario
	WHERE visita_sesion_chat.id_visita_sesion_chat = in_id_visita_sesion_chat;

	UPDATE visita_sesion_chat
	SET visto_operador = true
	WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
	AND visita_sesion_chat.visto_operador = false;

	UPDATE visita_sesion_chat_adjunto
	SET visto_operador = true
	WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
	AND visita_sesion_chat_adjunto.visto_operador = false;
	
	RETURN QUERY SELECT vsc.id_visita_sesion_chat 	as id_cliente_chat,
					vsc.id_visita_sesion 			as id_cliente, 
					vsc.mensaje,
					'' as nombre_original,
					'' as nombre_guardado,
					vsc.fecha_hora_creacion, 
					vsc.enviado_cliente, 
					vsc.visto_cliente, 
					vsc.visto_operador,
					vsc.id_usuario,
					u.usuario,
					vsc.marca_baja
	FROM visita_sesion_chat vsc JOIN usuario u
			ON (vsc.id_usuario = u.id_usuario)
	WHERE vsc.id_visita_sesion = in_id_visita_sesion
	UNION
	SELECT vsc.id_visita_sesion_chat_adjunto 	as id_cliente_chat,
					vsc.id_visita_sesion		as id_cliente, 
					'' as mensaje,
					vsc.nombre_original, 
					vsc.nombre_guardado, 
					vsc.fecha_hora_creacion, 
					vsc.enviado_cliente, 
					vsc.visto_cliente, 
					vsc.visto_operador,
					vsc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM visita_sesion_chat_adjunto vsc JOIN usuario u
			ON (vsc.id_usuario = u.id_usuario)
	WHERE vsc.id_visita_sesion = in_id_visita_sesion
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Insertar_Cliente_Chat(in_id_cliente INTEGER, in_es_cliente BOOLEAN, in_mensaje VARCHAR(200), in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Insertar_Cliente_Chat(in_id_cliente INTEGER, in_es_cliente BOOLEAN, in_mensaje VARCHAR(200), in_id_usuario INTEGER)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   enlace VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
DECLARE
    aux_visto_operador BOOLEAN;
BEGIN
	IF (in_es_cliente) THEN
		UPDATE cliente_chat
		SET visto_cliente = true
		WHERE cliente_chat.id_cliente = in_id_cliente
		AND cliente_chat.visto_cliente = false;
				
		UPDATE cliente_chat_adjunto
		SET visto_cliente = true
		WHERE cliente_chat_adjunto.id_cliente = in_id_cliente
		AND cliente_chat_adjunto.visto_cliente = false;
		
		aux_visto_operador := false;
	ELSE
		UPDATE cliente_chat
		SET visto_operador = true
		WHERE cliente_chat.id_cliente = in_id_cliente
		AND cliente_chat.visto_operador = false;
		
		UPDATE cliente_chat_adjunto
		SET visto_operador = true
		WHERE cliente_chat_adjunto.id_cliente = in_id_cliente
		AND cliente_chat_adjunto.visto_operador = false;
		
		aux_visto_operador := true;
	END IF;
	
	INSERT INTO cliente_chat (id_cliente, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
	VALUES (in_id_cliente, in_mensaje, NOW(), in_es_cliente, in_es_cliente, aux_visto_operador, in_id_usuario);
		
	RETURN QUERY SELECT clc.id_cliente_chat,
					clc.id_cliente, 
					clc.mensaje,
					COALESCE((SELECT UNNEST(regexp_match(clc.mensaje,'(https?://[^\s]+)'))),'')::VARCHAR(200) as enlace,
					'' as nombre_original,
					'' as nombre_guardado,
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					clc.marca_baja
	FROM cliente_chat clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	AND (clc.marca_baja = false OR in_es_cliente = false)
	UNION
	SELECT clc.id_cliente_chat_adjunto as id_cliente_chat,
					clc.id_cliente, 
					'' as mensaje,
					''::VARCHAR(200) as enlace,
					clc.nombre_original, 
					clc.nombre_guardado, 
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM cliente_chat_adjunto clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_cliente = in_id_cliente
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;
--select * from Insertar_Cliente_Chat(1, true, 'prueba mensaje 4 desde cliente', 1);
--select * from Insertar_Cliente_Chat(1, false, 'prueba mensaje 4 desde operador', 3);

--DROP FUNCTION Insertar_Cliente_Visita_Chat(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN, in_mensaje VARCHAR(200), in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Insertar_Cliente_Visita_Chat(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN, in_mensaje VARCHAR(200), in_id_usuario INTEGER)
RETURNS TABLE (id_cliente_chat INTEGER,
			   id_cliente INTEGER,
			   mensaje VARCHAR(200), 
			   enlace VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
DECLARE
    aux_visto_operador BOOLEAN;
BEGIN
	IF (in_es_cliente) THEN
		UPDATE visita_sesion_chat
		SET visto_cliente = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_cliente = false;
				
		UPDATE visita_sesion_chat_adjunto
		SET visto_cliente = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_cliente = false;
		
		aux_visto_operador := false;
	ELSE
		UPDATE visita_sesion_chat
		SET visto_operador = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_operador = false;
		
		UPDATE visita_sesion_chat_adjunto
		SET visto_operador = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_operador = false;
		
		aux_visto_operador := true;
	END IF;
	
	INSERT INTO visita_sesion_chat (id_visita_sesion, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
	VALUES (in_id_visita_sesion, in_mensaje, NOW(), in_es_cliente, in_es_cliente, aux_visto_operador, in_id_usuario);
		
	RETURN QUERY SELECT vsc.id_visita_sesion 	as id_cliente_chat,
					vsc.id_visita_sesion 		as id_cliente, 
					vsc.mensaje,
					COALESCE((SELECT UNNEST(regexp_match(vsc.mensaje,'(https?://[^\s]+)'))),'')::VARCHAR(200) as enlace,
					'' as nombre_original,
					'' as nombre_guardado,
					vsc.fecha_hora_creacion, 
					vsc.enviado_cliente, 
					vsc.visto_cliente, 
					vsc.visto_operador,
					vsc.id_usuario,
					u.usuario,
					vsc.marca_baja
	FROM visita_sesion_chat vsc JOIN usuario u
			ON (vsc.id_usuario = u.id_usuario)
	WHERE vsc.id_visita_sesion = in_id_visita_sesion
	AND (vsc.marca_baja = false OR in_es_cliente = false)
	UNION
	SELECT vsc.id_visita_sesion_chat_adjunto 	as id_cliente_chat,
					vsc.id_visita_sesion 		as id_cliente, 
					'' as mensaje,
					''::VARCHAR(200) as enlace,
					vsc.nombre_original, 
					vsc.nombre_guardado, 
					vsc.fecha_hora_creacion, 
					vsc.enviado_cliente, 
					vsc.visto_cliente, 
					vsc.visto_operador,
					vsc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM visita_sesion_chat_adjunto vsc JOIN usuario u
			ON (vsc.id_usuario = u.id_usuario)
	WHERE vsc.id_visita_sesion = in_id_visita_sesion
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Insertar_Cliente_Chat_Adjunto(in_id_cliente INTEGER, in_es_cliente BOOLEAN, in_nombre_original VARCHAR(200), in_nombre_guardado VARCHAR(200), in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Insertar_Cliente_Chat_Adjunto(in_id_cliente INTEGER, in_es_cliente BOOLEAN, in_nombre_original VARCHAR(200), in_nombre_guardado VARCHAR(200), in_id_usuario INTEGER)
RETURNS VOID AS $$
DECLARE
    aux_visto_operador BOOLEAN;
BEGIN	
	IF (in_es_cliente) THEN
		aux_visto_operador := false;
	ELSE
		aux_visto_operador := true;
	END IF;

	INSERT INTO cliente_chat_adjunto (id_cliente, nombre_original, nombre_guardado, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
	VALUES (in_id_cliente, in_nombre_original, in_nombre_guardado, NOW(), in_es_cliente, in_es_cliente, aux_visto_operador, in_id_usuario);
END;
$$ LANGUAGE plpgsql;
--select Insertar_Cliente_Chat_Adjunto(1, true, 'prueba adjunto 4 desde cliente', 'prueba adjunto 4 desde cliente guardado', 1);
--select Insertar_Cliente_Chat_Adjunto(1, false, 'prueba adjunto 4 desde operador', 'prueba adjunto 4 desde operador guardado', 3);

--DROP FUNCTION Insertar_Visita_Chat_Adjunto(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN, in_nombre_original VARCHAR(200), in_nombre_guardado VARCHAR(200), in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Insertar_Visita_Chat_Adjunto(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN, in_nombre_original VARCHAR(200), in_nombre_guardado VARCHAR(200), in_id_usuario INTEGER)
RETURNS VOID AS $$
DECLARE
    aux_visto_operador BOOLEAN;
BEGIN	
	IF (in_es_cliente) THEN
		aux_visto_operador := false;
	ELSE
		aux_visto_operador := true;
	END IF;

	INSERT INTO visita_sesion_chat_adjunto (id_visita_sesion, nombre_original, nombre_guardado, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
	VALUES (in_id_visita_sesion, in_nombre_original, in_nombre_guardado, NOW(), in_es_cliente, in_es_cliente, aux_visto_operador, in_id_usuario);
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_Visita_Chat(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN)
CREATE OR REPLACE FUNCTION Obtener_Visita_Chat(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN)
RETURNS TABLE (id_visita_sesion_chat INTEGER,
			   id_visita_sesion INTEGER,
			   mensaje VARCHAR(200), 
			   enlace VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100)) AS $$
BEGIN
	IF (in_es_cliente) THEN
		UPDATE visita_sesion_chat
		SET visto_cliente = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_cliente = false;
		
		UPDATE visita_sesion_chat_adjunto
		SET visto_cliente = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_cliente = false;
	ELSE
		UPDATE visita_sesion_chat
		SET visto_operador = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_operador = false;
		
		UPDATE visita_sesion_chat_adjunto
		SET visto_operador = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_operador = false;
	END IF;
	
	RETURN QUERY SELECT clc.id_visita_sesion_chat,
					clc.id_visita_sesion,
					clc.mensaje,
					COALESCE((SELECT UNNEST(regexp_match(clc.mensaje,'(https?://[^\s]+)'))),'')::VARCHAR(200) as enlace,
					'' as nombre_original,
					'' as nombre_guardado,
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario
	FROM visita_sesion_chat clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_visita_sesion = in_id_visita_sesion
		AND clc.marca_baja = false
	UNION
	SELECT clc.id_visita_sesion_chat_adjunto as id_visita_sesion_chat,
					clc.id_visita_sesion, 
					'' as mensaje,
					''::VARCHAR(200) as enlace,
					clc.nombre_original, 
					clc.nombre_guardado, 
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario
	FROM visita_sesion_chat_adjunto clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_visita_sesion = in_id_visita_sesion
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;

select * from visita_sesion order by 1 desc
select * from visita_sesion_chat order by 1 desc
select * from visita_sesion_chat_adjunto order by 1 desc

--DROP FUNCTION Insertar_Visita_Chat(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN, in_mensaje VARCHAR(200), in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Insertar_Visita_Chat(in_id_visita_sesion INTEGER, in_es_cliente BOOLEAN, in_mensaje VARCHAR(200), in_id_usuario INTEGER)
RETURNS TABLE (id_visita_sesion_chat INTEGER,
			   id_visita_sesion INTEGER,
			   mensaje VARCHAR(200), 
			   enlace VARCHAR(200), 
			   nombre_original VARCHAR(200), 
			   nombre_guardado VARCHAR(300), 
			   fecha_hora_creacion TIMESTAMP, 
			   enviado_cliente BOOLEAN, 
			   visto_cliente BOOLEAN, 
			   visto_operador BOOLEAN,
			   id_usuario INTEGER,
			   usuario VARCHAR(100), 
			   marca_baja BOOLEAN) AS $$
DECLARE
    aux_visto_operador BOOLEAN;
BEGIN
	IF (in_es_cliente) THEN
		UPDATE visita_sesion_chat
		SET visto_cliente = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_cliente = false;
				
		UPDATE visita_sesion_chat_adjunto
		SET visto_cliente = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_cliente = false;
		
		aux_visto_operador := false;
	ELSE
		UPDATE visita_sesion_chat
		SET visto_operador = true
		WHERE visita_sesion_chat.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat.visto_operador = false;
		
		UPDATE visita_sesion_chat_adjunto
		SET visto_operador = true
		WHERE visita_sesion_chat_adjunto.id_visita_sesion = in_id_visita_sesion
		AND visita_sesion_chat_adjunto.visto_operador = false;
		
		aux_visto_operador := true;
	END IF;
	
	INSERT INTO visita_sesion_chat (id_visita_sesion, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
	VALUES (in_id_visita_sesion, in_mensaje, NOW(), in_es_cliente, in_es_cliente, aux_visto_operador, in_id_usuario);
		
	RETURN QUERY SELECT clc.id_visita_sesion_chat,
					clc.id_visita_sesion, 
					clc.mensaje,
					COALESCE((SELECT UNNEST(regexp_match(clc.mensaje,'(https?://[^\s]+)'))),'')::VARCHAR(200) as enlace,
					'' as nombre_original,
					'' as nombre_guardado,
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					clc.marca_baja
	FROM visita_sesion_chat clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_visita_sesion = in_id_visita_sesion
		AND (clc.marca_baja = false OR in_es_cliente = false)
	UNION
	SELECT clc.id_visita_sesion_chat_adjunto as id_visita_sesion_chat,
					clc.id_visita_sesion, 
					'' as mensaje,
					''::VARCHAR(200) as enlace,
					clc.nombre_original, 
					clc.nombre_guardado, 
					clc.fecha_hora_creacion, 
					clc.enviado_cliente, 
					clc.visto_cliente, 
					clc.visto_operador,
					clc.id_usuario,
					u.usuario,
					false as marca_baja
	FROM visita_sesion_chat_adjunto clc JOIN usuario u
			ON (clc.id_usuario = u.id_usuario)
	WHERE clc.id_visita_sesion = in_id_visita_sesion
	ORDER BY fecha_hora_creacion;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Alerta_Cliente_Usuario(in_id_cliente INTEGER, in_id_usuario INTEGER)
CREATE OR REPLACE FUNCTION Alerta_Cliente_Usuario(in_id_cliente INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (id_cliente INTEGER) AS $$
DECLARE
    aux_id_cliente INTEGER;
BEGIN
	aux_id_cliente := 0;

	SELECT cl.id_cliente
	INTO aux_id_cliente		
	FROM cliente cl JOIN agente ag
			ON (cl.id_agente = ag.id_agente
				AND cl.id_cliente = in_id_cliente)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina)
		JOIN usuario u
			ON (u.id_usuario = in_id_usuario 
				AND (u.id_rol = 1 
					 OR
					 o.id_oficina = u.id_oficina))
	LIMIT 1;
	RETURN QUERY SELECT aux_id_cliente;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM Alerta_Cliente_Usuario(1,5)

--DROP FUNCTION Cargar_Bono_Referido(in_id_cliente INTEGER, in_id_usuario INTEGER, in_monto INTEGER, in_id_cuenta_bancaria INTEGER, in_id_operacion INTEGER)
CREATE OR REPLACE FUNCTION Cargar_Bono_Referido(in_id_cliente INTEGER, in_id_usuario INTEGER, in_monto INTEGER, in_id_cuenta_bancaria INTEGER, in_id_operacion INTEGER)
RETURNS TABLE (id_operacion INTEGER, codigo_operacion INTEGER) AS $$
DECLARE
    aux_id_operacion INTEGER;
    aux_codigo_operacion INTEGER;
    aux_codigo_operacion_referido INTEGER;
    aux_cliente_usuario_referido VARCHAR(100);
BEGIN
	SELECT COUNT(op.id_operacion) + 1
	INTO 	aux_codigo_operacion
	FROM 	operacion op
	WHERE 	op.id_cliente = in_id_cliente;
	
	SELECT	cl.cliente_usuario, op.codigo_operacion
	INTO aux_cliente_usuario_referido, aux_codigo_operacion_referido
	FROM operacion op JOIN cliente cl
		ON (op.id_cliente = cl.id_cliente
		   AND op.id_operacion = in_id_operacion);
		   
	INSERT INTO operacion (codigo_operacion, id_accion, id_cliente, id_estado, notificado, marca_baja, fecha_hora_creacion, fecha_hora_ultima_modificacion, id_usuario_ultima_modificacion)
	VALUES (aux_codigo_operacion, 9, in_id_cliente, 2, false, false, now(), now(), in_id_usuario)
	RETURNING operacion.id_operacion INTO aux_id_operacion;
	
	INSERT INTO operacion_carga (id_operacion, titular, importe, bono, id_cuenta_bancaria, sol_importe, sol_bono, sol_id_cuenta_bancaria, observaciones)
	VALUES (aux_id_operacion, '', 0, in_monto, in_id_cuenta_bancaria, 0, in_monto, in_id_cuenta_bancaria, CONCAT('Cliente : ', aux_cliente_usuario_referido, ' - Op : ', aux_codigo_operacion_referido));

	RETURN QUERY SELECT aux_id_operacion, aux_codigo_operacion;
END;
$$ LANGUAGE plpgsql;
--select * from Cargar_Bono_Referido(4, 1, 1000, 1 ,2);

DROP TABLE IF EXISTS notificacion;
CREATE TABLE IF NOT EXISTS notificacion
(
    id_notificacion serial NOT NULL,
    id_usuario integer NOT NULL,
    id_cuenta_bancaria integer NOT NULL,
    titulo varchar(100) NOT NULL,
    mensaje varchar(200) NOT NULL,
    fecha_hora timestamp NOT NULL,
	id_notificacion_origen varchar(200) NOT NULL DEFAULT '-',
	id_origen integer NOT NULL DEFAULT 1,
	anulada boolean NOT NULL DEFAULT false,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 1,
    fecha_hora_ultima_modificacion timestamp NOT NULL,
    PRIMARY KEY (id_notificacion)
);
CREATE INDEX notificacion_id_usuario ON notificacion (id_usuario);
CREATE INDEX notificacion_id_cuenta_bancaria ON notificacion (id_cuenta_bancaria);
CREATE INDEX notificacion_fecha_hora ON notificacion (fecha_hora DESC);
CREATE INDEX notificacion_fecha_hora_ultima_modificacion ON notificacion (fecha_hora_ultima_modificacion DESC);

DROP TABLE IF EXISTS notificacion_hist;
CREATE TABLE IF NOT EXISTS notificacion_hist
(
    id_notificacion integer NOT NULL,
    id_usuario integer NOT NULL,
    id_cuenta_bancaria integer NOT NULL,
    titulo varchar(100) NOT NULL,
    mensaje varchar(200) NOT NULL,
    fecha_hora timestamp NOT NULL,
	id_notificacion_origen varchar(200) NOT NULL DEFAULT '-',
	id_origen integer NOT NULL DEFAULT 1,
	anulada boolean NOT NULL DEFAULT false,
	id_usuario_ultima_modificacion integer NOT NULL DEFAULT 1,
    fecha_hora_ultima_modificacion timestamp NOT NULL
);
CREATE INDEX notificacion_hist_id_notificacion ON notificacion_hist (id_notificacion);
CREATE INDEX notificacion_hist_id_usuario ON notificacion_hist (id_usuario);
CREATE INDEX notificacion_hist_id_cuenta_bancaria ON notificacion_hist (id_cuenta_bancaria);
CREATE INDEX notificacion_hist_fecha_hora ON notificacion_hist (fecha_hora DESC);
CREATE INDEX notificacion_hist_fecha_hora_ultima_modificacion ON notificacion_hist (fecha_hora_ultima_modificacion DESC);

CREATE TABLE IF NOT EXISTS notificacion_transferencia
(
    id_notificacion integer NOT NULL,
    transferido boolean NOT NULL DEFAULT false,
    fecha_hora timestamp NOT NULL
);

DROP TABLE IF EXISTS notificacion_carga;
CREATE TABLE IF NOT EXISTS notificacion_carga
(
    id_notificacion_carga serial NOT NULL,
    id_notificacion integer NOT NULL,
    id_operacion_carga integer,
    carga_usuario varchar(100),
    carga_monto numeric,
    marca_procesado boolean NOT NULL DEFAULT false,
    fecha_hora_procesado timestamp NOT NULL,
	procesando boolean NOT NULL DEFAULT false
    PRIMARY KEY (id_notificacion_carga)
);
CREATE INDEX notificacion_carga_id_notificacion ON notificacion_carga (id_notificacion DESC);
CREATE INDEX notificacion_carga_id_operacion_carga ON notificacion_carga (id_operacion_carga DESC);
CREATE INDEX notificacion_carga_fecha_hora_procesado ON notificacion_carga (fecha_hora_procesado DESC);

DROP TABLE IF EXISTS notificacion_carga_hist;
CREATE TABLE IF NOT EXISTS notificacion_carga_hist
(
    id_notificacion_carga integer NOT NULL,
    id_notificacion integer NOT NULL,
    id_operacion_carga integer,
    carga_usuario varchar(100),
    carga_monto numeric,
    marca_procesado boolean NOT NULL DEFAULT false,
    fecha_hora_procesado timestamp NOT NULL
);
CREATE INDEX notificacion_carga_hist_id_notificacion ON notificacion_carga_hist (id_notificacion DESC);
CREATE INDEX notificacion_carga_hist_id_operacion_carga ON notificacion_carga_hist (id_operacion_carga DESC);
CREATE INDEX notificacion_carga_hist_fecha_hora_procesado ON notificacion_carga_hist (fecha_hora_procesado DESC);

CREATE TABLE IF NOT EXISTS notificacion_anular
(
    id_notificacion integer
);

/*DROP FUNCTION Registrar_Notificacion_PanelesNoti(	in_id_usuario INTEGER, 
															  in_aplicacion VARCHAR(100), 
															  in_titulo VARCHAR(100), 
															  in_mensaje VARCHAR(200), 
															  in_id_notificacion_origen VARCHAR(200), 
															  in_id_origen INTEGER);*/
CREATE OR REPLACE FUNCTION Registrar_Notificacion_PanelesNoti(in_id_usuario INTEGER, 
															  in_aplicacion VARCHAR(100), 
															  in_titulo VARCHAR(100), 
															  in_mensaje VARCHAR(200), 
															  in_id_notificacion_origen VARCHAR(200), 
															  in_id_origen INTEGER)
RETURNS TABLE (id_notificacion INTEGER, id_cuenta_bancaria INTEGER) AS $$
DECLARE aux_id_notificacion INTEGER;
DECLARE aux_id_cuenta_bancaria INTEGER;
BEGIN
	SELECT 	cb.id_cuenta_bancaria
	INTO aux_id_cuenta_bancaria
	FROM 	cuenta_bancaria cb JOIN billetera b
		ON (cb.id_billetera = b.id_billetera
		   	AND b.notificacion_descripcion = in_aplicacion
		   	AND cb.id_usuario_noti = in_id_usuario)
	ORDER BY cb.marca_baja, cb.fecha_hora_ultima_modificacion DESC
	LIMIT 1;
	
	IF (aux_id_cuenta_bancaria IS NULL) THEN
		aux_id_cuenta_bancaria := 0;
	END IF;
			
    -- Iniciar transacción explícita
    BEGIN
        -- Bloquear la tabla notificacion para evitar conflictos
        LOCK TABLE notificacion IN EXCLUSIVE MODE;
		
		IF EXISTS (	SELECT 1
					FROM notificacion
					WHERE id_notificacion_origen = in_id_notificacion_origen
						AND titulo = in_titulo
						AND mensaje = in_mensaje
						AND id_origen = in_id_origen) THEN
			-- Si ya existe, establecer aux_id_notificacion a -1
			aux_id_notificacion := -1;

		ELSE
			-- Si no existe, insertar nueva notificación y obtener el ID insertado
			INSERT INTO notificacion (id_usuario, id_cuenta_bancaria, titulo, mensaje, fecha_hora, id_notificacion_origen, id_origen, anulada, id_usuario_ultima_modificacion, fecha_hora_ultima_modificacion)
			VALUES (in_id_usuario, aux_id_cuenta_bancaria, in_titulo, in_mensaje, now(), in_id_notificacion_origen, in_id_origen, false, in_id_usuario, now())
			RETURNING notificacion.id_notificacion into aux_id_notificacion;

		END IF;
    END;
	
	RETURN QUERY SELECT aux_id_notificacion, aux_id_cuenta_bancaria;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM notificacion order by 1 desc;

--DROP FUNCTION Registrar_Notificacion(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_titulo VARCHAR(100), in_mensaje VARCHAR(200), in_id_notificacion_origen VARCHAR(200), in_id_origen INTEGER)
CREATE OR REPLACE FUNCTION Registrar_Notificacion(in_id_usuario INTEGER, in_id_cuenta_bancaria INTEGER, in_titulo VARCHAR(100), in_mensaje VARCHAR(200), in_id_notificacion_origen VARCHAR(200), in_id_origen INTEGER)
RETURNS TABLE (id_notificacion INTEGER) AS $$
DECLARE aux_id_notificacion INTEGER;
BEGIN
    -- Iniciar transacción explícita
    BEGIN
        -- Bloquear la tabla notificacion para evitar conflictos
        LOCK TABLE notificacion IN EXCLUSIVE MODE;
		
		IF EXISTS (	SELECT 1
					FROM notificacion
					WHERE id_notificacion_origen = in_id_notificacion_origen
				   		AND id_cuenta_bancaria = in_id_cuenta_bancaria
						AND titulo = in_titulo
						AND mensaje = in_mensaje
						AND id_origen = in_id_origen) THEN
			-- Si ya existe, establecer aux_id_notificacion a -1
			aux_id_notificacion := -1;

		ELSE
			-- Si no existe, insertar nueva notificación y obtener el ID insertado
			INSERT INTO notificacion (id_usuario, id_cuenta_bancaria, titulo, mensaje, fecha_hora, id_notificacion_origen, id_origen, anulada, id_usuario_ultima_modificacion, fecha_hora_ultima_modificacion)
			VALUES (in_id_usuario, in_id_cuenta_bancaria, in_titulo, in_mensaje, now(), in_id_notificacion_origen, in_id_origen, false, in_id_usuario, now())
			RETURNING notificacion.id_notificacion into aux_id_notificacion;

		END IF;
    END;
	
	RETURN QUERY SELECT aux_id_notificacion;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM notificacion order by 1 desc;

CREATE OR REPLACE FUNCTION Registrar_Notificacion_Carga(in_id_notificacion INTEGER, in_carga_usuario VARCHAR(100), in_carga_monto NUMERIC)
RETURNS TABLE (id_notificacion_carga INTEGER) AS $$
DECLARE aux_id_notificacion_carga INTEGER;
BEGIN
	insert into notificacion_carga (id_notificacion, carga_usuario, carga_monto, marca_procesado, fecha_hora_procesado)
	values (in_id_notificacion, in_carga_usuario, in_carga_monto, false, now())
	RETURNING notificacion_carga.id_notificacion_carga into aux_id_notificacion_carga;
	
	RETURN QUERY SELECT aux_id_notificacion_carga;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Registrar_Anulacion_Carga(in_id_notificacion INTEGER, in_id_usuario INTEGER)
RETURNS VOID AS $$
BEGIN
	UPDATE notificacion
	SET		anulada = true,
			id_usuario_ultima_modificacion = in_id_usuario,
			fecha_hora_ultima_modificacion = now()
	WHERE id_notificacion = in_id_notificacion;

	UPDATE notificacion_carga
	SET		marca_procesado = true,
			fecha_hora_procesado = now()
	WHERE id_notificacion = in_id_notificacion;
	
END;
$$ LANGUAGE plpgsql;
--select Registrar_Anulacion_Carga(1968, 1)
--select Registrar_Anulacion_Carga(1969, 1)
--select * from v_Notificaciones_Cargas where anulada = true order by 1 desc

CREATE OR REPLACE FUNCTION Registrar_Anulacion_Carga_Masiva()
RETURNS VOID AS $$
BEGIN
	TRUNCATE TABLE notificacion_anular;
	
	INSERT INTO notificacion_anular (id_notificacion)
	SELECT id_notificacion
	FROM v_Notificaciones_Cargas
	WHERE anulada = false 
	AND ((fecha_hora < NOW() - INTERVAL '30 minutes' AND id_origen = 1)
		OR (fecha_hora < NOW() - INTERVAL '30 minutes' AND id_origen = 2)
		OR (fecha_hora < NOW() - INTERVAL '30 minutes' AND id_origen = 3))
	AND id_cuenta_bancaria > 1 
	AND id_operacion_carga IS NULL 
	AND marca_procesado = false;
	--AND id_oficina = 15;

	UPDATE notificacion
	SET		anulada = true,
			id_usuario_ultima_modificacion = 1,
			fecha_hora_ultima_modificacion = now()
	WHERE id_notificacion IN (select id_notificacion from notificacion_anular);

	UPDATE notificacion_carga
	SET		marca_procesado = true,
			fecha_hora_procesado = now()
	WHERE id_notificacion IN (select id_notificacion from notificacion_anular);
END;
$$ LANGUAGE plpgsql;
--SELECT Registrar_Anulacion_Carga_Masiva();
--select count(*) from notificacion_anular

CREATE OR REPLACE FUNCTION CalcularPorcentajePalabrasEncontradas(A TEXT, B TEXT) RETURNS NUMERIC AS $$
DECLARE
    palabras_encontradas INT := 0;
    total_palabras_b INT := 0;
    palabra_b TEXT;
BEGIN
    -- Dividir la cadena B en palabras y contar el total de palabras
    -- total_palabras_b := array_length(string_to_array(B, ' '), 1);
	total_palabras_b := array_length(regexp_split_to_array(B, '\s+'), 1);

    -- Recorrer cada palabra de A y contar las palabras encontradas en B
    --FOREACH palabra_b IN ARRAY string_to_array(B, ' ') LOOP
	FOREACH palabra_b IN ARRAY regexp_split_to_array(B, '\s+') LOOP
        -- IF palabra_b = ANY(string_to_array(A, ' ')) THEN
		IF palabra_b <> '' AND palabra_b = ANY(regexp_split_to_array(A, '\s+')) THEN
            palabras_encontradas := palabras_encontradas + 1;
        END IF;
    END LOOP;

    -- Calcular el porcentaje de palabras encontradas
    IF total_palabras_b > 0 THEN
        RETURN palabras_encontradas::NUMERIC / total_palabras_b::NUMERIC;
    ELSE
        RETURN 0;
    END IF;
END;
$$ LANGUAGE plpgsql;
-- SELECT CalcularPorcentajePalabrasEncontradas('Guillermo Pinarello ola all', 'Guillermo Jorge Hugo Pinarello') AS porcentaje;
-- SELECT CalcularPorcentajePalabrasEncontradas('dario lopez', 'dario miguel lopez') AS porcentaje;

--DROP FUNCTION Verificar_Notificacion_Carga(in_id_notificacion_carga INTEGER);
CREATE OR REPLACE FUNCTION Verificar_Notificacion_Carga(in_id_notificacion_carga INTEGER)
RETURNS TABLE (	id_operacion_carga INTEGER,
			   	id_operacion INTEGER,
			   	cantidad_cargas INTEGER,
				monto numeric, 
				bono numeric, 
			   	id_cuenta_bancaria INTEGER,
				id_cliente INTEGER, 
				usuario VARCHAR(200), 
				id_cliente_ext BIGINT, 
				id_cliente_db INTEGER,
				agente_usuario VARCHAR(200),
				agente_password VARCHAR(200),
			  	id_plataforma INTEGER,
				referente_id_cliente INTEGER, 
				referente_usuario VARCHAR(200), 
				referente_id_cliente_ext BIGINT, 
				referente_id_cliente_db INTEGER) AS $$
DECLARE aux_id_operacion_carga INTEGER;
DECLARE aux_id_cliente INTEGER;
DECLARE aux_porcentaje_origen1 DECIMAL(10, 2);
DECLARE aux_porcentaje_origen2 DECIMAL(10, 2);
DECLARE aux_cantidad_cargas INTEGER;
DECLARE lock_key BIGINT := 1;  -- Una clave única para el bloqueo advisory
BEGIN
    -- Intentar adquirir el bloqueo. pg_advisory_lock es una llamada bloqueante.
    PERFORM pg_advisory_lock(lock_key);
	
	-- Aquí pones el código que deseas ejecutar de manera exclusiva
    BEGIN
		aux_porcentaje_origen1 := 0.3;
		aux_porcentaje_origen2 := 0.35;--tambien aplica a 3 (tucash)

		SELECT	opc.id_operacion_carga, op.id_cliente
		INTO 	aux_id_operacion_carga, aux_id_cliente
		FROM 	operacion_carga opc JOIN notificacion_carga nc
				ON (nc.marca_procesado = false
					AND nc.procesando = false
					-- AND CalcularPorcentajePalabrasEncontradas(lower(opc.titular), lower(nc.carga_usuario)) >= aux_porcentaje_origen1
					AND opc.procesando = false
					AND opc.importe = nc.carga_monto
					AND nc.id_operacion_carga IS NULL
					AND nc.id_notificacion_carga = in_id_notificacion_carga)
			JOIN notificacion n
				ON (n.id_notificacion = nc.id_notificacion
					AND (
						(CalcularPorcentajePalabrasEncontradas(lower(opc.titular), lower(nc.carga_usuario)) >= aux_porcentaje_origen1 AND n.id_origen = 1)
						OR
						(CalcularPorcentajePalabrasEncontradas(lower(opc.titular), lower(nc.carga_usuario)) >= aux_porcentaje_origen2 AND n.id_origen IN (2, 3))
						 ))
			JOIN operacion op
				ON (op.id_operacion = opc.id_operacion
				   AND op.id_estado = 1
				   AND op.marca_baja = false
				   AND op.id_accion = 1)
			JOIN cliente cl
				ON (op.id_cliente = cl.id_cliente)
			JOIN agente ag
				ON (cl.id_agente = ag.id_agente)
			JOIN usuario u
				ON (n.id_usuario = u.id_usuario
				   AND u.id_oficina = ag.id_oficina)
		ORDER BY 1 LIMIT 1;

		IF (aux_id_operacion_carga IS NULL) THEN
			aux_id_operacion_carga := -1;
			aux_id_cliente := -1;
		END IF;

		IF (aux_id_operacion_carga > 0) THEN
			UPDATE 	operacion_carga
			SET	procesando = true
			WHERE operacion_carga.id_operacion_carga = aux_id_operacion_carga;
			
			UPDATE 	notificacion_carga
			SET	procesando = true
			WHERE notificacion_carga.id_notificacion_carga = in_id_notificacion_carga;

			SELECT	COALESCE(COUNT(*), 0)
			INTO 	aux_cantidad_cargas
			FROM 	operacion_carga opc JOIN operacion op
					ON (op.id_estado = 2
						AND op.id_cliente = aux_id_cliente
						AND op.id_operacion = opc.id_operacion);

			RETURN QUERY SELECT	aux_id_operacion_carga,
					op.id_operacion,
					aux_cantidad_cargas,
					opc.importe + opc.bono,
					opc.bono,
					opc.id_cuenta_bancaria,
					cl.id_cliente,
					cl.cliente_usuario, 
					cl.id_cliente_ext, 
					cl.id_cliente_db,
					ag.agente_usuario,
					ag.agente_password,
					ag.id_plataforma,
					COALESCE(clt.id_cliente, 0),
					COALESCE(clt.cliente_usuario, ''), 
					COALESCE(clt.id_cliente_ext, 0)::BIGINT, 
					COALESCE(clt.id_cliente_db, 0)
			FROM 	operacion_carga opc JOIN operacion op
						ON (opc.id_operacion_carga = aux_id_operacion_carga
							AND op.id_operacion = opc.id_operacion)
					JOIN cliente cl
						ON (op.id_cliente = cl.id_cliente)
					JOIN agente ag
						ON (cl.id_agente = ag.id_agente)
					LEFT JOIN registro_token rt
						ON (rt.id_registro_token = cl.id_registro_token)
					LEFT JOIN cliente clt
						ON (rt.id_usuario = clt.id_cliente);
		ELSE 
			RETURN QUERY SELECT	aux_id_operacion_carga, 0, 0, 0::numeric, 0::numeric, 0, 0, ''::VARCHAR(200), 0::BIGINT, 0, ''::VARCHAR(200), ''::VARCHAR(200), 0, 0, ''::VARCHAR(200), 0::BIGINT, 0;
		END IF;
    EXCEPTION
        WHEN OTHERS THEN
            -- Si ocurre algún error, liberamos el bloqueo para evitar deadlocks
            PERFORM pg_advisory_unlock(lock_key);
            RAISE;
    END;
	
	-- Liberar el bloqueo advisory
    PERFORM pg_advisory_unlock(lock_key);
END;
$$ LANGUAGE plpgsql;
--select * from notificacion_carga where id_notificacion = 51759
-- select * from Verificar_Notificacion_Carga(51747);

-- DROP FUNCTION Verificar_Operacion_Carga(in_id_operacion INTEGER)
CREATE OR REPLACE FUNCTION Verificar_Operacion_Carga(in_id_operacion INTEGER)
RETURNS TABLE (	id_operacion_carga INTEGER,
			   	id_operacion INTEGER,
			   	id_notificacion_carga INTEGER,
			   	id_usuario INTEGER, 
			   	id_cuenta_bancaria INTEGER,
			   	cantidad_cargas INTEGER,
				monto numeric, 
				bono numeric, 
				id_cliente INTEGER, 
				usuario VARCHAR(200), 
				id_cliente_ext BIGINT, 
				id_cliente_db INTEGER,
				agente_usuario VARCHAR(200),
				agente_password VARCHAR(200),
			  	id_plataforma INTEGER,
				referente_id_cliente INTEGER, 
				referente_usuario VARCHAR(200), 
				referente_id_cliente_ext BIGINT, 
				referente_id_cliente_db INTEGER) AS $$
DECLARE aux_id_operacion_carga INTEGER;
DECLARE aux_id_cliente INTEGER;
DECLARE aux_porcentaje_origen1 DECIMAL(10, 2);
DECLARE aux_porcentaje_origen2 DECIMAL(10, 2);
DECLARE aux_cantidad_cargas INTEGER;
DECLARE aux_id_notificacion_carga INTEGER;
DECLARE aux_id_usuario INTEGER;
DECLARE aux_id_cuenta_bancaria INTEGER;
DECLARE lock_key BIGINT := 2;  -- Una clave única para el bloqueo advisory
BEGIN
    -- Intentar adquirir el bloqueo. pg_advisory_lock es una llamada bloqueante.
    PERFORM pg_advisory_lock(lock_key);
	
	-- Aquí pones el código que deseas ejecutar de manera exclusiva
    BEGIN
		aux_porcentaje_origen1 := 0.3;
		aux_porcentaje_origen2 := 0.35; --tambien aplica a 3 (tucash)

		SELECT	opc.id_operacion_carga, op.id_cliente, nc.id_notificacion_carga, n.id_usuario, n.id_cuenta_bancaria
		INTO 	aux_id_operacion_carga, aux_id_cliente, aux_id_notificacion_carga, aux_id_usuario, aux_id_cuenta_bancaria
		FROM 	operacion_carga opc JOIN notificacion_carga nc
				ON (nc.marca_procesado = false
					AND nc.procesando = false
					AND opc.procesando = false
					AND opc.importe = nc.carga_monto
					AND nc.id_operacion_carga IS NULL)
			JOIN notificacion n
				ON (n.id_notificacion = nc.id_notificacion
					AND (
						(CalcularPorcentajePalabrasEncontradas(lower(opc.titular), lower(nc.carga_usuario)) >= aux_porcentaje_origen1 AND n.id_origen = 1)
						OR
						(CalcularPorcentajePalabrasEncontradas(lower(opc.titular), lower(nc.carga_usuario)) >= aux_porcentaje_origen2 AND n.id_origen IN (2, 3))
						 ))
			JOIN operacion op
				ON (op.id_operacion = opc.id_operacion
					AND op.id_operacion = in_id_operacion
				   	AND op.id_cliente NOT IN (SELECT op2.id_cliente
											 	FROM operacion op2 JOIN operacion_carga opc2
											 		ON (op2.id_operacion = opc2.id_operacion
														AND op2.id_accion = 1
													   	AND op2.id_estado = 1
														AND opc2.procesando = true	
													   	AND op2.id_operacion < in_id_operacion)))
			JOIN cliente cl
				ON (op.id_cliente = cl.id_cliente)
			JOIN agente ag
				ON (cl.id_agente = ag.id_agente)
			JOIN usuario u
				ON (n.id_usuario = u.id_usuario
				   AND u.id_oficina = ag.id_oficina)
			ORDER BY 1 LIMIT 1;

		IF (aux_id_operacion_carga IS NULL) THEN
			aux_id_operacion_carga := -1;
			aux_id_cliente := -1;
		END IF;

		IF (aux_id_operacion_carga > 0) THEN
			UPDATE 	operacion_carga
			SET	procesando = true
			WHERE operacion_carga.id_operacion_carga = aux_id_operacion_carga;
			
			UPDATE notificacion_carga
			SET procesando = true
			WHERE notificacion_carga.id_notificacion_carga = aux_id_notificacion_carga;

			SELECT	COALESCE(COUNT(*), 0)
			INTO 	aux_cantidad_cargas
			FROM 	operacion_carga opc JOIN operacion op
					ON (op.id_estado = 2
						AND op.id_cliente = aux_id_cliente
						AND op.id_operacion = opc.id_operacion);

			RETURN QUERY SELECT	aux_id_operacion_carga,
					op.id_operacion,
					aux_id_notificacion_carga, 
					aux_id_usuario, 
					aux_id_cuenta_bancaria,
					aux_cantidad_cargas,
					opc.importe + opc.bono,
					opc.bono,
					cl.id_cliente,
					cl.cliente_usuario, 
					cl.id_cliente_ext, 
					cl.id_cliente_db,
					ag.agente_usuario,
					ag.agente_password,
					ag.id_plataforma,
					COALESCE(clt.id_cliente, 0),
					COALESCE(clt.cliente_usuario, ''), 
					COALESCE(clt.id_cliente_ext, 0)::BIGINT, 
					COALESCE(clt.id_cliente_db, 0)
			FROM 	operacion_carga opc JOIN operacion op
						ON (op.id_operacion = in_id_operacion
							AND op.id_operacion = opc.id_operacion)
					JOIN cliente cl
						ON (op.id_cliente = cl.id_cliente)
					JOIN agente ag
						ON (cl.id_agente = ag.id_agente)
					LEFT JOIN registro_token rt
						ON (rt.id_registro_token = cl.id_registro_token)
					LEFT JOIN cliente clt
						ON (rt.id_usuario = clt.id_cliente);
		ELSE 
			RETURN QUERY SELECT	aux_id_operacion_carga, 0, 0, 0, 0, 0, 0::numeric, 0::numeric, 0, ''::VARCHAR(200), 0::BIGINT, 0, ''::VARCHAR(200), ''::VARCHAR(200), 0, 0, ''::VARCHAR(200), 0::BIGINT, 0;
		END IF;
    EXCEPTION
        WHEN OTHERS THEN
            -- Si ocurre algún error, liberamos el bloqueo para evitar deadlocks
            PERFORM pg_advisory_unlock(lock_key);
            RAISE;
    END;
	
	-- Liberar el bloqueo advisory
    PERFORM pg_advisory_unlock(lock_key);
END;
$$ LANGUAGE plpgsql;
--select * from Verificar_Operacion_Carga(83925)
--select * from Verificar_Operacion_Carga(11749)
--select * from Verificar_Operacion_Carga(11802)

-- DROP FUNCTION Verificar_Operacion_Carga_Manual(in_id_operacion INTEGER)
CREATE OR REPLACE FUNCTION Verificar_Operacion_Carga_Manual(in_id_operacion INTEGER)
RETURNS TABLE (	id_operacion_carga INTEGER,
				procesando BOOLEAN,
				id_cliente INTEGER, 
				cliente_usuario VARCHAR(200), 
				id_cliente_ext BIGINT, 
				id_cliente_db INTEGER,
			   	id_agente INTEGER,
				agente_usuario VARCHAR(200),
				agente_password VARCHAR(200),
			  	id_plataforma INTEGER) AS $$
DECLARE aux_id_operacion_carga INTEGER;
DECLARE aux_id_estado INTEGER;
DECLARE aux_procesando BOOLEAN;
DECLARE lock_key BIGINT := 2;  -- Una clave única para el bloqueo advisory
BEGIN
    -- Intentar adquirir el bloqueo. pg_advisory_lock es una llamada bloqueante.
    PERFORM pg_advisory_lock(lock_key);
	
	-- Aquí pones el código que deseas ejecutar de manera exclusiva
    BEGIN
		
		SELECT	opc.id_operacion_carga, opc.procesando, op.id_estado
		INTO 	aux_id_operacion_carga, aux_procesando, aux_id_estado
		FROM 	operacion_carga opc JOIN operacion op
				ON (op.id_operacion = opc.id_operacion
					AND op.id_operacion = in_id_operacion);

		IF (aux_id_estado > 1) THEN
			aux_procesando := true;
		END IF;

		IF (NOT aux_procesando) THEN
			UPDATE 	operacion_carga
			SET	procesando = true
			WHERE operacion_carga.id_operacion_carga = aux_id_operacion_carga;
		END IF;

		RETURN QUERY SELECT	opc.id_operacion_carga,
				aux_procesando,
				cl.id_cliente,
				cl.cliente_usuario, 
				cl.id_cliente_ext, 
				cl.id_cliente_db,
				ag.id_agente,
				ag.agente_usuario,
				ag.agente_password,
				ag.id_plataforma
		FROM 	operacion_carga opc JOIN operacion op
					ON (op.id_operacion = in_id_operacion
						AND op.id_operacion = opc.id_operacion)
				JOIN cliente cl
					ON (op.id_cliente = cl.id_cliente)
				JOIN agente ag
					ON (cl.id_agente = ag.id_agente);
					
    EXCEPTION
        WHEN OTHERS THEN
            -- Si ocurre algún error, liberamos el bloqueo para evitar deadlocks
            PERFORM pg_advisory_unlock(lock_key);
            RAISE;
    END;
	
	-- Liberar el bloqueo advisory
    PERFORM pg_advisory_unlock(lock_key);
END;
$$ LANGUAGE plpgsql;

/* DROP FUNCTION Confirmar_Notificacion_Carga(in_id_notificacion_carga INTEGER, 
														in_id_operacion_carga INTEGER, 
														in_id_operacion INTEGER, 
														in_id_usuario INTEGER, 
														in_id_cuenta_bancaria INTEGER)*/
CREATE OR REPLACE FUNCTION Confirmar_Notificacion_Carga(in_id_notificacion_carga INTEGER, 
														in_id_operacion_carga INTEGER, 
														in_id_operacion INTEGER, 
														in_id_usuario INTEGER, 
														in_id_cuenta_bancaria INTEGER)
RETURNS VOID AS $$
BEGIN
	UPDATE 	notificacion_carga
	SET 	id_operacion_carga = in_id_operacion_carga,
			marca_procesado = true,
			procesando = false,
			fecha_hora_procesado = now()
	WHERE 	notificacion_carga.id_notificacion_carga = in_id_notificacion_carga;
	
	UPDATE 	operacion
	SET		id_estado = 2,
			notificado = false,
			fecha_hora_ultima_modificacion = now(),
			id_usuario_ultima_modificacion = in_id_usuario
	WHERE 	operacion.id_operacion = in_id_operacion;
	
	UPDATE 	operacion_carga
	SET		id_cuenta_bancaria = in_id_cuenta_bancaria,
			procesando = false
	WHERE 	operacion_carga.id_operacion_carga = in_id_operacion_carga;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Obtener_AppsCobro();
CREATE OR REPLACE FUNCTION Obtener_AppsCobro()
RETURNS TABLE (id_billetera INTEGER, billetera VARCHAR(100), notificacion_descripcion VARCHAR(100)) AS $$
begin
	RETURN QUERY 
	SELECT 	b.id_billetera, 
			b.billetera, 
			b.notificacion_descripcion
	FROM billetera b
	WHERE b.marca_baja = false
		AND b.notificacion_activa = true;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_AppsCobro();

--DROP FUNCTION Obtener_Sesion_Recupero_Whatsapp(in_id_rol INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Obtener_Sesion_Recupero_Whatsapp(in_id_rol INTEGER, in_id_oficina INTEGER)
RETURNS TABLE (	id_sesion_recupero_whatsapp integer,
				id_usuario integer,
				numero_telefono character varying(100),
				id_estado integer,
			   	fecha_hora_creacion timestamp,
			   	fecha_hora_cierre timestamp,
				id_oficina integer,
				oficina character varying(100),
				usuario character varying(50)) AS $$
BEGIN
	RETURN QUERY
	SELECT 	sr.id_sesion_recupero_whatsapp,
			sr.id_usuario,
			sr.numero_telefono,
			sr.id_estado,
			sr.fecha_hora_creacion,
			COALESCE(sr.fecha_hora_cierre, '2100-01-01') AS fecha_hora_cierre,
			o.id_oficina,
			o.oficina,
			u.usuario
	FROM sesion_recupero_whatsapp sr JOIN usuario u
				ON (sr.id_usuario = u.id_usuario)
			JOIN oficina o
				ON (u.id_oficina = o.id_oficina)
	WHERE CASE 
			WHEN in_id_rol = 1 THEN true
			WHEN o.id_oficina = in_id_oficina THEN true
			ELSE false END
	ORDER BY sr.id_sesion_recupero_whatsapp DESC;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Insertar_Sesion_Recupero_Whatsapp(in_id_usuario INTEGER, in_id_oficina INTEGER, in_numero_telefono VARCHAR(100))
RETURNS TABLE (id_sesion_recupero_whatsapp INTEGER) AS $$
DECLARE
    aux_id_sesion_recupero_whatsapp INTEGER;
BEGIN	   
	UPDATE sesion_recupero_whatsapp
	SET	id_estado = 2,
		fecha_hora_cierre = now()
	WHERE sesion_recupero_whatsapp.id_estado = 1
		AND sesion_recupero_whatsapp.fecha_hora_cierre IS NULL
		AND sesion_recupero_whatsapp.id_usuario IN (SELECT u.id_usuario FROM usuario u WHERE u.id_oficina = in_id_oficina);
			
	INSERT INTO sesion_recupero_whatsapp (id_usuario, numero_telefono, id_estado, fecha_hora_creacion)
	VALUES (in_id_usuario, in_numero_telefono, 1, now())
	RETURNING sesion_recupero_whatsapp.id_sesion_recupero_whatsapp INTO aux_id_sesion_recupero_whatsapp;		
	
	RETURN query SELECT aux_id_sesion_recupero_whatsapp;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Finalizar_Sesion_Recupero_Whatsapp(in_id_sesion_recupero_whatsapp INTEGER)
RETURNS VOID AS $$
BEGIN	   
	UPDATE sesion_recupero_whatsapp
	SET	id_estado = 2,
		fecha_hora_cierre = now()
	WHERE sesion_recupero_whatsapp.id_sesion_recupero_whatsapp = in_id_sesion_recupero_whatsapp;
END;
$$ LANGUAGE plpgsql;

--------------Vistas de Monitoreo y Gestión---------------------
--(excepto v_Cuenta_Bancaria_Activa)
--DROP VIEW v_Console_Logs;
CREATE OR REPLACE VIEW v_Console_Logs AS
SELECT 	cl.mensaje_log, 
		cl.fecha_hora, 
		cl.id 
FROM console_logs cl
ORDER BY cl.id DESC;
--select * from v_Console_Logs where mensaje_log like '%TuCash%';

SELECT date_trunc('minute', fecha_hora) AS fecha_hora, COUNT(*) AS errores
from v_Console_Logs 
where mensaje_log like 'Error en la Carga:%'
AND fecha_hora >= NOW() - INTERVAL '24 hours' 
GROUP BY date_trunc('minute', fecha_hora);

select * from v_Console_Logs where mensaje_log like 'Error en la Carga:%';

--DROP VIEW v_Oficinas;
CREATE OR REPLACE VIEW v_Oficinas AS
SELECT 	id_oficina,
		oficina,
		contacto_whatsapp,
		contacto_telegram,
		bono_carga_1,
		bono_carga_perpetua,
		minimo_carga,
		minimo_retiro,
		minimo_espera_retiro,
		fecha_hora_creacion,
		id_usuario_creacion,
		fecha_hora_ultima_modificacion,
		id_usuario_ultima_modificacion,
		marca_baja
FROM oficina;
--select * from v_Oficinas;

--DROP VIEW v_Plataformas;
CREATE OR REPLACE VIEW v_Plataformas AS
SELECT 	id_plataforma,
		plataforma,
		url_admin,
		url_juegos,
		imagen,
		marca_baja
FROM plataforma;
--select * from v_Plataformas;

--DROP FUNCTION Cuenta_Bancaria_Operador(in_id_rol INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Operador(in_id_rol INTEGER, in_id_oficina INTEGER)
RETURNS TABLE (	id_cuenta_bancaria integer,
			   	id_oficina integer,
			  	oficina character varying(100),
			  	id_billetera integer,
			  	billetera character varying(100),
			   	id_usuario_noti integer,
			  	usuario_noti character varying(100),
			   	id_agente integer,
			  	agente character varying(100),
			  	nombre character varying(200),
			  	alias character varying(200),
			  	cbu character varying(200),
			   	fecha_hora_creacion timestamp,
			  	marca_baja boolean) AS $$
BEGIN
	RETURN QUERY
	SELECT	cn.id_cuenta_bancaria,
			cn.id_oficina,
			o.oficina,
			cn.id_billetera,
			b.billetera,
			cn.id_usuario_noti,
			COALESCE(u.usuario, 'Sin Asignar') as usuario_noti,
			cn.id_agente,
			COALESCE(ag.agente_usuario, 'Sin Asignar') as agente,
			cn.nombre,
			cn.alias,
			cn.cbu,
			cn.fecha_hora_creacion,
			cn.marca_baja
	FROM cuenta_bancaria cn JOIN oficina o
				ON (cn.id_oficina = o.id_oficina
				   AND cn.marca_eliminada = false)
			JOIN billetera b
				ON (cn.id_billetera = b.id_billetera)
			LEFT JOIN usuario u
				ON (u.id_usuario = cn.id_usuario_noti)
			LEFT JOIN agente ag
				ON (cn.id_agente = ag.id_agente)
	WHERE CASE WHEN in_id_rol < 2 THEN true
			ELSE CASE WHEN in_id_oficina = cn.id_oficina THEN true ELSE false END 
			END
	ORDER BY marca_baja, nombre, alias, cbu;
END;
$$ LANGUAGE plpgsql;

--DROP FUNCTION Cuenta_Bancaria_Operador_2(id_cuenta_bancaria INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Operador_2(in_id_cuenta_bancaria INTEGER, in_id_oficina INTEGER)
RETURNS TABLE (	id_cuenta_bancaria integer,
			   	id_oficina integer,
			  	oficina character varying(100),
			  	id_billetera integer,
			  	billetera character varying(100),
			   	id_usuario_noti integer,
			  	usuario_noti character varying(100),
			   	id_agente integer,
			  	agente character varying(100),
			  	nombre character varying(200),
			  	alias character varying(200),
			  	cbu character varying(200),
			   	fecha_hora_creacion timestamp,
			  	marca_baja boolean,
			  	access_token character varying(200),
				public_key character varying(200),
				client_id character varying(200),
				client_secret character varying(200)) AS $$
BEGIN
	RETURN QUERY
	SELECT	cn.id_cuenta_bancaria,
			cn.id_oficina,
			o.oficina,
			cn.id_billetera,
			b.billetera,
			cn.id_usuario_noti,
			COALESCE(u.usuario, 'Sin Asignar') as usuario_noti,
			cn.id_agente,
			COALESCE(ag.agente_usuario, 'Sin Asignar') as agente,
			cn.nombre,
			cn.alias,
			cn.cbu,
			cn.fecha_hora_creacion,
			cn.marca_baja,
			COALESCE(cbmp.access_token, '') 	as access_token,
			COALESCE(cbmp.public_key, '') 		as public_key,
			COALESCE(cbmp.client_id, '') 		as client_id,
			COALESCE(cbmp.client_secret, '') 	as client_secret
	FROM cuenta_bancaria cn JOIN oficina o
				ON (cn.id_oficina = o.id_oficina
				   AND cn.marca_eliminada = false)
			JOIN billetera b
				ON (cn.id_billetera = b.id_billetera)
			LEFT JOIN usuario u
				ON (u.id_usuario = cn.id_usuario_noti)
			LEFT JOIN agente ag
				ON (cn.id_agente = ag.id_agente)
			LEFT JOIN cuenta_bancaria_mercado_pago cbmp
				ON (cn.id_cuenta_bancaria = cbmp.id_cuenta_bancaria)
	WHERE CASE WHEN in_id_cuenta_bancaria = 0 THEN true
			ELSE CASE WHEN in_id_cuenta_bancaria = cn.id_cuenta_bancaria THEN true ELSE false END 
			END
		AND CASE WHEN in_id_oficina = 0 THEN true
			ELSE CASE WHEN in_id_oficina = cn.id_oficina THEN true ELSE false END 
			END;
END;
$$ LANGUAGE plpgsql;

--DROP VIEW v_Cuentas_Bancarias;
CREATE OR REPLACE VIEW v_Cuentas_Bancarias AS
SELECT	cn.id_cuenta_bancaria,
		cn.id_oficina,
		o.oficina,
		cn.id_billetera,
		cn.id_usuario_noti,
		COALESCE(u.usuario, 'Sin Asignar') as usuario_noti,
		b.billetera,
		cn.nombre,
		cn.alias,
		cn.cbu,
		COALESCE(cbmp.access_token, '') 	as access_token,
		COALESCE(cbmp.public_key, '') 		as public_key,
		COALESCE(cbmp.client_id, '') 		as client_id,
		COALESCE(cbmp.client_secret, '') 	as client_secret,
		cn.fecha_hora_creacion,
		cn.marca_baja,
		cn.id_agente,
		COALESCE(ag.agente_usuario, 'Sin Asignar') as agente
FROM cuenta_bancaria cn JOIN oficina o
			ON (cn.id_oficina = o.id_oficina)
		JOIN billetera b
			ON (cn.id_billetera = b.id_billetera)
		LEFT JOIN cuenta_bancaria_mercado_pago cbmp
			ON (cn.id_cuenta_bancaria = cbmp.id_cuenta_bancaria)
		LEFT JOIN usuario u
			ON (u.id_usuario = cn.id_usuario_noti)
		LEFT JOIN agente ag
			ON (cn.id_agente = ag.id_agente);
--select * from v_Cuentas_Bancarias where id_cuenta_bancaria = 135 order by 1

-- DROP VIEW v_Cuentas_Bancarias_Descargas;
CREATE OR REPLACE VIEW v_Cuentas_Bancarias_Descargas AS
select 	cbd.id_cuenta_bancaria_descarga,
		cbd.id_cuenta_bancaria,
		cb.nombre,
		cb.alias,
		cb.cbu,
		cbd.id_usuario,
		u.usuario,
		cbd.fecha_hora_descarga,
		cbd.cargas_monto,
		cbd.cargas_cantidad,
		cbd.monto_restante
from cuenta_bancaria_descarga cbd join cuenta_bancaria cb
			on (cbd.id_cuenta_bancaria = cb.id_cuenta_bancaria)
		join usuario u
			on (u.id_usuario = cbd.id_usuario);
--select * from v_Cuentas_Bancarias_Descargas

-- DROP VIEW v_Agentes;
CREATE OR REPLACE VIEW v_Agentes AS
SELECT 	ag.id_agente,
		ag.agente_usuario,
		ag.agente_password,
		ag.id_plataforma,
		pl.plataforma,
		ag.id_oficina,
		ag.tokens_bono_carga_1,
		ag.tokens_bono_carga_1_porcentaje,
		ag.tokens_bono_creacion,
		o.oficina,
		ag.marca_baja,
		ag.fecha_hora_creacion,
		ag.id_usuario_creacion,
		ag.fecha_hora_ultima_modificacion,
		ag.id_usuario_ultima_modificacion		
FROM agente ag JOIN plataforma pl
		ON (ag.id_plataforma = pl.id_plataforma)
	JOIN oficina o
		ON (ag.id_oficina = o.id_oficina);
--SELECT * FROM v_Agentes

-- DROP VIEW v_Usuarios;
CREATE OR REPLACE VIEW v_Usuarios AS
SELECT 	u.id_usuario,
		u.usuario,
		u.fecha_hora_creacion,
		u.marca_baja,
		u.id_rol,
		r.nombre_rol,
		u.id_oficina,
		o.oficina,
		u.recibe_retiro
FROM usuario u JOIN rol r
		ON (u.id_rol = r.id_rol)
	JOIN oficina o
		ON (u.id_oficina = o.id_oficina);
--SELECT * FROM v_Usuarios

-- DROP VIEW v_Usuarios_Sesiones;
CREATE OR REPLACE VIEW v_Usuarios_Sesiones AS
SELECT 	us.id_usuario_sesion,
		us.id_usuario,
		us.ip,
		us.fecha_hora_creacion,
		CASE 
			WHEN us.cierre_abrupto = true THEN 'Cierre Abrupto'
			ELSE COALESCE(TO_CHAR(us.fecha_hora_cierre, 'YYYY/MM/DD HH24:MI:SS'), 'Abierta')
		END AS fecha_hora_cierre,
		u.usuario,
		r.nombre_rol,
		o.oficina
FROM	usuario_sesion us JOIN usuario u
			ON (us.id_usuario = u.id_usuario)
		JOIN rol r
			ON (u.id_rol = r.id_rol)
		JOIN oficina o
			ON (u.id_oficina = o.id_oficina);
--select * from v_Usuarios_Sesiones

-- DROP VIEW v_Clientes_Sesiones;
CREATE OR REPLACE VIEW v_Clientes_Sesiones AS
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cs.id_cliente_sesion,
		cs.ip,
		cs.moneda,
		cs.monto,
		cs.fecha_hora_creacion,
		CASE 
			WHEN cs.cierre_abrupto = true THEN 'Cierre Abrupto'
			ELSE COALESCE(TO_CHAR(cs.fecha_hora_cierre, 'YYYY/MM/DD HH24:MI:SS'), 'Abierta')
		END AS fecha_hora_cierre
FROM	cliente_sesion cs JOIN cliente cl
			ON (cs.id_cliente = cl.id_cliente);
--select * from v_Clientes_Sesiones where id_cliente = 4;

--DROP FUNCTION Clientes_Sesiones(in_id_cliente integer);
CREATE OR REPLACE FUNCTION Clientes_Sesiones(in_id_cliente integer)
RETURNS TABLE (	id_cliente integer,
			   	cliente_usuario character varying(200),
			   	id_cliente_sesion integer,
			   	ip character varying(100),
			   	moneda character varying(100),
			   	monto numeric,
			  	fecha_hora_creacion timestamp,
			  	fecha_hora_cierre text) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cs.id_cliente_sesion,
			cs.ip,
			cs.moneda,
			cs.monto,
			cs.fecha_hora_creacion,
			CASE 
				WHEN cs.cierre_abrupto = true THEN 'Cierre Abrupto'
				ELSE COALESCE(TO_CHAR(cs.fecha_hora_cierre, 'YYYY/MM/DD HH24:MI:SS'), 'Abierta')
			END AS fecha_hora_cierre
	FROM	cliente cl JOIN cliente_sesion cs
				ON (cl.id_cliente = in_id_cliente
					AND cs.id_cliente = cl.id_cliente)
	UNION
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cs.id_cliente_sesion,
			cs.ip,
			cs.moneda,
			cs.monto,
			cs.fecha_hora_creacion,
			CASE 
				WHEN cs.cierre_abrupto = true THEN 'Cierre Abrupto'
				ELSE COALESCE(TO_CHAR(cs.fecha_hora_cierre, 'YYYY/MM/DD HH24:MI:SS'), 'Abierta')
			END AS fecha_hora_cierre
	FROM	cliente cl JOIN cliente_sesion_hist cs
				ON (cl.id_cliente = in_id_cliente
					AND cs.id_cliente = cl.id_cliente)
	ORDER BY id_cliente_sesion DESC;
END;
$$ LANGUAGE plpgsql;
--select * from Clientes_Sesiones(4);

-- DROP VIEW v_Clientes_Cargas;
CREATE OR REPLACE VIEW v_Clientes_Cargas AS
SELECT 	c.id_cliente,
		c.cliente_usuario,
		c.id_agente,
		MAX(COALESCE(cr.cliente_usuario, ''))	AS cliente_usuario_referente,
		MAX(COALESCE(cr.id_cliente, 0))			AS id_cliente_usuario_referente,
		COALESCE(och.cantidad_cargas, 0) +
			COALESCE(oc.cantidad_cargas, 0) 	AS cantidad_cargas,
		COALESCE(och.importe, 0) +
			COALESCE(oc.importe, 0) 			AS total_importe_cargas,
		COALESCE(och.bono, 0) +
			COALESCE(oc.bono, 0) 				AS total_importe_bonos,
		CASE WHEN COALESCE(och.fecha_hora_creacion, '1900-01-01') > 
					COALESCE(oc.fecha_hora_creacion, '1900-01-01') 
				THEN COALESCE(och.fecha_hora_creacion, '1900-01-01')
				ELSE COALESCE(oc.fecha_hora_creacion, '1900-01-01')
		END AS ultima_carga
FROM cliente c LEFT JOIN (	SELECT	o.id_cliente,
						  	COUNT(o.id_operacion)		AS cantidad_cargas,
				   			SUM(oc.importe) 			AS importe,
				   			SUM(oc.bono) 				AS bono,
						  	MAX(o.fecha_hora_creacion)	AS fecha_hora_creacion
				  	FROM	operacion_hist o JOIN operacion_carga_hist oc
				  			ON (o.id_operacion = oc.id_operacion
							   AND o.marca_baja = false
							   AND o.id_accion IN (1, 5)
							   AND o.id_estado = 2)
				   	GROUP BY o.id_cliente) och
			ON (c.id_cliente = och.id_cliente)
		LEFT JOIN (	SELECT	o.id_cliente,
						  	COUNT(o.id_operacion)		AS cantidad_cargas,
				   			SUM(oc.importe) 			AS importe,
				   			SUM(oc.bono) 				AS bono,
						  	MAX(o.fecha_hora_creacion)	AS fecha_hora_creacion
				  	FROM	operacion o JOIN operacion_carga oc
				  			ON (o.id_operacion = oc.id_operacion
							   AND o.marca_baja = false
							   AND o.id_accion IN (1, 5)
							   AND o.id_estado = 2)
				   	GROUP BY o.id_cliente) oc
			ON (c.id_cliente = oc.id_cliente)
		LEFT JOIN registro_token rt
			ON (c.id_registro_token = rt.id_registro_token
			   AND rt.de_agente = false
			   AND rt.activo = true)
		LEFT JOIN cliente cr
			ON (rt.id_usuario = cr.id_cliente
			   AND cr.marca_baja = false
			   AND cr.bloqueado = false)
WHERE c.marca_baja = false
GROUP BY c.id_cliente, 
		c.cliente_usuario, 
		c.id_agente, 
		och.cantidad_cargas, 
		och.importe, 
		och.bono, 
		och.fecha_hora_creacion, 
		oc.cantidad_cargas, 
		oc.importe, 
		oc.bono, 
		oc.fecha_hora_creacion;
--SELECT * FROM v_Clientes_Cargas WHERE id_cliente = 54;

--DROP FUNCTION Clientes_Cargas_Cliente(in_id_cliente integer);
CREATE OR REPLACE FUNCTION Clientes_Cargas_Cliente(in_id_cliente integer)
RETURNS TABLE (	id_cliente integer,
			   	cantidad_cargas bigint) AS $$
BEGIN
	RETURN QUERY
	SELECT 	c.id_cliente,
			COALESCE(och.cantidad_cargas, 0) +
				COALESCE(oc.cantidad_cargas, 0) 	AS cantidad_cargas
	FROM cliente c LEFT JOIN (	SELECT	o.id_cliente,
								COUNT(o.id_operacion)		AS cantidad_cargas
						FROM	operacion_hist o JOIN operacion_carga_hist oc
								ON (o.id_operacion = oc.id_operacion
									AND o.id_cliente = in_id_cliente
								   	AND o.marca_baja = false
								   	AND o.id_accion IN (1, 5)
								   	AND o.id_estado = 2)
						GROUP BY o.id_cliente) och
				ON (c.id_cliente = och.id_cliente)
			LEFT JOIN (	SELECT	o.id_cliente,
								COUNT(o.id_operacion)		AS cantidad_cargas
						FROM	operacion o JOIN operacion_carga oc
								ON (o.id_operacion = oc.id_operacion
									AND o.id_cliente = in_id_cliente
								   	AND o.marca_baja = false
								   	AND o.id_accion IN (1, 5)
								   	AND o.id_estado = 2)
						GROUP BY o.id_cliente) oc
				ON (c.id_cliente = oc.id_cliente)
	WHERE c.marca_baja = false
		AND c.id_cliente = in_id_cliente
	GROUP BY c.id_cliente, 
			och.cantidad_cargas,
			oc.cantidad_cargas;
END;
$$ LANGUAGE plpgsql;
--select * from Clientes_Cargas_Cliente(54);

--DROP FUNCTION Clientes_Cargas_Operador(in_id_cliente integer);
CREATE OR REPLACE FUNCTION Clientes_Cargas_Operador(in_id_cliente integer)
RETURNS TABLE (	id_cliente integer,
			   	id_cliente_usuario_referente integer,
			   	cliente_usuario_referente text,
			   	cantidad_cargas bigint) AS $$
BEGIN
	RETURN QUERY
	SELECT 	c.id_cliente,
			MAX(COALESCE(cr.id_cliente, 0))			AS id_cliente_usuario_referente,
			MAX(COALESCE(cr.cliente_usuario, ''))	AS cliente_usuario_referente,
			COALESCE(och.cantidad_cargas, 0) +
				COALESCE(oc.cantidad_cargas, 0) 	AS cantidad_cargas
	FROM cliente c LEFT JOIN (	SELECT	o.id_cliente,
								COUNT(o.id_operacion)		AS cantidad_cargas
						FROM	operacion_hist o JOIN operacion_carga_hist oc
								ON (o.id_operacion = oc.id_operacion
									AND o.id_cliente = in_id_cliente
								   	AND o.marca_baja = false
								   	AND o.id_accion IN (1, 5)
								   	AND o.id_estado = 2)
						GROUP BY o.id_cliente) och
				ON (c.id_cliente = och.id_cliente)
			LEFT JOIN (	SELECT	o.id_cliente,
								COUNT(o.id_operacion)		AS cantidad_cargas
						FROM	operacion o JOIN operacion_carga oc
								ON (o.id_operacion = oc.id_operacion
									AND o.id_cliente = in_id_cliente
								   	AND o.marca_baja = false
								   	AND o.id_accion IN (1, 5)
								   	AND o.id_estado = 2)
						GROUP BY o.id_cliente) oc
				ON (c.id_cliente = oc.id_cliente)
			LEFT JOIN registro_token rt
				ON (c.id_registro_token = rt.id_registro_token
				   AND rt.de_agente = false
				   AND rt.activo = true)
			LEFT JOIN cliente cr
				ON (rt.id_usuario = cr.id_cliente
				   AND cr.marca_baja = false
				   AND cr.bloqueado = false)
	WHERE c.marca_baja = false
		AND c.id_cliente = in_id_cliente
	GROUP BY c.id_cliente, 
			och.cantidad_cargas, 
			oc.cantidad_cargas;
END;
$$ LANGUAGE plpgsql;
--select * from Clientes_Cargas_Operador(54);

-- DROP VIEW v_Clientes_Retiros;
CREATE OR REPLACE VIEW v_Clientes_Retiros AS
SELECT 	c.id_cliente,
		c.cliente_usuario,
		c.cliente_nombre,
		c.cliente_apellido,
		c.correo_electronico,
		c.telefono,
		c.id_agente,
		ofi.minimo_espera_retiro,
		COALESCE(o.cantidad_cargas, 0) + COALESCE(o_hist.cantidad_cargas, 0) 
			AS cantidad_cargas,
		COALESCE(o.total_importe_retiros, 0) + COALESCE(o_hist.total_importe_retiros, 0)
			AS total_importe_retiros,
		CASE WHEN COALESCE(o.ultimo_retiro, '1900-01-01') > COALESCE(o_hist.ultimo_retiro, '1900-01-01')
			THEN COALESCE(o.ultimo_retiro, '1900-01-01')
			ELSE COALESCE(o_hist.ultimo_retiro, '1900-01-01')
		END AS ultimo_retiro,
		CASE WHEN COALESCE(o.ultimo_retiro, '1900-01-01') > COALESCE(o_hist.ultimo_retiro, '1900-01-01')
			THEN ROUND(EXTRACT(EPOCH FROM (NOW() - COALESCE(o.ultimo_retiro, '2024-04-01 00:00:00'))) / 3600, 0)
			ELSE ROUND(EXTRACT(EPOCH FROM (NOW() - COALESCE(o_hist.ultimo_retiro, '2024-04-01 00:00:00'))) / 3600, 0)
		END AS horas_ultimo_retiro
FROM cliente c JOIN agente ag
			ON (c.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		LEFT JOIN (SELECT	o.id_cliente,
				   			COUNT(o.id_operacion) 		AS cantidad_cargas,
				   			SUM(oc.importe)				AS total_importe_retiros,
				   			MAX(o.fecha_hora_creacion)	AS ultimo_retiro
					FROM operacion_hist o JOIN operacion_retiro oc
						ON (o.id_operacion = oc.id_operacion
						   AND o.marca_baja = false
						   AND o.id_accion IN (2, 6)
						   AND o.id_estado IN (1, 2))
				  	GROUP BY o.id_cliente) o_hist
			ON (c.id_cliente = o_hist.id_cliente)
		LEFT JOIN (SELECT	o.id_cliente,
				   			COUNT(o.id_operacion) 		AS cantidad_cargas,
				   			SUM(oc.importe)				AS total_importe_retiros,
				   			MAX(o.fecha_hora_creacion)	AS ultimo_retiro
					FROM operacion o JOIN operacion_retiro oc
						ON (o.id_operacion = oc.id_operacion
						   AND o.marca_baja = false
						   AND o.id_accion IN (2, 6)
						   AND o.id_estado IN (1, 2))
				  	GROUP BY o.id_cliente) o
			ON (c.id_cliente = o.id_cliente)
WHERE c.marca_baja = false;
--GROUP BY c.id_cliente, c.cliente_usuario, c.id_agente, ofi.minimo_espera_retiro;
--SELECT * FROM v_Clientes_Retiros where id_cliente = 54

DROP VIEW v_Clientes_Operaciones;
CREATE OR REPLACE VIEW v_Clientes_Operaciones_v1 AS
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cl.id_cliente_ext,
		cl.id_cliente_db,
		cl.id_agente,
		ag.agente_usuario,
		ag.agente_password,
		ag.id_plataforma,
		pla.plataforma,
		ag.id_oficina,
		o.marca_baja,
		ofi.oficina,
		o.id_operacion,
		o.codigo_operacion,
		e.id_estado,
		e.estado,
		ac.id_accion,
		CASE WHEN ac.id_accion IN (1, 5, 9) THEN 1 ELSE 0 END AS es_carga,
		CASE WHEN ac.id_accion IN (2, 6) THEN 1 ELSE 0 END AS es_retiro,
		ac.accion,
		u.usuario,
		ROUND(COALESCE(clconf.Aceptadas::numeric, 0) / COALESCE(clconf.Totales::numeric, 1) * 100) AS cliente_confianza,
		COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
		COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
		COALESCE(opr.importe, 0)							AS retiro_importe,
		COALESCE(opr.cbu, '')								AS retiro_cbu,
		COALESCE(opr.titular, '')							AS retiro_titular,
		COALESCE(opr.observaciones, '')						AS retiro_observaciones,
		COALESCE(opc.importe, 0)							AS carga_importe,
		COALESCE(opc.titular, '')							AS carga_titular,
		COALESCE(opc.id_cuenta_bancaria, 0)					AS carga_id_cuenta_bancaria,
		COALESCE(opc.bono, 0)								AS carga_bono,
		COALESCE(opc.observaciones, '')						AS carga_observaciones,
		COALESCE(opcb.nombre || '-' || opcb.alias || '-' || opcb.cbu, '')	AS carga_cuenta_bancaria,
		COALESCE(opcb.nombre, '')	AS carga_cuenta_bancaria_nombre,
		COALESCE(opcb.alias, '')	AS carga_cuenta_bancaria_alias,
		COALESCE(opcb.cbu, '')	AS carga_cuenta_bancaria_cbu,
		COALESCE(opcb.id_billetera, 0)	AS id_billetera,
		CASE 
			WHEN ac.id_accion IN (1, 5, 9) 	THEN COALESCE(opcb.id_usuario_noti, 0) 
			WHEN ac.id_accion IN (2, 6) 	THEN COALESCE(opr.id_usuario, 0) 
			ELSE 0
		END AS id_usuario_noti,
		COALESCE(nc.id_notificacion, 0)	AS id_notificacion,
		COALESCE(n.id_origen, 0)		AS id_origen_notificacion,
		CASE WHEN COALESCE(n.id_notificacion, 0) = 0 THEN 0 ELSE 1 END AS carga_automatica
FROM cliente cl JOIN operacion o
			ON (cl.id_cliente = o.id_cliente
			   	AND o.marca_baja = false)
		JOIN usuario u
			ON (o.id_usuario_ultima_modificacion = u.id_usuario)
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		LEFT JOIN (SELECT id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion o
				WHERE id_accion = 1
					AND id_estado in (2,3)
					AND marca_baja = false
				GROUP BY id_cliente) clconf
			ON (cl.id_cliente = clconf.id_cliente)
		LEFT JOIN operacion_retiro opr
			ON (o.id_operacion = opr.id_operacion)
		LEFT JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion)
		LEFT JOIN cuenta_bancaria opcb
			ON (opc.id_cuenta_bancaria = opcb.id_cuenta_bancaria)
		LEFT JOIN notificacion_carga nc
			ON (nc.id_operacion_carga = opc.id_operacion_carga)
		LEFT JOIN notificacion n
			ON (n.id_notificacion = nc.id_notificacion)
WHERE cl.marca_baja = false;

DROP VIEW v_Clientes_Operaciones;
--CREATE OR REPLACE VIEW v_Clientes_Operaciones_v2 AS
CREATE OR REPLACE VIEW v_Clientes_Operaciones AS
SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			1 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			ROUND((COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0)) / 
				  (COALESCE(clconf_hist.Totales::numeric, 1) + COALESCE(clconf.Totales::numeric, 1))
				  * 100) AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01') AS fecha_hora_proceso,
			0	AS retiro_importe,
			''::character varying AS retiro_cbu,
			''::character varying AS retiro_titular,
			''::character varying AS retiro_observaciones,
			COALESCE(opc.importe, 0)							AS carga_importe,
			COALESCE(opc.titular, '')::character varying		AS carga_titular,
			COALESCE(opc.id_cuenta_bancaria, 0)					AS carga_id_cuenta_bancaria,
			COALESCE(opc.bono, 0)								AS carga_bono,
			COALESCE(opc.observaciones, '')::character varying	AS carga_observaciones,
			COALESCE(opcb.nombre || '-' || opcb.alias || '-' || opcb.cbu, '') AS carga_cuenta_bancaria,
			COALESCE(opcb.nombre, '')::character varying AS carga_cuenta_bancaria_nombre,
			COALESCE(opcb.alias, '')::character varying	AS carga_cuenta_bancaria_alias,
			COALESCE(opcb.cbu, '')::character varying AS carga_cuenta_bancaria_cbu,
			COALESCE(opcb.id_billetera, 0)	AS id_billetera,
			COALESCE(opcb.id_usuario_noti, 0) AS id_usuario_noti,
			COALESCE(nc.id_notificacion, 0)	AS id_notificacion,
			COALESCE(n.id_origen, 0)		AS id_origen_notificacion,
			CASE WHEN COALESCE(n.id_notificacion, 0) = 0 THEN 0 ELSE 1 END AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion IN (1, 5, 9)
			AND o.marca_baja = false)
	JOIN estado e
		ON (o.id_estado = e.id_estado)
	JOIN accion ac
		ON (o.id_accion = ac.id_accion)
	JOIN cliente cl
		ON (cl.id_cliente = o.id_cliente
			AND cl.marca_baja = false)		
	JOIN agente ag
		ON (cl.id_agente = ag.id_agente)
	JOIN oficina ofi
		ON (ag.id_oficina = ofi.id_oficina)
	JOIN plataforma pla
		ON (ag.id_plataforma = pla.id_plataforma)
	JOIN operacion_carga opc
		ON (o.id_operacion = opc.id_operacion)
	LEFT JOIN (SELECT id_cliente,
				COUNT(*) AS Totales,
				SUM(CASE WHEN id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
			FROM operacion o
			WHERE id_accion = 1
				AND id_estado in (2,3)
				AND marca_baja = false
			GROUP BY id_cliente) clconf
		ON (cl.id_cliente = clconf.id_cliente)
	LEFT JOIN (SELECT id_cliente,
				COUNT(*) AS Totales,
				SUM(CASE WHEN id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
			FROM operacion_hist o
			WHERE id_accion = 1
				AND id_estado in (2,3)
				AND marca_baja = false
			GROUP BY id_cliente) clconf_hist
		ON (cl.id_cliente = clconf_hist.id_cliente)
	LEFT JOIN cuenta_bancaria opcb
		ON (opc.id_cuenta_bancaria = opcb.id_cuenta_bancaria)
	LEFT JOIN notificacion_carga nc
		ON (nc.id_operacion_carga = opc.id_operacion_carga)
	LEFT JOIN notificacion n
		ON (n.id_notificacion = nc.id_notificacion)
UNION
SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			1 AS es_retiro,
			ac.accion,
			u.usuario,
			ROUND((COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0))/ 
				  (COALESCE(clconf_hist.Totales::numeric, 1) + COALESCE(clconf.Totales::numeric, 1))
				  * 100) AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_proceso,
			--COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			COALESCE(opr.importe, 0)							AS retiro_importe,
			COALESCE(opr.cbu, '')::character varying	 		AS retiro_cbu,
			COALESCE(opr.titular, '')::character varying		AS retiro_titular,
			COALESCE(opr.observaciones, '')::character varying	AS retiro_observaciones,
			0	AS carga_importe,
			''::character varying AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''::character varying	AS carga_observaciones,
			''::character varying	AS carga_cuenta_bancaria,
			''::character varying	AS carga_cuenta_bancaria_nombre,
			''::character varying	AS carga_cuenta_bancaria_alias,
			''::character varying	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			COALESCE(opr.id_usuario, 0) AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion IN (2, 6)
			AND o.marca_baja = false)
	JOIN estado e
		ON (o.id_estado = e.id_estado)
	JOIN accion ac
		ON (o.id_accion = ac.id_accion)
	JOIN cliente cl
		ON (cl.id_cliente = o.id_cliente
			AND cl.marca_baja = false)		
	JOIN agente ag
		ON (cl.id_agente = ag.id_agente)
	JOIN oficina ofi
		ON (ag.id_oficina = ofi.id_oficina)
	JOIN plataforma pla
		ON (ag.id_plataforma = pla.id_plataforma)
JOIN operacion_retiro opr
	ON (o.id_operacion = opr.id_operacion)
LEFT JOIN (SELECT id_cliente,
			COUNT(*) AS Totales,
			SUM(CASE WHEN id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
		FROM operacion o
		WHERE id_accion = 2
			AND id_estado in (2,3)
			AND marca_baja = false
		GROUP BY id_cliente) clconf
	ON (cl.id_cliente = clconf.id_cliente)
LEFT JOIN (SELECT id_cliente,
			COUNT(*) AS Totales,
			SUM(CASE WHEN id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
		FROM operacion_hist o
		WHERE id_accion = 2
			AND id_estado in (2,3)
			AND marca_baja = false
		GROUP BY id_cliente) clconf_hist
	ON (cl.id_cliente = clconf_hist.id_cliente)
UNION
SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			0 AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			0	AS retiro_importe,
			''::character varying AS retiro_cbu,
			''::character varying	AS retiro_titular,
			''::character varying	AS retiro_observaciones,
			0	AS carga_importe,
			''	AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''::character varying	AS carga_observaciones,
			''::character varying	AS carga_cuenta_bancaria,
			''::character varying	AS carga_cuenta_bancaria_nombre,
			''::character varying	AS carga_cuenta_bancaria_alias,
			''::character varying	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			0 	AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion NOT IN (1, 2, 5, 6, 9)
			AND o.marca_baja = false)
	JOIN estado e
		ON (o.id_estado = e.id_estado)
	JOIN accion ac
		ON (o.id_accion = ac.id_accion)
	JOIN cliente cl
		ON (cl.id_cliente = o.id_cliente
			AND cl.marca_baja = false)		
	JOIN agente ag
		ON (cl.id_agente = ag.id_agente)
	JOIN oficina ofi
		ON (ag.id_oficina = ofi.id_oficina)
	JOIN plataforma pla
		ON (ag.id_plataforma = pla.id_plataforma);

DROP FUNCTION Clientes_Operaciones_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER);
CREATE OR REPLACE FUNCTION Clientes_Operaciones_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				id_cliente_ext bigint,
				id_cliente_db integer,
				id_agente integer,
				agente_usuario character varying(200),
				agente_password character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				marca_baja boolean,
				oficina character varying(100),
				id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				id_accion integer,
				es_carga integer,
				es_retiro integer,
				accion character varying(200),
				usuario character varying(50),
				aceptadas numeric,
				totales numeric,
				cliente_confianza numeric,
				fecha_hora_operacion timestamp,
				fecha_hora_proceso timestamp,
				retiro_importe numeric,
				retiro_cbu character varying(200),
				retiro_titular character varying(200),
				retiro_observaciones character varying(200),
				carga_importe numeric,
				carga_titular character varying(200),
				carga_id_cuenta_bancaria integer,
				carga_bono numeric,
				carga_observaciones character varying(200),
				carga_cuenta_bancaria text,
				carga_cuenta_bancaria_nombre character varying(200),
				carga_cuenta_bancaria_alias character varying(200),
				carga_cuenta_bancaria_cbu character varying(200),
				id_billetera integer,
				id_usuario_noti integer,
				id_notificacion integer,
				id_origen_notificacion integer,
				carga_automatica integer) AS $$
DECLARE aux_periodo INTERVAL;
BEGIN
	IF (in_periodo = 1) THEN
		aux_periodo := '15 minutes'::INTERVAL;--60 minutes
	ELSE
		aux_periodo := '3 hours'::INTERVAL;--24 hours
	END IF;

	IF (in_id_usuario IN (1,4,310,654,699,829,846,847) AND in_periodo = 2) THEN
		aux_periodo := '24 hours'::INTERVAL;--24 hours
		/*"admin"
		"guille"
		"bitpina2"
		"bitbenja"
		"bitandy"
		"bitmati"
		"pinachat"
		"pinachat2"
		*/
	END IF;

	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			1 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0) AS aceptadas,
			COALESCE(clconf_hist.Totales::numeric, 0) + COALESCE(clconf.Totales::numeric, 0) AS totales,
			CASE WHEN (COALESCE(clconf_hist.Totales::numeric, 0) + COALESCE(clconf.Totales::numeric, 0) > 0) THEN
				ROUND((COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0)) / 
					  (COALESCE(clconf_hist.Totales::numeric, 0) + COALESCE(clconf.Totales::numeric, 0)) * 100) 
			ELSE 0 END AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01') AS fecha_hora_proceso,
			0	AS retiro_importe,
			''::character varying AS retiro_cbu,
			''::character varying AS retiro_titular,
			''::character varying AS retiro_observaciones,
			COALESCE(opc.importe, 0)							AS carga_importe,
			COALESCE(opc.titular, '')::character varying		AS carga_titular,
			COALESCE(opc.id_cuenta_bancaria, 0)					AS carga_id_cuenta_bancaria,
			COALESCE(opc.bono, 0)								AS carga_bono,
			COALESCE(opc.observaciones, '')::character varying	AS carga_observaciones,
			COALESCE(opcb.nombre || '-' || opcb.alias || '-' || opcb.cbu, '') AS carga_cuenta_bancaria,
			COALESCE(opcb.nombre, '')::character varying AS carga_cuenta_bancaria_nombre,
			COALESCE(opcb.alias, '')::character varying	AS carga_cuenta_bancaria_alias,
			COALESCE(opcb.cbu, '')::character varying AS carga_cuenta_bancaria_cbu,
			COALESCE(opcb.id_billetera, 0)	AS id_billetera,
			COALESCE(opcb.id_usuario_noti, 0) AS id_usuario_noti,
			COALESCE(nc.id_notificacion, 0)	AS id_notificacion,
			COALESCE(n.id_origen, 0)		AS id_origen_notificacion,
			CASE WHEN COALESCE(n.id_notificacion, 0) = 0 THEN 0 ELSE 1 END AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion IN (1, 5, 9)
			AND o.marca_baja = false
			--AND o.id_operacion NOT IN (SELECT cod.id_operacion FROM clientes_operaciones_diario cod WHERE cod.id_accion IN (1, 5, 9))
		   	AND (o.fecha_hora_creacion > NOW() - aux_periodo OR o.id_estado = 1))
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente
			   AND ag.marca_baja = false
			   AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina
			   AND ofi.marca_baja = false)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion)
		LEFT JOIN cuenta_bancaria opcb
			ON (opc.id_cuenta_bancaria = opcb.id_cuenta_bancaria)
		LEFT JOIN notificacion_carga nc
			ON (nc.id_operacion_carga = opc.id_operacion_carga)
		LEFT JOIN notificacion n
			ON (n.id_notificacion = nc.id_notificacion)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion o
				WHERE o.id_accion = 1
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf
			ON (cl.id_cliente = clconf.id_cliente)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion_hist o
				WHERE o.id_accion = 1
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf_hist
			ON (cl.id_cliente = clconf_hist.id_cliente)
	WHERE CASE 
			WHEN in_id_rol < 3 THEN true
			WHEN in_id_usuario = 0 THEN true
			WHEN COALESCE(opcb.id_usuario_noti, 0) = 0 OR COALESCE(opcb.id_usuario_noti, 0) = in_id_usuario THEN true
			ELSE false END
	UNION
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			1 AS es_retiro,
			ac.accion,
			u.usuario,
			COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0) AS aceptadas,
			COALESCE(clconf_hist.Totales::numeric, 0) + COALESCE(clconf.Totales::numeric, 0) AS totales,
			CASE WHEN COALESCE(clconf_hist.Totales::numeric, 0) + COALESCE(clconf.Totales::numeric, 0) > 0 THEN
				ROUND((COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0))/ 
					  (COALESCE(clconf_hist.Totales::numeric, 0) + COALESCE(clconf.Totales::numeric, 0))
					  * 100)
			ELSE 0 END AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			COALESCE(opr.importe, 0)							AS retiro_importe,
			COALESCE(opr.cbu, '')::character varying	 		AS retiro_cbu,
			COALESCE(opr.titular, '')::character varying		AS retiro_titular,
			COALESCE(opr.observaciones, '')::character varying	AS retiro_observaciones,
			0	AS carga_importe,
			''::character varying AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''::character varying	AS carga_observaciones,
			''::text	AS carga_cuenta_bancaria,
			''::character varying	AS carga_cuenta_bancaria_nombre,
			''::character varying	AS carga_cuenta_bancaria_alias,
			''::character varying	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			COALESCE(opr.id_usuario, 0) AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion IN (2, 6)
			AND o.marca_baja = false
			--AND o.id_operacion NOT IN (SELECT cod.id_operacion FROM clientes_operaciones_diario cod WHERE cod.id_accion IN (2,6))
		   	AND (o.fecha_hora_creacion > NOW() - aux_periodo OR o.id_estado = 1))
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente
			   AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina
			   AND ofi.marca_baja = false)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		JOIN operacion_retiro opr
			ON (o.id_operacion = opr.id_operacion)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion o
				WHERE o.id_accion = 2
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf
			ON (cl.id_cliente = clconf.id_cliente)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion_hist o
				WHERE o.id_accion = 2
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf_hist
			ON (cl.id_cliente = clconf_hist.id_cliente)
	WHERE CASE 
			WHEN in_id_rol < 3 THEN true
			WHEN in_id_usuario = 0 THEN true
			WHEN COALESCE(opr.id_usuario, 0) = 0 OR COALESCE(opr.id_usuario, 0) = in_id_usuario THEN true
			ELSE false END
	UNION
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			0 AS aceptadas,
			0 AS totales,
			0 AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			0	AS retiro_importe,
			''::character varying AS retiro_cbu,
			''::character varying	AS retiro_titular,
			''::character varying	AS retiro_observaciones,
			0	AS carga_importe,
			''	AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''::character varying	AS carga_observaciones,
			''::text	AS carga_cuenta_bancaria,
			''::character varying	AS carga_cuenta_bancaria_nombre,
			''::character varying	AS carga_cuenta_bancaria_alias,
			''::character varying	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			0 	AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion NOT IN (1, 2, 5, 6, 9)
			AND o.marca_baja = false
			--AND o.id_operacion NOT IN (SELECT cod.id_operacion FROM clientes_operaciones_diario cod WHERE cod.id_accion NOT IN (1, 2, 5, 6, 9))
		   	AND (o.fecha_hora_creacion > NOW() - aux_periodo OR o.id_estado = 1))
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente
			   AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina
			   AND ofi.marca_baja = false)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
	ORDER BY id_operacion DESC;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Clientes_Operaciones_Operador_Operacion_Carga(in_id_operacion INTEGER)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				id_agente integer,
				agente_usuario character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				oficina character varying(100),
				id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				fecha_hora_operacion timestamp,
				fecha_hora_proceso timestamp,
				carga_importe numeric,
				carga_titular character varying(200),
				carga_id_cuenta_bancaria integer,
				carga_bono numeric,
				carga_observaciones character varying(200)) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_agente,
			ag.agente_usuario,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01') AS fecha_hora_proceso,
			COALESCE(opc.importe, 0)							AS carga_importe,
			COALESCE(opc.titular, '')::character varying		AS carga_titular,
			COALESCE(opc.id_cuenta_bancaria, 0)					AS carga_id_cuenta_bancaria,
			COALESCE(opc.bono, 0)								AS carga_bono,
			COALESCE(opc.observaciones, '')::character varying	AS carga_observaciones
	FROM operacion o JOIN estado e
			ON (o.id_operacion = in_id_operacion
				AND o.id_estado = 1
				AND o.id_estado = e.id_estado)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Clientes_Operaciones_Operador_Ultima_Carga_TitularMonto(in_id_oficina INTEGER, in_importe NUMERIC, in_titular VARCHAR(200))
RETURNS TABLE (	fecha_hora_operacion timestamp) AS $$
BEGIN
	RETURN QUERY
	SELECT 	COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion
	FROM operacion o JOIN estado e
			ON (o.id_accion in (1,5)
				AND o.id_estado = 2)				
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente
			   AND ag.id_oficina = in_id_oficina)
		JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion
--			   	AND LOWER(opc.titular) = LOWER(in_titular)
				AND CalcularPorcentajePalabrasEncontradas(in_titular, opc.titular) = 1
				AND opc.importe = in_importe)
	UNION
	SELECT 	COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion
	FROM operacion_hist o JOIN estado e
			ON (o.id_accion in (1,5)
				AND o.id_estado = 2)				
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente
			   AND ag.id_oficina = in_id_oficina)
		JOIN operacion_carga_hist opc
			ON (o.id_operacion = opc.id_operacion
			   	--AND LOWER(opc.titular) = LOWER(in_titular)
				AND CalcularPorcentajePalabrasEncontradas(in_titular, opc.titular) = 1
				AND opc.importe = in_importe)
	ORDER BY 1 DESC LIMIT 1;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM Clientes_Operaciones_Operador_Ultima_Carga_TitularMonto(1, 'dario', 5000)

CREATE OR REPLACE FUNCTION Clientes_Operaciones_Operador_Operacion_Retiro(in_id_operacion INTEGER)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				id_agente integer,
				agente_usuario character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				oficina character varying(100),
				id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				fecha_hora_operacion timestamp,
				fecha_hora_proceso timestamp,
				retiro_importe numeric,
				retiro_cbu character varying(200),
				retiro_titular character varying(200),
				retiro_observaciones character varying(200)) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_agente,
			ag.agente_usuario,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			COALESCE(opr.importe, 0)							AS retiro_importe,
			COALESCE(opr.cbu, '')::character varying	 		AS retiro_cbu,
			COALESCE(opr.titular, '')::character varying		AS retiro_titular,
			COALESCE(opr.observaciones, '')::character varying	AS retiro_observaciones
	FROM operacion o JOIN estado e
			ON (o.id_operacion = in_id_operacion
				AND o.id_estado = 1
				AND o.id_estado = e.id_estado)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)	
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		JOIN operacion_retiro opr
			ON (o.id_operacion = opr.id_operacion);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Clientes_Operaciones_Operador_Ultima_Carga_Usuario(in_id_cliente INTEGER)
RETURNS TABLE (	fecha_hora_operacion timestamp,
			  	importe	numeric) AS $$
BEGIN
	RETURN QUERY
	SELECT 	COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(opc.importe, 0)							AS importe
	FROM operacion o JOIN estado e
			ON (o.id_accion in (1,5)
				AND o.id_cliente = in_id_cliente
				AND o.id_estado = 2)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion)
	UNION
	SELECT 	COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(opc.importe, 0)							AS importe
	FROM operacion_hist o JOIN estado e
			ON (o.id_accion in (1,5)
				AND o.id_cliente = in_id_cliente
				AND o.id_estado = 2)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN operacion_carga_hist opc
			ON (o.id_operacion = opc.id_operacion)
	ORDER BY 1 DESC LIMIT 1;
END;
$$ LANGUAGE plpgsql;
--select * from Clientes_Operaciones_Operador_Ultima_Carga_Usuario(54);

--DROP FUNCTION Clientes_Operaciones_Hist_Cliente(in_id_cliente INTEGER);
CREATE OR REPLACE FUNCTION Clientes_Operaciones_Hist_Cliente(in_id_cliente INTEGER)
RETURNS TABLE (	id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				id_accion integer,
				accion character varying(200),
				fecha_hora_operacion timestamp,
				retiro_importe numeric,
			  	carga_importe numeric,
			  	carga_bono numeric) AS $$
BEGIN
	RETURN QUERY
	SELECT 	o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			ac.accion,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			0	AS retiro_importe,
			COALESCE(opc.importe, 0)							AS carga_importe,
			COALESCE(opc.bono, 0)								AS carga_bono
	FROM operacion o JOIN estado e
			ON (o.id_estado = e.id_estado
				AND o.id_accion IN (1, 5, 9)
				AND o.marca_baja = false
			   	AND o.id_cliente = in_id_cliente)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion)
	UNION
	SELECT 	o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			ac.accion,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(opr.importe, 0)							AS retiro_importe,
			0	AS carga_importe,
			0	AS carga_bono
	FROM operacion o JOIN estado e
			ON (o.id_estado = e.id_estado
				AND o.id_accion IN (2, 6)
				AND o.marca_baja = false
			   	AND o.id_cliente = in_id_cliente)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN operacion_retiro opr
			ON (o.id_operacion = opr.id_operacion)
	UNION
	SELECT	oh.id_operacion,
			oh.codigo_operacion,
			oh.id_estado,
			oh.estado,
			oh.id_accion,
			oh.accion,
			oh.fecha_hora_operacion,
			oh.retiro_importe,
			oh.carga_importe,
			oh.carga_bono
	FROM	clientes_operaciones_hist oh
		WHERE	oh.id_accion IN (1, 2, 5, 6, 9)
		AND oh.id_cliente = in_id_cliente
	ORDER BY id_operacion DESC;
END;
$$ LANGUAGE plpgsql;

DROP VIEW v_Clientes_Operaciones_Hist;
CREATE OR REPLACE VIEW v_Clientes_Operaciones_Hist AS
SELECT	id_cliente,
		cliente_usuario,
		id_cliente_ext,
		id_cliente_db,
		id_agente,
		agente_usuario,
		agente_password,
		id_plataforma,
		plataforma,
		id_oficina,
		marca_baja,
		oficina,
		id_operacion,
		codigo_operacion,
		id_estado,
		estado,
		id_accion,
		es_carga,
		es_retiro,
		accion,
		usuario,
		cliente_confianza,
		fecha_hora_operacion,
		fecha_hora_proceso,
		retiro_importe,
		retiro_cbu,
		retiro_titular,
		retiro_observaciones,
		carga_importe,
		carga_titular,
		carga_id_cuenta_bancaria,
		carga_bono,
		carga_observaciones,
		carga_cuenta_bancaria,
		carga_cuenta_bancaria_nombre,
		carga_cuenta_bancaria_alias,
		carga_cuenta_bancaria_cbu,
		id_billetera,
		id_usuario_noti,
		id_notificacion,
		CASE WHEN id_origen_notificacion = 0 AND id_estado = 2 AND es_carga = 1 AND usuario = 'admin' THEN -1
			ELSE id_origen_notificacion 
		END AS id_origen_notificacion,
		CASE WHEN id_origen_notificacion = 0 AND id_estado = 2 AND es_carga = 1 AND usuario = 'admin' THEN 1
			ELSE carga_automatica 
		END AS carga_automatica
FROM	clientes_operaciones_hist
UNION
SELECT	id_cliente,
		cliente_usuario,
		id_cliente_ext,
		id_cliente_db,
		id_agente,
		agente_usuario,
		agente_password,
		id_plataforma,
		plataforma,
		id_oficina,
		marca_baja,
		oficina,
		id_operacion,
		codigo_operacion,
		id_estado,
		estado,
		id_accion,
		es_carga,
		es_retiro,
		accion,
		usuario,
		cliente_confianza,
		fecha_hora_operacion,
		fecha_hora_proceso,
		retiro_importe,
		retiro_cbu,
		retiro_titular,
		retiro_observaciones,
		carga_importe,
		carga_titular,
		carga_id_cuenta_bancaria,
		carga_bono,
		carga_observaciones,
		carga_cuenta_bancaria,
		carga_cuenta_bancaria_nombre,
		carga_cuenta_bancaria_alias,
		carga_cuenta_bancaria_cbu,
		id_billetera,
		id_usuario_noti,
		id_notificacion,
		CASE WHEN id_origen_notificacion = 0 AND id_estado = 2 AND es_carga = 1 AND usuario = 'admin' THEN -1
			ELSE id_origen_notificacion 
		END AS id_origen_notificacion,
		CASE WHEN id_origen_notificacion = 0 AND id_estado = 2 AND es_carga = 1 AND usuario = 'admin' THEN 1
			ELSE carga_automatica 
		END AS carga_automatica
FROM	v_Clientes_Operaciones;

--select * from v_Clientes_Operaciones_v1 where id_cliente = 4 order by id_operacion desc
--600msec

--select * from v_Clientes_Operaciones_v2 where id_cliente = 4 order by id_operacion desc
--600msec

--select * from v_Clientes_Operaciones where id_cliente = 4 order by id_operacion desc
--600msec

--select * from v_Clientes_Operaciones_Hist where id_cliente = 4 order by id_operacion desc
--600msec

--DROP VIEW v_operacion_completa
CREATE OR REPLACE VIEW v_operacion_completa AS
SELECT 	id_operacion,
		codigo_operacion,
		id_accion,
		id_cliente,
		id_estado,
		notificado,
		marca_baja,
		fecha_hora_creacion,
		fecha_hora_ultima_modificacion,
		id_usuario_ultima_modificacion
FROM 	operacion
UNION
SELECT 	id_operacion,
		codigo_operacion,
		id_accion,
		id_cliente,
		id_estado,
		notificado,
		marca_baja,
		fecha_hora_creacion,
		fecha_hora_ultima_modificacion,
		id_usuario_ultima_modificacion
FROM 	operacion_hist;

--DROP VIEW v_operacion_carga_completa
CREATE OR REPLACE VIEW v_operacion_carga_completa AS
SELECT 	id_operacion_carga,
		id_operacion,
		titular,
		importe,
		bono,
		id_cuenta_bancaria,
		sol_importe,
		sol_bono,
		sol_id_cuenta_bancaria,
		observaciones,
		procesando
FROM 	operacion_carga
UNION
SELECT 	id_operacion_carga,
		id_operacion,
		titular,
		importe,
		bono,
		id_cuenta_bancaria,
		sol_importe,
		sol_bono,
		sol_id_cuenta_bancaria,
		observaciones,
		procesando
FROM 	operacion_carga_hist;

--DROP VIEW v_notificacion_completa
CREATE OR REPLACE VIEW v_notificacion_completa AS
SELECT 	id_notificacion,
		id_usuario,
		id_cuenta_bancaria,
		titulo,
		mensaje,
		fecha_hora,
		id_notificacion_origen,
		id_origen,
		anulada,
		id_usuario_ultima_modificacion,
		fecha_hora_ultima_modificacion
FROM	notificacion
UNION
SELECT 	id_notificacion,
		id_usuario,
		id_cuenta_bancaria,
		titulo,
		mensaje,
		fecha_hora,
		id_notificacion_origen,
		id_origen,
		anulada,
		id_usuario_ultima_modificacion,
		fecha_hora_ultima_modificacion
FROM	notificacion_hist;

--DROP VIEW v_notificacion_carga_completa
CREATE OR REPLACE VIEW v_notificacion_carga_completa AS
SELECT 	id_notificacion_carga,
		id_notificacion,
		id_operacion_carga,
		carga_usuario,
		carga_monto,
		marca_procesado,
		fecha_hora_procesado
FROM	notificacion_carga
UNION
SELECT 	id_notificacion_carga,
		id_notificacion,
		id_operacion_carga,
		carga_usuario,
		carga_monto,
		marca_procesado,
		fecha_hora_procesado
FROM	notificacion_carga_hist;

--DROP VIEW v_Clientes
CREATE OR REPLACE VIEW v_Clientes AS
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cl.cliente_password,
		cl.bloqueado,
		cl.id_agente,
		cl.id_cliente_ext,
		cl.id_cliente_db,
		ag.agente_usuario,
		ag.agente_password,
		ag.id_plataforma,
		pla.plataforma,
		ag.id_oficina,
		ofi.oficina,
		COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
		COALESCE(ope.visto_cliente, 1)				as visto_cliente,
		COALESCE(ope.visto_operador, 1)			as visto_operador,
		COALESCE(rt.bono_creacion, 0)			as bono_creacion
FROM cliente cl JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		LEFT JOIN registro_token rt
			ON (rt.de_agente = false
			   AND rt.id_usuario = cl.id_cliente)
		LEFT JOIN (SELECT	id_cliente,
							MIN(visto_cliente::int) AS visto_cliente,
							MIN(visto_operador::int) AS visto_operador,
							MAX(ult_operacion) AS ult_operacion
					FROM
						(SELECT 	id_cliente,
								MIN(visto_cliente::int) AS visto_cliente,
								MIN(visto_operador::int) AS visto_operador,
								MAX(fecha_hora_creacion) AS ult_operacion
						FROM cliente_chat 
						GROUP BY id_cliente
						UNION
						SELECT 	id_cliente,
								MIN(visto_cliente::int) AS visto_cliente,
								MIN(visto_operador::int) AS visto_operador,
								MAX(fecha_hora_creacion) AS ult_operacion
						FROM cliente_chat_adjunto 
						GROUP BY id_cliente						 
						/*UNION
						SELECT 	id_cliente,
								1 AS visto_cliente,
								1 AS visto_operador,
								MAX(fecha_hora_ultima_modificacion) AS ult_operacion
						FROM operacion
						GROUP BY id_cliente*/) operacion
					GROUP BY id_cliente) ope
			ON (cl.id_cliente = ope.id_cliente)
WHERE cl.marca_baja = false
GROUP BY cl.id_cliente,
		cl.cliente_usuario,
		cl.cliente_password,
		cl.bloqueado,
		cl.id_agente,
		ag.agente_usuario,
		ag.agente_password,
		ag.id_plataforma,
		pla.plataforma,
		ag.id_oficina,
		ofi.oficina,
		ope.ult_operacion,
		ope.visto_cliente,
		ope.visto_operador,
		rt.bono_creacion;
--select * from v_Clientes where id_oficina = 2 order by ult_operacion desc

--DROP FUNCTION Clientes_Usuarios_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER);
--DROP FUNCTION Clientes_Usuarios_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER);
CREATE OR REPLACE FUNCTION Clientes_Usuarios_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				cliente_password character varying(200),
				bloqueado boolean,
				id_agente integer,
				id_cliente_ext bigint,
				id_cliente_db integer,
				agente_usuario character varying(200),
				agente_password character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				oficina character varying(100),
				ult_operacion timestamp,
				visto_cliente integer,
				visto_operador integer) AS $$
DECLARE aux_periodo INTERVAL;
BEGIN
	IF (in_periodo = 1) THEN
		aux_periodo := '10 minutes'::INTERVAL; --60 minutes
	ELSE
		aux_periodo := '3 hours'::INTERVAL; --24 hours
	END IF;

	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.cliente_password,
			cl.bloqueado,
			cl.id_agente,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			ofi.oficina,
			COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
			COALESCE(ope.visto_cliente, 1)				as visto_cliente,
			COALESCE(ope.visto_operador, 1)			as visto_operador
	FROM cliente cl JOIN agente ag
				ON (cl.id_agente = ag.id_agente
			   		AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
			LEFT JOIN (SELECT	operacion.id_cliente,
								MIN(operacion.visto_cliente::int) AS visto_cliente,
								MIN(operacion.visto_operador::int) AS visto_operador,
								MAX(operacion.ult_operacion) AS ult_operacion,
					   			MAX(operacion.id_usuario_noti) AS id_usuario_noti,
					   			MIN(operacion.estado_ope) AS estado_ope
						FROM
							(SELECT cc.id_cliente,
									MIN(cc.visto_cliente::int) AS visto_cliente,
									MIN(cc.visto_operador::int) AS visto_operador,
									MAX(cc.fecha_hora_creacion) AS ult_operacion,
							 		0 AS id_usuario_noti,
							 		2 AS estado_ope,
							 		1 AS num_fila
							FROM cliente_chat cc JOIN cliente cl 
									ON (cl.id_cliente = cc.id_cliente)
									   -- AND cc.fecha_hora_creacion > NOW() - aux_periodo)
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							GROUP BY cc.id_cliente
							UNION
							SELECT 	cca.id_cliente,
									MIN(cca.visto_cliente::int) AS visto_cliente,
									MIN(cca.visto_operador::int) AS visto_operador,
									MAX(cca.fecha_hora_creacion) AS ult_operacion,
							 		0 AS id_usuario_noti,
							 		2 AS estado_ope,
							 		1 AS num_fila
							FROM cliente_chat_adjunto cca JOIN cliente cl 
									ON (cl.id_cliente = cca.id_cliente)
									   -- AND cca.fecha_hora_creacion > NOW() - aux_periodo)
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							GROUP BY cca.id_cliente
							UNION							 
							SELECT 	o.id_cliente,
									1 AS visto_cliente,
									1 AS visto_operador,
									o.fecha_hora_ultima_modificacion AS ult_operacion,
									cb.id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_ultima_modificacion DESC) AS num_fila
							FROM operacion o JOIN operacion_carga oc
									ON (o.id_operacion = oc.id_operacion)
									   -- AND o.fecha_hora_ultima_modificacion > NOW() - aux_periodo)
								JOIN cuenta_bancaria cb
									ON (oc.id_cuenta_bancaria = cb.id_cuenta_bancaria)
										--AND (cb.id_usuario_noti = in_id_usuario OR in_id_rol < 3 OR cb.id_usuario_noti = 0))
							 	JOIN cliente cl 
									ON (cl.id_cliente = o.id_cliente)
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							UNION
							SELECT 	o.id_cliente,
									1 AS visto_cliente,
									1 AS visto_operador,
									o.fecha_hora_ultima_modificacion AS ult_operacion,
									0 AS id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_ultima_modificacion DESC) AS num_fila
							FROM operacion o JOIN operacion_retiro oret
									ON (o.id_operacion = oret.id_operacion)
									   	--AND o.fecha_hora_ultima_modificacion > NOW() - aux_periodo)
							 	JOIN cliente cl 
									ON (cl.id_cliente = o.id_cliente)
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							 ) operacion
					   		WHERE num_fila = 1
						GROUP BY operacion.id_cliente) ope
				ON (cl.id_cliente = ope.id_cliente)
	WHERE cl.marca_baja = false	
		AND (ope.ult_operacion > NOW() - aux_periodo or ope.estado_ope = 1)
		AND (ope.id_usuario_noti = in_id_usuario OR in_id_rol < 3 OR ope.id_usuario_noti = 0)
	GROUP BY cl.id_cliente,
			cl.cliente_usuario,
			cl.cliente_password,
			cl.bloqueado,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			ofi.oficina,
			ope.ult_operacion,
			ope.visto_cliente,
			ope.visto_operador
	ORDER BY visto_operador, ult_operacion DESC;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION Clientes_Usuarios_Operador_Chat(in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER);
CREATE OR REPLACE FUNCTION Clientes_Usuarios_Operador_Chat(in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				agente_usuario character varying(200),
				plataforma character varying(200),
				oficina character varying(100),
				ult_operacion timestamp,
				visto_cliente integer,
				visto_operador integer,
				no_leidos integer,
				id_usuario integer,
				usuario character varying(100),
				visita character varying(100)) AS $$
DECLARE aux_periodo INTERVAL;
DECLARE aux_id_oficina_visita INTEGER;
BEGIN
	aux_periodo := '3 hours'::INTERVAL;--24
	aux_id_oficina_visita := 1;

	RETURN QUERY
	--NO LEIDOS:
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina,
			COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
			ope.visto_cliente,
			ope.visto_operador,
			COALESCE(ope.no_leidos, 0)::integer			as no_leidos,
			COALESCE(u.id_usuario, 0)::integer			as id_usuario,
			COALESCE(u.usuario, '')						as usuario,
			''::character varying(100)					as visita
	FROM cliente cl JOIN agente ag
				ON (cl.id_agente = ag.id_agente
			   		AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina
				   AND ofi.marca_baja = false)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
			JOIN ( SELECT 	cc.id_cliente,
							0 AS visto_cliente,
							0 AS visto_operador,
							MIN(cc.fecha_hora_creacion) AS ult_operacion,
							0 AS id_usuario_noti,
							--CASE WHEN MIN(cc.visto_operador::int) = 0 THEN 1 ELSE 2 END AS estado_ope,
				  			2 AS estado_ope,
							--SUM(CASE WHEN cc.visto_operador = false THEN 1 ELSE 0 END) AS no_leidos
				  			COUNT(cc.id_cliente_chat) AS no_leidos
					FROM cliente_chat cc JOIN cliente cl 
							ON (cl.id_cliente = cc.id_cliente
								AND cc.visto_operador = false
								AND cl.marca_baja = false
								AND cl.bloqueado = false)							 
						JOIN agente ag
							ON (cl.id_agente = ag.id_agente
								AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
					GROUP BY cc.id_cliente) ope
				ON (cl.id_cliente = ope.id_cliente)
			LEFT JOIN ( SELECT 	cc.id_cliente,
				  			cc.id_usuario,
				  			ROW_NUMBER() OVER (PARTITION BY cc.id_cliente ORDER BY cc.fecha_hora_creacion DESC) AS num_fila
					FROM cliente_chat cc JOIN cliente cl 
							ON (cl.id_cliente = cc.id_cliente
								AND cc.fecha_hora_creacion > NOW() - aux_periodo
								AND cc.enviado_cliente = false
								AND cl.marca_baja = false
								AND cl.bloqueado = false)							 
						JOIN agente ag
							ON (cl.id_agente = ag.id_agente
								AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))) ope2
				ON (cl.id_cliente = ope2.id_cliente
				   	AND ope2.num_fila = 1)
			LEFT JOIN usuario u
				ON (ope2.id_usuario = u.id_usuario)
	WHERE cl.marca_baja = false
		--AND ope.estado_ope = 1
		--AND (ope.ult_operacion > NOW() - aux_periodo OR ope.estado_ope = 1)
		--AND (ope.id_usuario_noti = in_id_usuario OR in_id_rol < 3 OR ope.id_usuario_noti = 0)
	GROUP BY cl.id_cliente,
			cl.cliente_usuario,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina,
			ope.ult_operacion,
			ope.visto_cliente,
			ope.visto_operador,
			ope.no_leidos,
			u.id_usuario,
			u.usuario
	UNION
	--LEIDOS
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina,
			COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
			ope.visto_cliente,
			ope.visto_operador,
			COALESCE(ope.no_leidos, 0)::integer			as no_leidos,
			COALESCE(u.id_usuario, 0)::integer			as id_usuario,
			COALESCE(u.usuario, '')						as usuario,
			''::character varying(100)					as visita
	FROM cliente cl JOIN agente ag
				ON (cl.id_agente = ag.id_agente
			   		AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1)
				   	AND cl.id_cliente NOT IN (SELECT DISTINCT vcc.id_cliente FROM cliente_chat vcc WHERE vcc.visto_operador = false))
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina
				   AND ofi.marca_baja = false)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
			JOIN ( SELECT 	cc.id_cliente,
							0 AS visto_cliente,
							1 AS visto_operador,
							--MIN(cc.visto_cliente::int) AS visto_cliente,
							--MIN(cc.visto_operador::int) AS visto_operador,
							MAX(cc.fecha_hora_creacion) AS ult_operacion,
							0 AS id_usuario_noti,
							1 AS estado_ope,
							0 AS no_leidos
					FROM cliente_chat cc JOIN cliente cl 
							ON (cl.id_cliente = cc.id_cliente
								AND cc.fecha_hora_creacion > NOW() - aux_periodo
								AND cc.enviado_cliente = true
								AND cl.marca_baja = false
								AND cl.bloqueado = false)							 
						JOIN agente ag
							ON (cl.id_agente = ag.id_agente
								AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
					GROUP BY cc.id_cliente) ope
				ON (cl.id_cliente = ope.id_cliente)
			LEFT JOIN ( SELECT 	cc.id_cliente,
				  			cc.id_usuario,
					   		cc.mensaje,
				  			ROW_NUMBER() OVER (PARTITION BY cc.id_cliente ORDER BY cc.fecha_hora_creacion DESC) AS num_fila
					FROM cliente_chat cc JOIN cliente cl 
							ON (cl.id_cliente = cc.id_cliente
								AND cc.fecha_hora_creacion > NOW() - aux_periodo
								--AND cc.enviado_cliente = false
								AND cl.marca_baja = false
								AND cl.bloqueado = false)							 
						JOIN agente ag
							ON (cl.id_agente = ag.id_agente
								AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))) ope2
				ON (cl.id_cliente = ope2.id_cliente
				   	AND ope2.num_fila = 1
				   	AND ope2.mensaje != 'Finalizado')
			LEFT JOIN usuario u
				ON (ope2.id_usuario = u.id_usuario)
	WHERE cl.marca_baja = false
	GROUP BY cl.id_cliente,
			cl.cliente_usuario,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina,
			ope.ult_operacion,
			ope.visto_cliente,
			ope.visto_operador,
			ope.no_leidos,
			u.id_usuario,
			u.usuario
	UNION
	--VISITA NO LEIDOS:
	SELECT 	vs.id_visita_sesion as id_cliente,
			vs.alias_sesion as cliente_usuario,
			'' as agente_usuario,
			'' as plataforma,
			ofi.oficina,
			COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
			0											as visto_cliente,
			0											as visto_operador,
			COALESCE(ope.no_leidos, 0)::integer			as no_leidos,
			COALESCE(u.id_usuario, 0)::integer			as id_usuario,
			COALESCE(u.usuario, '')						as usuario,
			vs.id_token									as visita
	FROM visita_sesion vs JOIN oficina ofi
				ON (ofi.id_oficina = aux_id_oficina_visita
					AND vs.fecha_hora_cierre IS NULL
					AND (aux_id_oficina_visita = in_id_oficina OR in_id_rol = 1)
				   	AND ofi.marca_baja = false)
			JOIN ( SELECT 	vsc.id_visita_sesion,
							MIN(vsc.fecha_hora_creacion) AS ult_operacion,
							0 AS id_usuario_noti,
				  			2 AS estado_ope,
				  			COUNT(vsc.id_visita_sesion_chat) AS no_leidos
					FROM visita_sesion_chat vsc
					WHERE vsc.visto_operador = false
					GROUP BY vsc.id_visita_sesion) ope
				ON (vs.id_visita_sesion = ope.id_visita_sesion)
			LEFT JOIN ( SELECT 	vsc.id_visita_sesion,
				  				vsc.id_usuario,
				  				ROW_NUMBER() OVER (PARTITION BY vsc.id_visita_sesion ORDER BY vsc.fecha_hora_creacion DESC) AS num_fila
					FROM visita_sesion_chat vsc
					WHERE vsc.fecha_hora_creacion > NOW() - aux_periodo
						AND vsc.enviado_cliente = false
						AND vsc.marca_baja = false) ope2
				ON (vs.id_visita_sesion = ope2.id_visita_sesion
				   	AND ope2.num_fila = 1)
			LEFT JOIN usuario u
				ON (ope2.id_usuario = u.id_usuario)
	UNION
	--VISITA LEIDOS:
	SELECT 	vs.id_visita_sesion as id_cliente,
			vs.alias_sesion as cliente_usuario,
			'' as agente_usuario,
			'' as plataforma,
			ofi.oficina,
			COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
			0											as visto_cliente,
			1											as visto_operador,
			0											as no_leidos,
			COALESCE(u.id_usuario, 0)::integer			as id_usuario,
			COALESCE(u.usuario, '')						as usuario,
			vs.id_token									as visita
	FROM visita_sesion vs JOIN oficina ofi
				ON (ofi.id_oficina = aux_id_oficina_visita
					AND vs.fecha_hora_cierre IS NULL
					AND (aux_id_oficina_visita = in_id_oficina OR in_id_rol = 1)
					AND vs.id_visita_sesion NOT IN (SELECT DISTINCT vsc.id_visita_sesion FROM visita_sesion_chat vsc WHERE vsc.visto_operador = false)
				   	AND ofi.marca_baja = false)
			JOIN ( SELECT 	vsc.id_visita_sesion,
							MIN(vsc.fecha_hora_creacion) AS ult_operacion,
							0 AS id_usuario_noti,
				  			1 AS estado_ope
					FROM visita_sesion_chat vsc
					WHERE vsc.fecha_hora_creacion > NOW() - aux_periodo
						AND vsc.enviado_cliente = true
					GROUP BY vsc.id_visita_sesion) ope
				ON (vs.id_visita_sesion = ope.id_visita_sesion)
			LEFT JOIN ( SELECT 	vsc.id_visita_sesion,
				  				vsc.id_usuario,
					   			vsc.mensaje,
				  				ROW_NUMBER() OVER (PARTITION BY vsc.id_visita_sesion ORDER BY vsc.fecha_hora_creacion DESC) AS num_fila
					FROM visita_sesion_chat vsc
					WHERE vsc.fecha_hora_creacion > NOW() - '48 hours'::INTERVAL--aux_periodo
						AND vsc.enviado_cliente = false
						AND vsc.marca_baja = false) ope2
				ON (vs.id_visita_sesion = ope2.id_visita_sesion
				   	AND ope2.num_fila = 1
				   	AND ope2.mensaje != 'Finalizado')
			LEFT JOIN usuario u
				ON (ope2.id_usuario = u.id_usuario)
	ORDER BY ult_operacion DESC;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM Clientes_Usuarios_Operador_Chat(2, 2, 7);
--SELECT * FROM Clientes_Usuarios_Operador_Chat(2, 1, 2);
--SELECT * FROM Clientes_Usuarios_Operador_Chat(3, 1, 3);
--SELECT * FROM Clientes_Usuarios_Operador_Chat(1, 1, 1);

CREATE OR REPLACE FUNCTION Clientes_Usuarios_Operador_Busqueda(in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER, in_cliente_busca character varying(200))
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				bloqueado boolean,
				agente_usuario character varying(200),
				plataforma character varying(200),
				oficina character varying(100)
) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.bloqueado,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina
	FROM cliente cl JOIN agente ag
				ON (cl.id_agente = ag.id_agente
			   		AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina
				   AND ofi.marca_baja = false)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
	WHERE cl.marca_baja = false
		AND lower(cl.cliente_usuario) LIKE '%' || lower(in_cliente_busca) || '%'
	ORDER BY cl.cliente_usuario, ag.agente_usuario;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION Clientes_Usuarios_Operador_Operacion(in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				bloqueado boolean,
				agente_usuario character varying(200),
				plataforma character varying(200),
				oficina character varying(100),
				ult_operacion timestamp,
				visto_operador integer) AS $$
DECLARE aux_periodo INTERVAL;
BEGIN
	aux_periodo := '72 hours'::INTERVAL; --24 hours

	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.bloqueado,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina,
			COALESCE(ope.ult_operacion, '2000-01-01') 	as ult_operacion,
			COALESCE(ope.visto_operador, 1)			as visto_operador
	FROM cliente cl JOIN agente ag
				ON (cl.id_agente = ag.id_agente
			   		AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina
				   AND ofi.marca_baja = false)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
			JOIN (SELECT	operacion.id_cliente,
								1 AS visto_cliente,
								1 AS visto_operador,
								--MIN(operacion.visto_cliente::int) AS visto_cliente,
								--MIN(operacion.visto_operador::int) AS visto_operador,
								MAX(operacion.ult_operacion) AS ult_operacion,
					   			MAX(operacion.id_usuario_noti) AS id_usuario_noti,
					   			MIN(operacion.estado_ope) AS estado_ope
						FROM
							(SELECT o.id_cliente,
									/*1 AS visto_cliente,
									1 AS visto_operador,*/
									o.fecha_hora_ultima_modificacion AS ult_operacion,
									0 AS id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_ultima_modificacion DESC) AS num_fila
							FROM operacion o JOIN cliente cl 
									ON (cl.id_cliente = o.id_cliente
									   AND o.id_accion IN (7,8))
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							UNION
							SELECT o.id_cliente,
									/*1 AS visto_cliente,
									1 AS visto_operador,*/
									o.fecha_hora_ultima_modificacion AS ult_operacion,
									cb.id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_ultima_modificacion DESC) AS num_fila
							FROM operacion o JOIN operacion_carga oc
									ON (o.id_operacion = oc.id_operacion)
								JOIN cuenta_bancaria cb
									ON (oc.id_cuenta_bancaria = cb.id_cuenta_bancaria)
							 	JOIN cliente cl 
									ON (cl.id_cliente = o.id_cliente)
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							UNION
							SELECT 	o.id_cliente,
									/*1 AS visto_cliente,
									1 AS visto_operador,*/
									o.fecha_hora_ultima_modificacion AS ult_operacion,
									0 AS id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_ultima_modificacion DESC) AS num_fila
							FROM operacion o JOIN operacion_retiro oret
									ON (o.id_operacion = oret.id_operacion)
							 	JOIN cliente cl 
									ON (cl.id_cliente = o.id_cliente)
								JOIN agente ag
									ON (cl.id_agente = ag.id_agente
										AND (ag.id_oficina = in_id_oficina OR in_id_rol = 1))
							/*UNION
							 SELECT  o.id_cliente,
									/*1 AS visto_cliente,
									1 AS visto_operador,*/
									o.fecha_hora_proceso AS ult_operacion,
									o.id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_proceso DESC) AS num_fila
							FROM clientes_operaciones_diario o 
							WHERE (o.id_oficina = in_id_oficina 
							 		OR in_id_rol = 1)
							 		AND o.fecha_hora_proceso > NOW() - aux_periodo*/
							UNION
							SELECT  o.id_cliente,
									/*1 AS visto_cliente,
									1 AS visto_operador,*/
									o.fecha_hora_proceso AS ult_operacion,
									o.id_usuario_noti,
							 		o.id_estado AS estado_ope,
									ROW_NUMBER() OVER (PARTITION BY o.id_cliente ORDER BY o.fecha_hora_proceso DESC) AS num_fila
							FROM clientes_operaciones_hist o 
							WHERE (o.id_oficina = in_id_oficina 
							 		OR in_id_rol = 1)
							 		AND o.fecha_hora_proceso > NOW() - aux_periodo
							 ) operacion
					   		WHERE num_fila = 1
						GROUP BY operacion.id_cliente) ope
				ON (cl.id_cliente = ope.id_cliente)
	WHERE cl.marca_baja = false	
		AND (ope.ult_operacion > NOW() - aux_periodo or ope.estado_ope = 1)
		AND (ope.id_usuario_noti = in_id_usuario OR in_id_rol < 3 OR ope.id_usuario_noti = 0)
	GROUP BY cl.id_cliente,
			cl.cliente_usuario,
			cl.bloqueado,
			ag.agente_usuario,
			pla.plataforma,
			ofi.oficina,
			ope.ult_operacion,
			ope.visto_operador
	ORDER BY ult_operacion DESC;
END;
$$ LANGUAGE plpgsql;

--DROP VIEW v_Tokens_Operacion
CREATE OR REPLACE VIEW v_Tokens_Operacion AS
SELECT 	tk.id_registro_token,
		tk.id_token,
		tk.ingresos,
		COUNT(DISTINCT tko.id_cliente)								AS registros,
		SUM(CASE WHEN tko.operaciones_carga > 0 THEN 1 ELSE 0 END)	AS cargaron,
		COALESCE(SUM(tko.cantidad_operaciones_carga),0)				AS total_cargas,
		COALESCE(SUM(tko.total_importe),0)							AS total_importe,
		COALESCE(SUM(tko.total_bono),0)								AS total_bono
FROM	registro_token tk LEFT JOIN (SELECT clr.id_registro_token,
									clr.id_cliente,
									/*COALESCE(MIN(opc.id_operacion_carga),0)			AS operaciones_carga,
									COALESCE(COUNT(opc.id_operacion_carga), 0)		AS cantidad_operaciones_carga,
									COALESCE(SUM(opc.importe),0)					AS total_importe,
									COALESCE(SUM(opc.bono),0)						AS total_bono
							FROM cliente clr LEFT JOIN v_operacion_completa op
									ON (clr.id_cliente = op.id_cliente
									   AND op.id_estado = 2)
								LEFT JOIN v_operacion_carga_completa opc
									ON (op.id_operacion = opc.id_operacion)*/
									COALESCE(MIN(opc.id_operacion),0)			AS operaciones_carga,
									COALESCE(COUNT(opc.id_operacion), 0)		AS cantidad_operaciones_carga,
									COALESCE(SUM(opc.carga_importe),0)			AS total_importe,
									COALESCE(SUM(opc.carga_bono),0)				AS total_bono
							FROM cliente clr LEFT JOIN rpt_Clientes_Operaciones_Completo_Cliente(0) opc
								ON (clr.id_cliente = opc.id_cliente
									   	AND opc.id_estado = 2
								   		AND opc.es_carga = 1)
							WHERE clr.id_registro_token > 0
							GROUP BY clr.id_registro_token, clr.id_cliente) tko
					ON (tk.id_registro_token = tko.id_registro_token)
GROUP BY tk.id_registro_token,
		tk.id_token,
		tk.ingresos;
--select * from v_Tokens_Operacion
--select * from v_Tokens_Operacion where id_registro_token = 14

--DROP FUNCTION Obtener_Tokens_Operacion(in_id_registro_token integer);
CREATE OR REPLACE FUNCTION Obtener_Tokens_Operacion(in_id_registro_token integer)
RETURNS TABLE (	id_registro_token integer,
			   	id_token character varying(60),
			   	ingresos integer,
			   	registros bigint,
			   	cargaron bigint,
			   	total_cargas numeric,
			   	total_importe numeric,
			   	total_bono numeric) AS $$
BEGIN
	RETURN QUERY
	SELECT 	tk.id_registro_token,
			tk.id_token,
			tk.ingresos,
			COUNT(DISTINCT tko.id_cliente)								AS registros,
			SUM(CASE WHEN tko.operaciones_carga > 0 THEN 1 ELSE 0 END)	AS cargaron,
			COALESCE(SUM(tko.cantidad_operaciones_carga),0)				AS total_cargas,
			COALESCE(SUM(tko.total_importe),0)							AS total_importe,
			COALESCE(SUM(tko.total_bono),0)								AS total_bono
	FROM	registro_token tk LEFT JOIN (SELECT clr.id_registro_token,
										clr.id_cliente,
										COALESCE(MIN(opc.id_operacion),0)			AS operaciones_carga,
										COALESCE(COUNT(opc.id_operacion), 0)		AS cantidad_operaciones_carga,
										COALESCE(SUM(opc.carga_importe),0)			AS total_importe,
										COALESCE(SUM(opc.carga_bono),0)				AS total_bono
								--FROM cliente clr LEFT JOIN rpt_Clientes_Operaciones_Completo_Cliente(0) opc
								FROM cliente clr LEFT JOIN clientes_operaciones_hist opc
									ON (clr.id_cliente = opc.id_cliente
											AND opc.id_estado = 2
											AND opc.es_carga = 1)
								WHERE clr.id_registro_token > 0
								GROUP BY clr.id_registro_token, clr.id_cliente) tko
						ON (tk.id_registro_token = tko.id_registro_token)
	WHERE tk.id_registro_token = in_id_registro_token OR in_id_registro_token = 0
	GROUP BY tk.id_registro_token,
			tk.id_token,
			tk.ingresos;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Tokens_Operacion(14)

--DROP VIEW v_Cliente_Registro
CREATE OR REPLACE VIEW v_Cliente_Registro AS
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cl.cliente_password,
		cl.fecha_hora_creacion,
		cl.correo_electronico,
		cl.telefono,
		rto.id_token,
		rto.ingresos,
		rto.registros,
		rto.cargaron,
		rto.total_cargas,
		rto.total_importe,
		rto.total_bono,
		COALESCE(rtr.id_registro_token, 0)	AS id_registro_token,
		COALESCE(rtr.de_agente, false)		AS de_agente,
		COALESCE(clr.cliente_usuario, '')	AS cliente_referente
FROM	cliente cl JOIN registro_token rt
			ON (rt.de_agente = false
			   AND rt.id_usuario = cl.id_cliente)
		/*JOIN v_Tokens_Operacion rto
			ON (rt.id_registro_token = rto.id_registro_token)*/
		JOIN Obtener_Tokens_Operacion(0) rto
			ON (rt.id_registro_token = rto.id_registro_token)
		LEFT JOIN registro_token rtr
			ON (cl.id_registro_token  = rtr.id_registro_token)
		LEFT JOIN cliente clr
			ON (rtr.de_agente = false AND rtr.id_usuario = clr.id_cliente);
--SELECT * FROM v_Cliente_Registro where id_cliente = 54

--DROP FUNCTION Obtener_Cliente_Registro(in_id_cliente integer);
CREATE OR REPLACE FUNCTION Obtener_Cliente_Registro(in_id_cliente integer)
RETURNS TABLE (	id_cliente integer,
				cliente_usuario character varying(200),
				cliente_password character varying(200),
				fecha_hora_creacion timestamp,
				correo_electronico  character varying(100),
				telefono character varying(100),
				id_token character varying,
				ingresos integer,
				registros bigint,
				cargaron bigint,
				total_cargas numeric,
				total_importe numeric,
				total_bono numeric,
				id_registro_token integer,
				de_agente boolean,
				cliente_referente character varying) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.cliente_password,
			cl.fecha_hora_creacion,
			cl.correo_electronico,
			cl.telefono,
			rto.id_token,
			rto.ingresos,
			rto.registros,
			rto.cargaron,
			rto.total_cargas,
			rto.total_importe,
			rto.total_bono,
			COALESCE(rtr.id_registro_token, 0)	AS id_registro_token,
			COALESCE(rtr.de_agente, false)		AS de_agente,
			COALESCE(clr.cliente_usuario, '')	AS cliente_referente
	FROM cliente cl JOIN registro_token rt
				ON (rt.de_agente = false
					AND cl.id_cliente = in_id_cliente
				   	AND rt.id_usuario = cl.id_cliente)
			JOIN Obtener_Tokens_Operacion(rt.id_registro_token) rto
				ON (rt.id_registro_token = rto.id_registro_token)
			LEFT JOIN registro_token rtr
				ON (cl.id_registro_token  = rtr.id_registro_token)
			LEFT JOIN cliente clr
				ON (rtr.de_agente = false AND rtr.id_usuario = clr.id_cliente);
	--WHERE cl.id_cliente = in_id_cliente;
END;
$$ LANGUAGE plpgsql;
--select * from Obtener_Cliente_Registro(54)

--DROP VIEW v_Cliente_Alertas
CREATE OR REPLACE VIEW v_Cliente_Alertas AS
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cl.cliente_password,
		cl.fecha_hora_creacion,
		cl.correo_electronico,
		cl.telefono,
		rto.id_token,
		rto.ingresos,
		rto.registros,
		rto.cargaron,
		rto.total_cargas,
		rto.total_importe,
		rto.total_bono,
		COALESCE(rtr.id_registro_token, 0)	AS id_registro_token,
		COALESCE(rtr.de_agente, false)		AS de_agente,
		COALESCE(clr.cliente_usuario, '')	AS cliente_referente
FROM	cliente cl JOIN registro_token rt
			ON (rt.de_agente = false
			   AND rt.id_usuario = cl.id_cliente)
		JOIN v_Tokens_Operacion rto
			ON (rt.id_registro_token = rto.id_registro_token)
		LEFT JOIN registro_token rtr
			ON (cl.id_registro_token  = rtr.id_registro_token)
		LEFT JOIN cliente clr
			ON (rtr.de_agente = false AND rtr.id_usuario = clr.id_cliente);

--DROP VIEW v_Tokens
CREATE OR REPLACE VIEW v_Tokens AS
SELECT 	rt.id_registro_token,
		rt.id_token,
		rt.activo,
		rt.de_agente,
		rt.bono_creacion,
		rt.bono_carga_1,
		rt.observaciones,
		COALESCE(cl.id_cliente, 0) 	AS id_cliente,
		COALESCE(cl.cliente_usuario, '') as cliente_usuario,
		COALESCE(cl.id_registro_token, 0) 	AS id_registro_token_cliente,
		ag.id_agente,
		ag.agente_usuario,
		ag.id_oficina,
		o.oficina,
		ag.id_plataforma,
		pl.plataforma,
		pl.url_juegos,
		rt.bono_carga_1_porcentaje

FROM registro_token rt LEFT JOIN cliente cl
		ON (rt.id_usuario = cl.id_cliente 
			AND NOT rt.de_agente)
	JOIN agente ag
		ON ((cl.id_agente = ag.id_agente AND NOT rt.de_agente) 
			OR 
			(rt.id_usuario = ag.id_agente AND rt.de_agente))
	JOIN plataforma pl
		ON (ag.id_plataforma = pl.id_plataforma)
	JOIN oficina o
		ON (ag.id_oficina = o.id_oficina);
--select * from v_Tokens where id_token = 'a-1-9473764e8de991edc3899a179f8af9c1ce3d6ba';
--select * from v_Tokens where id_token = 'c-4-9473764e8de991edc3899a179f8af9c1ce3d6ba';
--select * from v_Tokens where id_cliente = 4;

--DROP FUNCTION Obtener_Token(in_id_registro_token INTEGER);
CREATE OR REPLACE FUNCTION Obtener_Token(in_id_registro_token INTEGER)
RETURNS TABLE (	id_registro_token integer,
				id_token character varying(100),
				activo boolean,
				de_agente boolean,
				bono_creacion integer,
				bono_carga_1 integer,
				bono_carga_1_porcentaje boolean,
				observaciones character varying(200),
				id_cliente integer,
				cliente_usuario character varying(200),
			   	id_registro_token_cliente integer,
				id_agente integer,
				agente_usuario character varying(200),
				id_oficina integer,
				oficina character varying(100),
				id_plataforma integer,
				plataforma character varying(200),
				url_juegos character varying(200)) AS $$
BEGIN
	RETURN QUERY 
	SELECT 	rt.id_registro_token,
			rt.id_token,
			rt.activo,
			rt.de_agente,
			rt.bono_creacion,
			rt.bono_carga_1,
			rt.bono_carga_1_porcentaje,
			rt.observaciones,
			COALESCE(cl.id_cliente, 0) 	AS id_cliente,
			COALESCE(cl.cliente_usuario, '') as cliente_usuario,
			COALESCE(cl.id_registro_token, 0) 	AS id_registro_token_cliente,
			ag.id_agente,
			ag.agente_usuario,
			ag.id_oficina,
			o.oficina,
			ag.id_plataforma,
			pl.plataforma,
			pl.url_juegos

	FROM registro_token rt LEFT JOIN cliente cl
			ON (rt.id_usuario = cl.id_cliente 
				AND NOT rt.de_agente
			   	AND rt.id_registro_token NOT IN (SELECT cr.id_registro_token FROM contactos_recupero cr))
		JOIN agente ag
			ON ((cl.id_agente = ag.id_agente AND NOT rt.de_agente) 
				OR 
				(rt.id_usuario = ag.id_agente AND rt.de_agente))
		JOIN plataforma pl
			ON (ag.id_plataforma = pl.id_plataforma)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina)
	WHERE rt.id_registro_token = in_id_registro_token 
		   OR in_id_registro_token = 0;
END;
$$ LANGUAGE plpgsql;
select * from registro_token where de_agente order by id_usuario;
--SELECT * FROM Obtener_Token(14);
--SELECT * FROM Obtener_Token(0) WHERE id_registro_token = 14;

--DROP VIEW v_Tokens_Completo
CREATE OR REPLACE VIEW v_Tokens_Completo AS
SELECT 	tk.id_registro_token,
		tk.id_token,
		tk.activo,
		tk.id_oficina,
		tk.oficina,
		tk.id_plataforma,
		tk.plataforma,
		tk.url_juegos,
		tk.id_agente,
		tk.agente_usuario,
		tk.de_agente,
		tk.bono_creacion,
		tk.bono_carga_1,
		tko.ingresos,
		tko.registros,
		tko.cargaron,
		tko.total_cargas,
		tko.total_importe,
		tko.total_bono
FROM v_Tokens tk JOIN v_Tokens_Operacion tko
	ON (tk.id_registro_token = tko.id_registro_token)
WHERE tk.de_agente = true
UNION
SELECT 	0		AS id_registro_token,
		''		AS id_token,
		true	AS activo,
		tk.id_oficina,
		tk.oficina,
		tk.id_plataforma,
		tk.plataforma,
		tk.url_juegos,
		tk.id_agente,
		tk.agente_usuario,
		tk.de_agente,
		MAX(tk.bono_creacion) 	AS bono_creacion,
		MAX(tk.bono_carga_1)	AS bono_carga_1,
		SUM(tko.ingresos)		AS ingresos,
		SUM(tko.registros)		AS registros,
		SUM(tko.cargaron) 		AS cargaron,
		SUM(tko.total_cargas)	as total_cargas,
		SUM(tko.total_importe)	AS total_importe,
		SUM(tko.total_bono)		AS total_bono
FROM v_Tokens tk JOIN v_Tokens_Operacion tko
	ON (tk.id_registro_token = tko.id_registro_token)
WHERE tk.de_agente = false AND tk.activo = true
GROUP BY tk.id_oficina,
		tk.oficina,
		tk.id_plataforma,
		tk.plataforma,
		tk.url_juegos,
		tk.id_agente,
		tk.agente_usuario,
		tk.de_agente;
--select * from v_Tokens_Completo;

--DROP FUNCTION Tokens_Completo_Operador(in_id_rol INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Tokens_Completo_Operador(in_id_rol INTEGER, in_id_oficina INTEGER)
RETURNS TABLE (	id_registro_token integer,
				id_token character varying(100),
				activo boolean,
				de_agente boolean,
				bono_creacion integer,
				bono_carga_1 integer,
				bono_carga_1_porcentaje boolean,
				observaciones character varying(200),
				id_agente integer,
				agente_usuario character varying(200),
				id_oficina integer,
				oficina character varying(100),
				id_plataforma integer,
				plataforma character varying(200),
				url_juegos character varying(200),
				ingresos bigint,
				registros numeric,
				cargaron numeric,
				total_cargas numeric,
				total_importe numeric,
				total_bono numeric) AS $$
BEGIN
	RETURN QUERY 
	SELECT rt.id_registro_token,
			rt.id_token,
			rt.activo,
			rt.de_agente,
			rt.bono_creacion,
			rt.bono_carga_1,
			rt.bono_carga_1_porcentaje,
			rt.observaciones,
			ag.id_agente,
			ag.agente_usuario,
			ag.id_oficina,
			o.oficina,
			ag.id_plataforma,
			pl.plataforma,
			pl.url_juegos,
			rt.ingresos,
			COALESCE(COUNT(DISTINCT tko.id_cliente), 0)					AS registros,
			SUM(CASE WHEN tko.operaciones_carga > 0 THEN 1 ELSE 0 END)	AS cargaron,
			COALESCE(SUM(tko.cantidad_operaciones_carga),0)				AS total_cargas,
			COALESCE(SUM(tko.total_importe),0)							AS total_importe,
			COALESCE(SUM(tko.total_bono),0)								AS total_bono
	FROM agente ag JOIN plataforma pl
			ON (ag.id_plataforma = pl.id_plataforma)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina
			   	AND o.marca_baja = false)
		JOIN registro_token rt 
			ON (rt.id_usuario = ag.id_agente
				 	AND rt.de_agente
			   		AND rt.id_registro_token NOT IN (SELECT cr.id_registro_token FROM contactos_recupero cr))
		LEFT JOIN (SELECT clr.id_registro_token,
						clr.id_cliente,
						/*COALESCE(MIN(opc.id_operacion_carga),0)			AS operaciones_carga,
						COALESCE(COUNT(opc.id_operacion_carga), 0)		AS cantidad_operaciones_carga,
						COALESCE(SUM(opc.importe),0)					AS total_importe,
						COALESCE(SUM(opc.bono),0)						AS total_bono
					FROM cliente clr LEFT JOIN v_operacion_completa op
							ON (clr.id_cliente = op.id_cliente
							   AND op.id_estado = 2)
						LEFT JOIN v_operacion_carga_completa opc
							ON (op.id_operacion = opc.id_operacion)*/
						COALESCE(MIN(opc.id_operacion),0)			AS operaciones_carga,
						COALESCE(COUNT(opc.id_operacion), 0)		AS cantidad_operaciones_carga,
						COALESCE(SUM(opc.carga_importe),0)			AS total_importe,
						COALESCE(SUM(opc.carga_bono),0)				AS total_bono
				   	--FROM cliente clr LEFT JOIN rpt_Clientes_Operaciones_Completo_Cliente(0) opc
					FROM cliente clr LEFT JOIN clientes_operaciones_hist opc
						ON (clr.id_cliente = opc.id_cliente
								AND opc.id_estado = 2
								AND opc.es_carga = 1)
					WHERE clr.id_registro_token > 0
					GROUP BY clr.id_registro_token, clr.id_cliente) tko
			ON (rt.id_registro_token = tko.id_registro_token)		
	WHERE ag.id_oficina = in_id_oficina OR in_id_rol = 1
	GROUP BY rt.id_registro_token,
			rt.id_token,
			rt.activo,
			rt.de_agente,
			rt.bono_creacion,
			rt.bono_carga_1,
			rt.bono_carga_1_porcentaje,
			rt.observaciones,
			ag.id_agente,
			ag.agente_usuario,
			ag.id_oficina,
			o.oficina,
			ag.id_plataforma,
			pl.plataforma,
			pl.url_juegos,
			rt.ingresos
	UNION
	SELECT 	0		AS id_registro_token,
			''		AS id_token,
			true	AS activo,
			false	AS de_agente,
			ag.tokens_bono_creacion,
			ag.tokens_bono_carga_1,
			ag.tokens_bono_carga_1_porcentaje,
			''		AS observaciones,
			ag.id_agente,
			ag.agente_usuario,
			ag.id_oficina,
			o.oficina,
			ag.id_plataforma,
			pl.plataforma,
			pl.url_juegos,
			COALESCE(SUM(tkocl.ingresos), 0)	AS ingresos,
			COALESCE(SUM(tkocl.registros), 0)	AS registros,
			COALESCE(SUM(tkocl.cargaron), 0)	AS cargaron,
			COALESCE(SUM(tkocl.total_cargas),0)	AS total_cargas,
			COALESCE(SUM(tkocl.total_importe),0)	AS total_importe,
			COALESCE(SUM(tkocl.total_bono),0)		AS total_bono
	FROM agente ag JOIN plataforma pl
			ON (ag.id_plataforma = pl.id_plataforma)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina)
		JOIN (
				SELECT 	cl.id_agente,
						rt.id_registro_token,
						rt.ingresos,
						COALESCE(COUNT(DISTINCT tko.id_cliente), 0)					AS registros,
						SUM(CASE WHEN tko.operaciones_carga > 0 THEN 1 ELSE 0 END)	AS cargaron,
						COALESCE(SUM(tko.cantidad_operaciones_carga),0)				AS total_cargas,
						COALESCE(SUM(tko.total_importe),0)							AS total_importe,
						COALESCE(SUM(tko.total_bono),0)								AS total_bono
				FROM cliente cl JOIN registro_token rt
					ON (rt.id_usuario = cl.id_cliente
						AND NOT rt.de_agente
			   			AND rt.id_registro_token NOT IN (SELECT cr.id_registro_token FROM contactos_recupero cr))
				LEFT JOIN (SELECT clr.id_registro_token,
								clr.id_cliente,
								/*COALESCE(MIN(opc.id_operacion_carga),0)			AS operaciones_carga,
								COALESCE(COUNT(opc.id_operacion_carga), 0)		AS cantidad_operaciones_carga,
								COALESCE(SUM(opc.importe),0)					AS total_importe,
								COALESCE(SUM(opc.bono),0)						AS total_bono
							FROM cliente clr LEFT JOIN v_operacion_completa op
									ON (clr.id_cliente = op.id_cliente
									   AND op.id_estado = 2)
								LEFT JOIN v_operacion_carga_completa opc
									ON (op.id_operacion = opc.id_operacion)*/
								COALESCE(MIN(opc.id_operacion),0)			AS operaciones_carga,
								COALESCE(COUNT(opc.id_operacion), 0)		AS cantidad_operaciones_carga,
								COALESCE(SUM(opc.carga_importe),0)			AS total_importe,
								COALESCE(SUM(opc.carga_bono),0)				AS total_bono
				   			--FROM cliente clr LEFT JOIN rpt_Clientes_Operaciones_Completo_Cliente(0) opc
							FROM cliente clr LEFT JOIN clientes_operaciones_hist opc
								ON (clr.id_cliente = opc.id_cliente
										AND opc.id_estado = 2
										AND opc.es_carga = 1)
								WHERE clr.id_registro_token > 0
								GROUP BY clr.id_registro_token, clr.id_cliente) tko
					ON (rt.id_registro_token = tko.id_registro_token)
				GROUP BY cl.id_agente, 
						rt.id_registro_token, 
						rt.ingresos
			) tkocl
			ON (ag.id_agente = tkocl.id_agente)
	WHERE ag.id_oficina = in_id_oficina OR in_id_rol = 1
	GROUP BY ag.tokens_bono_creacion,
			ag.tokens_bono_carga_1,
			ag.tokens_bono_carga_1_porcentaje,
			ag.id_agente,
			ag.agente_usuario,
			ag.id_oficina,
			o.oficina,
			ag.id_plataforma,
			pl.plataforma,
			pl.url_juegos
	ORDER BY cargaron DESC, registros DESC, ingresos DESC, plataforma;
END;
$$ LANGUAGE plpgsql;
--select * from Tokens_Completo_Operador(1, 1)

--DROP VIEW v_Tokens_Completo_Clientes
CREATE OR REPLACE VIEW v_Tokens_Completo_Clientes AS
SELECT 	tk.id_registro_token,
		tk.id_token,
		tk.id_agente,
		tk.agente_usuario,
		tk.cliente_usuario,
		tk.bono_carga_1,
		tk.activo,
		tk.id_registro_token_cliente,
		tko.ingresos,
		tko.registros,
		tko.cargaron,
		tko.total_cargas,
		tko.total_importe,
		tko.total_bono
FROM Obtener_Token(0) tk JOIN Obtener_Tokens_Operacion(0) tko
--FROM v_Tokens tk JOIN v_Tokens_Operacion tko
	ON (tk.id_registro_token = tko.id_registro_token)
WHERE tk.de_agente = false;
--select * from v_Tokens_Completo_Clientes WHERE id_registro_token = 2

--DROP FUNCTION Obtener_Token_Completo_Clientes(in_id_agente INTEGER, in_id_registro_token_cliente INTEGER);
CREATE OR REPLACE FUNCTION Obtener_Token_Completo_Clientes(in_id_agente INTEGER, in_id_registro_token_cliente INTEGER)
RETURNS TABLE (	id_registro_token integer,
				id_token character varying,
			   	id_agente integer,
			   	agente_usuario character varying,
			   	cliente_usuario character varying,
				bono_carga_1 integer,
			   	activo boolean,
			   	id_registro_token_cliente integer,
				ingresos integer,
				registros bigint,
				cargaron bigint,
				total_cargas numeric,
				total_importe numeric,
				total_bono numeric) AS $$
BEGIN
	RETURN QUERY 
	SELECT 	tk.id_registro_token,
			tk.id_token,
			tk.id_agente,
			tk.agente_usuario,
			tk.cliente_usuario,
			tk.bono_carga_1,
			tk.activo,
			tk.id_registro_token_cliente,
			tko.ingresos,
			tko.registros,
			tko.cargaron,
			tko.total_cargas,
			tko.total_importe,
			tko.total_bono
	FROM Obtener_Token(0) tk JOIN Obtener_Tokens_Operacion(0) tko
	--FROM v_Tokens tk JOIN v_Tokens_Operacion tko
		ON (tk.id_registro_token = tko.id_registro_token)
	WHERE tk.de_agente = false
		AND tk.id_agente = in_id_agente
		AND tk.id_registro_token_cliente = in_id_registro_token_cliente
	ORDER BY tko.cargaron desc, tko.registros desc, tko.ingresos desc;
END;
$$ LANGUAGE plpgsql;

/*update registro_token set activo = false
--select * from registro_token 
where id_registro_token in (10102,194869,116584,10103,2616,15601,2,99426,10122,97,41,106079,89176,10927,15604,87299,24362,25,24361,15607)
or id_registro_token in (7326,2513,6394,73966,73977)
--and id_registro_token not in (select id_registro_token from contactos_recupero)*/

--DROP VIEW v_IP_Lista_Blanca
CREATE OR REPLACE VIEW v_IP_Lista_Blanca AS
SELECT	ip_ok.ip, max(ip_ok.fecha_hora_creacion) ult_conexion
FROM	(
	SELECT	cs.ip, cs.fecha_hora_creacion
	FROM	cliente_sesion cs JOIN cliente cl
				ON (cs.id_cliente = cl.id_cliente
				   AND cl.marca_baja = false
				   AND cl.bloqueado = false)
	UNION
	SELECT	us.ip, us.fecha_hora_creacion
	FROM	usuario_sesion us JOIN usuario u
				ON (us.id_usuario = u.id_usuario
				   AND u.marca_baja = false)) ip_ok
WHERE ip_ok.ip NOT IN (SELECT ipln.ip FROM ip_lista_negra ipln)
/*WHERE ip_ok.ip NOT IN (SELECT	cs.ip
						FROM	cliente_sesion cs JOIN cliente cl
									ON (cs.id_cliente = cl.id_cliente
									   AND cl.marca_baja = false
									   AND cl.bloqueado = true))*/
GROUP BY ip_ok.ip;
--select * from v_IP_Lista_Blanca;

--DROP VIEW v_Cuenta_Bancaria_Mercado_Pago;
CREATE OR REPLACE VIEW v_Cuenta_Bancaria_Mercado_Pago AS
SELECT 	cb.id_cuenta_bancaria,
		cb.id_oficina,
		cb.nombre,
		cb.alias,
		cb.cbu,
		/*cba.nombre_aplicacion,
		cba.notificacion_descripcion,*/
		cbmp.access_token,
		cbmp.public_key,
		cbmp.client_id,
		cbmp.client_secret,
		us.id_usuario
FROM 	cuenta_bancaria cb JOIN cuenta_bancaria_mercado_pago cbmp
				ON (cb.id_cuenta_bancaria = cbmp.id_cuenta_bancaria
				   	AND cbmp.marca_baja = false)
			JOIN (SELECT 	u.id_oficina, 
				  			MIN(u.id_usuario) AS id_usuario 
				  	FROM usuario u
				  	WHERE u.id_rol = 2 
				  		AND u.marca_baja = false
				  	GROUP BY u.id_oficina) us
				ON (us.id_oficina = cb.id_oficina);
--select * from v_Cuenta_Bancaria_Mercado_Pago order by 1

--DROP FUNCTION Cuenta_Bancaria_Mercado_Pago(in_id_cuenta_bancaria INTEGER);
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_Mercado_Pago(in_id_cuenta_bancaria INTEGER)
RETURNS TABLE (	id_cuenta_bancaria integer,
				id_oficina integer,
				nombre character varying(200),
				alias character varying(200),
				cvu character varying(200),
				access_token character varying(200),
				public_key character varying(200),
				client_id character varying(200),
				client_secret character varying(200),
				id_usuario integer) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cb.id_cuenta_bancaria,
			cb.id_oficina,
			cb.nombre,
			cb.alias,
			cb.cbu,
			cbmp.access_token,
			cbmp.public_key,
			cbmp.client_id,
			cbmp.client_secret,
			cb.id_usuario_ultima_modificacion as id_usuario
	FROM 	cuenta_bancaria cb JOIN cuenta_bancaria_mercado_pago cbmp
					ON (cb.id_cuenta_bancaria = cbmp.id_cuenta_bancaria
						AND cb.id_cuenta_bancaria = in_id_cuenta_bancaria
						AND cb.marca_eliminada = false
						AND cbmp.marca_baja = false)
					JOIN oficina o
						ON (cb.id_oficina = o.id_oficina
						   AND o.marca_baja = false);
END;
$$ LANGUAGE plpgsql;
--select * from Cuenta_Bancaria_Mercado_Pago(326);
--select * from v_Cuenta_Bancaria_Mercado_Pago where id_cuenta_bancaria = 100;

--DROP FUNCTION Cuenta_Bancaria_TuCash(in_cvu VARCHAR(200));
CREATE OR REPLACE FUNCTION Cuenta_Bancaria_TuCash(in_cvu VARCHAR(200))
RETURNS TABLE (	id_cuenta_bancaria integer,
				id_oficina integer,
				nombre character varying(200),
				alias character varying(200),
				cvu character varying(200),
				id_usuario_ultima_modificacion integer,
				id_usuario_noti integer) AS $$
BEGIN
	RETURN QUERY
	SELECT 	cb.id_cuenta_bancaria,
			cb.id_oficina,
			cb.nombre,
			cb.alias,
			cb.cbu AS cvu,
			cb.id_usuario_ultima_modificacion,
			cb.id_usuario_noti
	FROM cuenta_bancaria cb
	WHERE cb.id_billetera = 8 --tucash
		AND cb.marca_baja = false
		AND cb.cbu = in_cvu;
END;
$$ LANGUAGE plpgsql;

--DROP VIEW v_Notificaciones_Cargas;
CREATE OR REPLACE VIEW v_Notificaciones_Cargas AS
SELECT	n.id_notificacion,
		n.id_usuario,
		u.id_oficina,
		o.oficina,
		n.id_cuenta_bancaria,
		b.billetera,
		cb.nombre,
		cb.alias,
		cb.cbu,
		n.titulo,
		n.mensaje,
		n.fecha_hora,
		n.id_notificacion_origen,
		n.id_origen,
		n.anulada,
		nc.id_notificacion_carga,
		nc.id_operacion_carga,
		nc.carga_usuario,
		nc.carga_monto,
		nc.marca_procesado,
		nc.fecha_hora_procesado,
		cb.id_usuario_noti,
		COALESCE(opc.importe, 0)	AS importe,
		COALESCE(opc.bono, 0)		AS bono,
		COALESCE(op.id_cliente,0 )	AS id_cliente,
		COALESCE(op.fecha_hora_creacion, '1900-01-01 00:00:00')	AS fecha_hora_creacion,
		COALESCE(cl.cliente_usuario, '' )	AS cliente_usuario,
		COALESCE(ag.agente_usuario, '' )	AS agente_usuario,
		COALESCE(pl.plataforma, '' )		AS plataforma,
		COALESCE(pl.url_admin, '' )			AS url_admin
FROM	notificacion n JOIN notificacion_carga nc
				ON (n.id_notificacion = nc.id_notificacion)
			JOIN usuario u
				ON (u.id_usuario = n.id_usuario)
			JOIN oficina o
				ON (u.id_oficina = o.id_oficina)
			JOIN cuenta_bancaria cb
				ON (n.id_cuenta_bancaria = cb.id_cuenta_bancaria)
			JOIN billetera b
				ON (cb.id_billetera = b.id_billetera)
			LEFT JOIN operacion_carga opc
				ON (nc.id_operacion_carga = opc.id_operacion_carga
				   AND cb.id_cuenta_bancaria = opc.id_cuenta_bancaria) -- revisar!!
			LEFT JOIN operacion op
				ON (op.id_operacion = opc.id_operacion
				   AND op.id_accion = 1
				   AND op.id_estado = 2
				   AND op.marca_baja = false)
			LEFT JOIN cliente cl
				ON (op.id_cliente = cl.id_cliente)
			LEFT JOIN agente ag
				ON (cl.id_agente = ag.id_agente)
			LEFT JOIN plataforma pl
				ON (pl.id_plataforma = ag.id_plataforma); 
--select * from v_Notificaciones_Cargas order by fecha_hora_procesado desc;

--DROP FUNCTION Clientes_Usuarios_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER);
CREATE OR REPLACE FUNCTION Notificaciones_Cargas_Operador(in_periodo INTEGER, in_id_rol INTEGER, in_id_oficina INTEGER, in_id_usuario INTEGER)
RETURNS TABLE (	id_notificacion integer,
				id_usuario integer,
				id_oficina integer,
				oficina character varying(100),
				id_cuenta_bancaria integer,
				billetera character varying(100),
				nombre character varying(200),
				alias character varying(200),
				cbu character varying(200),
				titulo character varying(200),
				mensaje character varying(200),
				fecha_hora timestamp,
				id_notificacion_origen character varying(200),
				id_origen integer,
				anulada boolean,
				id_notificacion_carga integer,
				id_operacion_carga integer,
				carga_usuario character varying(100),
				carga_monto numeric,
				marca_procesado boolean,
				fecha_hora_procesado timestamp,
				id_usuario_noti integer,
				importe numeric,
				bono numeric,
				id_cliente integer,
				fecha_hora_creacion timestamp,
				cliente_usuario character varying,
				agente_usuario character varying,
				plataforma character varying,
				url_admin character varying) AS $$
DECLARE aux_periodo INTERVAL;
BEGIN
	IF (in_periodo = 1) THEN
		aux_periodo := '1 hour'::INTERVAL;
	ELSE
		aux_periodo := '24 hours'::INTERVAL;
	END IF;

	RETURN QUERY
	SELECT	n.id_notificacion,
			n.id_usuario,
			u.id_oficina,
			o.oficina,
			n.id_cuenta_bancaria,
			b.billetera,
			cb.nombre,
			cb.alias,
			cb.cbu,
			n.titulo,
			n.mensaje,
			n.fecha_hora,
			n.id_notificacion_origen,
			n.id_origen,
			n.anulada,
			nc.id_notificacion_carga,
			nc.id_operacion_carga,
			nc.carga_usuario,
			nc.carga_monto,
			nc.marca_procesado,
			nc.fecha_hora_procesado,
			cb.id_usuario_noti,
			COALESCE(opc.importe, 0)	AS importe,
			COALESCE(opc.bono, 0)		AS bono,
			COALESCE(op.id_cliente,0 )	AS id_cliente,
			COALESCE(op.fecha_hora_creacion, '1900-01-01 00:00:00')	AS fecha_hora_creacion,
			COALESCE(cl.cliente_usuario, '' )	AS cliente_usuario,
			COALESCE(ag.agente_usuario, '' )	AS agente_usuario,
			COALESCE(pl.plataforma, '' )		AS plataforma,
			COALESCE(pl.url_admin, '' )			AS url_admin
	FROM	notificacion n JOIN notificacion_carga nc
					ON (n.id_notificacion = nc.id_notificacion
					   	AND (n.fecha_hora > NOW() - aux_periodo OR nc.marca_procesado = false))
				JOIN usuario u
					ON (u.id_usuario = n.id_usuario)
				JOIN oficina o
					ON (u.id_oficina = o.id_oficina
			   			AND (o.id_oficina = in_id_oficina OR in_id_rol = 1))
				JOIN cuenta_bancaria cb
					ON (n.id_cuenta_bancaria = cb.id_cuenta_bancaria)
				JOIN billetera b
					ON (cb.id_billetera = b.id_billetera)
				LEFT JOIN operacion_carga opc
					ON (nc.id_operacion_carga = opc.id_operacion_carga
					   AND cb.id_cuenta_bancaria = opc.id_cuenta_bancaria) -- revisar!!
				LEFT JOIN operacion op
					ON (op.id_operacion = opc.id_operacion
					   AND op.id_accion = 1
					   AND op.id_estado = 2
					   AND op.marca_baja = false)
				LEFT JOIN cliente cl
					ON (op.id_cliente = cl.id_cliente)
				LEFT JOIN agente ag
					ON (cl.id_agente = ag.id_agente)
				LEFT JOIN plataforma pl
					ON (pl.id_plataforma = ag.id_plataforma)
	WHERE CASE 
			WHEN in_id_rol < 3 THEN true
			WHEN in_id_usuario = 0 THEN true
			WHEN COALESCE(cb.id_usuario_noti, 0) = 0 OR COALESCE(cb.id_usuario_noti, 0) = in_id_usuario THEN true
			ELSE false END
	ORDER BY TO_CHAR(nc.fecha_hora_procesado, 'YYYY/MM/DD HH24:MI:SS') DESC,
			TO_CHAR(n.fecha_hora, 'YYYY/MM/DD HH24:MI:SS') DESC;
END;
$$ LANGUAGE plpgsql;

--DROP VIEW v_Usuarios_Cuentas_Bancarias;
CREATE OR REPLACE VIEW v_Usuarios_Cuentas_Bancarias AS
SELECT	u.id_usuario,
		u.usuario,
		u.id_oficina,
		COALESCE(COUNT(cb.id_cuenta_bancaria))	AS cantidad_cuentas
FROM 	usuario u LEFT JOIN cuenta_bancaria cb
			ON (u.id_usuario = cb.id_usuario_noti)
WHERE 	u.marca_baja = false
		AND u.id_rol = 3
GROUP BY u.id_usuario,
		u.usuario,
		u.id_oficina;
--select * from v_Usuarios_Cuentas_Bancarias;

--DROP VIEW v_Agentes_Cuentas_Bancarias;
CREATE OR REPLACE VIEW v_Agentes_Cuentas_Bancarias AS
SELECT	ag.id_agente,
		ag.agente_usuario,
		ag.id_oficina,
		COALESCE(COUNT(cb.id_cuenta_bancaria))	AS cantidad_cuentas
FROM 	agente ag LEFT JOIN cuenta_bancaria cb
			ON (ag.id_agente = cb.id_agente)
WHERE 	ag.marca_baja = false
GROUP BY ag.id_agente,
		ag.agente_usuario,
		ag.id_oficina;
--select * from v_Agentes_Cuentas_Bancarias;

--DROP VIEW v_Clientes_Bloqueantes_IP;
CREATE OR REPLACE VIEW v_Clientes_Bloqueantes_IP AS	   
SELECT DISTINCT	csb.ip,
				clb.id_cliente			AS id_cliente_bloqueante, 
				clb.cliente_usuario		AS cliente_usuario_bloqueante, 
				clb.id_agente			AS id_agente_bloqueante, 
				agb.agente_usuario		AS agente_usuario_bloqueante, 
				ob.id_oficina			AS id_oficina_bloqueante,
				ob.oficina				AS oficina_bloqueante,
				COALESCE(cl.id_cliente, 0) 		AS id_cliente_afectado, 
				COALESCE(cl.cliente_usuario, '')	AS cliente_usuario_afectado, 
				COALESCE(cl.id_agente, 0)			AS id_agente_afectado, 
				COALESCE(ag.agente_usuario, '')		AS agente_usuario_afectado, 
				COALESCE(o.id_oficina, 0)			AS id_oficina_afectada,
				COALESCE(o.oficina, '')				AS oficina_afectada
FROM cliente clb JOIN cliente_sesion csb
		ON (csb.id_cliente = clb.id_cliente
		   	AND clb.bloqueado = true)
	JOIN agente agb ON (clb.id_agente = agb.id_agente)
	JOIN oficina ob ON (agb.id_oficina = ob.id_oficina)
	LEFT JOIN cliente_sesion cs ON (cs.ip = csb.ip)
	LEFT JOIN cliente cl ON (cs.id_cliente = cl.id_cliente
					   	AND cl.bloqueado = false)
	LEFT JOIN agente ag ON (cl.id_agente = ag.id_agente)
	LEFT JOIN oficina o ON (ag.id_oficina = o.id_oficina)
ORDER BY csb.ip, clb.cliente_usuario, agb.agente_usuario, ob.oficina;
--select * from v_Clientes_Bloqueantes_IP where ip like '%186.141.136.106%'

--DROP FUNCTION FUNCTION Clientes_Bloqueantes_IP();
CREATE OR REPLACE FUNCTION Clientes_Bloqueantes_IP()
RETURNS TABLE (	ip character varying(100),
				id_cliente_bloqueante integer,
			   	cliente_usuario_bloqueante character varying(200),
				id_agente_bloqueante integer,
			   	agente_usuario_bloqueante character varying(200),
				id_oficina_bloqueante integer,
				oficina_bloqueante character varying(100),
			   	id_cliente_afectado integer,
			   	cliente_usuario_afectado character varying(200),
				id_agente_afectado integer,
			   	agente_usuario_afectado character varying(200),
				id_oficina_afectada integer,
				oficina_afectada character varying(100)) AS $$
BEGIN
	RETURN QUERY
	SELECT DISTINCT	csb.ip,
					clb.id_cliente			AS id_cliente_bloqueante, 
					clb.cliente_usuario		AS cliente_usuario_bloqueante, 
					clb.id_agente			AS id_agente_bloqueante, 
					agb.agente_usuario		AS agente_usuario_bloqueante, 
					ob.id_oficina			AS id_oficina_bloqueante,
					ob.oficina				AS oficina_bloqueante,
					COALESCE(cl.id_cliente, 0) 		AS id_cliente_afectado, 
					COALESCE(cl.cliente_usuario, '')	AS cliente_usuario_afectado, 
					COALESCE(cl.id_agente, 0)			AS id_agente_afectado, 
					COALESCE(ag.agente_usuario, '')		AS agente_usuario_afectado, 
					COALESCE(o.id_oficina, 0)			AS id_oficina_afectada,
					COALESCE(o.oficina, '')				AS oficina_afectada
	FROM cliente clb JOIN cliente_sesion csb
			ON (csb.id_cliente = clb.id_cliente
				AND clb.bloqueado = true)
		JOIN agente agb ON (clb.id_agente = agb.id_agente)
		JOIN oficina ob ON (agb.id_oficina = ob.id_oficina)
		LEFT JOIN cliente_sesion cs ON (cs.ip = csb.ip)
		LEFT JOIN cliente cl ON (cs.id_cliente = cl.id_cliente
							AND cl.bloqueado = false)
		LEFT JOIN agente ag ON (cl.id_agente = ag.id_agente)
		LEFT JOIN oficina o ON (ag.id_oficina = o.id_oficina)
	UNION
	SELECT DISTINCT	csb.ip,
					clb.id_cliente			AS id_cliente_bloqueante, 
					clb.cliente_usuario		AS cliente_usuario_bloqueante, 
					clb.id_agente			AS id_agente_bloqueante, 
					agb.agente_usuario		AS agente_usuario_bloqueante, 
					ob.id_oficina			AS id_oficina_bloqueante,
					ob.oficina				AS oficina_bloqueante,
					COALESCE(cl.id_cliente, 0) 		AS id_cliente_afectado, 
					COALESCE(cl.cliente_usuario, '')	AS cliente_usuario_afectado, 
					COALESCE(cl.id_agente, 0)			AS id_agente_afectado, 
					COALESCE(ag.agente_usuario, '')		AS agente_usuario_afectado, 
					COALESCE(o.id_oficina, 0)			AS id_oficina_afectada,
					COALESCE(o.oficina, '')				AS oficina_afectada
	FROM cliente clb JOIN cliente_sesion csb
			ON (csb.id_cliente = clb.id_cliente
				AND clb.bloqueado = true)
		JOIN agente agb ON (clb.id_agente = agb.id_agente)
		JOIN oficina ob ON (agb.id_oficina = ob.id_oficina)
		LEFT JOIN cliente_sesion_hist cs ON (cs.ip = csb.ip)
		LEFT JOIN cliente cl ON (cs.id_cliente = cl.id_cliente
							AND cl.bloqueado = false)
		LEFT JOIN agente ag ON (cl.id_agente = ag.id_agente)
		LEFT JOIN oficina o ON (ag.id_oficina = o.id_oficina)
	UNION
	SELECT DISTINCT	csb.ip,
					clb.id_cliente			AS id_cliente_bloqueante, 
					clb.cliente_usuario		AS cliente_usuario_bloqueante, 
					clb.id_agente			AS id_agente_bloqueante, 
					agb.agente_usuario		AS agente_usuario_bloqueante, 
					ob.id_oficina			AS id_oficina_bloqueante,
					ob.oficina				AS oficina_bloqueante,
					COALESCE(cl.id_cliente, 0) 		AS id_cliente_afectado, 
					COALESCE(cl.cliente_usuario, '')	AS cliente_usuario_afectado, 
					COALESCE(cl.id_agente, 0)			AS id_agente_afectado, 
					COALESCE(ag.agente_usuario, '')		AS agente_usuario_afectado, 
					COALESCE(o.id_oficina, 0)			AS id_oficina_afectada,
					COALESCE(o.oficina, '')				AS oficina_afectada
	FROM cliente clb JOIN cliente_sesion_hist csb
			ON (csb.id_cliente = clb.id_cliente
				AND clb.bloqueado = true)
		JOIN agente agb ON (clb.id_agente = agb.id_agente)
		JOIN oficina ob ON (agb.id_oficina = ob.id_oficina)
		LEFT JOIN cliente_sesion cs ON (cs.ip = csb.ip)
		LEFT JOIN cliente cl ON (cs.id_cliente = cl.id_cliente
							AND cl.bloqueado = false)
		LEFT JOIN agente ag ON (cl.id_agente = ag.id_agente)
		LEFT JOIN oficina o ON (ag.id_oficina = o.id_oficina)
	UNION
	SELECT DISTINCT	csb.ip,
					clb.id_cliente			AS id_cliente_bloqueante, 
					clb.cliente_usuario		AS cliente_usuario_bloqueante, 
					clb.id_agente			AS id_agente_bloqueante, 
					agb.agente_usuario		AS agente_usuario_bloqueante, 
					ob.id_oficina			AS id_oficina_bloqueante,
					ob.oficina				AS oficina_bloqueante,
					COALESCE(cl.id_cliente, 0) 		AS id_cliente_afectado, 
					COALESCE(cl.cliente_usuario, '')	AS cliente_usuario_afectado, 
					COALESCE(cl.id_agente, 0)			AS id_agente_afectado, 
					COALESCE(ag.agente_usuario, '')		AS agente_usuario_afectado, 
					COALESCE(o.id_oficina, 0)			AS id_oficina_afectada,
					COALESCE(o.oficina, '')				AS oficina_afectada
	FROM cliente clb JOIN cliente_sesion_hist csb
			ON (csb.id_cliente = clb.id_cliente
				AND clb.bloqueado = true)
		JOIN agente agb ON (clb.id_agente = agb.id_agente)
		JOIN oficina ob ON (agb.id_oficina = ob.id_oficina)
		LEFT JOIN cliente_sesion_hist cs ON (cs.ip = csb.ip)
		LEFT JOIN cliente cl ON (cs.id_cliente = cl.id_cliente
							AND cl.bloqueado = false)
		LEFT JOIN agente ag ON (cl.id_agente = ag.id_agente)
		LEFT JOIN oficina o ON (ag.id_oficina = o.id_oficina)
	ORDER BY ip, cliente_usuario_bloqueante, agente_usuario_bloqueante, oficina_bloqueante;
END;
$$ LANGUAGE plpgsql;

SELECT ip, id_cliente_bloqueante, cliente_usuario_bloqueante, COUNT(*) AS Cantidad
FROM Clientes_Bloqueantes_IP()
GROUP BY ip, id_cliente_bloqueante, cliente_usuario_bloqueante
ORDER BY COUNT(*) DESC;

FROM Clientes_Bloqueantes_IP();

/**DESBLOQUEO MASIVO***/
update cliente set bloqueado = false
where id_cliente in (select id_cliente_bloqueante
					from v_Clientes_Bloqueantes_IP
					group by id_cliente_bloqueante
					having max(id_cliente_afectado) > 0)
/******************/

select 	TO_CHAR(fecha_hora, 'YYYY-MM-DD HH24:MI') AS fecha_hora,
    	COUNT(*) AS total_eventos
from notificacion
where fecha_hora >= '2024-05-25 00:00:00'
group by TO_CHAR(fecha_hora, 'YYYY-MM-DD HH24:MI')
order by 1 desc;

--DROP VIEW rpt_Operaciones_Diarias_Oficinas_Agentes;
CREATE OR REPLACE VIEW rpt_Operaciones_Diarias_Oficinas_Agentes AS
SELECT 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') AS fecha,
		id_oficina,
		oficina,
		agente_usuario, 
		id_plataforma, 
		plataforma,
		sum(carga_importe)	as cargas,
		sum(carga_bono)		as bonos,
		sum(es_carga) as cant_cargas,
		sum(carga_automatica) as cant_automaticas,
		sum(retiro_importe)	as retiros,
		sum(es_retiro) as cant_retiros
FROM v_Clientes_Operaciones
WHERE id_estado = 2
AND id_accion in (1, 2, 5, 6, 9)
GROUP BY TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD'),
		id_oficina,
		oficina,
		agente_usuario, 
		id_plataforma, 
		plataforma;
/*select * from rpt_Operaciones_Diarias_Oficinas_Agentes
ORDER BY fecha DESC,
		oficina,
		agente_usuario, 
		plataforma;*/

--DROP VIEW rpt_Operaciones_Diarias_Oficinas;
CREATE OR REPLACE VIEW rpt_Operaciones_Diarias_Oficinas AS
SELECT 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') AS fecha,
		id_oficina,
		oficina,
		sum(carga_importe)	as cargas,
		sum(carga_bono)		as bonos,
		sum(es_carga) as cant_cargas,
		sum(carga_automatica) as cant_automaticas,
		sum(retiro_importe)	as retiros,
		sum(es_retiro) as cant_retiros
FROM v_Clientes_Operaciones
WHERE id_estado = 2
AND id_accion in (1, 2, 5, 6, 9)
GROUP BY TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD'),
		id_oficina,
		oficina;
/*select * from rpt_Operaciones_Diarias_Oficinas
ORDER BY fecha DESC,
		oficina,
		agente_usuario, 
		plataforma;*/

--DROP VIEW rpt_Operaciones_Diarias;
CREATE OR REPLACE VIEW rpt_Operaciones_Diarias AS
SELECT 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') AS fecha,
		id_plataforma, 
		plataforma, 
		sum(carga_importe)	as cargas,
		sum(carga_bono)		as bonos,
		sum(es_carga) as cant_cargas,
		sum(carga_automatica) as cant_automaticas,
		sum(retiro_importe)	as retiros,
		sum(es_retiro) as cant_retiros
FROM v_Clientes_Operaciones
WHERE id_estado = 2
AND id_accion in (1, 2, 5, 6, 9)
GROUP BY TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD'), id_plataforma, plataforma;
-- select * from rpt_Operaciones_Diarias

--DROP VIEW rpt_Operaciones;
CREATE OR REPLACE VIEW rpt_Operaciones AS
SELECT 	ope.fecha,
		ope.id_oficina,
		ope.oficina,
		ope.agente_usuario,
		ope.id_plataforma, 
		ope.plataforma,
		ope.cargas,
		ope.bonos,
		CASE WHEN ope.cant_cargas > 0 THEN 
			ROUND(ope.cant_automaticas::numeric / ope.cant_cargas::numeric * 100, 2) 
		ELSE 0 END AS porcentaje_automaticas,
		ope.cant_automaticas,
		ope.cant_cargas,
		ope.retiros,
		ope.cant_retiros
FROM (	SELECT fecha,
			0 as id_oficina,
			'Todas' as oficina,
			'Todos' as agente_usuario,
			id_plataforma, 
			plataforma,
			cargas,
			bonos,
			cant_cargas,
	  		cant_automaticas,
			/*CASE WHEN cant_cargas > 0 THEN 
				ROUND(cant_automaticas::numeric / cant_cargas::numeric * 100, 2) 
			ELSE 0 END AS porcentaje_automaticas,*/
			retiros,
			cant_retiros
		FROM rpt_Operaciones_Diarias
	  	UNION
		SELECT fecha,
			0 as id_oficina,
			'Todas'::character varying as oficina,
			'Todos'::text as agente_usuario,
			0 as id_plataforma, 
			'Ambas'::character varying as plataforma,
			SUM(cargas)::numeric as cargas,
			SUM(bonos)::numeric as bonos,
			SUM(cant_cargas)::bigint as cant_cargas,
	  		SUM(cant_automaticas)::bigint as cant_automaticas,
			/*CASE WHEN SUM(cant_cargas) > 0 THEN 
				ROUND(SUM(cant_automaticas)::numeric / SUM(cant_cargas)::numeric * 100, 2) 
			ELSE 0 END AS porcentaje_automaticas,*/
			SUM(retiros)::numeric as retiros,
			SUM(cant_retiros)::bigint as cant_retiros
		FROM rpt_Operaciones_Diarias GROUP BY fecha
		UNION
		SELECT fecha,
			id_oficina,
			oficina,
			'Todos' as agente_usuario,
			0 as id_plataforma, 
			'Ambas' as plataforma,
			cargas,
			bonos,
			cant_cargas,
	  		cant_automaticas,
			/*CASE WHEN cant_cargas > 0 THEN 
				ROUND(cant_automaticas::numeric / cant_cargas::numeric * 100, 2) 
			ELSE 0 END AS porcentaje_automaticas,*/
			retiros,
			cant_retiros
		FROM rpt_Operaciones_Diarias_Oficinas
		UNION
		SELECT fecha,
			id_oficina,
			oficina,
			agente_usuario,
			id_plataforma, 
			plataforma,
			cargas,
			bonos,
			cant_cargas,
	  		cant_automaticas,
			/*CASE WHEN cant_cargas > 0 THEN 
				ROUND(cant_automaticas::numeric / cant_cargas::numeric * 100, 2) 
			ELSE 0 END AS porcentaje_automaticas,*/
			retiros,
			cant_retiros
		FROM rpt_Operaciones_Diarias_Oficinas_Agentes) ope;
/*select * from rpt_Operaciones
where oficina = 'Pina'
order by fecha desc, id_oficina, agente_usuario, plataforma*/

--DROP TABLE rpt_operaciones_hist;
CREATE TABLE IF NOT EXISTS rpt_operaciones_hist
(
	id_registro serial NOT NULL,
    fecha character varying(20) NOT NULL,
    id_oficina integer NOT NULL,
    oficina character varying(200) NOT NULL,
	agente_usuario character varying(200) NOT NULL,
    id_plataforma integer NOT NULL,
	plataforma character varying(200) NOT NULL,
    cargas NUMERIC NOT NULL,
    bonos NUMERIC NOT NULL,
    porcentaje_automaticas NUMERIC NOT NULL,
    cant_automaticas BIGINT NOT NULL,
    cant_cargas BIGINT NOT NULL,
    retiros NUMERIC NOT NULL,
    cant_retiros BIGINT NOT NULL,
    PRIMARY KEY (id_registro)
);
CREATE INDEX rpt_operaciones_hist_fecha ON rpt_operaciones_hist (fecha);
CREATE INDEX rpt_operaciones_hist_id_oficina ON rpt_operaciones_hist (id_oficina);
CREATE INDEX rpt_operaciones_hist_id_plataforma ON rpt_operaciones_hist (id_plataforma);

--DROP TABLE rpt_plataforma_club;
CREATE TABLE IF NOT EXISTS rpt_plataforma_club
(
    id_registro serial NOT NULL,
    id_plataforma integer NOT NULL,
    fecha character varying(20) NOT NULL,
    agente character varying(50) NOT NULL,
    total_cantidad NUMERIC NOT NULL,
    total_monto NUMERIC NOT NULL,
    cargas_cantidad NUMERIC NOT NULL,
    cargas_monto NUMERIC NOT NULL,
    retiros_cantidad NUMERIC NOT NULL,
    retiros_monto NUMERIC NOT NULL,
    marca_baja boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_registro)
);
CREATE INDEX rpt_plataforma_club_fecha ON rpt_plataforma_club (fecha);
CREATE INDEX rpt_plataforma_club_id_plataforma ON rpt_plataforma_club (id_plataforma);

--DROP VIEW rpt_Operaciones_Plataforma;
CREATE OR REPLACE VIEW rpt_Operaciones_Plataforma AS
SELECT 	pl.fecha::varchar(20) 					AS fecha,
		COALESCE(la.id_oficina, 0) 				AS id_oficina,
		COALESCE(la.oficina, '')				AS oficina,
		pl.agente 								AS agente_usuario,
		pla.plataforma,
		COALESCE(la.cargas, 0) 					AS cargas,
		COALESCE(la.bonos, 0) 					AS bonos,
		COALESCE(la.porcentaje_automaticas, 0) 	AS porcentaje_automaticas,
		COALESCE(la.cant_automaticas, 0) 		AS cant_automaticas,
		COALESCE(la.cant_cargas, 0) 			AS cant_cargas,
		COALESCE(la.retiros, 0) 				AS retiros,
		COALESCE(la.cant_retiros, 0) 			AS cant_retiros,
		pl.cargas_monto							AS pl_cargas,
		pl.cargas_cantidad						AS pl_cant_cargas,
		pl.retiros_monto						AS pl_retiros,
		pl.retiros_cantidad						AS pl_cant_retiros
FROM rpt_plataforma_club pl JOIN plataforma pla
		ON (pl.id_plataforma = pla.id_plataforma)
	LEFT JOIN rpt_operaciones_hist la
	ON (pl.fecha = la.fecha
		AND pl.id_plataforma = la.id_plataforma
	   	AND pl.agente = la.agente_usuario)
WHERE pl.fecha::timestamp > NOW() - INTERVAL '14 DAYS'
UNION
SELECT 	fecha::varchar(20) as fecha,
		id_oficina,
		oficina,
		agente_usuario,
		plataforma,
		cargas,
		bonos,
		porcentaje_automaticas,
		cant_automaticas,
		cant_cargas,
		retiros,
		cant_retiros,
		0 AS pl_cargas,
		0 AS pl_cant_cargas,
		0 AS pl_retiros,
		0 AS pl_cant_retiros
FROM rpt_operaciones_hist
WHERE fecha::varchar(20) NOT IN (SELECT DISTINCT fecha FROM rpt_plataforma_club)
AND fecha::timestamp > NOW() - INTERVAL '14 DAYS'
UNION
SELECT 	fecha::varchar(20) as fecha,
		id_oficina,
		oficina,
		agente_usuario,
		plataforma,
		cargas,
		bonos,
		porcentaje_automaticas,
		cant_automaticas,
		cant_cargas,
		retiros,
		cant_retiros,
		0 AS pl_cargas,
		0 AS pl_cant_cargas,
		0 AS pl_retiros,
		0 AS pl_cant_retiros
FROM rpt_operaciones_hist
WHERE plataforma = 'Ambas'
AND fecha::timestamp > NOW() - INTERVAL '14 DAYS';
--select * from rpt_Operaciones_Plataforma
--select * from rpt_plataforma_club order by 1 desc
--select * from rpt_Operaciones_Plataforma order by cant_cargas desc

--DROP FUNCTION Insertar_Rpt_Plataforma_Club(in_id_usuario INTEGER, usr VARCHAR(50), pass VARCHAR(200), rol INTEGER, ofi INTEGER);
CREATE OR REPLACE FUNCTION Insertar_Rpt_Plataforma_Club(in_id_plataforma integer,
														in_fecha varchar(20),
														in_agente varchar(50),
														in_total_cantidad numeric,
														in_total_monto numeric,
														in_cargas_cantidad numeric,
														in_cargas_monto numeric,
														in_retiros_cantidad numeric,
														in_retiros_monto numeric) 
RETURNS VOID AS $$
BEGIN
	INSERT INTO rpt_plataforma_club (id_plataforma,
								fecha,
								agente,
								total_cantidad,
								total_monto,
								cargas_cantidad,
								cargas_monto,
								retiros_cantidad,
								retiros_monto,
								marca_baja)
	VALUES (in_id_plataforma,
			in_fecha,
			in_agente,
			in_total_cantidad,
			in_total_monto,
			in_cargas_cantidad,
			in_cargas_monto,
			in_retiros_cantidad,
			in_retiros_monto,
			false);
END;
$$ LANGUAGE plpgsql;

/*DROP FUNCTION Insertar_Rpt_Plataforma_Club_Totales(in_id_plataforma integer,
														in_fecha varchar(20));*/
CREATE OR REPLACE FUNCTION Insertar_Rpt_Plataforma_Club_Totales(in_id_plataforma integer,
														in_fecha varchar(20)) 
RETURNS VOID AS $$
BEGIN
	INSERT INTO rpt_plataforma_club (id_plataforma,
								fecha,
								agente,
								total_cantidad,
								total_monto,
								cargas_cantidad,
								cargas_monto,
								retiros_cantidad,
								retiros_monto,
								marca_baja)
	SELECT 	in_id_plataforma,
			in_fecha,
			'Todos',
			SUM(total_cantidad),
			SUM(total_monto),
			SUM(cargas_cantidad),
			SUM(cargas_monto),
			SUM(retiros_cantidad),
			SUM(retiros_monto),
			false
	FROM rpt_plataforma_club
	WHERE 	fecha = in_fecha
	AND id_plataforma = in_id_plataforma;		
END;
$$ LANGUAGE plpgsql;
--select Insertar_Rpt_Plataforma_Club_Totales(1, '2024-05-30')
--select Insertar_Rpt_Plataforma_Club_Totales(2, '2024-05-30')

--DROP FUNCTION Insertar_Rpt_Plataforma_Club(in_id_usuario INTEGER, usr VARCHAR(50), pass VARCHAR(200), rol INTEGER, ofi INTEGER);
CREATE OR REPLACE FUNCTION Insertar_Rpt_Operaciones(in_fecha varchar(20)) 
RETURNS VOID AS $$
BEGIN
	DELETE FROM rpt_operaciones_hist WHERE fecha = in_fecha;

	INSERT INTO rpt_operaciones_hist (fecha,
									id_oficina,
									oficina,
									agente_usuario,
									id_plataforma,
									plataforma,
									cargas,
									bonos,
									porcentaje_automaticas,
									cant_automaticas,
									cant_cargas,
									retiros,
									cant_retiros)
	SELECT	fecha,
			id_oficina,
			oficina,
			agente_usuario,
			id_plataforma,
			plataforma,
			cargas,
			bonos,
			porcentaje_automaticas,
			cant_automaticas,
			cant_cargas,
			retiros,
			cant_retiros
	FROM rpt_Operaciones
	WHERE fecha = in_fecha;
	
	ANALYZE rpt_operaciones_hist;
END;
$$ LANGUAGE plpgsql;
-- select Insertar_Rpt_Operaciones('2024-12-26')

--DROP FUNCTION rpt_Clientes_Operaciones_Completo_Cliente(in_id_cliente integer);
CREATE OR REPLACE FUNCTION rpt_Clientes_Operaciones_Completo_Cliente(in_id_cliente integer)
RETURNS TABLE (	fecha character varying(10),
				id_cliente integer,
				cliente_usuario character varying(200),
				id_cliente_ext bigint,
				id_cliente_db integer,
				id_agente integer,
				agente_usuario character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				marca_baja boolean,
				oficina character varying(100),
				id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				id_accion integer,
				es_carga integer,
				es_retiro integer,
				accion character varying(200),
				usuario character varying(50),
				cliente_confianza numeric,
				fecha_hora_operacion character varying(80),
			   	fecha_hora_proceso character varying(80),
				retiro_importe numeric,
				retiro_cbu character varying(200),
				retiro_titular character varying(200),
				retiro_observaciones character varying(200),
				carga_importe numeric,
				carga_titular character varying(200),
				carga_id_cuenta_bancaria integer,
				carga_bono numeric,
				carga_observaciones character varying(200),
				carga_cuenta_bancaria character varying(200),
				carga_cuenta_bancaria_nombre character varying(200),
				carga_cuenta_bancaria_alias character varying(200),
				carga_cuenta_bancaria_cbu character varying(200),
				id_billetera integer,
				id_usuario_noti integer,
				id_notificacion integer,
				id_origen_notificacion integer,
				carga_automatica integer) AS $$
BEGIN
	
	RETURN QUERY
	SELECT	TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD')::character varying(10) as fecha,
			od.id_cliente,
			od.cliente_usuario,
			od.id_cliente_ext,
			od.id_cliente_db,
			od.id_agente,
			od.agente_usuario,
			od.id_plataforma,
			od.plataforma,
			od.id_oficina,
			od.marca_baja,
			od.oficina,
			od.id_operacion,
			od.codigo_operacion,
			od.id_estado,
			od.estado,
			od.id_accion,
			od.es_carga,
			od.es_retiro,
			od.accion,
			od.usuario,
			od.cliente_confianza,
			TO_CHAR(od.fecha_hora_operacion, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_operacion,
			TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_proceso,
			od.retiro_importe,
			od.retiro_cbu,
			od.retiro_titular,
			od.retiro_observaciones,
			od.carga_importe,
			od.carga_titular,
			od.carga_id_cuenta_bancaria,
			od.carga_bono,
			od.carga_observaciones,
			od.carga_cuenta_bancaria,
			od.carga_cuenta_bancaria_nombre,
			od.carga_cuenta_bancaria_alias,
			od.carga_cuenta_bancaria_cbu,
			od.id_billetera,
			od.id_usuario_noti,
			od.id_notificacion,
			od.id_origen_notificacion,
			od.carga_automatica
	FROM clientes_operaciones_diario od
	WHERE od.id_cliente = in_id_cliente OR in_id_cliente = 0
	UNION
	SELECT	TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD')::character varying(10) as fecha,
			oh.id_cliente,
			oh.cliente_usuario,
			oh.id_cliente_ext,
			oh.id_cliente_db,
			oh.id_agente,
			oh.agente_usuario,
			oh.id_plataforma,
			oh.plataforma,
			oh.id_oficina,
			oh.marca_baja,
			oh.oficina,
			oh.id_operacion,
			oh.codigo_operacion,
			oh.id_estado,
			oh.estado,
			oh.id_accion,
			oh.es_carga,
			oh.es_retiro,
			oh.accion,
			oh.usuario,
			oh.cliente_confianza,
			TO_CHAR(oh.fecha_hora_operacion, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_operacion,
			TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_proceso,
			oh.retiro_importe,
			oh.retiro_cbu,
			oh.retiro_titular,
			oh.retiro_observaciones,
			oh.carga_importe,
			oh.carga_titular,
			oh.carga_id_cuenta_bancaria,
			oh.carga_bono,
			oh.carga_observaciones,
			oh.carga_cuenta_bancaria,
			oh.carga_cuenta_bancaria_nombre,
			oh.carga_cuenta_bancaria_alias,
			oh.carga_cuenta_bancaria_cbu,
			oh.id_billetera,
			oh.id_usuario_noti,
			oh.id_notificacion,
			oh.id_origen_notificacion,
			oh.carga_automatica
	FROM clientes_operaciones_hist oh
	WHERE oh.id_cliente = in_id_cliente OR in_id_cliente = 0
	ORDER BY fecha_hora_proceso DESC;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM rpt_Clientes_Operaciones_Completo_Cliente(54)

--DROP FUNCTION rpt_Clientes_Operaciones_Completo(in_fecha character varying(10), in_agente character varying(200), in_oficina character varying(100));
CREATE OR REPLACE FUNCTION rpt_Clientes_Operaciones_Completo(in_fecha character varying(10), in_agente character varying(200), in_oficina character varying(100))
RETURNS TABLE (	fecha character varying(10),
				id_cliente integer,
				cliente_usuario character varying(200),
				id_cliente_ext bigint,
				id_cliente_db integer,
				id_agente integer,
				agente_usuario character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				marca_baja boolean,
				oficina character varying(100),
				id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				id_accion integer,
				es_carga integer,
				es_retiro integer,
				accion character varying(200),
				usuario character varying(50),
				cliente_confianza numeric,
				fecha_hora_operacion character varying(80),
			   	fecha_hora_proceso character varying(80),
				retiro_importe numeric,
				retiro_cbu character varying(200),
				retiro_titular character varying(200),
				retiro_observaciones character varying(200),
				carga_importe numeric,
				carga_titular character varying(200),
				carga_id_cuenta_bancaria integer,
				carga_bono numeric,
				carga_observaciones character varying(200),
				carga_cuenta_bancaria character varying(200),
				carga_cuenta_bancaria_nombre character varying(200),
				carga_cuenta_bancaria_alias character varying(200),
				carga_cuenta_bancaria_cbu character varying(200),
				id_billetera integer,
				id_usuario_noti integer,
				id_notificacion integer,
				id_origen_notificacion integer,
				carga_automatica integer) AS $$
BEGIN
	
	RETURN QUERY
	SELECT	TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD')::character varying(10) as fecha,
			od.id_cliente,
			od.cliente_usuario,
			od.id_cliente_ext,
			od.id_cliente_db,
			od.id_agente,
			od.agente_usuario,
			od.id_plataforma,
			od.plataforma,
			od.id_oficina,
			od.marca_baja,
			od.oficina,
			od.id_operacion,
			od.codigo_operacion,
			od.id_estado,
			od.estado,
			od.id_accion,
			od.es_carga,
			od.es_retiro,
			od.accion,
			od.usuario,
			od.cliente_confianza,
			TO_CHAR(od.fecha_hora_operacion, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_operacion,
			TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_proceso,
			od.retiro_importe,
			od.retiro_cbu,
			od.retiro_titular,
			od.retiro_observaciones,
			od.carga_importe,
			od.carga_titular,
			od.carga_id_cuenta_bancaria,
			od.carga_bono,
			od.carga_observaciones,
			od.carga_cuenta_bancaria,
			od.carga_cuenta_bancaria_nombre,
			od.carga_cuenta_bancaria_alias,
			od.carga_cuenta_bancaria_cbu,
			od.id_billetera,
			od.id_usuario_noti,
			od.id_notificacion,
			od.id_origen_notificacion,
			od.carga_automatica
	FROM clientes_operaciones_diario od
	WHERE (TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD') = in_fecha OR in_fecha = 'Todas')
		AND (od.agente_usuario = in_agente OR in_agente = 'Todos')
		AND (od.oficina = in_oficina OR in_oficina = 'Todas')
	UNION
	SELECT	TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD')::character varying(10) as fecha,
			oh.id_cliente,
			oh.cliente_usuario,
			oh.id_cliente_ext,
			oh.id_cliente_db,
			oh.id_agente,
			oh.agente_usuario,
			oh.id_plataforma,
			oh.plataforma,
			oh.id_oficina,
			oh.marca_baja,
			oh.oficina,
			oh.id_operacion,
			oh.codigo_operacion,
			oh.id_estado,
			oh.estado,
			oh.id_accion,
			oh.es_carga,
			oh.es_retiro,
			oh.accion,
			oh.usuario,
			oh.cliente_confianza,
			TO_CHAR(oh.fecha_hora_operacion, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_operacion,
			TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_proceso,
			oh.retiro_importe,
			oh.retiro_cbu,
			oh.retiro_titular,
			oh.retiro_observaciones,
			oh.carga_importe,
			oh.carga_titular,
			oh.carga_id_cuenta_bancaria,
			oh.carga_bono,
			oh.carga_observaciones,
			oh.carga_cuenta_bancaria,
			oh.carga_cuenta_bancaria_nombre,
			oh.carga_cuenta_bancaria_alias,
			oh.carga_cuenta_bancaria_cbu,
			oh.id_billetera,
			oh.id_usuario_noti,
			oh.id_notificacion,
			oh.id_origen_notificacion,
			oh.carga_automatica
	FROM clientes_operaciones_hist oh
	WHERE (TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD') = in_fecha OR in_fecha = 'Todas')
		AND (oh.agente_usuario = in_agente OR in_agente = 'Todos')
		AND (oh.oficina = in_oficina OR in_oficina = 'Todas')
	ORDER BY fecha_hora_proceso DESC;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM rpt_Clientes_Operaciones_Completo('2024-11-18', 'Todos','Pina')

--DROP FUNCTION rpt_Operaciones_Operadores(in_fecha character varying(10), in_operador character varying(200), in_oficina character varying(100), in_id_oficina integer);
CREATE OR REPLACE FUNCTION rpt_Operaciones_Operadores(in_fecha character varying(10), in_operador character varying(200), in_oficina character varying(100), in_id_oficina integer)
RETURNS TABLE (	fecha character varying(10),
				id_cliente integer,
				cliente_usuario character varying(200),
				id_cliente_ext bigint,
				id_cliente_db integer,
				id_agente integer,
				agente_usuario character varying(200),
				id_plataforma integer,
				plataforma character varying(200),
				id_oficina integer,
				marca_baja boolean,
				oficina character varying(100),
				id_operacion integer,
				codigo_operacion integer,
				id_estado integer,
				estado character varying(200),
				id_accion integer,
				es_carga integer,
				es_retiro integer,
				accion character varying(200),
				usuario character varying(50),
				cliente_confianza numeric,
				fecha_hora_operacion character varying(80),
			   	fecha_hora_proceso character varying(80),
				retiro_importe numeric,
				retiro_cbu character varying(200),
				retiro_titular character varying(200),
				retiro_observaciones character varying(200),
				carga_importe numeric,
				carga_titular character varying(200),
				carga_id_cuenta_bancaria integer,
				carga_bono numeric,
				carga_observaciones character varying(200),
				carga_cuenta_bancaria character varying(200),
				carga_cuenta_bancaria_nombre character varying(200),
				carga_cuenta_bancaria_alias character varying(200),
				carga_cuenta_bancaria_cbu character varying(200),
				id_billetera integer,
				id_usuario_noti integer,
				id_notificacion integer,
				id_origen_notificacion integer,
				carga_automatica integer) AS $$
BEGIN
	
	RETURN QUERY
	SELECT	TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD')::character varying(10) as fecha,
			od.id_cliente,
			od.cliente_usuario,
			od.id_cliente_ext,
			od.id_cliente_db,
			od.id_agente,
			od.agente_usuario,
			od.id_plataforma,
			od.plataforma,
			od.id_oficina,
			od.marca_baja,
			od.oficina,
			od.id_operacion,
			od.codigo_operacion,
			od.id_estado,
			od.estado,
			od.id_accion,
			od.es_carga,
			od.es_retiro,
			od.accion,
			od.usuario,
			od.cliente_confianza,
			TO_CHAR(od.fecha_hora_operacion, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_operacion,
			TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_proceso,
			od.retiro_importe,
			od.retiro_cbu,
			od.retiro_titular,
			od.retiro_observaciones,
			od.carga_importe,
			od.carga_titular,
			od.carga_id_cuenta_bancaria,
			od.carga_bono,
			od.carga_observaciones,
			od.carga_cuenta_bancaria,
			od.carga_cuenta_bancaria_nombre,
			od.carga_cuenta_bancaria_alias,
			od.carga_cuenta_bancaria_cbu,
			od.id_billetera,
			od.id_usuario_noti,
			od.id_notificacion,
			od.id_origen_notificacion,
			od.carga_automatica
	FROM clientes_operaciones_diario od
	WHERE (TO_CHAR(od.fecha_hora_proceso, 'YYYY-MM-DD') = in_fecha OR in_fecha = 'Todas')
		AND (od.usuario = in_operador OR in_operador = 'Todos')
		AND (od.oficina = in_oficina OR in_oficina = 'Todas')
		AND (od.id_oficina = in_id_oficina OR in_id_oficina = 0)
		AND od.usuario NOT IN ('admin')
		AND od.fecha_hora_proceso::timestamp > NOW() - INTERVAL '7 DAYS'
	UNION
	SELECT	TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD')::character varying(10) as fecha,
			oh.id_cliente,
			oh.cliente_usuario,
			oh.id_cliente_ext,
			oh.id_cliente_db,
			oh.id_agente,
			oh.agente_usuario,
			oh.id_plataforma,
			oh.plataforma,
			oh.id_oficina,
			oh.marca_baja,
			oh.oficina,
			oh.id_operacion,
			oh.codigo_operacion,
			oh.id_estado,
			oh.estado,
			oh.id_accion,
			oh.es_carga,
			oh.es_retiro,
			oh.accion,
			oh.usuario,
			oh.cliente_confianza,
			TO_CHAR(oh.fecha_hora_operacion, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_operacion,
			TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD HH24:MI:SS')::character varying(80) AS fecha_hora_proceso,
			oh.retiro_importe,
			oh.retiro_cbu,
			oh.retiro_titular,
			oh.retiro_observaciones,
			oh.carga_importe,
			oh.carga_titular,
			oh.carga_id_cuenta_bancaria,
			oh.carga_bono,
			oh.carga_observaciones,
			oh.carga_cuenta_bancaria,
			oh.carga_cuenta_bancaria_nombre,
			oh.carga_cuenta_bancaria_alias,
			oh.carga_cuenta_bancaria_cbu,
			oh.id_billetera,
			oh.id_usuario_noti,
			oh.id_notificacion,
			oh.id_origen_notificacion,
			oh.carga_automatica
	FROM clientes_operaciones_hist oh
	WHERE (TO_CHAR(oh.fecha_hora_proceso, 'YYYY-MM-DD') = in_fecha OR in_fecha = 'Todas')
		AND (oh.usuario = in_operador OR in_operador = 'Todos')
		AND (oh.oficina = in_oficina OR in_oficina = 'Todas')
		AND (oh.id_oficina = in_id_oficina OR in_id_oficina = 0)
		AND oh.usuario NOT IN ('admin')
		AND oh.fecha_hora_proceso::timestamp > NOW() - INTERVAL '7 DAYS'
	ORDER BY fecha_hora_proceso DESC;
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM rpt_Operaciones_Operadores('2024-11-18', 'Todos','Pina')

INSERT INTO rpt_operaciones_hist (fecha,
									id_oficina,
									oficina,
									agente_usuario,
									id_plataforma,
									plataforma,
									cargas,
									bonos,
									porcentaje_automaticas,
									cant_automaticas,
									cant_cargas,
									retiros,
									cant_retiros)
SELECT	fecha,
		id_oficina,
		oficina,
		'Todos' as agente_usuario,
		0 as id_plataforma,
		'Ambas' as plataforma,
		SUM(carga_importe) as cargas,
		SUM(carga_bono) as bonos,
		CASE WHEN SUM(es_carga) > 0 THEN 
			ROUND(SUM(carga_automatica)::numeric / SUM(es_carga)::numeric * 100, 2) 
		ELSE 0 END AS porcentaje_automaticas,
		SUM(carga_automatica) as cant_automaticas,
		SUM(es_carga) as cant_cargas,
		SUM(retiro_importe) as retiros,
		SUM(es_retiro) as cant_retiros
--FROM rpt_Operaciones
FROM rpt_Clientes_Operaciones_Completo('Todas', 'Todos','Todas')
GROUP BY fecha, id_oficina, oficina;

select fecha, cant_cargas, porcentaje_automaticas, cant_retiros
from rpt_operaciones_hist
where oficina = 'Pina' 
and agente_usuario = 'Todos'
order by cant_cargas desc

--DROP TABLE rpt_peticiones_nginx;
CREATE TABLE IF NOT EXISTS rpt_peticiones_nginx
(
    id_registro_nginx SERIAL NOT NULL,
    id_servidor INTEGER NOT NULL,
    fecha_hora TIMESTAMP NOT NULL DEFAULT NOW(),
	peticiones INTEGER NOT NULL DEFAULT 0,
    PRIMARY KEY (id_registro_nginx)
);

--DROP VIEW rpt_Peticiones_Nginx_Ult24hs;
CREATE OR REPLACE VIEW rpt_Peticiones_Nginx_Ult24hs AS
SELECT 	id_servidor,
		servidor,
		fecha_hora,
		peticiones - LAG(peticiones) OVER (PARTITION BY id_servidor ORDER BY fecha_hora) AS peticiones
FROM (SELECT CASE WHEN id_servidor = 2 THEN 2 ELSE 1 END AS id_servidor,
	  		CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END AS servidor,
	  		fecha_hora,
	  		SUM(peticiones) AS peticiones
	  FROM rpt_peticiones_nginx GROUP BY CASE WHEN id_servidor = 2 THEN 2 ELSE 1 END, 
	  CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END,
	  fecha_hora) rpt 
WHERE fecha_hora >= NOW() - INTERVAL '24 hours'
ORDER BY id_servidor, fecha_hora desc;
--select * from rpt_Peticiones_Nginx_Ult24hs

--DROP VIEW rpt_Peticiones_Nginx_Ult24hs;
CREATE OR REPLACE VIEW rpt_Peticiones_Nginx_Ult24hs AS
SELECT 	id_servidor,
		servidor,
		fecha_hora,
		peticiones - LAG(peticiones) OVER (PARTITION BY id_servidor ORDER BY fecha_hora) AS peticiones
FROM (SELECT CASE WHEN id_servidor = 2 THEN 2 ELSE 1 END AS id_servidor,
	  		CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END AS servidor,
	  		fecha_hora,
	  		SUM(peticiones) AS peticiones
	  FROM rpt_peticiones_nginx GROUP BY CASE WHEN id_servidor = 2 THEN 2 ELSE 1 END, 
	  CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END,
	  fecha_hora) rpt 
WHERE fecha_hora >= NOW() - INTERVAL '24 hours'
ORDER BY id_servidor, fecha_hora desc;
--select * from rpt_Peticiones_Nginx_Ult24hs

--DROP VIEW rpt_Peticiones_Operador_Nginx_Ult24hs;
CREATE OR REPLACE VIEW rpt_Peticiones_Operador_Nginx_Ult24hs AS
SELECT 	id_servidor, 
	  	CASE WHEN id_servidor = 1 THEN 'Paneleslanding.com' ELSE
			CASE WHEN id_servidor = 3 THEN 'Paneleslanding.online' ELSE 
				CASE WHEN id_servidor = 4 THEN 'Paneleslanding.store' ELSE
					CASE WHEN id_servidor = 5 THEN 'Paneleslanding.com.ar' ELSE
						CASE WHEN id_servidor = 6 THEN 'Paneleslanding.site' ELSE 
							CASE WHEN id_servidor = 10 THEN 'Ofitester.website' ELSE
								CASE WHEN id_servidor = 11 THEN 'Paneleslanding.uno' ELSE 'No Encontrado' END
							END
						END
					END
				END
			END
	   	END AS servidor,
		fecha_hora,
		peticiones - LAG(peticiones) OVER (PARTITION BY id_servidor ORDER BY fecha_hora) AS peticiones
FROM (SELECT id_servidor,
	  		fecha_hora,
	  		SUM(peticiones) AS peticiones
	  FROM rpt_peticiones_nginx 
	  WHERE id_servidor in (1,3,4,5,6,10,11)
	  GROUP BY id_servidor, fecha_hora) rpt 
WHERE fecha_hora >= NOW() - INTERVAL '24 hours'
ORDER BY id_servidor desc, fecha_hora desc;
--SELECT DISTINCT id_servidor FROM rpt_Peticiones_Operador_Nginx_Ult24hs WHERE peticiones > 0;

--DROP VIEW rpt_Peticiones_Notificaciones_Nginx_Ult24hs;
CREATE OR REPLACE VIEW rpt_Peticiones_Notificaciones_Nginx_Ult24hs AS
SELECT 	id_servidor, 
	  	CASE WHEN id_servidor = 7 THEN 'recibott.com' ELSE
			CASE WHEN id_servidor = 9 THEN 'recibott.online' ELSE 
				CASE WHEN id_servidor = 8 THEN 'recibott.store' ELSE 'No Encontrado' END
			END
	   	END AS servidor,
		fecha_hora,
		peticiones - LAG(peticiones) OVER (PARTITION BY id_servidor ORDER BY fecha_hora) AS peticiones
FROM (SELECT id_servidor,
	  		fecha_hora,
	  		SUM(peticiones) AS peticiones
	  FROM rpt_peticiones_nginx 
	  WHERE id_servidor in (7,8,9)
	  GROUP BY id_servidor, fecha_hora) rpt 
WHERE fecha_hora >= NOW() - INTERVAL '24 hours'
ORDER BY id_servidor desc, fecha_hora desc;
--SELECT DISTINCT id_servidor FROM rpt_Peticiones_Notificaciones_Nginx_Ult24hs WHERE peticiones > 0;

CREATE OR REPLACE VIEW rpt_Peticiones_Nginx_Historico AS
SELECT 	id_servidor,
		servidor,
		TO_CHAR(fecha_hora, 'HH24') AS horario,
		AVG(peticiones)		as promedio,
		STDDEV(peticiones)	as desviacion
FROM (SELECT id_servidor,
			CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END AS servidor,
			fecha_hora,
			peticiones - LAG(peticiones) OVER (PARTITION BY id_servidor ORDER BY fecha_hora) AS peticiones
	FROM (SELECT CASE WHEN id_servidor = 2 THEN 2 ELSE 1 END AS id_servidor,
	  		CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END AS servidor,
	  		fecha_hora,
	  		SUM(peticiones) AS peticiones
	  FROM rpt_peticiones_nginx GROUP BY CASE WHEN id_servidor = 2 THEN 2 ELSE 1 END, 
	  CASE WHEN id_servidor = 2 THEN 'Landing' ELSE 'PanelesLanding' END,
	  fecha_hora) rpt 
	WHERE fecha_hora < NOW() - INTERVAL '24 hours'
	ORDER BY id_servidor, fecha_hora desc) n
WHERE peticiones > 0
GROUP BY id_servidor,
		servidor,
		TO_CHAR(fecha_hora, 'HH24')
ORDER BY id_servidor, TO_CHAR(fecha_hora, 'HH24') desc;
--SELECT * FROM rpt_Peticiones_Nginx_Ult24hs WHERE peticiones > 0 and id_servidor = 2;
--SELECT * FROM rpt_Peticiones_Nginx_Historico;
--SELECT * FROM rpt_peticiones_nginx where id_servidor = 2 ORDER BY 1 DESC LIMIT 100;

--DROP VIEW rpt_Chat_PorMinuto;
CREATE OR REPLACE VIEW rpt_Chat_PorMinuto AS
SELECT	o.id_oficina,
		o.oficina,
		CASE WHEN cc.enviado_cliente THEN 'Cliente' ELSE 'Operador' END AS origen,
		date_trunc('minute', cc.fecha_hora_creacion) AS fecha_hora,
		COUNT(*) AS cantidad
FROM	cliente_chat cc JOIN cliente c
			ON (cc.id_cliente = c.id_cliente)
		JOIN agente ag
			ON (c.id_agente = ag.id_agente)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina)
GROUP BY 	o.id_oficina, 
			o.oficina, 
			CASE WHEN cc.enviado_cliente THEN 'Cliente' ELSE 'Operador' END,
			date_trunc('minute', cc.fecha_hora_creacion);
--SELECT * FROM rpt_Chat_PorMinuo;

CREATE OR REPLACE VIEW rpt_Chat_Detalle AS
SELECT	o.id_oficina,
		o.oficina,
		CASE WHEN cc.enviado_cliente THEN 'Cliente' ELSE 'Operador' END AS origen,
		cc.fecha_hora_creacion,
		cc.mensaje
FROM	cliente_chat cc JOIN cliente c
			ON (cc.id_cliente = c.id_cliente)
		JOIN agente ag
			ON (c.id_agente = ag.id_agente)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina);
--select * from rpt_Chat_Detalle;

--DROP VIEW rpt_Chat_Ult24hs;
CREATE OR REPLACE VIEW rpt_Chat_Ult24hs AS
SELECT 	id_oficina,
		oficina,
		origen,
		fecha_hora,
		cantidad
FROM rpt_Chat_PorMinuto
WHERE fecha_hora >= NOW() - INTERVAL '24 hours'
UNION
SELECT 	0 			AS id_oficina,
		'Todas' 	AS oficina,
		origen,
		fecha_hora,
		SUM(cantidad) 	AS cantidad
FROM rpt_Chat_PorMinuto
WHERE fecha_hora >= NOW() - INTERVAL '24 hours'
GROUP BY origen, fecha_hora;

--DROP FUNCTION Reporte_Chat_Historico(in_fecha_desde TIMESTAMP) ;
CREATE OR REPLACE FUNCTION Reporte_Chat_Historico(in_fecha_desde TIMESTAMP) 
RETURNS TABLE (	id_oficina  integer,
				oficina		character varying(200),
				origen		varchar(20),
				horario		varchar(20),
				promedio	numeric,
				desviacion	numeric) AS $$
BEGIN
	RETURN QUERY SELECT	chat_hist.id_oficina,
						chat_hist.oficina,
						chat_hist.origen::varchar(20),
						TO_CHAR(chat_hist.fecha_hora, 'HH24')::varchar(20) AS horario,
						AVG(chat_hist.cantidad)		as promedio,
						STDDEV(chat_hist.cantidad)	as desviacion
	FROM (SELECT r1.id_oficina,
				r1.oficina,
				r1.origen,
				r1.fecha_hora,
				r1.cantidad
		FROM rpt_Chat_PorMinuto r1
		UNION
		SELECT 	0 			AS id_oficina,
				'Todas' 	AS oficina,
				r2.origen,
				r2.fecha_hora,
				SUM(r2.cantidad) 	AS cantidad
		FROM rpt_Chat_PorMinuto r2
		GROUP BY r2.origen, r2.fecha_hora) chat_hist
	GROUP BY chat_hist.id_oficina,
			chat_hist.oficina,
			chat_hist.origen,
			TO_CHAR(chat_hist.fecha_hora, 'HH24');
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM Reporte_Chat_Historico('2024-07-01') ORDER BY horario;
/*DROP VIEW rpt_Chat_Historico;
CREATE OR REPLACE VIEW rpt_Chat_Historico AS
SELECT	id_oficina,
		oficina,
		origen,
		TO_CHAR(fecha_hora, 'HH24') AS horario,
		AVG(cantidad)		as promedio,
		STDDEV(cantidad)	as desviacion
FROM (SELECT id_oficina,
			oficina,
			origen,
			fecha_hora,
			cantidad
	FROM rpt_Chat_PorMinuto
	UNION
	SELECT 	0 			AS id_oficina,
			'Todas' 	AS oficina,
			origen,
			fecha_hora,
			SUM(cantidad) 	AS cantidad
	FROM rpt_Chat_PorMinuto
	GROUP BY origen, fecha_hora) chat_hist
GROUP BY id_oficina,
		oficina,
		origen,
		TO_CHAR(fecha_hora, 'HH24');*/
--SELECT * FROM rpt_Chat_Historico WHERE id_oficina = 0 order by horario, origen;

-- Consulta concurrencia_registro_aplicadas
SELECT 	date_trunc('minute', fecha_hora) AS fecha_hora, 
		COUNT(*) AS aplicadas
FROM concurrencia_registro_aplicadas
WHERE id_proceso = 1 
AND resultado = 'ok'
--AND resultado = 'en_espera'
--AND resultado = 'error'
GROUP BY date_trunc('minute', fecha_hora)
ORDER BY 1 DESC;

select * from concurrencia_registro_aplicadas
order by 1 desc

select * from concurrencia_registro_aplicadas where id_notificacion_carga = 407298
select * from notificacion_carga where id_notificacion = 412202

DROP VIEW rpt_Registro_Sesiones_Sockets_Ult24hs
CREATE OR REPLACE VIEW rpt_Registro_Sesiones_Sockets_Ult24hs AS
SELECT 	s.id_oficina,
		o.oficina,
		date_trunc('minute', s.fecha_hora) AS fecha_hora, 
		MAX(s.conexiones) AS cantidad
FROM registro_sesiones_sockets s JOIN oficina o
	ON (s.id_oficina = o.id_oficina)
WHERE s.fecha_hora >= NOW() - INTERVAL '24 hours'
GROUP BY s.id_oficina,
		o.oficina,
		date_trunc('minute', s.fecha_hora)
ORDER BY s.id_oficina, date_trunc('minute', s.fecha_hora) DESC;

SELECT 	'Todas'	as oficina,
		fecha_hora,
		SUM(cantidad) AS cantidad
FROM rpt_Registro_Sesiones_Sockets_Ult24hs
GROUP BY fecha_hora
ORDER BY fecha_hora DESC;

select * from accion

select * from rpt_plataforma_club where agente like '%roby3vip%'

DROP VIEW rpt_Acciones_Diarias;
CREATE OR REPLACE VIEW rpt_Acciones_Diarias AS
SELECT 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') AS fecha,
		id_oficina,
		oficina,
		id_accion,
		accion,
		id_origen_notificacion,
		id_plataforma, 
		plataforma,
		count(*)	as cantidad
FROM v_Clientes_Operaciones_Hist
WHERE id_estado = 2
GROUP BY 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD'), 
			id_oficina,
			oficina,
			id_accion,
			accion,
			id_origen_notificacion,
			id_plataforma, 
			plataforma
UNION
SELECT 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD') AS fecha,
		0 AS id_oficina,
		'Todas' AS oficina,
		id_accion,
		accion,
		id_origen_notificacion,
		id_plataforma, 
		plataforma,
		count(*)	as cantidad
FROM v_Clientes_Operaciones_Hist
WHERE id_estado = 2
GROUP BY 	TO_CHAR(fecha_hora_proceso, 'YYYY-MM-DD'),
			id_accion,
			accion,
			id_origen_notificacion,
			id_plataforma, 
			plataforma
ORDER BY 1, 3, 8, 4, 6;

SELECT TO_CHAR(fecha, 'YYYY-MM-DD hh'), *
SELECT * FROM rpt_Acciones_Diarias 
where fecha = '2024-08-08';

SELECT 	EXTRACT(DAY FROM fecha_hora_proceso) AS dia,
		EXTRACT(HOUR FROM fecha_hora_proceso) AS hora,
		COUNT(*) AS Q
FROM v_Clientes_Operaciones 
WHERE fecha_hora_operacion >= '2024-08-03'
AND EXTRACT(HOUR FROM fecha_hora_proceso) IN (0,22,23)
AND id_accion in (1,5) 
AND es_carga = 1 
AND id_estado = 2
GROUP BY EXTRACT(DAY FROM fecha_hora_proceso), 
EXTRACT(HOUR FROM fecha_hora_proceso)
ORDER BY 1 DESC, 2;


select 	id_oficina,
		oficina,
		fecha_hora_operacion,
		fecha_hora_proceso,
		EXTRACT(EPOCH FROM (fecha_hora_proceso - fecha_hora_operacion)) AS demora,
		carga_importe,
		carga_bono,
		retiro_importe
from v_Clientes_Operaciones 
where id_estado = 2
and id_accion = 2
and id_cliente = 4

select * from accion

select * from v_Notificaciones_Cargas where id_cuenta_bancaria = 29

select * from v_Console_Logs

SELECT * 
FROM registro_sesiones_sockets 
where fecha_hora >= '2024-05-27 12:00:00'
order by 1 desc

select *
from v_Clientes_Operaciones 
where marca_baja = false

select * from v_Cuentas_Bancarias
WHERE id_oficina > 2
order by 1

select * from v_Tokens

insert into registro_token (id_token, de_agente, activo, id_usuario, ingresos, registros, fecha_hora_creacion, id_usuario_ultima_modificacion, fecha_hora_ultima_modificacion)
select 	concat('c-', id_cliente::varchar, '-', substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)),
		false,
		true,
		id_cliente,
		0,
		0,
		now(),
		1,
		now()
from cliente
where id_cliente not in (select id_usuario 
						 from registro_token 
						 where activo = true and de_agente = false);
						 
SELECT substr(translate(encode(gen_random_bytes(40), 'base64'), '/+', 'ab'), 1, 40)
INTO aux_token;

select * from v_Tokens_Completo where id_oficina = 2

select 	TO_CHAR(fecha_hora, 'YYYY-MM-DD HH24:MI') AS fecha_hora,
    	COUNT(*) AS total_eventos
from notificacion
where fecha_hora >= '2024-06-10 14:00:00'
group by TO_CHAR(fecha_hora, 'YYYY-MM-DD HH24:MI')
order by 1 desc;

/*Reporte de Cuentas Bancarias*/
DROP TABLE IF EXISTS rpt_cuentas_bancarias;
CREATE TABLE IF NOT EXISTS rpt_cuentas_bancarias
(
    id_rpt_cuentas_bancarias serial NOT NULL,
	id_cuenta_bancaria integer NOT NULL,
    fecha_hora_saldo timestamp NOT NULL,
    saldo numeric NOT NULL,
    cargas_cantidad integer NOT NULL,
    PRIMARY KEY (id_rpt_cuentas_bancarias)
);
CREATE INDEX rpt_cuentas_bancarias_id_cuenta_bancaria ON rpt_cuentas_bancarias (id_cuenta_bancaria);

--DROP FUNCTION Reporte_Cuentas_Bancarias_Actualiza()
/*CREATE OR REPLACE FUNCTION Reporte_Cuentas_Bancarias_Actualiza()
RETURNS TABLE (	id_cuenta_bancaria integer,
				fecha_hora_saldo timestamp,
				saldo numeric,
				cargas_cantidad integer) AS $$
BEGIN	
	INSERT INTO rpt_cuentas_bancarias (id_cuenta_bancaria, fecha_hora_saldo, saldo, cargas_cantidad)
	SELECT 	oc.id_cuenta_bancaria, 
			Now() AS fecha_hora_saldo,
			SUM(oc.importe) + COALESCE(cbd.monto_restante,0) AS monto_cargas,
			COUNT(oc.importe) AS cantidad_cargas
	FROM 	operacion_carga oc JOIN operacion op
				ON (op.id_operacion = oc.id_operacion
						AND op.marca_baja = false
						AND op.id_accion IN (1,5)  --accion de carga
						AND op.id_estado = 2) --estado aceptado
			LEFT JOIN cuenta_bancaria_descarga cbd
				ON (oc.id_cuenta_bancaria = cbd.id_cuenta_bancaria
				   AND cbd.ultima_descarga = true)
	WHERE op.fecha_hora_ultima_modificacion >= COALESCE(cbd.fecha_hora_descarga, '1900-01-01 00:00:00')
	--AND oc.id_cuenta_bancaria IN (1,2,134)
	GROUP BY oc.id_cuenta_bancaria, 
		cbd.monto_restante;
	
	/*SELECT	v.id_cuenta_bancaria,
			Now() AS fecha_hora_saldo,
			v.monto_cargas,
			v.cantidad_cargas
	FROM v_Cuenta_Bancaria_Subtotales v;*/
	
	RETURN QUERY SELECT rpt.id_cuenta_bancaria,
				rpt.fecha_hora_saldo,
				rpt.saldo,
				rpt.cargas_cantidad
	FROM rpt_cuentas_bancarias rpt ORDER BY 1 DESC;
END;
$$ LANGUAGE plpgsql;*/
CREATE OR REPLACE FUNCTION Reporte_Cuentas_Bancarias_Actualiza()
RETURNS VOID AS $$
BEGIN	
	INSERT INTO rpt_cuentas_bancarias (id_cuenta_bancaria, fecha_hora_saldo, saldo, cargas_cantidad)
	SELECT 	oc.id_cuenta_bancaria, 
			Now() AS fecha_hora_saldo,
			SUM(oc.importe) + COALESCE(cbd.monto_restante,0) AS monto_cargas,
			COUNT(oc.importe) AS cantidad_cargas
	FROM 	v_operacion_carga_completa oc JOIN v_operacion_completa op
				ON (op.id_operacion = oc.id_operacion
						AND op.marca_baja = false
						AND op.id_accion IN (1,5)  --accion de carga
						AND op.id_estado = 2) --estado aceptado
			LEFT JOIN cuenta_bancaria_descarga cbd
				ON (oc.id_cuenta_bancaria = cbd.id_cuenta_bancaria
				   AND cbd.ultima_descarga = true)
	WHERE op.fecha_hora_ultima_modificacion >= COALESCE(cbd.fecha_hora_descarga, '1900-01-01 00:00:00')
	--AND oc.id_cuenta_bancaria IN (1,2,134)
	GROUP BY oc.id_cuenta_bancaria, 
		cbd.monto_restante;
	
	/*SELECT	v.id_cuenta_bancaria,
			Now() AS fecha_hora_saldo,
			v.monto_cargas,
			v.cantidad_cargas
	FROM v_Cuenta_Bancaria_Subtotales v;*/
END;
$$ LANGUAGE plpgsql;
--select Reporte_Cuentas_Bancarias_Actualiza();
--select * from rpt_cuentas_bancarias order by 1 desc

--DROP VIEW rpt_Cuentas_Bancarias_Completo;
CREATE OR REPLACE VIEW rpt_Cuentas_Bancarias_Completo AS
SELECT 	cb.id_oficina,
		o.oficina,
		cb.id_cuenta_bancaria,
		cb.nombre,
		cb.alias,
		cb.cbu,
		cb.id_billetera,
		b.billetera,
		cbd.cargas_monto		AS monto_descarga,
		cbd.monto_restante		AS monto_saldo,
		cbd.fecha_hora_descarga	AS fecha_hora_saldo
FROM 	cuenta_bancaria cb JOIN oficina o
			on (cb.id_oficina = o.id_oficina)
		JOIN billetera b
			ON (cb.id_billetera = b.id_billetera)
		JOIN cuenta_bancaria_descarga cbd
			on (cb.id_cuenta_bancaria = cbd.id_cuenta_bancaria)
UNION
SELECT 	cb.id_oficina,
		o.oficina,
		cb.id_cuenta_bancaria,
		cb.nombre,
		cb.alias,
		cb.cbu,
		cb.id_billetera,
		b.billetera,
		0						AS monto_descarga,
		rpt.saldo				AS monto_saldo,
		rpt.fecha_hora_saldo	AS fecha_hora_saldo
FROM 	cuenta_bancaria cb JOIN oficina o
			on (cb.id_oficina = o.id_oficina)
		JOIN billetera b
			ON (cb.id_billetera = b.id_billetera)
		JOIN rpt_cuentas_bancarias rpt
			on (cb.id_cuenta_bancaria = rpt.id_cuenta_bancaria)
UNION
SELECT 	cb.id_oficina,
		o.oficina,
		cb.id_cuenta_bancaria,
		cb.nombre,
		cb.alias,
		cb.cbu,
		cb.id_billetera,
		b.billetera,
		0						AS monto_descarga,
		v.monto_cargas			AS monto_saldo,
		Now()					AS fecha_hora_saldo
FROM 	cuenta_bancaria cb JOIN oficina o
			on (cb.id_oficina = o.id_oficina)
		JOIN billetera b
			ON (cb.id_billetera = b.id_billetera)
		JOIN v_Cuenta_Bancaria_Subtotales v
			on (cb.id_cuenta_bancaria = v.id_cuenta_bancaria);
			
SELECT * FROM rpt_Cuentas_Bancarias_Completo 
WHERE fecha_hora_saldo > NOW() - INTERVAL '3 DAYS'
ORDER BY id_oficina, id_cuenta_bancaria, fecha_hora_saldo DESC;

/*****************************/
/*Anulacion de notificaciones*/
SELECT n.*
FROM notificacion n JOIN notificacion_carga nc
	ON (n.id_notificacion = nc.id_notificacion
		AND n.fecha_hora < NOW() - INTERVAL '5 hour'
		AND n.anulada = false
	   	AND nc.id_operacion_carga IS NULL
	   	AND nc.marca_procesado = false)
ORDER BY n.fecha_hora

SELECT  COUNT(*)
FROM v_Notificaciones_Cargas
WHERE anulada = false 
/*AND ((fecha_hora < NOW() - INTERVAL '60 minutes' AND id_origen = 1)
	OR (fecha_hora < NOW() - INTERVAL '60 minutes' AND id_origen = 2))*/
AND id_cuenta_bancaria > 1 
AND id_operacion_carga IS NULL 
AND marca_procesado = false 
AND id_oficina = 15
ORDER BY fecha_hora 

DROP TABLE IF EXISTS rpt_clientes;
CREATE TABLE IF NOT EXISTS rpt_clientes
(
    id_cliente integer NOT NULL DEFAULT 0,
    cliente_usuario character varying(200) NOT NULL,
    id_agente integer NOT NULL,
    agente_usuario character varying(200) NOT NULL,
    id_oficina integer NOT NULL,
    oficina character varying(200) NOT NULL,
    en_sesion boolean NOT NULL DEFAULT false,
    marca_baja boolean NOT NULL DEFAULT false,
	bloqueado boolean NOT NULL DEFAULT false,
	afectado_ip boolean NOT NULL DEFAULT false,
	id_registro_token integer NOT NULL DEFAULT 0,
	cantidad_cargas numeric NOT NULL DEFAULT 0,
	total_importe_cargas numeric NOT NULL DEFAULT 0,
	total_importe_bonos numeric NOT NULL DEFAULT 0,
	ultima_carga timestamp,
	cantidad_retiros numeric NOT NULL DEFAULT 0,
	total_importe_retiros numeric NOT NULL DEFAULT 0,
	ultimo_retiro timestamp,
	datos_telefono integer NOT NULL DEFAULT 0,
	datos_email integer NOT NULL DEFAULT 0,
	datos_nombre integer NOT NULL DEFAULT 0
);
CREATE INDEX rpt_clientes_id_cliente ON rpt_clientes (id_cliente);
CREATE INDEX rpt_clientes_id_agente ON rpt_clientes (id_agente);
CREATE INDEX rpt_clientes_id_oficina ON rpt_clientes (id_oficina);


--DROP FUNCTION Reporte_Clientes()
CREATE OR REPLACE FUNCTION Reporte_Clientes() 
RETURNS VOID AS $$
BEGIN
	TRUNCATE TABLE rpt_clientes;
	
	INSERT INTO rpt_clientes (id_cliente,
							cliente_usuario,
							id_agente,
							agente_usuario,
							id_oficina,
							oficina,
							en_sesion,
							marca_baja,
							bloqueado,
							id_registro_token,
							datos_telefono,
							datos_email,
							datos_nombre)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_agente,
			ag.agente_usuario,
			ag.id_oficina,
			o.oficina,
			cl.en_sesion,
			cl.marca_baja,
			cl.bloqueado,
			cl.id_registro_token,
			CASE WHEN cl.telefono = '' THEN 0 ELSE 1 END as datos_telefono,
			CASE WHEN cl.correo_electronico = '' THEN 0 ELSE 1 END as datos_email,			
			CASE WHEN CONCAT(cl.cliente_apellido, cl.cliente_nombre) = '' THEN 0 ELSE 1 END as datos_nombre
	FROM cliente cl JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina o
			ON (ag.id_oficina = o.id_oficina);
	
	UPDATE rpt_clientes
	SET cantidad_cargas = v_Clientes_Cargas.cantidad_cargas,
		total_importe_cargas = v_Clientes_Cargas.total_importe_cargas,
		total_importe_bonos = v_Clientes_Cargas.total_importe_bonos,
		ultima_carga = v_Clientes_Cargas.ultima_carga
	FROM v_Clientes_Cargas
	WHERE v_Clientes_Cargas.id_cliente = rpt_clientes.id_cliente;
	
	UPDATE rpt_clientes
	SET cantidad_retiros = v_Clientes_Retiros.cantidad_cargas,
		total_importe_retiros = v_Clientes_Retiros.total_importe_retiros,
		ultimo_retiro = v_Clientes_Retiros.ultimo_retiro
	FROM v_Clientes_Retiros
	WHERE v_Clientes_Retiros.id_cliente = rpt_clientes.id_cliente;
	
	UPDATE rpt_clientes
	SET afectado_ip = true
	WHERE rpt_clientes.id_cliente IN (SELECT cbip.id_cliente_afectado
									  FROM 	v_Clientes_Bloqueantes_IP cbip);
END;
$$ LANGUAGE plpgsql;
--SELECT * FROM Reporte_Clientes();

/*Cuentas Bancarias*/
select * from cuenta_bancaria 
--where id_cuenta_bancaria in (795,935,885,886,851,807,868,800,806,790,777,792,855,849,858,808,870,846,926,784,805,799,859,804,925,782,850,862,863,857,869,941,928,847,901,856,787,867,866,884,780,793,798,786,781,865)
--where id_cuenta_bancaria in (859,850,856,925,858)
where id_cuenta_bancaria in (849)
order by id_oficina, marca_eliminada, marca_baja

select * from cuenta_bancaria_hist
where id_cuenta_bancaria in (849)
order by 1 desc

select * from cliente where cliente_usuario like '%dario010%'
select * from cliente where cliente_usuario like '%dario_ofi3_vip%'
select * from agente where id_agente = 26

select * from clientes_operaciones_hist 
where carga_id_cuenta_bancaria = 849
limit 10

select * from oficina where id_oficina = 19

update cuenta_bancaria 
set id_oficina = 2,
	marca_baja = true,
	id_usuario_noti = 0,
	id_agente = 0,
	marca_eliminada = false,
	fecha_hora_ultima_modificacion = now(),
	id_usuario_ultima_modificacion = 1
where id_cuenta_bancaria in (859,850,856,925,858);

INSERT INTO cuenta_bancaria_hist (id_cuenta_bancaria,
								id_oficina,
								nombre,
								alias,
								cbu,
								id_billetera,
								id_usuario_noti,
								id_agente,
								marca_baja,
								fecha_hora_ultima_modificacion,
								id_usuario_ultima_modificacion)
SELECT	cn.id_cuenta_bancaria,
		cn.id_oficina,
		cn.nombre,
		cn.alias,
		cn.cbu,
		cn.id_billetera,
		cn.id_usuario_noti,
		cn.id_agente,
		cn.marca_baja,
		cn.fecha_hora_ultima_modificacion,
		cn.id_usuario_ultima_modificacion
FROM cuenta_bancaria cn
where cn.id_cuenta_bancaria in (859,850,856,925,858);

select * from cuenta_bancaria_hist
select * from oficina where id_oficina in (17,19)

/*Unificación de Clientes*/
CREATE TABLE IF NOT EXISTS cliente_eneplicacion
(
    id_cliente integer NOT NULL,
    cliente_usuario character varying (200) NOT NULL,
	id_agente integer NOT NULL,
    fecha_hora_ultima_modificacion timestamp,
	eneplicado bigint NOT NULL,
	num_fila bigint NOT NULL,
	id_cliente_unificado integer NOT NULL,
	procesado boolean NOT NULL DEFAULT false,
    PRIMARY KEY (id_cliente)
);
CREATE INDEX cliente_eneplicacion_id_cliente_unificado ON cliente_eneplicacion (id_cliente_unificado);

INSERT INTO cliente_eneplicacion (id_cliente,
    							cliente_usuario,
								id_agente,
    							fecha_hora_ultima_modificacion,
								eneplicado,
								num_fila,
								id_cliente_unificado)
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cl.id_agente,
		cl.fecha_hora_ultima_modificacion,
		e.eneplicado,
		ROW_NUMBER() OVER (PARTITION BY cl.cliente_usuario, cl.id_agente ORDER BY cl.fecha_hora_ultima_modificacion DESC) AS num_fila,
		FIRST_VALUE(cl.id_cliente) OVER (PARTITION BY cl.cliente_usuario, cl.id_agente ORDER BY cl.fecha_hora_ultima_modificacion DESC) AS id_cliente_unificado
FROM cliente cl JOIN (SELECT cl.cliente_usuario,
						cl.id_agente,
						COUNT(DISTINCT cl.bloqueado) 	AS bloqueo,
						COUNT(cl.id_cliente) 			AS eneplicado,
						MAX(cl.fecha_hora_creacion)		AS fecha_hora_creacion
				FROM cliente cl
				WHERE cl.marca_baja = false
				GROUP BY cl.cliente_usuario,
						cl.id_agente
				HAVING COUNT(cl.id_cliente) > 1) e
		ON (e.cliente_usuario = cl.cliente_usuario 
			AND e.id_agente = cl.id_agente 
			AND cl.marca_baja = false)
WHERE cl.marca_baja = false
ORDER BY e.eneplicado DESC,cl.cliente_usuario,cl.id_agente, 6;

INSERT INTO cliente_eneplicacion (id_cliente,
    							cliente_usuario,
								id_agente,
    							fecha_hora_ultima_modificacion,
								eneplicado,
								num_fila,
								id_cliente_unificado)
--DELETE FROM cliente_eneplicacion WHERE id_cliente IN (SELECT cl.id_cliente
SELECT 	cl.id_cliente,
		cl.cliente_usuario,
		cl.id_agente,
		cl.fecha_hora_ultima_modificacion,
		e.eneplicado,
		ROW_NUMBER() OVER (PARTITION BY cl.id_cliente_ext, cl.id_cliente_db, cl.id_agente ORDER BY cl.fecha_hora_ultima_modificacion DESC) AS num_fila,
		FIRST_VALUE(cl.id_cliente) OVER (PARTITION BY cl.id_cliente_ext, cl.id_cliente_db, cl.id_agente ORDER BY cl.fecha_hora_ultima_modificacion DESC) AS id_cliente_unificado
FROM cliente cl JOIN (SELECT cl.id_cliente_ext,
						cl.id_cliente_db,
						cl.id_agente,
						COUNT(DISTINCT cl.bloqueado) 	AS bloqueo,
						COUNT(cl.id_cliente) 			AS eneplicado,
						MAX(cl.fecha_hora_creacion)		AS fecha_hora_creacion
				FROM cliente cl
				WHERE cl.marca_baja = false
				GROUP BY cl.id_cliente_ext,
						cl.id_cliente_db,
						cl.id_agente
				HAVING COUNT(cl.id_cliente) > 1) e
		ON (e.id_cliente_ext = cl.id_cliente_ext
			AND e.id_cliente_db = cl.id_cliente_db
			AND e.id_agente = cl.id_agente)
WHERE cl.marca_baja = false
--AND cl.id_cliente not in (SELECT id_cliente FROM cliente_eneplicacion WHERE procesado = true)
ORDER BY e.eneplicado DESC,cl.cliente_usuario,cl.id_agente, 6;

select * from cliente where id_cliente in (193312,156846,169998,193431,193387,177456) order by cliente_usuario, fecha_hora_creacion desc
select * from plataforma
--select * from cliente  where cliente_usuario like '%ngel7601'

select * from cliente where cliente_usuario like 'dar%o_acento%'

select * from cliente where cliente_usuario like 'darío_acento'

SELECT * FROM cliente_eneplicacion WHERE procesado = false;
--DELETE FROM cliente_eneplicacion where id_cliente in (10356,6098,6087,1917,931,2816,6226,6225,6222,6218,6211,6210,6206)


SELECT id_cliente, cliente_usuario FROM cliente_eneplicacion WHERE num_fila > 1 and procesado = false
UPDATE cliente set marca_baja = true where id_cliente in (SELECT id_cliente FROM cliente_eneplicacion WHERE num_fila > 1 and procesado = false);

UPDATE  cliente_sesion
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	cliente_sesion.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  cliente_sesion_hist
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	cliente_sesion_hist.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  cliente_chat
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	cliente_chat.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  cliente_chat_adjunto
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	cliente_chat_adjunto.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  operacion
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	operacion.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  operacion_hist
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	operacion_hist.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  clientes_operaciones_hist
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	clientes_operaciones_hist.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE  clientes_operaciones_diario
SET		id_cliente = cl.id_cliente_unificado
FROM	cliente_eneplicacion cl
WHERE	clientes_operaciones_diario.id_cliente = cl.id_cliente
AND cl.procesado = false
AND cl.num_fila > 1;

UPDATE cliente_eneplicacion SET procesado = true WHERE procesado = false;

/*Fin Unificación de Clientes*/

select * from oficina order by 1 desc
select * from usuario where usuario in ('pinachat','pinachat2','guille','bitbenja','bitandy','bitmati','bitpina2')
select * from usuario_sesion where ip in ('190.120.188.170','190.120.188.167','201.178.228.21','181.24.18.176','186.143.197.142')
select * from usuario_sesion where ip in ('190.190.233.108')


select 	id_usuario,
		usuario
from usuario 
where id_oficina = 2
and id_usuario not in (7,8)
and upper(usuario) not in ('SONIAOP','GEROMDQOP','GIANEOP','GUILLEOP','IVANOP','ANDYOP','BITUSER','KIMURAOP','JORGEOP',
'JOSEOPP','LAUTIMDQOP','LIDIAOP','LOREOP','LUCASOP','AQUINOOP','LUISOP','LUNAMDPOP','MAGAOP','MARCELOP','MARCOSOP','MATEOP',
'MAXIOP','MONSE','MOREOP','NEILAOP','OLGAOP','PABLOOP','PANCHOMDQOP','PAOLAOP','RAFITAOP','REINALDOOP','RICARDOOP','RODRIGOOP','ROSIOP',
'SEBA','SEBAMDQOP','SOFIAOP','SONIAOP','TAMIOP','VALENOP','VALENTINAOP','VASCOMDQOP','VEREOP','VICTOROP','VICTORIA','ALANMDQOP')
order by 2

update usuario 
set marca_baja = true
where id_oficina = 2
and id_usuario not in (7,8)
and upper(usuario) not in ('SONIAOP','GEROMDQOP','GIANEOP','GUILLEOP','IVANOP','ANDYOP','BITUSER','KIMURAOP','JORGEOP',
'JOSEOPP','LAUTIMDQOP','LIDIAOP','LOREOP','LUCASOP','AQUINOOP','LUISOP','LUNAMDPOP','MAGAOP','MARCELOP','MARCOSOP','MATEOP',
'MAXIOP','MONSE','MOREOP','NEILAOP','OLGAOP','PABLOOP','PANCHOMDQOP','PAOLAOP','RAFITAOP','REINALDOOP','RICARDOOP','RODRIGOOP','ROSIOP',
'SEBA','SEBAMDQOP','SOFIAOP','SONIAOP','TAMIOP','VALENOP','VALENTINAOP','VASCOMDQOP','VEREOP','VICTOROP','VICTORIA','ALANMDQOP')

select 	*
from usuario 
where id_oficina = 2
and id_rol = 3
and recibe_retiro = true
and marca_baja = false

update usuario 
set recibe_retiro = false
where id_oficina = 2
and id_rol = 3
and marca_baja = false



select * from cliente where cliente_usuario in ('gloriaquiroz365','ada601')
select * from cliente_sesion where id_cliente in (145438) order by 1 desc
select * from cliente_sesion where id_cliente in (3509) order by 1 desc
select * from cliente_sesion where ip in ('190.120.188.170','190.120.188.167','201.178.228.21','181.24.18.176','186.143.197.142')


SELECT r.id_cliente,
	r.cliente_usuario,
	r.id_agente,
	r.agente_usuario,
	r.id_oficina,
	r.oficina,
	r.en_sesion,
	r.marca_baja,
	r.bloqueado,
	r.afectado_ip,
	r.id_registro_token,
	r.cantidad_cargas,
	r.total_importe_cargas,
	r.total_importe_bonos,
	r.ultima_carga,
	EXTRACT(DAY FROM CURRENT_DATE - r.ultima_carga) AS dias_ultima_carga,
	r.cantidad_retiros,
	r.total_importe_retiros,
	r.ultimo_retiro
FROM rpt_clientes r;
 
select * from rpt_peticiones_nginx where id_servidor = 2 order by 1 desc limit 2000
delete from rpt_peticiones_nginx where id_registro_nginx in (53047,53034,53029,53011,52956)

/***************Bloqueados***********/
select id_cliente, horas_ultimo_retiro, minimo_espera_retiro from v_Clientes_Retiros where id_cliente = 54

select * from cliente where lower(trim(cliente_usuario)) = 'dario010';

select distinct cs.ip, cl.id_cliente, cl.cliente_usuario, cl.bloqueado, cl.id_agente, ag.agente_usuario, o.oficina
from cliente_sesion cs 
	join cliente cl on (cs.id_cliente = cl.id_cliente)
	join agente ag on (cl.id_agente = ag.id_agente)
	join oficina o on (ag.id_oficina = o.id_oficina)
and cs.ip in ('181.209.119.220')

select * from cliente_sesion where id_cliente in (1019)
and ip in (select cs.ip 
		   from cliente_sesion cs 
		   	join cliente cl on (cs.id_cliente = cl.id_cliente)
		   where cl.bloqueado = true)

select count(*) 
--select *
from v_IP_Lista_Blanca 
where ip not in ('', '0.0.0.0', '::1');

/*cuenta_bancaria_descarga*/
update cuenta_bancaria_descarga set ultima_descarga = false

update cuenta_bancaria_descarga
set ultima_descarga = true
where cuenta_bancaria_descarga.id_cuenta_bancaria_descarga in (
					select 	ctas.id_cuenta_bancaria_descarga
					from (
						select 	cbd.id_cuenta_bancaria,
								cbd.id_cuenta_bancaria_descarga,
								cbd.fecha_hora_descarga,
								cbd.cargas_monto,
								cbd.monto_restante,
								DENSE_RANK() OVER (PARTITION BY cbd.id_cuenta_bancaria
												   ORDER BY cbd.id_cuenta_bancaria, cbd.fecha_hora_descarga DESC) as orden
						from cuenta_bancaria_descarga cbd) ctas
					where ctas.orden = 1)

select 	ctas.id_cuenta_bancaria, 
		ctas.id_cuenta_bancaria_descarga, 
		ctas.fecha_hora_descarga
from (
	select 	cbd.id_cuenta_bancaria,
			cbd.id_cuenta_bancaria_descarga,
			cbd.fecha_hora_descarga,
			cbd.cargas_monto,
			cbd.monto_restante,
			DENSE_RANK() OVER (PARTITION BY cbd.id_cuenta_bancaria
							   ORDER BY cbd.id_cuenta_bancaria, cbd.fecha_hora_descarga DESC) as orden
	from cuenta_bancaria_descarga cbd) ctas
where ctas.orden = 1

/*
INSERT INTO cliente_chat (id_cliente, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
select distinct id_cliente, 'in_mensaje', now(), false, false, true, 1
from v_Clientes_Operaciones
where id_estado = 1
and id_oficina = 2
and es_carga = 1
and fecha_hora_operacion < '2024-08-10 00:00:00'

INSERT INTO cliente_chat (id_cliente, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
SELECT cl.id_cliente, in_mensaje, now(), false, false, true, in_id_usuario
FROM cliente cl JOIN agente ag
	ON (cl.id_agente = ag.id_agente
		AND ag.id_oficina = in_id_oficina
		AND cl.marca_baja = false);

update operacion
set marca_baja = true,
	id_estado = 3
where id_operacion in (select id_operacion
						from v_Clientes_Operaciones
						where id_estado = 1
						and id_oficina = 2
						and es_carga = 1
						and fecha_hora_operacion < '2024-08-10 00:00:00')
*/
select now();
SHOW timezone;
SELECT * FROM pg_timezone_names order by 3,1;
SET timezone = 'America/Argentina/Buenos_Aires';
ALTER DATABASE panelesweb SET timezone TO 'America/Argentina/Buenos_Aires';

select * from cliente where id_agente = 48 order by 1 desc
select * from oficina order by 1 desc
select * from usuario order by 1 desc

select * from Obtener_Usuario_Noti('francoop')

/*Mensaje Masivo*/
INSERT INTO cliente_chat (id_cliente, mensaje, fecha_hora_creacion, enviado_cliente, visto_cliente, visto_operador, id_usuario)
SELECT id_cliente, 'Queridos clientes desde 365 les queremos recordar que todas las cargas que envíen desde su cuenta de mercado pago serán acreditas en el acto para una mejor atención', now(), false, false, true, 1
FROM cliente 
WHERE marca_baja = false;

select count(*)
from cliente_chat
where mensaje = 'Queridos clientes desde 365 les queremos recordar que todas las cargas que envíen desde su cuenta de mercado pago serán acreditas en el acto para una mejor atención'
and visto_cliente = true

CREATE OR REPLACE FUNCTION Mantenimiento_Cliente_Sesion(in_fecha_hasta TIMESTAMP) 
RETURNS VOID AS $$
BEGIN
	/*Sesiones de Clientes*/
	INSERT INTO cliente_sesion_transferencia (id_cliente_sesion, transferido, fecha_hora)
	SELECT	cls.id_cliente_sesion, false, now()
	FROM	cliente_sesion cls
	WHERE	cls.fecha_hora_cierre IS NOT NULL
	AND cls.fecha_hora_creacion < in_fecha_hasta
	-- AND cls.fecha_hora_creacion < '2024-10-02'
	AND cls.id_cliente_sesion NOT IN (SELECT cs.id_cliente_sesion
										FROM operacion op JOIN cliente cl
												ON (op.id_cliente = cl.id_cliente
													AND op.id_accion = 8
													AND op.marca_baja = false
													AND op.id_estado = 2
													AND op.fecha_hora_creacion > NOW() - INTERVAL '72 hours')
											JOIN cliente_sesion cs
												ON (cl.id_cliente = cs.id_cliente))
	AND cls.id_cliente_sesion NOT IN (SELECT cs.id_cliente_sesion
											FROM operacion_hist op JOIN cliente cl
													ON (op.id_cliente = cl.id_cliente
														AND op.id_accion = 8
														AND op.marca_baja = false
														AND op.id_estado = 2
														AND op.fecha_hora_creacion > NOW() - INTERVAL '72 hours')
												JOIN cliente_sesion cs
													ON (cl.id_cliente = cs.id_cliente));

	INSERT INTO cliente_sesion_hist (id_cliente_sesion,
									id_cliente,
									id_token,
									ip,
									moneda,
									monto,
									fecha_hora_creacion,
									fecha_hora_cierre,
									cierre_abrupto)
	SELECT	id_cliente_sesion,
			id_cliente,
			id_token,
			ip,
			moneda,
			monto,
			fecha_hora_creacion,
			fecha_hora_cierre,
			cierre_abrupto
	FROM 	cliente_sesion
	WHERE	id_cliente_sesion IN (SELECT id_cliente_sesion
							FROM	cliente_sesion_transferencia
							WHERE 	transferido = false);

	DELETE FROM cliente_sesion
	WHERE id_cliente_sesion IN (SELECT id_cliente_sesion 
							FROM cliente_sesion_transferencia
							WHERE transferido = false);

	UPDATE cliente_sesion_transferencia SET transferido = true WHERE transferido = false;

	ANALYZE cliente_sesion;
	ANALYZE cliente_sesion_hist;
END;
$$ LANGUAGE plpgsql;
-- Select Mantenimiento_Cliente_Sesion('2024-10-02');

CREATE OR REPLACE FUNCTION Mantenimiento_Operaciones_Cargas(in_fecha_hasta TIMESTAMP) 
RETURNS VOID AS $$
BEGIN
	/*Cargas*/
	INSERT INTO operacion_transferencia (id_operacion, transferido, fecha_hora)
	SELECT	op.id_operacion, false, now()
	FROM	operacion op JOIN operacion_carga opc
			ON (op.id_operacion = opc.id_operacion)
	WHERE op.id_estado <> 1
	AND id_accion IN (1, 5, 9)
	AND opc.procesando = false
	-- AND op.notificado = true
	AND op.fecha_hora_ultima_modificacion < in_fecha_hasta;

	INSERT INTO operacion_hist (id_operacion,
								codigo_operacion,
								id_accion,
								id_cliente,
								id_estado,
								notificado,
								marca_baja,
								fecha_hora_creacion,
								fecha_hora_ultima_modificacion,
								id_usuario_ultima_modificacion)
	SELECT	id_operacion,
			codigo_operacion,
			id_accion,
			id_cliente,
			id_estado,
			notificado,
			marca_baja,
			fecha_hora_creacion,
			fecha_hora_ultima_modificacion,
			id_usuario_ultima_modificacion
	FROM 	operacion
	WHERE	id_operacion IN (SELECT id_operacion
							FROM	operacion_transferencia
							WHERE 	transferido = false);

	DELETE FROM operacion
	WHERE id_operacion IN (SELECT id_operacion 
							FROM operacion_transferencia
							WHERE transferido = false);

	INSERT INTO operacion_carga_hist (id_operacion_carga,
									id_operacion,
									titular,
									importe,
									bono,
									id_cuenta_bancaria,
									sol_importe,
									sol_bono,
									sol_id_cuenta_bancaria,
									observaciones,
									procesando)
	SELECT 	id_operacion_carga,
			id_operacion,
			titular,
			importe,
			bono,
			id_cuenta_bancaria,
			sol_importe,
			sol_bono,
			sol_id_cuenta_bancaria,
			observaciones,
			procesando
	FROM 	operacion_carga
	WHERE	id_operacion IN (SELECT id_operacion
							FROM	operacion_transferencia
							WHERE 	transferido = false);

	DELETE FROM operacion_carga
	WHERE id_operacion IN (SELECT id_operacion 
							FROM operacion_transferencia
							WHERE transferido = false);

	UPDATE operacion_transferencia SET transferido = true WHERE transferido = false;

	ANALYZE operacion;
	ANALYZE operacion_carga;
	ANALYZE operacion_hist;
	ANALYZE operacion_carga_hist;
END;
$$ LANGUAGE plpgsql;
-- Select Mantenimiento_Operaciones_Cargas('2024-09-17')

CREATE OR REPLACE FUNCTION Mantenimiento_Operaciones_Retiros(in_fecha_hasta TIMESTAMP) 
RETURNS VOID AS $$
BEGIN
	/*Retiros*/
	INSERT INTO operacion_transferencia (id_operacion, transferido, fecha_hora)
	SELECT	op.id_operacion, false, now()
	FROM	operacion op JOIN operacion_retiro opr
		ON (op.id_operacion = opr.id_operacion)
	WHERE op.id_estado <> 1
	AND op.id_accion IN (2, 6)
	AND op.fecha_hora_ultima_modificacion < in_fecha_hasta;

	INSERT INTO operacion_hist (id_operacion,
								codigo_operacion,
								id_accion,
								id_cliente,
								id_estado,
								notificado,
								marca_baja,
								fecha_hora_creacion,
								fecha_hora_ultima_modificacion,
								id_usuario_ultima_modificacion)
	SELECT	id_operacion,
			codigo_operacion,
			id_accion,
			id_cliente,
			id_estado,
			notificado,
			marca_baja,
			fecha_hora_creacion,
			fecha_hora_ultima_modificacion,
			id_usuario_ultima_modificacion
	FROM 	operacion
	WHERE	id_operacion IN (SELECT id_operacion
							FROM	operacion_transferencia
							WHERE 	transferido = false);

	DELETE FROM operacion
	WHERE id_operacion IN (SELECT id_operacion 
							FROM operacion_transferencia
							WHERE transferido = false);

	UPDATE operacion_transferencia SET transferido = true WHERE transferido = false;

	ANALYZE operacion;
	ANALYZE operacion_retiro;
	ANALYZE operacion_hist;
END;
$$ LANGUAGE plpgsql;
-- Select Mantenimiento_Operaciones_Retiros('2024-09-17')

CREATE OR REPLACE FUNCTION Mantenimiento_Operaciones_Resto(in_fecha_hasta TIMESTAMP) 
RETURNS VOID AS $$
BEGIN
	/*No Cargas ni Retiros*/
	INSERT INTO operacion_transferencia (id_operacion, transferido, fecha_hora)
	SELECT	op.id_operacion, false, now()
	FROM	operacion op 
	WHERE op.id_estado <> 1
	AND op.id_accion NOT IN (1, 2, 5, 6, 9)
	AND op.fecha_hora_ultima_modificacion < in_fecha_hasta;

	INSERT INTO operacion_hist (id_operacion,
								codigo_operacion,
								id_accion,
								id_cliente,
								id_estado,
								notificado,
								marca_baja,
								fecha_hora_creacion,
								fecha_hora_ultima_modificacion,
								id_usuario_ultima_modificacion)
	SELECT	id_operacion,
			codigo_operacion,
			id_accion,
			id_cliente,
			id_estado,
			notificado,
			marca_baja,
			fecha_hora_creacion,
			fecha_hora_ultima_modificacion,
			id_usuario_ultima_modificacion
	FROM 	operacion
	WHERE	id_operacion IN (SELECT id_operacion
							FROM	operacion_transferencia
							WHERE 	transferido = false);

	DELETE FROM operacion
	WHERE id_operacion IN (SELECT id_operacion 
							FROM operacion_transferencia
							WHERE transferido = false);

	UPDATE operacion_transferencia SET transferido = true WHERE transferido = false;

	ANALYZE operacion;
	ANALYZE operacion_hist;
END;
$$ LANGUAGE plpgsql;
-- Select Mantenimiento_Operaciones_Resto('2024-09-17')

CREATE OR REPLACE FUNCTION Mantenimiento_Notificaciones(in_fecha_hasta TIMESTAMP) 
RETURNS VOID AS $$
BEGIN
	/*Notificaciones*/
	INSERT INTO notificacion_transferencia (id_notificacion, transferido, fecha_hora)
	SELECT	n.id_notificacion, false, now()
	FROM 	notificacion n LEFT JOIN notificacion_carga nc
			ON (n.id_notificacion = nc.id_notificacion)
	WHERE	(nc.marca_procesado = true	AND nc.fecha_hora_procesado < in_fecha_hasta)
		OR
			(COALESCE(nc.id_notificacion, 0) = 0);

	INSERT INTO notificacion_hist (id_notificacion,
								id_usuario,
								id_cuenta_bancaria,
								titulo,
								mensaje,
								fecha_hora,
								id_notificacion_origen,
								id_origen,
								anulada,
								id_usuario_ultima_modificacion,
								fecha_hora_ultima_modificacion)
	SELECT 	id_notificacion,
			id_usuario,
			id_cuenta_bancaria,
			titulo,
			mensaje,
			fecha_hora,
			id_notificacion_origen,
			id_origen,
			anulada,
			id_usuario_ultima_modificacion,
			fecha_hora_ultima_modificacion
	FROM	notificacion
	WHERE	id_notificacion IN (SELECT id_notificacion
							FROM	notificacion_transferencia
							WHERE 	transferido = false);

	DELETE FROM notificacion
	WHERE id_notificacion IN (SELECT id_notificacion
							FROM notificacion_transferencia
							WHERE transferido = false);

	INSERT INTO notificacion_carga_hist (id_notificacion_carga,
										id_notificacion,
										id_operacion_carga,
										carga_usuario,
										carga_monto,
										marca_procesado,
										fecha_hora_procesado)
	SELECT 	id_notificacion_carga,
			id_notificacion,
			id_operacion_carga,
			carga_usuario,
			carga_monto,
			marca_procesado,
			fecha_hora_procesado
	FROM	notificacion_carga
	WHERE	id_notificacion IN (SELECT id_notificacion
							FROM	notificacion_transferencia
							WHERE 	transferido = false);

	DELETE FROM notificacion_carga
	WHERE id_notificacion IN (SELECT id_notificacion
							FROM notificacion_transferencia
							WHERE transferido = false);

	UPDATE notificacion_transferencia SET transferido = true WHERE transferido = false;

	ANALYZE notificacion;
	ANALYZE notificacion_carga;
	ANALYZE notificacion_hist;
	ANALYZE notificacion_carga_hist;
END;
$$ LANGUAGE plpgsql;
-- select Mantenimiento_Notificaciones('2024-09-17')

CREATE OR REPLACE FUNCTION Mantenimiento_Clientes_Operaciones_Hist() 
RETURNS VOID AS $$
BEGIN
	TRUNCATE TABLE clientes_operaciones_hist;
	
	INSERT INTO clientes_operaciones_hist (
										id_cliente,
										cliente_usuario,
										id_cliente_ext,
										id_cliente_db,
										id_agente,
										agente_usuario,
										agente_password,
										id_plataforma,
										plataforma,
										id_oficina,
										marca_baja,
										oficina,
										id_operacion,
										codigo_operacion,
										id_estado,
										estado,
										id_accion,
										es_carga,
										es_retiro,
										accion,
										usuario,
										cliente_confianza,
										fecha_hora_operacion,
										fecha_hora_proceso,
										retiro_importe,
										retiro_cbu,
										retiro_titular,
										retiro_observaciones,
										carga_importe,
										carga_titular,
										carga_id_cuenta_bancaria,
										carga_bono,
										carga_observaciones,
										carga_cuenta_bancaria,
										carga_cuenta_bancaria_nombre,
										carga_cuenta_bancaria_alias,
										carga_cuenta_bancaria_cbu,
										id_billetera,
										id_usuario_noti,
										id_notificacion,
										id_origen_notificacion,
										carga_automatica)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			1 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			0 AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01') AS fecha_hora_proceso,
			0	AS retiro_importe,
			''	AS retiro_cbu,
			''	AS retiro_titular,
			''	AS retiro_observaciones,
			COALESCE(opc.importe, 0)							AS carga_importe,
			COALESCE(opc.titular, '')							AS carga_titular,
			COALESCE(opc.id_cuenta_bancaria, 0)					AS carga_id_cuenta_bancaria,
			COALESCE(opc.bono, 0)								AS carga_bono,
			COALESCE(opc.observaciones, '')						AS carga_observaciones,
			COALESCE(opcb.nombre || '-' || opcb.alias || '-' || opcb.cbu, '')	AS carga_cuenta_bancaria,
			COALESCE(opcb.nombre, '')	AS carga_cuenta_bancaria_nombre,
			COALESCE(opcb.alias, '')	AS carga_cuenta_bancaria_alias,
			COALESCE(opcb.cbu, '')	AS carga_cuenta_bancaria_cbu,
			COALESCE(opcb.id_billetera, 0)	AS id_billetera,
			COALESCE(opcb.id_usuario_noti, 0) AS id_usuario_noti,
			COALESCE(nc.id_notificacion, 0)	AS id_notificacion,
			COALESCE(n.id_origen, 0)		AS id_origen_notificacion,
			CASE WHEN COALESCE(n.id_notificacion, 0) = 0 THEN 0 ELSE 1 END AS carga_automatica
	FROM cliente cl JOIN operacion_hist o
				ON (cl.id_cliente = o.id_cliente
					AND o.marca_baja = false)
			JOIN usuario u
				ON (o.id_usuario_ultima_modificacion = u.id_usuario)
			JOIN estado e
				ON (o.id_estado = e.id_estado)
			JOIN accion ac
				ON (o.id_accion = ac.id_accion)
			JOIN agente ag
				ON (cl.id_agente = ag.id_agente)
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
			JOIN operacion_carga_hist opc
				ON (o.id_operacion = opc.id_operacion)
			LEFT JOIN cuenta_bancaria opcb
				ON (opc.id_cuenta_bancaria = opcb.id_cuenta_bancaria)
			LEFT JOIN notificacion_carga_hist nc
				ON (nc.id_operacion_carga = opc.id_operacion_carga)
			LEFT JOIN notificacion_hist n
				ON (n.id_notificacion = nc.id_notificacion)
	WHERE cl.marca_baja = false
	AND o.id_accion IN (1, 5, 9);
	
	INSERT INTO clientes_operaciones_hist (
										id_cliente,
										cliente_usuario,
										id_cliente_ext,
										id_cliente_db,
										id_agente,
										agente_usuario,
										agente_password,
										id_plataforma,
										plataforma,
										id_oficina,
										marca_baja,
										oficina,
										id_operacion,
										codigo_operacion,
										id_estado,
										estado,
										id_accion,
										es_carga,
										es_retiro,
										accion,
										usuario,
										cliente_confianza,
										fecha_hora_operacion,
										fecha_hora_proceso,
										retiro_importe,
										retiro_cbu,
										retiro_titular,
										retiro_observaciones,
										carga_importe,
										carga_titular,
										carga_id_cuenta_bancaria,
										carga_bono,
										carga_observaciones,
										carga_cuenta_bancaria,
										carga_cuenta_bancaria_nombre,
										carga_cuenta_bancaria_alias,
										carga_cuenta_bancaria_cbu,
										id_billetera,
										id_usuario_noti,
										id_notificacion,
										id_origen_notificacion,
										carga_automatica)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			1 AS es_retiro,
			ac.accion,
			u.usuario,
			0 AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_proceso,
			--COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			COALESCE(opr.importe, 0)							AS retiro_importe,
			COALESCE(opr.cbu, '')								AS retiro_cbu,
			COALESCE(opr.titular, '')							AS retiro_titular,
			COALESCE(opr.observaciones, '')						AS retiro_observaciones,
			0	AS carga_importe,
			''	AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''	AS carga_observaciones,
			''	AS carga_cuenta_bancaria,
			''	AS carga_cuenta_bancaria_nombre,
			''	AS carga_cuenta_bancaria_alias,
			''	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			COALESCE(opr.id_usuario, 0) AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM cliente cl JOIN operacion_hist o
				ON (cl.id_cliente = o.id_cliente
					AND o.marca_baja = false)
			JOIN usuario u
				ON (o.id_usuario_ultima_modificacion = u.id_usuario)
			JOIN estado e
				ON (o.id_estado = e.id_estado)
			JOIN accion ac
				ON (o.id_accion = ac.id_accion)
			JOIN agente ag
				ON (cl.id_agente = ag.id_agente)
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
			JOIN operacion_retiro opr
				ON (o.id_operacion = opr.id_operacion)
	WHERE cl.marca_baja = false 
	AND o.id_accion IN (2, 6);

	INSERT INTO clientes_operaciones_hist (
										id_cliente,
										cliente_usuario,
										id_cliente_ext,
										id_cliente_db,
										id_agente,
										agente_usuario,
										agente_password,
										id_plataforma,
										plataforma,
										id_oficina,
										marca_baja,
										oficina,
										id_operacion,
										codigo_operacion,
										id_estado,
										estado,
										id_accion,
										es_carga,
										es_retiro,
										accion,
										usuario,
										cliente_confianza,
										fecha_hora_operacion,
										fecha_hora_proceso,
										retiro_importe,
										retiro_cbu,
										retiro_titular,
										retiro_observaciones,
										carga_importe,
										carga_titular,
										carga_id_cuenta_bancaria,
										carga_bono,
										carga_observaciones,
										carga_cuenta_bancaria,
										carga_cuenta_bancaria_nombre,
										carga_cuenta_bancaria_alias,
										carga_cuenta_bancaria_cbu,
										id_billetera,
										id_usuario_noti,
										id_notificacion,
										id_origen_notificacion,
										carga_automatica)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			0 AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			0	AS retiro_importe,
			''	AS retiro_cbu,
			''	AS retiro_titular,
			''	AS retiro_observaciones,
			0	AS carga_importe,
			''	AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''	AS carga_observaciones,
			''	AS carga_cuenta_bancaria,
			''	AS carga_cuenta_bancaria_nombre,
			''	AS carga_cuenta_bancaria_alias,
			''	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			0 	AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM cliente cl JOIN operacion_hist o
				ON (cl.id_cliente = o.id_cliente
					AND o.marca_baja = false)
			JOIN usuario u
				ON (o.id_usuario_ultima_modificacion = u.id_usuario)
			JOIN estado e
				ON (o.id_estado = e.id_estado)
			JOIN accion ac
				ON (o.id_accion = ac.id_accion)
			JOIN agente ag
				ON (cl.id_agente = ag.id_agente)
			JOIN oficina ofi
				ON (ag.id_oficina = ofi.id_oficina)
			JOIN plataforma pla
				ON (ag.id_plataforma = pla.id_plataforma)
	WHERE cl.marca_baja = false 
	AND o.id_accion NOT IN (1, 2, 5, 6, 9);
	
	ANALYZE clientes_operaciones_hist;
	ANALYZE cliente_chat;
	ANALYZE cliente_sesion;
	ANALYZE operacion_retiro;
END;
$$ LANGUAGE plpgsql;
-- select Mantenimiento_Clientes_Operaciones_Hist();

CREATE OR REPLACE FUNCTION Mantenimiento_Clientes_Operaciones_Diario() 
RETURNS VOID AS $$
BEGIN
	TRUNCATE TABLE clientes_operaciones_diario;

	INSERT INTO clientes_operaciones_diario (
										id_cliente,
										cliente_usuario,
										id_cliente_ext,
										id_cliente_db,
										id_agente,
										agente_usuario,
										agente_password,
										id_plataforma,
										plataforma,
										id_oficina,
										marca_baja,
										oficina,
										id_operacion,
										codigo_operacion,
										id_estado,
										estado,
										id_accion,
										es_carga,
										es_retiro,
										accion,
										usuario,
										cliente_confianza,
										fecha_hora_operacion,
										fecha_hora_proceso,
										retiro_importe,
										retiro_cbu,
										retiro_titular,
										retiro_observaciones,
										carga_importe,
										carga_titular,
										carga_id_cuenta_bancaria,
										carga_bono,
										carga_observaciones,
										carga_cuenta_bancaria,
										carga_cuenta_bancaria_nombre,
										carga_cuenta_bancaria_alias,
										carga_cuenta_bancaria_cbu,
										id_billetera,
										id_usuario_noti,
										id_notificacion,
										id_origen_notificacion,
										carga_automatica)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			1 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			ROUND((COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0)) / 
				  (COALESCE(clconf_hist.Totales::numeric, 1) + COALESCE(clconf.Totales::numeric, 1))
				  * 100) AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01') AS fecha_hora_proceso,
			0	AS retiro_importe,
			''::character varying AS retiro_cbu,
			''::character varying AS retiro_titular,
			''::character varying AS retiro_observaciones,
			COALESCE(opc.importe, 0)							AS carga_importe,
			COALESCE(opc.titular, '')::character varying		AS carga_titular,
			COALESCE(opc.id_cuenta_bancaria, 0)					AS carga_id_cuenta_bancaria,
			COALESCE(opc.bono, 0)								AS carga_bono,
			COALESCE(opc.observaciones, '')::character varying	AS carga_observaciones,
			COALESCE(opcb.nombre || '-' || opcb.alias || '-' || opcb.cbu, '') AS carga_cuenta_bancaria,
			COALESCE(opcb.nombre, '')::character varying AS carga_cuenta_bancaria_nombre,
			COALESCE(opcb.alias, '')::character varying	AS carga_cuenta_bancaria_alias,
			COALESCE(opcb.cbu, '')::character varying AS carga_cuenta_bancaria_cbu,
			COALESCE(opcb.id_billetera, 0)	AS id_billetera,
			COALESCE(opcb.id_usuario_noti, 0) AS id_usuario_noti,
			COALESCE(nc.id_notificacion, 0)	AS id_notificacion,
			COALESCE(n.id_origen, 0)		AS id_origen_notificacion,
			CASE WHEN COALESCE(n.id_notificacion, 0) = 0 THEN 0 ELSE 1 END AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion IN (1, 5, 9)
			AND o.marca_baja = false
			--AND o.id_operacion NOT IN (SELECT coh.id_operacion FROM operacion_hist coh WHERE coh.id_accion IN (1, 5, 9))
		   	AND o.id_estado > 1)
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		JOIN operacion_carga opc
			ON (o.id_operacion = opc.id_operacion)
		LEFT JOIN cuenta_bancaria opcb
			ON (opc.id_cuenta_bancaria = opcb.id_cuenta_bancaria)
		LEFT JOIN notificacion_carga nc
			ON (nc.id_operacion_carga = opc.id_operacion_carga)
		LEFT JOIN notificacion n
			ON (n.id_notificacion = nc.id_notificacion)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion o
				WHERE o.id_accion = 1
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf
			ON (cl.id_cliente = clconf.id_cliente)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion_hist o
				WHERE o.id_accion = 1
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf_hist
			ON (cl.id_cliente = clconf_hist.id_cliente);
			
	INSERT INTO clientes_operaciones_diario (
										id_cliente,
										cliente_usuario,
										id_cliente_ext,
										id_cliente_db,
										id_agente,
										agente_usuario,
										agente_password,
										id_plataforma,
										plataforma,
										id_oficina,
										marca_baja,
										oficina,
										id_operacion,
										codigo_operacion,
										id_estado,
										estado,
										id_accion,
										es_carga,
										es_retiro,
										accion,
										usuario,
										cliente_confianza,
										fecha_hora_operacion,
										fecha_hora_proceso,
										retiro_importe,
										retiro_cbu,
										retiro_titular,
										retiro_observaciones,
										carga_importe,
										carga_titular,
										carga_id_cuenta_bancaria,
										carga_bono,
										carga_observaciones,
										carga_cuenta_bancaria,
										carga_cuenta_bancaria_nombre,
										carga_cuenta_bancaria_alias,
										carga_cuenta_bancaria_cbu,
										id_billetera,
										id_usuario_noti,
										id_notificacion,
										id_origen_notificacion,
										carga_automatica)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			1 AS es_retiro,
			ac.accion,
			u.usuario,
			ROUND((COALESCE(clconf_hist.Aceptadas::numeric, 0) + COALESCE(clconf.Aceptadas::numeric, 0))/ 
				  (COALESCE(clconf_hist.Totales::numeric, 1) + COALESCE(clconf.Totales::numeric, 1))
				  * 100) AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_proceso,
			--COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			COALESCE(opr.importe, 0)							AS retiro_importe,
			COALESCE(opr.cbu, '')::character varying	 		AS retiro_cbu,
			COALESCE(opr.titular, '')::character varying		AS retiro_titular,
			COALESCE(opr.observaciones, '')::character varying	AS retiro_observaciones,
			0	AS carga_importe,
			''::character varying AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''::character varying	AS carga_observaciones,
			''::text	AS carga_cuenta_bancaria,
			''::character varying	AS carga_cuenta_bancaria_nombre,
			''::character varying	AS carga_cuenta_bancaria_alias,
			''::character varying	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			COALESCE(opr.id_usuario, 0) AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion IN (2, 6)
			AND o.marca_baja = false
			--AND o.id_operacion NOT IN (SELECT coh.id_operacion FROM operacion_hist coh WHERE coh.id_accion IN (2, 6))
		   	AND o.id_estado > 1)
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma)
		JOIN operacion_retiro opr
			ON (o.id_operacion = opr.id_operacion)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion o
				WHERE o.id_accion = 2
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf
			ON (cl.id_cliente = clconf.id_cliente)
		LEFT JOIN (SELECT o.id_cliente,
					COUNT(*) AS Totales,
					SUM(CASE WHEN o.id_estado = 2 THEN 1 ELSE 0 END) AS Aceptadas
				FROM operacion_hist o
				WHERE o.id_accion = 2
					AND o.id_estado in (2,3)
					AND o.marca_baja = false
				GROUP BY o.id_cliente) clconf_hist
			ON (cl.id_cliente = clconf_hist.id_cliente);

	INSERT INTO clientes_operaciones_diario (
										id_cliente,
										cliente_usuario,
										id_cliente_ext,
										id_cliente_db,
										id_agente,
										agente_usuario,
										agente_password,
										id_plataforma,
										plataforma,
										id_oficina,
										marca_baja,
										oficina,
										id_operacion,
										codigo_operacion,
										id_estado,
										estado,
										id_accion,
										es_carga,
										es_retiro,
										accion,
										usuario,
										cliente_confianza,
										fecha_hora_operacion,
										fecha_hora_proceso,
										retiro_importe,
										retiro_cbu,
										retiro_titular,
										retiro_observaciones,
										carga_importe,
										carga_titular,
										carga_id_cuenta_bancaria,
										carga_bono,
										carga_observaciones,
										carga_cuenta_bancaria,
										carga_cuenta_bancaria_nombre,
										carga_cuenta_bancaria_alias,
										carga_cuenta_bancaria_cbu,
										id_billetera,
										id_usuario_noti,
										id_notificacion,
										id_origen_notificacion,
										carga_automatica)
	SELECT 	cl.id_cliente,
			cl.cliente_usuario,
			cl.id_cliente_ext,
			cl.id_cliente_db,
			cl.id_agente,
			ag.agente_usuario,
			ag.agente_password,
			ag.id_plataforma,
			pla.plataforma,
			ag.id_oficina,
			o.marca_baja,
			ofi.oficina,
			o.id_operacion,
			o.codigo_operacion,
			e.id_estado,
			e.estado,
			ac.id_accion,
			0 AS es_carga,
			0 AS es_retiro,
			ac.accion,
			u.usuario,
			0 AS cliente_confianza,
			COALESCE(o.fecha_hora_creacion, '1900-01-01')		AS fecha_hora_operacion,
			COALESCE(o.fecha_hora_ultima_modificacion, '1900-01-01')	AS fecha_hora_proceso,
			0	AS retiro_importe,
			''::character varying AS retiro_cbu,
			''::character varying	AS retiro_titular,
			''::character varying	AS retiro_observaciones,
			0	AS carga_importe,
			''	AS carga_titular,
			0	AS carga_id_cuenta_bancaria,
			0	AS carga_bono,
			''::character varying	AS carga_observaciones,
			''::text	AS carga_cuenta_bancaria,
			''::character varying	AS carga_cuenta_bancaria_nombre,
			''::character varying	AS carga_cuenta_bancaria_alias,
			''::character varying	AS carga_cuenta_bancaria_cbu,
			0	AS id_billetera,
			0 	AS id_usuario_noti,
			0	AS id_notificacion,
			0	AS id_origen_notificacion,
			0 	AS carga_automatica
	FROM operacion o JOIN usuario u
		ON (o.id_usuario_ultima_modificacion = u.id_usuario
			AND o.id_accion NOT IN (1, 2, 5, 6, 9)
			AND o.marca_baja = false
			--AND o.id_operacion NOT IN (SELECT coh.id_operacion FROM operacion_hist coh WHERE coh.id_accion NOT IN (1, 2, 5, 6, 9))
		   	AND o.id_estado > 1)
		JOIN estado e
			ON (o.id_estado = e.id_estado)
		JOIN accion ac
			ON (o.id_accion = ac.id_accion)
		JOIN cliente cl
			ON (cl.id_cliente = o.id_cliente
				AND cl.marca_baja = false)		
		JOIN agente ag
			ON (cl.id_agente = ag.id_agente)
		JOIN oficina ofi
			ON (ag.id_oficina = ofi.id_oficina)
		JOIN plataforma pla
			ON (ag.id_plataforma = pla.id_plataforma);
			
	ANALYZE clientes_operaciones_diario;
END;
$$ LANGUAGE plpgsql;
-- select Mantenimiento_Clientes_Operaciones_Diario();
SELECT COUNT(*) FROM clientes_operaciones_diario


/*****REPLICACION*****************/
CREATE ROLE replicator WITH REPLICATION PASSWORD 'paneles0110' LOGIN;
--Maestro:
SELECT * FROM pg_stat_replication;
--Esclavo:
SELECT * FROM pg_stat_wal_receiver;
SELECT pg_is_in_recovery();
--debe devolver true, sino:
--agregar línea "hot_standby = on" en postgresql.conf

--select * from usuario;
--update usuario set id_rol = 4 where id_usuario = 3;
--update usuario set id_rol = 3 where id_usuario = 3;

CREATE EXTENSION pg_stat_statements;
select * from pg_stat_statements;

SELECT round((100 * total_exec_time / sum(total_exec_time)                       
           OVER ())::numeric, 2) percent,                        
           round(total_exec_time::numeric, 2) AS total,                  
           calls,                                                   
           round(mean_exec_time::numeric, 2) AS mean,                    
           query                                 
 FROM  pg_stat_statements                                               
           ORDER BY total_exec_time DESC                                               
           LIMIT 10;

SELECT count(*), state FROM pg_stat_activity GROUP BY state;

SHOW max_connections;
SELECT * FROM pg_stat_activity;

SELECT pg_terminate_backend(pid)
FROM pg_stat_activity
WHERE state = 'idle' 
AND pid <> pg_backend_pid();
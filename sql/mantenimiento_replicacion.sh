#!/bin/bash

# Script de ejemplo para monitorear el tamaño de la carpeta pg_wal y eliminar archivos antiguos
PG_WAL_DIR="/var/lib/postgresql/16/main/pg_wal"
ARCHIVE_DIR="/var/lib/postgresql/16/main/archive"

# Archivar y eliminar WALs más antiguos que 1 día
find $PG_WAL_DIR -type f -mtime +1 -exec cp {} $ARCHIVE_DIR \; -exec rm {} \;

# Eliminar archivos WAL en el archivo más antiguos que 2 días
find $ARCHIVE_DIR -type f -mtime +2 -exec rm {} \;

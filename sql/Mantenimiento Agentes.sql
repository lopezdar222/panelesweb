DROP TABLE IF EXISTS cliente_agente_bkp;
CREATE TABLE IF NOT EXISTS cliente_agente_bkp
(
    id_cliente INTEGER NOT NULL DEFAULT 0,
    id_agente_anterior integer NOT NULL,
	id_agente integer NOT NULL,
    fecha_hora_modificacion timestamp NOT NULL
);

select * from agente order by 2
34	"ofi2vip"
36	"ofi3vip"
42	"ofi4vip"
63	"tato1vip"
64	"tato2vip"
56	"cyfvip"

26 "ofi1vip"

select * from agente where id_agente in (34,36,42,63,64,56)
select count(*) from cliente where id_agente in (34,36,42,63,64,56);

select * from cliente_agente_bkp;

insert into cliente_agente_bkp (id_cliente, id_agente_anterior, id_agente, fecha_hora_modificacion)
select id_cliente, id_agente, 26, now()
from cliente
where id_agente in (34,36,42,63,64,56);

update cliente
set id_agente = 26
where id_cliente in (select id_cliente from cliente_agente_bkp);

select * from cuenta_bancaria
where id_agente in (34,36,42,63,64,56);





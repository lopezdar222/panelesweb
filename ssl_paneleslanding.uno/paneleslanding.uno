upstream node_app {
    server 127.0.0.1:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.165:3000 weight=1;  # Servidor con 8 vCPUs
    server 192.168.200.189:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.180:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.137:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.102:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.212:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.57:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.152:3000 weight=5;  # Servidor con 8 vCPUs
    server 192.168.200.79:3000 weight=5;  # Servidor con 8 vCPUs
}

server {
    listen 443 ssl;
    server_name paneleslanding.uno;

    # Configuración SSL aquí
    ssl_certificate /home/dario/panelesweb/ssl/fullchain.pem;
    # ssl_certificate /home/dario/panelesweb/ssl/certificado.crt;
    ssl_certificate_key /home/dario/panelesweb/ssl/clave_privada.key;

    # Aquí va el resto de tu configuración para paneleslanding.uno
    root /var/www/paneleslanding.uno;
    location / {
        # proxy_pass http://localhost:3000;
        proxy_pass http://node_app;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;        
	proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Real-IP $remote_addr;
    }

    location /api/mp_api {
        proxy_pass http://node_app/api/mp_api;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}

server {
    listen 80;
    server_name paneleslanding.uno;

    # Redirigir www.paneleslanding.uno a paneleslanding.uno
    if ($host = www.paneleslanding.uno) {
        return 301 https://paneleslanding.uno$request_uri;
    }

    # Aquí va el resto de tu configuración, como el proxy_pass hacia tu aplicación Node.js
    root /var/www/paneleslanding.uno;
    location / {
        proxy_pass http://node_app;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
	proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Real-IP $remote_addr;
    }
}

server {
    listen 80;
    server_name www.paneleslanding.uno;

    # Redirigir www.paneleslanding.uno a paneleslanding.uno
    if ($host = www.paneleslanding.uno) {
        return 301 https://paneleslanding.uno$request_uri;
    }

    # Aquí va el resto de tu configuración, como el proxy_pass hacia tu aplicación Node.js
    root /var/www/paneleslanding.uno;
    location / {
        proxy_pass http://node_app;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
	proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Real-IP $remote_addr;
    }
}

server {
    listen 443 ssl;
    server_name www.paneleslanding.uno;

    # Configuración SSL aquí
    ssl_certificate /home/dario/panelesweb/ssl/fullchain.pem;
    # ssl_certificate /home/dario/panelesweb/ssl/certificado.crt;
    ssl_certificate_key /home/dario/panelesweb/ssl/clave_privada.key;

    # Redirigir paneleslanding.uno a www.paneleslanding.uno
    if ($host = paneleslanding.uno) {
        return 301 https://paneleslanding.uno$request_uri;
    }

    # Aquí va el resto de tu configuración para www.paneleslanding.uno
    root /var/www/paneleslanding.uno;
    location / {
        proxy_pass http://node_app;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_cache_bypass $http_upgrade;
		proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Real-IP $remote_addr;
    }
}

